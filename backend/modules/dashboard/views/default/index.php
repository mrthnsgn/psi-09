<?php
use yii\web\View;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\PuroMessengerApiClient;


$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
// $this->params['header'] = "Dashboard";


$ui = \Yii::$app->uiHelper;
?>
<?=$ui->beginContentRow() ?>

    <?= $ui->beginContentBlock(['id' => 'left-block',
            'width' => 6,
            // 'header' => 'left'
        ]) ?>
<h1>
         <?php 
echo date('l, ');
    date_default_timezone_set('Asia/Jakarta');
    echo date('d-m-Y H:i:s');
    ?>

        <?=$ui->beginSingleRowBlock(['id' => 'courses-block', 'header' => 'My Courses']) ?>
       
       <?=$ui->endSingleRowBlock() ?>



        <?=$ui->beginSingleRowBlock(['id' => 'courses-block', 'header' => 'My Weekly Schedules']) ?>
       
        <?=$ui->endSingleRowBlock() ?>

        
        <?=$ui->endContentBlock()?>
       
        <?=$ui->beginContentBlock(['id' => 'right-block',
        'width' => 6,
      // 'header' => 'right'
        ]) ?>
        
        <?=$ui->beginSingleRowBlock(['id' => 'pengumuman-block', 'header' => 'Pengumuman']) ?>
       
        <?=$ui->endSingleRowBlock() ?>


    <?=$ui->endContentBlock()?>

<?=$ui->endContentRow() ?>
