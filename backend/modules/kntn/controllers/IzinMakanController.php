<?php

namespace backend\modules\kntn\controllers;

use Yii;
use backend\modules\kntn\models\Pegawai;
use backend\modules\kntn\models\IzinMakan;
use backend\modules\kntn\models\DimMeja;
use backend\modules\kntn\models\search\IzinMakanSearch;
use backend\modules\kntn\models\search\MejaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * IzinMakanController implements the CRUD actions for IzinMakan model.
 */
class IzinMakanController extends Controller
{
    public function behaviors()
    {
        return [
            //TODO: crud controller actions are bypassed by default, set the appropriate privilege
            // 'privilege' => [
            //      'class' => \Yii::$app->privilegeControl->getAppPrivilegeControlClass(),
            //      'skipActions' => ['*'],
            //     ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all IzinMakan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IzinMakanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all IzinMakan models.
     * @return mixed
     */
    public function actionIndexByMahasiswa()
    {
        $searchModel = new IzinMakanSearch();
        $dataProvider = $searchModel->searchByMahasiswa(Yii::$app->request->queryParams);

        return $this->render('indexByMahasiswa', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all IzinMakan models.
     * @return mixed
     */
    public function actionIndexByKeasramaan()
    {
      $izin = IzinMakan::find()->where(['status_by_keasramaan' => 2])->all();

      $a = array();
      foreach ($izin as $i) {
        $a[] = $i->dim_id;
      }

      // $dim = DimMeja::find()->where('kntn_dim_meja.deleted != 1')->joinWith(['dim' => function($query) use($a){
      //   $query->where(['in', 'dimx_dim.dim_id', $a]);
      // }])->all();

      $dim = DimMeja::find()->where(['in', 'dim_id', $a])->orderBy(['meja_id' => SORT_ASC])->all();
      // foreach ($dim as $d) {
      //   echo $d->dim_id;
      // }
      // die();

        $searchModel = new IzinMakanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = [
            'status_by_keasramaan' => SORT_ASC,
            'status_by_dosen' => SORT_ASC,
            'created_at' => SORT_ASC,
        ];

        $dataProvider->pagination->pageSize=10;

        $searchModel2 = new MejaSearch();
        $dataProvider2 = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('indexByKeasramaan', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProvider2' => $dataProvider2,
            'dim' => $dim,
        ]);
    }

    /**
     * Lists all IzinMakan models.
     * @return mixed
     */
    public function actionIndexByDosen()
    {
        $searchModel = new IzinMakanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('indexByDosen', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single IzinMakan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Displays a single IzinMakan model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewByMahasiswa($id)
    {
        return $this->render('viewByMahasiswa', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Displays a single IzinMakan model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewByDosen($id)
    {
        return $this->render('viewByDosen', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Displays a single IzinMakan model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewByKeasramaan($id)
    {
        return $this->render('viewByKeasramaan', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Approve IzinMakan model.
     * If approvement is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionApproveByDosen($id, $id_dosen)
    {
        $model = $this->findModel($id);
        $pegawai = Pegawai::find()->where('deleted!=1')->all();

        foreach ($pegawai as $p) {
            if ($p->pegawai_id == $id_dosen) {
                if ($model->status_by_dosen == 1) {
                    $model->status_by_dosen = 2;
                    $model->dosen_id = $id_dosen;
                    $model->save();

                    \Yii::$app->messenger->addSuccessFlash("Izin makan telah disetujui");
                    return $this->redirect(['index-by-dosen']);
                } else {
                    \Yii::$app->messenger->addErrorFlash("Request tidak bisa diubah bila status sudah Rejected atau Canceled");
                    return $this->render('indexByDosen', [
                        'model'=>$model
                    ]);
                }
            }
        }
    }

    /**
     * Approve IzinMakan model.
     * If approvement is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionApproveByKeasramaan($id, $id_keasramaan)
    {
        $model = $this->findModel($id);
        $pegawai = Pegawai::find()->where('deleted!=1')->all();

        foreach ($pegawai as $p) {
            if ($p->pegawai_id == $id_keasramaan) {
                if ($model->status_by_keasramaan == 1) {
                    $model->status_by_keasramaan = 2;
                    $model->keasramaan_id = $id_keasramaan;
                    $model->save();

                    \Yii::$app->messenger->addSuccessFlash("Izin makan telah disetujui");
                    return $this->redirect(['index-by-keasramaan']);
                } else {
                    \Yii::$app->messenger->addErrorFlash("Request tidak bisa diubah bila status sudah Rejected atau Canceled");
                    return $this->render('indexByKeasramaan', [
                        'model'=>$model
                    ]);
                }
            }
        }
    }

    /**
     * Approve IzinMakan model.
     * If approvement is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRejectByKeasramaan($id, $id_keasramaan)
    {
        $model = $this->findModel($id);
        $pegawai = Pegawai::find()->where('deleted!=1')->all();

        foreach ($pegawai as $p) {
            if ($p->pegawai_id == $id_keasramaan) {
                if ($model->status_by_keasramaan == 1) {
                    $model->status_by_keasramaan = 3;
                    $model->keasramaan_id = $id_keasramaan;
                    $model->save();

                    \Yii::$app->messenger->addInfoFlash("Izin makan telah ditolak");
                    return $this->redirect(['index-by-keasramaan']);
                } else {
                    \Yii::$app->messenger->addErrorFlash("Request tidak bisa diubah bila status sudah Rejected atau Canceled");
                    return $this->render('indexByKeasramaan', [
                        'model'=>$model
                    ]);
                }
            }
        }
    }

    /**
     * Approve IzinMakan model.
     * If approvement is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRejectByDosen($id, $id_dosen)
    {
        $model = $this->findModel($id);
        $pegawai = Pegawai::find()->where('deleted!=1')->all();

        foreach ($pegawai as $p) {
            if ($p->pegawai_id == $id_dosen) {
                if ($model->status_by_dosen == 1) {
                    $model->status_by_dosen = 3;
                    $model->keasramaan_id = $id_dosen;
                    $model->save();

                    \Yii::$app->messenger->addInfoFlash("Izin makan telah ditolak");
                    return $this->redirect(['index-by-dosen']);
                } else {
                    \Yii::$app->messenger->addErrorFlash("Request tidak bisa diubah bila status sudah Rejected atau Canceled");
                    return $this->render('indexByDosen', [
                        'model'=>$model
                    ]);
                }
            }
        }
    }

    /**
     * Cancel IzinMakan model.
     * If approvement is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCancelByMahasiswa($id)
    {
        $model = $this->findModel($id);
        $model->status_by_keasramaan = 4;
        $model->status_by_dosen == 4;

        if ($model->save()) {
            \Yii::$app->messenger->addInfoFlash("Izin makan telah dibatalkan");
            return $this->redirect(['index-by-mahasiswa']);
        } else {
            \Yii::$app->messenger->addErrorFlash("Request tidak bisa diubah bila status sudah Rejected atau Canceled");
            return $this->render('indexByMahasiswa', [
                'model'=>$model
            ]);
        }
    }

    /**
     * Creates a new IzinMakan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAdd()
    {
        $model = new IzinMakan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view-by-mahasiswa', 'id' => $model->izin_makan_id]);
        } else {
            return $this->render('add', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new IzinMakan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAddByMahasiswa()
    {
        $model = new IzinMakan();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->jenis_kegiatan_id == 2) {
                $model->status_by_keasramaan = 1;
                $model->status_by_dosen = null;
                $model->dosen_id = null;
                $model->save();
            } else {
                $model->status_by_keasramaan = 1;
                $model->status_by_dosen = 1;
                $model->save();
            }
            return $this->redirect(['view-by-mahasiswa', 'id' => $model->izin_makan_id]);
        } else {
            return $this->render('add', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new IzinMakan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAddByMahasiswaKelompok()
    {
        $model = new IzinMakan();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->jenis_kegiatan_id == 2) {
                $model->status_by_keasramaan = 1;
                $model->status_by_dosen = null;
                $model->dosen_id = null;
                $model->save();
            } else {
                $model->status_by_keasramaan = 1;
                $model->status_by_dosen = 1;
                $model->save();
            }
            return $this->redirect(['view-by-mahasiswa', 'id' => $model->izin_makan_id]);
        } else {
            return $this->render('add', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing IzinMakan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionEdit($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->izin_makan_id]);
        } else {
            return $this->render('edit', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing IzinMakan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionEditByMahasiswa($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->izin_makan_id]);
        } else {
            return $this->render('edit', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing IzinMakan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the IzinMakan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return IzinMakan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IzinMakan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
