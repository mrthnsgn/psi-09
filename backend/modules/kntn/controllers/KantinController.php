<?php

namespace backend\modules\kntn\controllers;

use Yii;
use backend\modules\kntn\models\Kantin;
use backend\modules\kntn\models\search\KantinSearch;
use backend\modules\kntn\models\search\MejaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KantinController implements the CRUD actions for Kantin model.
 */
class KantinController extends Controller
{
    public function behaviors()
    {
        return [
            //TODO: crud controller actions are bypassed by default, set the appropriate privilege
            // 'privilege' => [
            //      'class' => \Yii::$app->privilegeControl->getAppPrivilegeControlClass(),
            //      'skipActions' => ['*'],
            //     ],
                
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Kantin models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MejaSearch();
        $dataProvider1 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider2 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider3 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider4 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider5 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider6 = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider1->query->where('kantin_id=1');
        $dataProvider2->query->where('kantin_id=2');
        $dataProvider3->query->where('kantin_id=3');
        $dataProvider4->query->where('kantin_id=4');
        $dataProvider5->query->where('kantin_id=5');
        $dataProvider6->query->where('kantin_id=6');


        //$datadimmeja = DimMeja::find()->where (['status_dim_meja' => 1])->joinwith(['meja' => function($query){
          //  $query->groupBy(['kantin_id']);
        //}])->all();
        //echo "<pre>";
        //print_r($datadimmeja);
        //die;
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider1' => $dataProvider1,
            'dataProvider2' => $dataProvider2,
            'dataProvider3' => $dataProvider3,
            'dataProvider4' => $dataProvider1,
            'dataProvider5' => $dataProvider5,
            'dataProvider6' => $dataProvider6,
            'model1' => $this->findModel(1),
            'model2' => $this->findModel(2),
            'model3' => $this->findModel(3),
            'model4' => $this->findModel(4),
            'model5' => $this->findModel(5),
            //'datadimmeja'=> $datadimmeja,
            'model6' => $this->findModel(6),
        ]);
    }

    /**
     * Displays a single Kantin model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Kantin model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Kantin();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->kantin_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Kantin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->kantin_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Kantin model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Kantin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Kantin the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kantin::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
