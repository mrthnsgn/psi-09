<?php

namespace backend\modules\kntn\models;

use Yii;

use common\behaviors\TimestampBehavior;
use common\behaviors\BlameableBehavior;
use common\behaviors\DeleteBehavior;

/**
 * This is the model class for table "kntn_meja".
 *
 * @property integer $meja_id
 * @property string $kode
 * @property integer $kantin_id
 * @property integer $dim_id
 * @property integer $deleted
 * @property string $deleted_at
 * @property string $deleted_by
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property DimMeja[] $dimMejas
 * @property Kantin $kantin
 */
class Meja extends \yii\db\ActiveRecord
{

    /**
     * behaviour to add created_at and updatet_at field with current datetime (timestamp)
     * and created_by and updated_by field with current user id (blameable)
     */
    public function behaviors(){
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
            ],
            'delete' => [
                'class' => DeleteBehavior::className(),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kntn_meja';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kantin_id', 'dim_id', 'deleted'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['kode', 'deleted_by', 'created_by', 'updated_by'], 'string', 'max' => 32],
            [['kantin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kantin::className(), 'targetAttribute' => ['kantin_id' => 'kantin_id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'meja_id' => 'Meja ID',
            'kode' => 'Kode',
            'kantin_id' => 'Kantin',
            'dim_id' => 'Mahasiswa',
            'deleted' => 'Deleted',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDimMejas()
    {
        return $this->hasOne(DimMeja::className(), ['meja_id' => 'meja_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKantin()
    {
        return $this->hasOne(Kantin::className(), ['kantin_id' => 'kantin_id']);
    }
}
