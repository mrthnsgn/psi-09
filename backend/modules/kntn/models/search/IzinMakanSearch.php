<?php

namespace backend\modules\kntn\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\kntn\models\IzinMakan;

/**
 * IzinMakanSearch represents the model behind the search form about `backend\modules\kntn\models\IzinMakan`.
 */
class IzinMakanSearch extends IzinMakan
{
    public $dim_nama;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['izin_makan_id', 'status_by_keasramaan', 'status_by_dosen', 'jenis_izin_id', 'jenis_kegiatan_id', 'dim_id', 'dosen_id', 'keasramaan_id', 'deleted'], 'integer'],
            [['dim_nama', 'jam_makan', 'tanggal', 'desc', 'deleted_at', 'deleted_by', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IzinMakan::find();
        $query->joinWith(['dim']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 15,
            ],
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC, 'status_by_dosen' => SORT_ASC, 'status_by_keasramaan' => SORT_ASC]],
        ]);

        $dataProvider->sort->attributes['dim_nama'] = [
            'asc' => ['dimx_dim.nama' => SORT_ASC],
            'desc' => ['dimx_dim.nama' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'izin_makan_id' => $this->izin_makan_id,
            'tanggal' => $this->tanggal,
            'status_by_keasramaan' => $this->status_by_keasramaan,
            'status_by_dosen' => $this->status_by_dosen,
            'jenis_izin_id' => $this->jenis_izin_id,
            'jenis_kegiatan_id' => $this->jenis_kegiatan_id,
            'dim_id' => $this->dim_id,
            'dosen_id' => $this->dosen_id,
            'keasramaan_id' => $this->keasramaan_id,
            'deleted' => $this->deleted,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'jam_makan', $this->jam_makan])
            ->andFilterWhere(['like', 'dimx_dim.nama', $this->dim_nama])
            ->andFilterWhere(['like', 'desc', $this->desc])
            ->andFilterWhere(['like', 'deleted_by', $this->deleted_by])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by])
            ->andFilterWhere(['not', ['kntn_izin_makan.deleted' => 1]]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchByMahasiswa($params)
    {
        $query = IzinMakan::find();
        $query->joinWith(['dim']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 15,
            ],
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC, 'status_by_dosen' => SORT_ASC, 'status_by_keasramaan' => SORT_ASC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'izin_makan_id' => $this->izin_makan_id,
            'tanggal' => $this->tanggal,
            'status_by_keasramaan' => $this->status_by_keasramaan,
            'status_by_dosen' => $this->status_by_dosen,
            'jenis_izin_id' => $this->jenis_izin_id,
            'jenis_kegiatan_id' => $this->jenis_kegiatan_id,
            'dimx_dim.user_id' => Yii::$app->user->identity->user_id,
            'dosen_id' => $this->dosen_id,
            'keasramaan_id' => $this->keasramaan_id,
            'deleted' => $this->deleted,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'jam_makan', $this->jam_makan])
            ->andFilterWhere(['like', 'dimx_dim.nama', $this->dim_nama])
            ->andFilterWhere(['like', 'desc', $this->desc])
            ->andFilterWhere(['like', 'deleted_by', $this->deleted_by])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by])
            ->andFilterWhere(['not', ['kntn_izin_makan.deleted' => 1]]);

        return $dataProvider;
    }
}
