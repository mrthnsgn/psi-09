<?php

namespace backend\modules\kntn\models;

use Yii;

use common\behaviors\TimestampBehavior;
use common\behaviors\BlameableBehavior;
use common\behaviors\DeleteBehavior;

/**
 * This is the model class for table "kntn_izin_makan".
 *
 * @property integer $izin_makan_id
 * @property string $jam_makan
 * @property string $tanggal
 * @property string $desc
 * @property integer $status_by_keasramaan
 * @property integer $status_by_dosen
 * @property integer $jenis_izin_id
 * @property integer $jenis_kegiatan_id
 * @property integer $dim_id
 * @property integer $dosen_id
 * @property integer $keasramaan_id
 * @property integer $deleted
 * @property string $deleted_at
 * @property string $deleted_by
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property Dim $dim
 * @property Pegawai $dosen
 * @property Pegawai $keasramaan
 * @property JenisKegiatan $jenisKegiatan
 * @property JenisIzin $jenisIzin
 * @property Request $statusByKeasramaan
 * @property Request $statusByDosen
 */
class IzinMakan extends \yii\db\ActiveRecord
{

    /**
     * behaviour to add created_at and updatet_at field with current datetime (timestamp)
     * and created_by and updated_by field with current user id (blameable)
     */
    public function behaviors(){
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
            ],
            'delete' => [
                'class' => DeleteBehavior::className(),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kntn_izin_makan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jam_makan', 'tanggal'], 'required'],
            [['tanggal', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['desc'], 'string'],
            [['status_by_keasramaan', 'status_by_dosen', 'jenis_izin_id', 'jenis_kegiatan_id', 'dim_id', 'dosen_id', 'keasramaan_id', 'deleted'], 'integer'],
            [['jam_makan'], 'string', 'max' => 45],
            [['deleted_by', 'created_by', 'updated_by'], 'string', 'max' => 32],
            [['dim_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dim::className(), 'targetAttribute' => ['dim_id' => 'dim_id']],
            [['dosen_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['dosen_id' => 'pegawai_id']],
            [['keasramaan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['keasramaan_id' => 'pegawai_id']],
            [['jenis_kegiatan_id'], 'exist', 'skipOnError' => true, 'targetClass' => JenisKegiatan::className(), 'targetAttribute' => ['jenis_kegiatan_id' => 'jenis_kegiatan_id']],
            [['jenis_izin_id'], 'exist', 'skipOnError' => true, 'targetClass' => JenisIzin::className(), 'targetAttribute' => ['jenis_izin_id' => 'jenis_izin_id']],
            [['status_by_keasramaan'], 'exist', 'skipOnError' => true, 'targetClass' => Request::className(), 'targetAttribute' => ['status_by_keasramaan' => 'request_id']],
            [['status_by_dosen'], 'exist', 'skipOnError' => true, 'targetClass' => Request::className(), 'targetAttribute' => ['status_by_dosen' => 'request_id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'izin_makan_id' => 'Izin Makan ID',
            'jam_makan' => 'Jam Makan',
            'tanggal' => 'Tanggal Izin',
            'desc' => 'Keterangan',
            'status_by_keasramaan' => 'Izin Keasramaan',
            'status_by_dosen' => 'Izin Dosen',
            'jenis_izin_id' => 'Jenis Izin',
            'jenis_kegiatan_id' => 'Jenis Kegiatan',
            'dim_id' => 'Mahasiswa',
            'dosen_id' => 'Dosen ID',
            'keasramaan_id' => 'Keasramaan ID',
            'deleted' => 'Deleted',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDim()
    {
        return $this->hasOne(Dim::className(), ['dim_id' => 'dim_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDosen()
    {
        return $this->hasOne(Pegawai::className(), ['pegawai_id' => 'dosen_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKeasramaan()
    {
        return $this->hasOne(Pegawai::className(), ['pegawai_id' => 'keasramaan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisKegiatan()
    {
        return $this->hasOne(JenisKegiatan::className(), ['jenis_kegiatan_id' => 'jenis_kegiatan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisIzin()
    {
        return $this->hasOne(JenisIzin::className(), ['jenis_izin_id' => 'jenis_izin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusByKeasramaan()
    {
        return $this->hasOne(Request::className(), ['request_id' => 'status_by_keasramaan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusByDosen()
    {
        return $this->hasOne(Request::className(), ['request_id' => 'status_by_dosen']);
    }
}
