<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\widgets\DetailView;
use yii\grid\GridView;
use common\components\ToolsColumn;
use backend\modules\kntn\models\DimMeja;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\kntn\models\search\KantinSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kantins';
$this->params['breadcrumbs'][] = $this->title;
$uiHelper=\Yii::$app->uiHelper;
?>
<div class="kantin-index">

    <h2>Daftar Kantin</h2>
    <?= $uiHelper->renderLine(); ?>

    <?= $uiHelper->beginTab([
        'icon' => 'fa fa-shield',
        'tabs' => [
            ['id' => 'tab_1', 'label' => 'KB 1 Lt. 1', 'isActive' => true],
            ['id' => 'tab_2', 'label' => 'KB 1 Lt. 2', 'isActive' => false],
            ['id' => 'tab_3', 'label' => 'KB 2 Lt. 1', 'isActive' => false],
            ['id' => 'tab_4', 'label' => 'KB 2 Lt. 2', 'isActive' => false],
            ['id' => 'tab_5', 'label' => 'KB Tengah Lt. 1', 'isActive' => false],
            ['id' => 'tab_6', 'label' => 'KB Tengah Lt. 2', 'isActive' => false],
        ]
    ]) ?>

    <?= $uiHelper->beginTabContent(['id'=>'tab_1', 'isActive' => true]) ?>
    <?= DetailView::widget([
        'model' => $model1,
        'attributes' => [
            'name',
            'kapasitas',
        ],
    ]) ?>

    <?= $uiHelper->renderLine(); ?>

    <div class="row">
        <div class="col-md-3" style="text-align: center;">
            <?php
                $counter=0;
                
                for ($i=0; $i < 8; $i++) {
                $counter++;
                $datadimmeja = DimMeja::find()->where (['status_dim_meja' =>1, 'kntn_dim_meja.meja_id'=>$counter]) ->joinWith
                (['meja'=> function($query){
                $query->where(['kantin_id'=>1]);
                
                }])->count();
                    echo "
                    <div style='margin-bottom: 5px;' class='btn btn-default'>
                    <div style='margin-bottom: 5px;'>";

                    if($datadimmeja == 1){
                        echo "
                        <button class='btn btn-danger'><i class='fa fa-user'></i></button>
                        <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>";
                    }
                    else if ($datadimmeja == 2){
                        echo "
                        <button class='btn btn-danger'><i class='fa fa-user'></i></button>
                        <button class='btn btn-danger'><i class='fa fa-user'></i></button>
                        <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>

                        <div>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>";
                    }
                    else if ($datadimmeja == 3){
                        echo "
                        <button class='btn btn-danger'><i class='fa fa-user'></i></button>
                        <button class='btn btn-danger'><i class='fa fa-user'></i></button>
                        <button class='btn btn-danger'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>";
                    }
                    else if ($datadimmeja == 4){
                        echo "
                        <button class='btn btn-danger'><i class='fa fa-user'></i></button>
                        <button class='btn btn-danger'><i class='fa fa-user'></i></button>
                        <button class='btn btn-danger'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-danger'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>";
                    }
                    else if ($datadimmeja == 5){
                        echo "
                        <button class='btn btn-danger'><i class='fa fa-user'></i></button>
                        <button class='btn btn-danger'><i class='fa fa-user'></i></button>
                        <button class='btn btn-danger'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-danger'><i class='fa fa-user'></i></button>
                            <button class='btn btn-danger'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>";
                    }
                    else if ($datadimmeja == 6){
                        echo "
                        <button class='btn btn-danger'><i class='fa fa-user'></i></button>
                        <button class='btn btn-danger'><i class='fa fa-user'></i></button>
                        <button class='btn btn-danger'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-danger'><i class='fa fa-user'></i></button>
                            <button class='btn btn-danger'><i class='fa fa-user'></i></button>
                            <button class='btn btn-danger'><i class='fa fa-user'></i></button>
                        </div>
                    </div>";
                    }
                    else {
                        echo "
                        <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>";
                    }
                }

            ?>
        </div>
        <div class="col-md-6" style="text-align: center;">
            <?php
                $counter++;
                for ($i=0; $i < 8; $i++) {
                    echo "

                    <div style='margin-bottom: 5px;' class='btn btn-default'>
                        <div style='margin-bottom: 5px;'>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>

                    ";
                } 

            ?>
        </div>
        <div class="col-md-3" style="text-align: center;">
            <?php

                $counter++;
                for ($i=0; $i < 8; $i++) {
                    echo "

                    <div style='margin-bottom: 5px;' class='btn btn-default'>
                        <div style='margin-bottom: 5px;'>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>

                    ";
                }

            ?>
        </div>
    </div>

    <?= $uiHelper->endTabContent() ?>

    <?= $uiHelper->beginTabContent(['id'=>'tab_2', 'isActive' => false]) ?>
    <?= DetailView::widget([
        'model' => $model2,
        'attributes' => [
            'name',
            'kapasitas',
            
        ],
    ]) ?>

    <div class="row">
        <div class="col-md-3" style="text-align: center;">
            <?php

                for ($i=0; $i < 8; $i++) {
                    echo "

                    <div style='margin-bottom: 5px;' class='btn btn-default'>
                        <div style='margin-bottom: 5px;'>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>

                    ";
                }

            ?>
        </div>
        <div class="col-md-6" style="text-align: center;">
            <?php

                for ($i=0; $i < 8; $i++) {
                    echo "

                    <div style='margin-bottom: 5px;' class='btn btn-default'>
                        <div style='margin-bottom: 5px;'>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>

                    ";
                }

            ?>
        </div>
        <div class="col-md-3" style="text-align: center;">
            <?php

                for ($i=0; $i < 8; $i++) {
                    echo "

                    <div style='margin-bottom: 5px;' class='btn btn-default'>
                        <div style='margin-bottom: 5px;'>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>

                    ";
                }

            ?>
        </div>
    </div>

    <?= $uiHelper->endTabContent() ?>

    <?= $uiHelper->beginTabContent(['id'=>'tab_3', 'isActive' => false]) ?>
    <?= DetailView::widget([
        'model' => $model3,
        'attributes' => [
            'name',
            'kapasitas',
        ],
    ]) ?>

    <div class="row">
        <div class="col-md-3" style="text-align: center;">
            <?php

                for ($i=0; $i < 8; $i++) {
                    echo "

                    <div style='margin-bottom: 5px;' class='btn btn-default'>
                        <div style='margin-bottom: 5px;'>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>

                    ";
                }

            ?>
        </div>
        <div class="col-md-6" style="text-align: center;">
            <?php

                for ($i=0; $i < 8; $i++) {
                    echo "

                    <div style='margin-bottom: 5px;' class='btn btn-default'>
                        <div style='margin-bottom: 5px;'>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>

                    ";
                }

            ?>
        </div>
        <div class="col-md-3" style="text-align: center;">
            <?php

                for ($i=0; $i < 8; $i++) {
                    echo "

                    <div style='margin-bottom: 5px;' class='btn btn-default'>
                        <div style='margin-bottom: 5px;'>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>

                    ";
                }

            ?>
        </div>
    </div>

    <?= $uiHelper->endTabContent() ?>

    <?= $uiHelper->beginTabContent(['id'=>'tab_4', 'isActive' => false]) ?>
    <?= DetailView::widget([
        'model' => $model4,
        'attributes' => [
            'name',
            'kapasitas',
        ],
    ]) ?>

    <div class="row">
        <div class="col-md-3" style="text-align: center;">
            <?php

                for ($i=0; $i < 8; $i++) {
                    echo "

                    <div style='margin-bottom: 5px;' class='btn btn-default'>
                        <div style='margin-bottom: 5px;'>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>

                    ";
                }

            ?>
        </div>
        <div class="col-md-6" style="text-align: center;">
            <?php

                for ($i=0; $i < 8; $i++) {
                    echo "

                    <div style='margin-bottom: 5px;' class='btn btn-default'>
                        <div style='margin-bottom: 5px;'>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>

                    ";
                }

            ?>
        </div>
        <div class="col-md-3" style="text-align: center;">
            <?php

                for ($i=0; $i < 8; $i++) {
                    echo "

                    <div style='margin-bottom: 5px;' class='btn btn-default'>
                        <div style='margin-bottom: 5px;'>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>

                    ";
                }

            ?>
        </div>
    </div>

    <?= $uiHelper->endTabContent() ?>

    <?= $uiHelper->beginTabContent(['id'=>'tab_5', 'isActive' => false]) ?>
    <?= DetailView::widget([
        'model' => $model5,
        'attributes' => [
            'name',
            'kapasitas',
        ],
    ]) ?>

    <div class="row">
        <div class="col-md-3" style="text-align: center;">
            <?php

                for ($i=0; $i < 4; $i++) {
                    echo "

                    <div style='margin-bottom: 5px;' class='btn btn-default'>
                        <div style='margin-bottom: 5px;'>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>

                    ";
                }

            ?>
        </div>
        <div class="col-md-6" style="text-align: center;">
            <?php

                for ($i=0; $i < 4; $i++) {
                    echo "

                    <div style='margin-bottom: 5px;' class='btn btn-default'>
                        <div style='margin-bottom: 5px;'>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>

                    ";
                }

            ?>
        </div>
        <div class="col-md-3" style="text-align: center;">
            <?php

                for ($i=0; $i < 4; $i++) {
                    echo "

                    <div style='margin-bottom: 5px;' class='btn btn-default'>
                        <div style='margin-bottom: 5px;'>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>

                    ";
                }

            ?>
        </div>
    </div>

    <?= $uiHelper->endTabContent() ?>

    <?= $uiHelper->beginTabContent(['id'=>'tab_6', 'isActive' => false]) ?>
    <?= DetailView::widget([
        'model' => $model6,
        'attributes' => [
            'name',
            'kapasitas',
        ],
    ]) ?>

    <div class="row">
        <div class="col-md-3" style="text-align: center;">
            <?php

                for ($i=0; $i < 4; $i++) {
                    echo "

                    <div style='margin-bottom: 5px;' class='btn btn-default'>
                        <div style='margin-bottom: 5px;'>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>

                    ";
                }

            ?>
        </div>
        <div class="col-md-6" style="text-align: center;">
            <?php

                for ($i=0; $i < 4; $i++) {
                    echo "

                    <div style='margin-bottom: 5px;' class='btn btn-default'>
                        <div style='margin-bottom: 5px;'>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>

                    ";
                }

            ?>
        </div>
        <div class="col-md-3" style="text-align: center;">
            <?php

                for ($i=0; $i < 4; $i++) {
                    echo "

                    <div style='margin-bottom: 5px;' class='btn btn-default'>
                        <div style='margin-bottom: 5px;'>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                        <div>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                            <button class='btn btn-success'><i class='fa fa-user'></i></button>
                        </div>
                    </div>

                    ";
                }

            ?>
        </div>
    </div>

    <?= $uiHelper->endTabContent() ?>

</div>
