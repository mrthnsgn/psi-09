<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\kntn\models\Kantin */

$this->title = 'Create Kantin';
$this->params['breadcrumbs'][] = ['label' => 'Kantins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kantin-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
