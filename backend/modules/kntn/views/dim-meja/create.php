<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\kntn\models\DimMeja */

$this->title = 'Create Dim Meja';
$this->params['breadcrumbs'][] = ['label' => 'Dim Mejas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dim-meja-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
