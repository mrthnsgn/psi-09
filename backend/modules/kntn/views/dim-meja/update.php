<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\kntn\models\DimMeja */

$this->title = 'Update Dim Meja: ' . ' ' . $model->dim_meja_id;
$this->params['breadcrumbs'][] = ['label' => 'Dim Mejas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dim_meja_id, 'url' => ['view', 'id' => $model->dim_meja_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dim-meja-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
