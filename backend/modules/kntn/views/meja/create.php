<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\kntn\models\Meja */

$this->title = 'Create Meja';
$this->params['breadcrumbs'][] = ['label' => 'Mejas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meja-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
