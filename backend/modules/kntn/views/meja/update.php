<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\kntn\models\Meja */

$this->title = 'Update Meja: ' . ' ' . $model->meja_id;
$this->params['breadcrumbs'][] = ['label' => 'Mejas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->meja_id, 'url' => ['view', 'id' => $model->meja_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="meja-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
