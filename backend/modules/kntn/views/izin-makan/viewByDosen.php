<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use backend\modules\askm\models\Pegawai;

/* @var $this yii\web\View */
/* @var $model backend\modules\kntn\models\IzinMakan */

$this->title = $model->dim['nama'];
$this->params['breadcrumbs'][] = ['label' => 'Izin Makan', 'url' => ['index-by-dosen']];
$this->params['breadcrumbs'][] = $this->title;
$uiHelper=\Yii::$app->uiHelper;
$dosen = Pegawai::find()->where('deleted != 1')->andWhere(['user_id' => Yii::$app->user->identity->user_id])->one();
?>
<div class="izin-makan-view">

    <?php
        if ($model->status_by_dosen == 1) {
    ?>

    <div class="pull-right">
        Pengaturan
        <?= $uiHelper->renderButtonSet([
                'template' => ['approve', 'reject'],
                'buttons' => [
                    'approve' => ['url' => Url::toRoute(['approve-by-dosen', 'id' => $model->izin_makan_id, 'id_dosen' => $dosen->pegawai_id]), 'label'=> 'Setuju', 'icon'=>'fa fa-check'],
                    'reject' => ['url' => Url::toRoute(['reject-by-dosen', 'id' => $model->izin_makan_id, 'id_dosen' => $dosen->pegawai_id]), 'label'=> 'Tolak', 'icon'=>'fa fa-times'],
                ],

            ]) ?>
    </div>

    <h1><?= $this->title ?></h1>
    <?= $uiHelper->renderLine(); ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            [
                'attribute' => 'jenis_izin_id',
                'value' => function($model){
                    return $model->jenisIzin['jenis_izin'];
                }
            ],
            [
                'attribute' => 'jenis_kegiatan_id',
                'value' => function($model){
                    return $model->jenisKegiatan['jenis_kegiatan'];
                }
            ],
            'jam_makan',
            [
                'attribute' => 'tanggal',
                'value' => function($model){
                    if (is_null($model->tanggal)) {
                        return '-';
                    }else{
                        return date('d M Y', strtotime($model->tanggal));
                    }
                }
            ],
            'desc:ntext',
            [
                'label' => 'Izin Dari Keasramaan', 
                'value' => function($model){
                    if (is_null($model->statusByKeasramaan['request'])) {
                        return '-';
                    }else{
                        return $model->statusByKeasramaan['request'];
                    }
                }
            ],
            [
                'label' => 'Disetujui/Ditolak oleh',
                'value' => function($model){
                    if (is_null($model->keasramaan['nama'])) {
                        return '-';
                    }else{
                        return $model->keasramaan['nama'];
                    }
                }
            ],
            [
                'label' => 'Izin Dari Dosen', 
                'value' => function($model){
                    if ($model->statusByDosen['request_id'] == null && $model->statusByKeasramaan['request_id'] != null){
                        return 'Tidak perlu persetujuan dosen';
                    }else if (is_null($model->statusByDosen['request'])) {
                        return '-';
                    }else{
                        return $model->statusByDosen['request'];
                    }
                }
            ],
            [
                'label' => 'Disetujui/Ditolak oleh',
                'value' => function($model){
                    if (is_null($model->dosen['nama'])) {
                        return '-';
                    }else{
                        return $model->dosen['nama'];
                    }
                }
            ],
            
        ],
    ]) ?>

    <?php }else { ?>

    <h1><?= $this->title ?></h1>
    <?= $uiHelper->renderLine(); ?>

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [

            [
                'attribute' => 'jenis_izin_id',
                'value' => function($model){
                    return $model->jenisIzin['jenis_izin'];
                }
            ],
            [
                'attribute' => 'jenis_kegiatan_id',
                'value' => function($model){
                    return $model->jenisKegiatan['jenis_kegiatan'];
                }
            ],
            'jam_makan',
            [
                'attribute' => 'tanggal',
                'value' => function($model){
                    if (is_null($model->tanggal)) {
                        return '-';
                    }else{
                        return date('d M Y', strtotime($model->tanggal));
                    }
                }
            ],
            'desc:ntext',
            [
                'label' => 'Izin Dari Keasramaan', 
                'value' => function($model){
                    if (is_null($model->statusByKeasramaan['request'])) {
                        return '-';
                    }else{
                        return $model->statusByKeasramaan['request'];
                    }
                }
            ],
            [
                'label' => 'Disetujui/Ditolak oleh',
                'value' => function($model){
                    if (is_null($model->keasramaan['nama'])) {
                        return '-';
                    }else{
                        return $model->keasramaan['nama'];
                    }
                }
            ],
            [
                'label' => 'Izin Dari Dosen', 
                'value' => function($model){
                    if ($model->statusByDosen['request_id'] == null && $model->statusByKeasramaan['request_id'] != null){
                        return 'Tidak perlu persetujuan dosen';
                    }else if (is_null($model->statusByDosen['request'])) {
                        return '-';
                    }else{
                        return $model->statusByDosen['request'];
                    }
                }
            ],
            [
                'label' => 'Disetujui/Ditolak oleh',
                'value' => function($model){
                    if (is_null($model->dosen['nama'])) {
                        return '-';
                    }else{
                        return $model->dosen['nama'];
                    }
                }
            ],
            
        ],
    ]);} ?>

</div>
