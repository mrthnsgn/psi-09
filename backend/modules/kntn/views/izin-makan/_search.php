<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\kntn\models\search\IzinMakanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="izin-makan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'izin_makan_id') ?>

    <?= $form->field($model, 'jam_makan') ?>

    <?= $form->field($model, 'tanggal') ?>

    <?= $form->field($model, 'desc') ?>

    <?= $form->field($model, 'status_by_keasramaan') ?>

    <?php // echo $form->field($model, 'status_by_dosen') ?>

    <?php // echo $form->field($model, 'jenis_izin_id') ?>

    <?php // echo $form->field($model, 'jenis_kegiatan_id') ?>

    <?php // echo $form->field($model, 'dim_id') ?>

    <?php // echo $form->field($model, 'dosen_id') ?>

    <?php // echo $form->field($model, 'keasramaan_id') ?>

    <?php // echo $form->field($model, 'deleted') ?>

    <?php // echo $form->field($model, 'deleted_at') ?>

    <?php // echo $form->field($model, 'deleted_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
