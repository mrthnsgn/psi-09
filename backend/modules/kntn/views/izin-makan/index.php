<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\components\ToolsColumn;
use common\helpers\LinkHelper;
use backend\modules\kntn\models\Request;
use backend\modules\kntn\models\Dim;
use dosamigos\datetimepicker\DateTimePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\kntn\models\search\IzinMakanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Izin Makan';
$this->params['breadcrumbs'][] = $this->title;
$uiHelper=\Yii::$app->uiHelper;
?>
<div class="izin-makan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $uiHelper->renderLine(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Request Izin Makan', ['izin-by-mahasiswa-add'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['style' => 'font-size:12px;'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'dim_nama',
                'label'=>'Nama Mahasiswa',
                'format' => 'raw',
                'value'=>function ($model) {
                    return $model->dim->nama;
                },
            ],
            // 'jenis_izin_id',
            // 'jenis_kegiatan_id',
            // 'desc:ntext',
            [
                'attribute' => 'jam_makan',
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:100px'],
                'filter' => array("Pagi"=>"Pagi","Siang"=>"Siang","Malam"=>"Malam"),
                'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'ALL'],
                'value' => function($model){
                    if($model->jam_makan == "Pagi"){
                        return 'Pagi';
                    }
                    elseif($model->jam_makan == "Siang"){
                        return 'Siang';
                    }
                    elseif($model->jam_makan == "Malam"){
                        return 'Malam';
                    }
                },
            ],
            [
                'attribute'=>'tanggal',
                'format'=> 'raw',
                'headerOptions' => ['style' => 'color:#3c8dbc'],
                'value'=>function($model,$key,$index){
                    if($model->tanggal==NULL){
                        return '-';
                    }
                    else{
                        return date('d M Y', strtotime($model->tanggal));
                    }
                },
                'filter'=>DateTimePicker::widget([
                    'model'=>$searchModel,
                    'attribute'=>'tanggal',
                    'template'=>'{input}{reset}{button}',
                        'clientOptions' => [
                            'startView' => 2,
                            'minView' => 2,
                            'maxView' => 2,
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                        ],
                ])
            ],
            [
                'label' => 'Status Izin',
                'filter' => '',
                'value' => function($model){
                    if ($model->status_by_dosen == 4 || $model->status_by_keasramaan == 4) {
                        return 'Dibatalkan';
                    } else if ($model->status_by_dosen == 1) {
                        return 'Menunggu Persetujuan Dosen Wali';
                    } elseif ($model->status_by_keasramaan == 1) {
                        return 'Menunggu Persetujuan Keasramaan';
                    } elseif ($model->status_by_dosen == 3) {
                        return 'Ditolak oleh Dosen Wali';
                    } elseif ($model->status_by_keasramaan == 3) {
                        return 'Ditolak oleh Keasramaan';
                    } else {
                        return 'Selesai';
                    }
                },
            ],

            ['class' => 'common\components\ToolsColumn',
                'template' => '{view} {edit} {cancel}',
                'header' => 'Aksi',
                'buttons' => [
                    'view' => function ($url, $model){
                        return ToolsColumn::renderCustomButton($url, $model, 'View Detail', 'fa fa-eye');
                    },
                    'cancel' => function ($url, $model){
                        if ($model->status_by_dosen == 1 || $model->status_by_keasramaan == 1) {
                            return ToolsColumn::renderCustomButton($url, $model, 'Cancel', 'fa fa-times');
                        }else if ($model->status_by_dosen != 1 || $model->status_by_keasramaan != 1){
                            return "";
                        }
                    },
                    'edit' => function ($url, $model){
                        if ($model->status_by_dosen != 1 || $model->status_by_keasramaan != 1) {
                            return "";
                        }else{
                            return ToolsColumn::renderCustomButton($url, $model, 'Edit', 'fa fa-pencil');
                        }
                    },
                ],
                'urlCreator' => function ($action, $model, $key, $index){
                    if ($action === 'view') {
                        return Url::toRoute(['izin-by-mahasiswa-view', 'id' => $key]);
                    }else if ($action === 'edit') {
                        return Url::toRoute(['izin-by-mahasiswa-edit', 'id' => $key]);
                    }else if ($action === 'cancel') {
                        return Url::toRoute(['izin-by-mahasiswa-cancel', 'id' => $key]);
                    }
                }
            ],
        ],
    ]); ?>

</div>
