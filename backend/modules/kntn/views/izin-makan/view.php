<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\kntn\models\IzinMakan */

$this->title = $model->dim['nama'];
$this->params['breadcrumbs'][] = ['label' => 'Izin Makan', 'url' => ['index-by-mahasiswa']];
$this->params['breadcrumbs'][] = $this->title;
$uiHelper=\Yii::$app->uiHelper;
?>
<div class="izin-makan-view">

    <?php
        if ($model->status_by_dosen == 1 || $model->status_by_keasramaan == 1) {
    ?>

    <div class="pull-right">
        Pengaturan
        <?= $uiHelper->renderButtonSet([
                'template' => ['edit', 'cancel'],
                'buttons' => [
                    'edit' => ['url' => Url::toRoute(['izin-by-mahasiswa-edit', 'id' => $model->izin_makan_id]), 'label'=> 'Edit Request', 'icon'=>'fa fa-pencil'], // id keasramaan diambil saat sudah login
                    'cancel' => ['url' => Url::toRoute(['cancel-by-mahasiswa', 'id' => $model->izin_makan_id]), 'label'=> 'Cancel Request', 'icon'=>'fa fa-times'],
                ],
                
            ]) ?>
    </div>
    <h1><?= $this->title ?></h1>
    <?= $uiHelper->renderLine(); ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            [
                'attribute' => 'jenis_izin_id',
                'value' => function($model){
                    return $model->jenisIzin['jenis_izin'];
                }
            ],
            'jenis_kegiatan_id',
            'jam_makan',
            [
                'attribute' => 'tanggal',
                'value' => function($model){
                    if (is_null($model->tanggal)) {
                        return '-';
                    }else{
                        return date('d M Y', strtotime($model->tanggal));
                    }
                }
            ],
            'desc:ntext',
            [
                'label' => 'Izin Dari Keasramaan', 
                'value' => function($model){
                    if (is_null($model->statusByKeasramaan['request'])) {
                        return '-';
                    }else{
                        return $model->statusByKeasramaan['request'];
                    }
                }
            ],
            [
                'label' => 'Disetujui/Ditolak oleh',
                'value' => function($model){
                    if (is_null($model->keasramaan['nama'])) {
                        return '-';
                    }else{
                        return $model->keasramaan['nama'];
                    }
                }
            ],
            [
                'label' => 'Izin Dari Dosen', 
                'value' => function($model){
                    if ($model->statusByDosen['request_id'] == null && $model->statusByKeasramaan['request_id'] != null){
                        return 'Tidak perlu persetujuan dosen';
                    }else if (is_null($model->statusByDosen['request'])) {
                        return '-';
                    }else{
                        return $model->statusByDosen['request'];
                    }
                }
            ],
            [
                'label' => 'Disetujui/Ditolak oleh',
                'value' => function($model){
                    if (is_null($model->dosen['nama'])) {
                        return '-';
                    }else{
                        return $model->dosen['nama'];
                    }
                }
            ],
            
        ],
    ]) ?>

    <?php }else { ?>

    <h1><?= $this->title ?></h1>
    <?= $uiHelper->renderLine(); ?>

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [

            'jenis_izin_id',
            'jenis_kegiatan_id',
            'jam_makan',
            [
                'attribute' => 'tanggal',
                'value' => function($model){
                    if (is_null($model->tanggal)) {
                        return '-';
                    }else{
                        return date('d M Y', strtotime($model->tanggal));
                    }
                }
            ],
            'desc:ntext',
            [
                'label' => 'Izin Dari Keasramaan', 
                'value' => function($model){
                    if (is_null($model->statusByKeasramaan['request'])) {
                        return '-';
                    }else{
                        return $model->statusByKeasramaan['request'];
                    }
                }
            ],
            [
                'label' => 'Disetujui/Ditolak oleh',
                'value' => function($model){
                    if (is_null($model->keasramaan['nama'])) {
                        return '-';
                    }else{
                        return $model->keasramaan['nama'];
                    }
                }
            ],
            [
                'label' => 'Izin Dari Dosen', 
                'value' => function($model){
                    if ($model->statusByDosen['request_id'] == null && $model->statusByKeasramaan['request_id'] != null){
                        return 'Tidak perlu persetujuan dosen';
                    }else if (is_null($model->statusByDosen['request'])) {
                        return '-';
                    }else{
                        return $model->statusByDosen['request'];
                    }
                }
            ],
            [
                'label' => 'Disetujui/Ditolak oleh',
                'value' => function($model){
                    if (is_null($model->dosen['nama'])) {
                        return '-';
                    }else{
                        return $model->dosen['nama'];
                    }
                }
            ],
            
        ],
    ]);} ?>

</div>
