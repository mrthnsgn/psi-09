<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\kntn\models\IzinMakan */

$this->title = 'Buat Izin Makan';
$this->params['breadcrumbs'][] = ['label' => 'Izin Makan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="izin-makan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
