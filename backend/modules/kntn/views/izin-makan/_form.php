<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\components\ToolsColumn;
use common\helpers\LinkHelper;
use dosamigos\datetimepicker\DateTimePicker;
use backend\modules\kntn\models\JenisKegiatan;
use backend\modules\kntn\models\JenisIzin;
use backend\modules\kntn\models\Dim;

/* @var $this yii\web\View */
/* @var $model backend\modules\kntn\models\IzinMakan */
/* @var $form yii\widgets\ActiveForm */
$datetime = new DateTime();
$datetime->modify('+1 day');
$dim = Dim::find()->where('deleted != 1')->andWhere(['user_id' => Yii::$app->user->identity->user_id])->one();
?>

<div class="izin-makan-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6 col-sm-6">
            <?= $form->field($model, 'jam_makan')->dropDownList(array("Pagi"=>"Pagi","Siang"=>"Siang","Malam"=>"Malam"), ['prompt'=>'Pilih Jam Makan'])?>
        </div>

        <div class="col-md-6 col-sm-6">
            <?= $form->field($model, 'tanggal')->widget(DateTimePicker::className(), [
                'language' => 'en',
                'size' => 'ms',
                'pickButtonIcon' => 'glyphicon glyphicon-time',
                'inline' => false,
                'clientOptions' => [
                    'startView' => 2,
                    'minView' => 2,
                    'maxView' => 2,
                    'pickerPosition' => 'bottom-left',
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd', // if inline = false
                    // 'todayBtn' => true,
                    'startDate' => date($datetime->format("Y-m-d")),
                ]
            ]);?>

            
        </div>
        
    </div>
    
    

    <div class="row">
        <div class="col-md-6 col-sm-6">
            <?= $form->field($model, 'jenis_kegiatan_id')->dropDownList(ArrayHelper::map(JenisKegiatan::find()->where('deleted!=1')->all(), 'jenis_kegiatan_id', 'jenis_kegiatan'), ['prompt'=>'Pilih Jenis Kegiatan'])?>
        </div>

        <div class="col-md-6 col-sm-6">
            <?= $form->field($model, 'jenis_izin_id')->dropDownList(ArrayHelper::map(JenisIzin::find()->where('deleted!=1')->all(), 'jenis_izin_id', 'jenis_izin'), ['prompt'=>'Pilih Jenis Izin'])?>
        </div>
    </div>

    <?= $form->field($model, 'desc')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'dim_id')->hiddenInput(['value' => $dim->dim_id])->label(false) ?>

    <hr>

    
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Buat' : 'Edit', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>