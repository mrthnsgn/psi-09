<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\kntn\models\IzinMakan */

$this->title = 'Edit Izin Makan';
$this->params['breadcrumbs'][] = ['label' => 'Izin Makan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dim['nama'], 'url' => ['view', 'id' => $model->izin_makan_id]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="izin-makan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
