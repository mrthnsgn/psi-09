/*
SQLyog Ultimate v8.6 Beta2
MySQL - 5.5.5-10.1.21-MariaDB : Database - cis_production
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cis_production` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `cis_production`;

/*Table structure for table `absn_absensi` */

DROP TABLE IF EXISTS `absn_absensi`;

CREATE TABLE `absn_absensi` (
  `absensi_id` int(11) NOT NULL AUTO_INCREMENT,
  `sesi_kuliah_id` int(11) NOT NULL,
  `dim_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`absensi_id`),
  KEY `FK_absn_absensi_sesi_kuliah_idx` (`sesi_kuliah_id`),
  KEY `FK_absn_absensi_dim` (`dim_id`),
  CONSTRAINT `FK_absn_absensi_dim` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_absn_absensi_sesi_kuliah` FOREIGN KEY (`sesi_kuliah_id`) REFERENCES `absn_sesi_kuliah` (`sesi_kuliah_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `absn_absensi` */

/*Table structure for table `absn_sesi_kuliah` */

DROP TABLE IF EXISTS `absn_sesi_kuliah`;

CREATE TABLE `absn_sesi_kuliah` (
  `sesi_kuliah_id` int(11) NOT NULL AUTO_INCREMENT,
  `penugasan_pengajaran_id` int(11) NOT NULL,
  `lokasi_id` int(11) NOT NULL,
  `sesi` char(1) NOT NULL,
  `waktu_mulai` datetime DEFAULT NULL,
  `waktu_akhir` datetime DEFAULT NULL,
  `catatan` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`sesi_kuliah_id`),
  KEY `FK_absn_sesi_kuliah_lokasi_idx` (`lokasi_id`),
  KEY `FK_absn_sesi_kuliah_penugasan_pengajaran` (`penugasan_pengajaran_id`),
  CONSTRAINT `FK_absn_sesi_kuliah_lokasi` FOREIGN KEY (`lokasi_id`) REFERENCES `mref_r_lokasi` (`lokasi_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_absn_sesi_kuliah_penugasan_pengajaran` FOREIGN KEY (`penugasan_pengajaran_id`) REFERENCES `adak_penugasan_pengajaran` (`penugasan_pengajaran_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `absn_sesi_kuliah` */

/*Table structure for table `adak_kelas` */

DROP TABLE IF EXISTS `adak_kelas`;

CREATE TABLE `adak_kelas` (
  `kelas_id` int(11) NOT NULL AUTO_INCREMENT,
  `ta` int(4) NOT NULL DEFAULT '0',
  `nama` varchar(20) NOT NULL DEFAULT '',
  `ket` text,
  `dosen_wali_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`kelas_id`),
  KEY `FK_adak_kelas_wali` (`dosen_wali_id`),
  CONSTRAINT `FK_adak_kelas_wali` FOREIGN KEY (`dosen_wali_id`) REFERENCES `hrdx_dosen` (`dosen_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `adak_kelas` */

/*Table structure for table `adak_mahasiswa_assistant` */

DROP TABLE IF EXISTS `adak_mahasiswa_assistant`;

CREATE TABLE `adak_mahasiswa_assistant` (
  `mahasiswa_assistant_id` int(11) NOT NULL AUTO_INCREMENT,
  `pengajaran_id` int(11) DEFAULT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `is_fulltime` tinyint(1) DEFAULT '1',
  `start_date` date DEFAULT '0000-00-00',
  `end_date` date DEFAULT '0000-00-00',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`mahasiswa_assistant_id`),
  KEY `FK_adak_mahasiswa_assistant_pengajaran` (`pengajaran_id`),
  KEY `FK_adak_mahasiswa_assistant_dim` (`dim_id`),
  CONSTRAINT `FK_adak_mahasiswa_assistant_dim` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_adak_mahasiswa_assistant_pengajaran` FOREIGN KEY (`pengajaran_id`) REFERENCES `adak_pengajaran` (`pengajaran_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `adak_mahasiswa_assistant` */

/*Table structure for table `adak_pengajaran` */

DROP TABLE IF EXISTS `adak_pengajaran`;

CREATE TABLE `adak_pengajaran` (
  `pengajaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `kuliah_id` int(11) DEFAULT NULL,
  `ta` int(11) DEFAULT NULL,
  `sem_ta` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`pengajaran_id`),
  KEY `FK_adak_pengajaran_kuliah` (`kuliah_id`),
  CONSTRAINT `FK_adak_pengajaran_kuliah` FOREIGN KEY (`kuliah_id`) REFERENCES `krkm_kuliah` (`kuliah_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `adak_pengajaran` */

/*Table structure for table `adak_penugasan_pengajaran` */

DROP TABLE IF EXISTS `adak_penugasan_pengajaran`;

CREATE TABLE `adak_penugasan_pengajaran` (
  `penugasan_pengajaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `pengajaran_id` int(11) DEFAULT NULL,
  `pegawai_id` int(11) DEFAULT NULL,
  `role_pengajar_id` int(11) NOT NULL,
  `is_fulltime` tinyint(1) DEFAULT '1',
  `start_date` date DEFAULT '0000-00-00',
  `end_date` date DEFAULT '0000-00-00',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`penugasan_pengajaran_id`),
  KEY `FK_prkl_pengajaran_role_pengajar` (`role_pengajar_id`),
  KEY `FK_adak_penugasan_pengajaran_pegawai` (`pegawai_id`),
  KEY `FK_adak_penugasan_pengajaran` (`pengajaran_id`),
  CONSTRAINT `FK_adak_penugasan_pengajaran` FOREIGN KEY (`pengajaran_id`) REFERENCES `adak_pengajaran` (`pengajaran_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_adak_penugasan_pengajaran_pegawai` FOREIGN KEY (`pegawai_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_prkl_pengajaran_role_pengajar` FOREIGN KEY (`role_pengajar_id`) REFERENCES `mref_r_role_pengajar` (`role_pengajar_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `adak_penugasan_pengajaran` */

/*Table structure for table `adak_registrasi` */

DROP TABLE IF EXISTS `adak_registrasi`;

CREATE TABLE `adak_registrasi` (
  `registrasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `nim` varchar(8) NOT NULL DEFAULT '',
  `status_akhir_registrasi` varchar(50) DEFAULT 'Aktif',
  `ta` varchar(30) NOT NULL DEFAULT '0',
  `sem_ta` int(11) NOT NULL DEFAULT '0',
  `sem` smallint(6) NOT NULL DEFAULT '0',
  `tgl_daftar` date DEFAULT NULL,
  `keuangan` double DEFAULT NULL,
  `kelas` varchar(20) DEFAULT NULL,
  `id` varchar(20) DEFAULT NULL,
  `nr` float DEFAULT NULL,
  `koa_approval` int(11) NOT NULL DEFAULT '0',
  `koa_approval_bp` int(11) NOT NULL DEFAULT '0',
  `kelas_id` int(11) DEFAULT NULL,
  `dosen_wali_id` int(11) DEFAULT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`registrasi_id`),
  KEY `fk_t_registrasi_t_kelas1_idx` (`kelas_id`),
  KEY `fk_t_registrasi_t_profile1_idx` (`dosen_wali_id`),
  KEY `fk_t_registrasi_t_dim1_idx` (`dim_id`),
  CONSTRAINT `FK_adak_registrasi_dosen_wali` FOREIGN KEY (`dosen_wali_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_prkl_registrasi_kelas` FOREIGN KEY (`kelas_id`) REFERENCES `adak_kelas` (`kelas_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_t_registrasi_t_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `adak_registrasi` */

/*Table structure for table `arsp_arsip` */

DROP TABLE IF EXISTS `arsp_arsip`;

CREATE TABLE `arsp_arsip` (
  `arsip_id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) DEFAULT NULL,
  `desc` text,
  `user_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`arsip_id`),
  KEY `FK_arsip_user` (`user_id`),
  CONSTRAINT `FK_arsip_user` FOREIGN KEY (`user_id`) REFERENCES `sysx_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `arsp_arsip` */

/*Table structure for table `arsp_arsip_file` */

DROP TABLE IF EXISTS `arsp_arsip_file`;

CREATE TABLE `arsp_arsip_file` (
  `arsip_file_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_file` varchar(100) DEFAULT NULL,
  `kode_file` varchar(50) DEFAULT NULL,
  `desc` text,
  `arsip_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`arsip_file_id`),
  KEY `FK_arsip_file` (`arsip_id`),
  CONSTRAINT `FK_arsip_file` FOREIGN KEY (`arsip_id`) REFERENCES `arsp_arsip` (`arsip_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `arsp_arsip_file` */

/*Table structure for table `artk_post` */

DROP TABLE IF EXISTS `artk_post`;

CREATE TABLE `artk_post` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(225) DEFAULT NULL,
  `body` text,
  `user_id` int(11) DEFAULT NULL,
  `category` varchar(150) DEFAULT NULL,
  `in_category` varchar(50) DEFAULT 'home',
  `public_status` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`post_id`),
  KEY `FK_artk_post_user` (`user_id`),
  CONSTRAINT `FK_artk_post_user` FOREIGN KEY (`user_id`) REFERENCES `sysx_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `artk_post` */

/*Table structure for table `artk_post_attachment` */

DROP TABLE IF EXISTS `artk_post_attachment`;

CREATE TABLE `artk_post_attachment` (
  `post_attachment_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `id_file` varchar(100) DEFAULT NULL,
  `nama_file` varchar(150) DEFAULT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`post_attachment_id`),
  KEY `FK_artk_post_attachment` (`post_id`),
  CONSTRAINT `FK_artk_post_attachment` FOREIGN KEY (`post_id`) REFERENCES `artk_post` (`post_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `artk_post_attachment` */

/*Table structure for table `askm_asrama` */

DROP TABLE IF EXISTS `askm_asrama`;

CREATE TABLE `askm_asrama` (
  `asrama_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `lokasi` varchar(45) NOT NULL,
  `desc` text,
  `kapasitas` int(11) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`asrama_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `askm_asrama` */

insert  into `askm_asrama`(`asrama_id`,`name`,`lokasi`,`desc`,`kapasitas`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,'Pniel','Area Kampus','0',208,0,NULL,NULL,NULL,NULL,'2018-07-11 16:28:37','keasramaan'),(2,'Kapernaum','Area Kampus','3',144,0,NULL,NULL,NULL,NULL,'2018-07-16 11:08:46','keasramaan'),(3,'Silo','Area Kampus','0',144,0,NULL,NULL,NULL,NULL,'2018-07-11 14:53:25','keasramaan'),(4,'Betfage','Area Kampus','0',64,0,NULL,NULL,NULL,NULL,'2018-07-09 10:55:45','baak'),(5,'Antiokhia','Area Kampus','0',64,0,NULL,NULL,NULL,NULL,'2018-07-09 10:55:30','baak'),(6,'Mahanaim','Luar Kampus/Pintu Bosi','0',248,0,NULL,NULL,NULL,NULL,'2018-07-09 10:55:15','baak'),(7,'Mamre','Luar Kampus/Pintu Bosi','0',242,0,NULL,NULL,NULL,NULL,'2018-07-09 10:55:02','baak'),(8,'Nazareth','Luar Kampus/Pintu Bosi','Asrama Putra Sarjana Tkt. 3 dan Tkt. 4',198,0,NULL,NULL,NULL,NULL,'2018-07-17 17:04:10','keasramaan');

/*Table structure for table `askm_bentuk_pelanggaran` */

DROP TABLE IF EXISTS `askm_bentuk_pelanggaran`;

CREATE TABLE `askm_bentuk_pelanggaran` (
  `bentuk_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`bentuk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `askm_bentuk_pelanggaran` */

insert  into `askm_bentuk_pelanggaran`(`bentuk_id`,`name`,`desc`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,'DI ASRAMA','Belum ada deskripsi',0,NULL,'',NULL,'','2018-08-06 09:26:29','keasramaan'),(2,'DI LINGKUNGAN KANTIN/RUANG MAKAN','Belum ada deskripsi',0,NULL,NULL,NULL,NULL,NULL,NULL),(3,'DI KELAS','Belum ada deskripsi',0,NULL,NULL,NULL,NULL,NULL,NULL),(4,'PENAMPILAN','Belum ada deskripsi',0,NULL,NULL,NULL,NULL,NULL,NULL),(5,'MELAKUKAN TINDAKAN TIDAK TERPUJI','Belum ada deskripsi',0,NULL,NULL,NULL,NULL,NULL,NULL),(6,'MELAKUKAN PELANGGARAN PADA KEGIA','Belum ada deskripsi',0,NULL,NULL,NULL,NULL,NULL,NULL),(7,'TINDAKAN MERUSAK FASILITAS/ MENG','Belum ada deskripsi',0,NULL,NULL,NULL,NULL,NULL,NULL),(8,'MEROKOK/NARKOBA/ PERJUDIAN/MIRAS','Belum ada deskripsi',0,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `askm_dim_kamar` */

DROP TABLE IF EXISTS `askm_dim_kamar`;

CREATE TABLE `askm_dim_kamar` (
  `dim_kamar_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_dim_kamar` tinyint(1) DEFAULT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `kamar_id` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`dim_kamar_id`),
  KEY `askm_dim_kamar_ibfk_2` (`kamar_id`),
  KEY `askm_dim_kamar_ibfk_3` (`dim_id`),
  CONSTRAINT `askm_dim_kamar_ibfk_2` FOREIGN KEY (`kamar_id`) REFERENCES `askm_kamar` (`kamar_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `askm_dim_kamar_ibfk_3` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `askm_dim_kamar` */

/*Table structure for table `askm_dim_pelanggaran` */

DROP TABLE IF EXISTS `askm_dim_pelanggaran`;

CREATE TABLE `askm_dim_pelanggaran` (
  `pelanggaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_pelanggaran` tinyint(1) DEFAULT '0',
  `pembinaan_id` int(11) NOT NULL,
  `penilaian_id` int(11) NOT NULL,
  `poin_id` int(11) NOT NULL,
  `desc_pembinaan` text,
  `desc_pelanggaran` text,
  `tanggal` date NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`pelanggaran_id`),
  KEY `pembinaan_id` (`pembinaan_id`),
  KEY `penilaian_id` (`penilaian_id`),
  KEY `poin_id` (`poin_id`),
  CONSTRAINT `askm_dim_pelanggaran_ibfk_1` FOREIGN KEY (`pembinaan_id`) REFERENCES `askm_pembinaan` (`pembinaan_id`),
  CONSTRAINT `askm_dim_pelanggaran_ibfk_2` FOREIGN KEY (`penilaian_id`) REFERENCES `askm_dim_penilaian` (`penilaian_id`),
  CONSTRAINT `askm_dim_pelanggaran_ibfk_3` FOREIGN KEY (`poin_id`) REFERENCES `askm_poin_pelanggaran` (`poin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

/*Data for the table `askm_dim_pelanggaran` */

insert  into `askm_dim_pelanggaran`(`pelanggaran_id`,`status_pelanggaran`,`pembinaan_id`,`penilaian_id`,`poin_id`,`desc_pembinaan`,`desc_pelanggaran`,`tanggal`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (25,1,1,24,1,'Dilakukan oleh Bapak Ery Nababan','Terlambat 30 menit','2018-08-10',0,NULL,NULL,'2018-08-10 14:40:20','keasramaan','2018-08-10 14:46:15','keasramaan'),(26,1,1,24,2,'Dilakukan oleh Pdt. Ria Simanjuntak','Tidak berbaris karena terlambat menuju kantin','2018-08-10',0,NULL,NULL,'2018-08-10 14:41:09','keasramaan','2018-08-10 14:48:56','keasramaan'),(27,1,1,24,3,'test','test','2018-08-10',0,NULL,NULL,'2018-08-10 15:27:10','keasramaan','2018-08-10 15:28:21','keasramaan'),(28,1,3,24,5,'','','2018-08-10',0,NULL,NULL,'2018-08-10 15:29:49','keasramaan','2018-08-13 12:24:14','keasramaan'),(29,0,1,25,1,'','','2018-08-20',0,NULL,NULL,'2018-08-20 12:40:54','dosen','2018-08-20 12:40:54','dosen'),(30,1,1,26,4,'','','2018-08-20',0,NULL,NULL,'2018-08-20 12:41:18','dosen','2018-08-21 14:41:32','dosen'),(31,1,1,26,4,'','','2018-08-20',0,NULL,NULL,'2018-08-20 12:41:28','dosen','2018-08-21 14:40:59','dosen'),(32,0,2,27,5,'','','2018-08-20',0,NULL,NULL,'2018-08-20 12:41:46','dosen','2018-08-20 12:41:46','dosen'),(33,0,2,28,5,'','','2018-08-20',0,NULL,NULL,'2018-08-20 12:42:00','dosen','2018-08-20 12:42:00','dosen'),(34,0,1,28,4,'','','2018-08-20',0,NULL,NULL,'2018-08-20 12:42:15','dosen','2018-08-20 12:42:15','dosen'),(35,0,1,29,7,'','','2018-08-20',0,NULL,NULL,'2018-08-20 12:42:36','dosen','2018-08-20 12:42:36','dosen'),(36,0,2,29,5,'','','2018-08-20',0,NULL,NULL,'2018-08-20 12:42:45','dosen','2018-08-20 12:42:45','dosen'),(37,0,2,29,4,'','','2018-08-20',0,NULL,NULL,'2018-08-20 12:42:55','dosen','2018-08-20 12:42:55','dosen'),(38,0,2,29,4,'','','2018-08-21',0,NULL,NULL,'2018-08-21 14:39:05','dosen','2018-08-21 14:39:05','dosen');

/*Table structure for table `askm_dim_penilaian` */

DROP TABLE IF EXISTS `askm_dim_penilaian`;

CREATE TABLE `askm_dim_penilaian` (
  `penilaian_id` int(11) NOT NULL AUTO_INCREMENT,
  `desc` text,
  `ta` int(4) DEFAULT NULL,
  `sem_ta` int(1) DEFAULT NULL,
  `akumulasi_skor` int(11) DEFAULT '0',
  `dim_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`penilaian_id`),
  KEY `dim_id` (`dim_id`),
  CONSTRAINT `askm_dim_penilaian_ibfk_1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

/*Data for the table `askm_dim_penilaian` */

insert  into `askm_dim_penilaian`(`penilaian_id`,`desc`,`ta`,`sem_ta`,`akumulasi_skor`,`dim_id`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (24,NULL,2018,1,0,5,0,NULL,NULL,'2018-08-10 14:38:59','keasramaan','2018-08-13 12:24:14','keasramaan'),(25,NULL,2018,1,1,6,0,NULL,NULL,'2018-08-10 14:39:00','keasramaan','2018-08-20 12:40:54','dosen'),(26,NULL,2018,1,0,7,0,NULL,NULL,'2018-08-10 14:39:00','keasramaan','2018-08-21 14:41:19','dosen'),(27,NULL,2018,1,15,8,0,NULL,NULL,'2018-08-10 14:39:00','keasramaan','2018-08-20 12:41:46','dosen'),(28,NULL,2018,1,19,9,0,NULL,NULL,'2018-08-10 14:39:00','keasramaan','2018-08-20 12:42:14','dosen'),(29,NULL,2018,1,26,10,0,NULL,NULL,'2018-08-10 14:39:00','keasramaan','2018-08-21 14:39:05','dosen');

/*Table structure for table `askm_izin_bermalam` */

DROP TABLE IF EXISTS `askm_izin_bermalam`;

CREATE TABLE `askm_izin_bermalam` (
  `izin_bermalam_id` int(11) NOT NULL AUTO_INCREMENT,
  `rencana_berangkat` datetime NOT NULL,
  `rencana_kembali` datetime NOT NULL,
  `realisasi_berangkat` datetime DEFAULT NULL,
  `realisasi_kembali` datetime DEFAULT NULL,
  `desc` text NOT NULL,
  `tujuan` varchar(45) NOT NULL,
  `dim_id` int(11) NOT NULL,
  `keasramaan_id` int(11) DEFAULT NULL,
  `status_request_id` int(11) DEFAULT '1',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`izin_bermalam_id`),
  KEY `fk_askm_izin_bermalam_dimx_dim1_idx` (`dim_id`),
  KEY `fk_askm_izin_bermalam_askm_keasramaan1_idx` (`keasramaan_id`),
  KEY `fk_askm_izin_bermalam_askm_r_status_request1_idx` (`status_request_id`),
  CONSTRAINT `askm_izin_bermalam_ibfk_1` FOREIGN KEY (`keasramaan_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`),
  CONSTRAINT `fk_askm_izin_bermalam_askm_r_status_request1` FOREIGN KEY (`status_request_id`) REFERENCES `askm_r_status_request` (`status_request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_askm_izin_bermalam_dimx_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `askm_izin_bermalam` */

insert  into `askm_izin_bermalam`(`izin_bermalam_id`,`rencana_berangkat`,`rencana_kembali`,`realisasi_berangkat`,`realisasi_kembali`,`desc`,`tujuan`,`dim_id`,`keasramaan_id`,`status_request_id`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,'2019-04-10 00:00:00','2019-04-11 00:00:00',NULL,NULL,'test','test',5,NULL,1,0,NULL,'','2019-04-08 11:59:38','if316021','2019-04-08 11:59:38','if316021');

/*Table structure for table `askm_izin_keluar` */

DROP TABLE IF EXISTS `askm_izin_keluar`;

CREATE TABLE `askm_izin_keluar` (
  `izin_keluar_id` int(11) NOT NULL AUTO_INCREMENT,
  `rencana_berangkat` datetime NOT NULL,
  `rencana_kembali` datetime NOT NULL,
  `realisasi_berangkat` datetime DEFAULT NULL,
  `realisasi_kembali` datetime DEFAULT NULL,
  `desc` text NOT NULL,
  `dim_id` int(11) NOT NULL,
  `dosen_wali_id` int(11) DEFAULT NULL,
  `baak_id` int(11) DEFAULT NULL,
  `keasramaan_id` int(11) DEFAULT NULL,
  `status_request_baak` int(11) DEFAULT '1',
  `status_request_keasramaan` int(11) DEFAULT '1',
  `status_request_dosen_wali` int(11) DEFAULT '1',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`izin_keluar_id`),
  KEY `fk_askm_izin_keluar_dimx_dim1_idx` (`dim_id`),
  KEY `fk_askm_izin_keluar_hrdx_dosen1_idx` (`dosen_wali_id`),
  KEY `fk_askm_izin_keluar_hrdx_staf1_idx` (`baak_id`),
  KEY `fk_askm_izin_keluar_askm_r_status_request1_idx` (`status_request_dosen_wali`),
  KEY `fk_askm_izin_keluar_askm_keasramaan1_idx` (`keasramaan_id`),
  KEY `status_request_keasramaan` (`status_request_keasramaan`),
  KEY `status_request_baak` (`status_request_baak`),
  CONSTRAINT `askm_izin_keluar_ibfk_1` FOREIGN KEY (`status_request_keasramaan`) REFERENCES `askm_r_status_request` (`status_request_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `askm_izin_keluar_ibfk_2` FOREIGN KEY (`status_request_baak`) REFERENCES `askm_r_status_request` (`status_request_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `askm_izin_keluar_ibfk_3` FOREIGN KEY (`dosen_wali_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `askm_izin_keluar_ibfk_4` FOREIGN KEY (`keasramaan_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `askm_izin_keluar_ibfk_5` FOREIGN KEY (`baak_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_askm_izin_keluar_askm_r_status_request1` FOREIGN KEY (`status_request_dosen_wali`) REFERENCES `askm_r_status_request` (`status_request_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_askm_izin_keluar_dimx_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `askm_izin_keluar` */

/*Table structure for table `askm_izin_kolaboratif` */

DROP TABLE IF EXISTS `askm_izin_kolaboratif`;

CREATE TABLE `askm_izin_kolaboratif` (
  `izin_kolaboratif_id` int(11) NOT NULL AUTO_INCREMENT,
  `rencana_mulai` date NOT NULL,
  `rencana_berakhir` date NOT NULL,
  `batas_waktu` time NOT NULL,
  `desc` text NOT NULL,
  `dim_id` int(11) NOT NULL,
  `status_request_id` int(11) DEFAULT '1',
  `baak_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`izin_kolaboratif_id`),
  KEY `fk_askm_izin_tambahan_jam_kolaboratif_dimx_dim1_idx` (`dim_id`),
  KEY `fk_askm_izin_tambahan_jam_kolaboratif_askm_r_status_request_idx` (`status_request_id`),
  KEY `fk_askm_izin_tambahan_jam_kolaboratif_hrdx_staf1_idx` (`baak_id`),
  CONSTRAINT `askm_izin_kolaboratif_ibfk_1` FOREIGN KEY (`baak_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_askm_izin_tambahan_jam_kolaboratif_askm_r_status_request1` FOREIGN KEY (`status_request_id`) REFERENCES `askm_r_status_request` (`status_request_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_askm_izin_tambahan_jam_kolaboratif_dimx_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `askm_izin_kolaboratif` */

/*Table structure for table `askm_izin_pengampu` */

DROP TABLE IF EXISTS `askm_izin_pengampu`;

CREATE TABLE `askm_izin_pengampu` (
  `id_izin_pengampu` int(11) NOT NULL AUTO_INCREMENT,
  `izin_keluar_id` int(11) DEFAULT NULL,
  `dosen_id` int(11) DEFAULT NULL,
  `status_request_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id_izin_pengampu`),
  KEY `dosen_id` (`dosen_id`),
  KEY `status_request_id` (`status_request_id`),
  KEY `izin_keluar_id` (`izin_keluar_id`),
  CONSTRAINT `askm_izin_pengampu_ibfk_1` FOREIGN KEY (`dosen_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`),
  CONSTRAINT `askm_izin_pengampu_ibfk_2` FOREIGN KEY (`status_request_id`) REFERENCES `askm_r_status_request` (`status_request_id`),
  CONSTRAINT `askm_izin_pengampu_ibfk_3` FOREIGN KEY (`izin_keluar_id`) REFERENCES `askm_izin_keluar` (`izin_keluar_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `askm_izin_pengampu` */

/*Table structure for table `askm_izin_ruangan` */

DROP TABLE IF EXISTS `askm_izin_ruangan`;

CREATE TABLE `askm_izin_ruangan` (
  `izin_ruangan_id` int(11) NOT NULL AUTO_INCREMENT,
  `rencana_mulai` datetime NOT NULL,
  `rencana_berakhir` datetime NOT NULL,
  `desc` text NOT NULL,
  `dim_id` int(11) NOT NULL,
  `baak_id` int(11) DEFAULT NULL,
  `lokasi_id` int(11) DEFAULT NULL,
  `status_request_id` int(11) DEFAULT '1',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`izin_ruangan_id`),
  KEY `fk_askm_izin_penggunaan_ruangan_dimx_dim1_idx` (`dim_id`),
  KEY `fk_askm_izin_penggunaan_ruangan_hrdx_staf1_idx` (`baak_id`),
  KEY `fk_askm_izin_penggunaan_ruangan_askm_r_status_request1_idx` (`status_request_id`),
  KEY `lokasi_id` (`lokasi_id`),
  CONSTRAINT `askm_izin_ruangan_ibfk_1` FOREIGN KEY (`lokasi_id`) REFERENCES `mref_r_lokasi` (`lokasi_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `askm_izin_ruangan_ibfk_2` FOREIGN KEY (`baak_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_askm_izin_penggunaan_ruangan_askm_r_status_request1` FOREIGN KEY (`status_request_id`) REFERENCES `askm_r_status_request` (`status_request_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_askm_izin_penggunaan_ruangan_dimx_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `askm_izin_ruangan` */

/*Table structure for table `askm_kamar` */

DROP TABLE IF EXISTS `askm_kamar`;

CREATE TABLE `askm_kamar` (
  `kamar_id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor_kamar` varchar(45) DEFAULT NULL,
  `asrama_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`kamar_id`),
  KEY `fk_askm_kamar_askm_asrama1_idx` (`asrama_id`),
  CONSTRAINT `fk_askm_kamar_askm_asrama1` FOREIGN KEY (`asrama_id`) REFERENCES `askm_asrama` (`asrama_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `askm_kamar` */

/*Table structure for table `askm_keasramaan` */

DROP TABLE IF EXISTS `askm_keasramaan`;

CREATE TABLE `askm_keasramaan` (
  `keasramaan_id` int(11) NOT NULL AUTO_INCREMENT,
  `no_hp` varchar(32) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `asrama_id` int(11) DEFAULT NULL,
  `pegawai_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`keasramaan_id`),
  KEY `fk_askm_keasramaan_hrdx_pegawai1_idx` (`pegawai_id`),
  KEY `askm_keasramaan_pegawai_ibfk_2` (`asrama_id`),
  CONSTRAINT `askm_keasramaan_ibfk_1` FOREIGN KEY (`pegawai_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `askm_keasramaan_ibfk_2` FOREIGN KEY (`asrama_id`) REFERENCES `askm_asrama` (`asrama_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `askm_keasramaan` */

/*Table structure for table `askm_log_mahasiswa` */

DROP TABLE IF EXISTS `askm_log_mahasiswa`;

CREATE TABLE `askm_log_mahasiswa` (
  `log_mahasiswa_id` int(11) NOT NULL AUTO_INCREMENT,
  `dim_id` int(11) NOT NULL,
  `tanggal_keluar` datetime DEFAULT NULL,
  `tanggal_masuk` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`log_mahasiswa_id`),
  KEY `fk_dim_log_mahasiswa` (`dim_id`),
  CONSTRAINT `fk_dim_log_mahasiswa` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `askm_log_mahasiswa` */

insert  into `askm_log_mahasiswa`(`log_mahasiswa_id`,`dim_id`,`tanggal_keluar`,`tanggal_masuk`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (2,5,'2018-07-30 11:10:25','2018-07-30 11:30:25',0,NULL,NULL,NULL,NULL,NULL,NULL),(3,8,'2018-07-30 12:00:28','2018-07-30 13:30:25',0,NULL,NULL,NULL,NULL,NULL,NULL),(4,5,'2018-07-30 11:05:28','2018-07-30 10:30:25',0,NULL,NULL,NULL,NULL,NULL,NULL),(5,5,'2018-07-30 10:30:25','2018-07-30 11:30:25',0,NULL,NULL,NULL,NULL,NULL,NULL),(6,8,'2018-07-30 10:23:28','2018-07-30 11:30:25',0,NULL,NULL,NULL,NULL,NULL,NULL),(7,8,'2018-07-30 08:10:25','2018-07-30 10:30:25',0,NULL,NULL,NULL,NULL,NULL,NULL),(8,5,'2018-07-30 12:48:28',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(9,8,'2018-07-30 12:30:28',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(10,8,'2018-07-30 09:10:25','2018-07-30 12:30:25',0,NULL,NULL,NULL,NULL,NULL,NULL),(11,5,'2018-07-30 10:48:28','2018-07-30 10:50:28',0,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `askm_pedoman` */

DROP TABLE IF EXISTS `askm_pedoman`;

CREATE TABLE `askm_pedoman` (
  `pedoman_id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) DEFAULT NULL,
  `isi` longtext,
  `jenis_izin` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`pedoman_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `askm_pedoman` */

insert  into `askm_pedoman`(`pedoman_id`,`judul`,`isi`,`jenis_izin`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,'Pedoman Izin Bermalam','<ul><li>Mahasiswa diberikan Izin Bermalam di Luar Kampus (IBL) di hari Jumat atau Sabtu atau di hari lain dimana keesokan harinya tidak ada kegiatan akademik atau kegiatan lainnya yang tidak mengharuskan mahasiswa berada di kampus IT Del.</li><li>Mahasiswa yang IBL wajib menjaga nama baik IT Del di luar kampus.</li><li>Mahasiswa mengisi pengajuan IBL di Aplikasi CIS (https://cis.del.ac.id/askm/izin-bermalam) selambatnya H-2. Dan mencetak form IBL untuk ditandatangani Bapak/Ibu Asrama dan ditunjukan di Pos Satpam saat keluar kampus.</li><li>Pada saat kembali ke kampus, mahasiswa mengumpulkan kertas IBL yang telah ditandatangani oleh orangtua di Pos Satpam untuk selanjutnya dikumpulkan dan direkap oleh Pembina Asrama.</li><li>Apabila terdapat kegiatan Badan Eksekutif Mahasiswa (BEM) yang mengharuskan seluruh mahasiswa mengikuti kegiatan tersebut, maka mahasiswa tidak diperbolehkan IBL.</li><li>Mahasiswa yang tidak mengajukan IBL sesuai ketentuan pada butir 3 (tiga) tidak diizinkan untuk IBL kecuali dalam kondisi mendesak (emergency) seperti sakit atau ada keluarga meninggal</li></ul>',1,0,NULL,NULL,'2018-07-18 13:50:53','keasramaan','2018-07-30 18:05:59','keasramaan'),(2,'Pedoman Izin Keluar','<p>Belum ada</p>',2,0,NULL,NULL,'2018-07-18 15:51:21','keasramaan','2018-07-27 13:57:52','dosen'),(3,'Pedoman Izin Penggunaan Ruangan','<p>Belum ada</p>',4,0,NULL,NULL,'2018-07-18 15:51:40','keasramaan','2018-07-18 16:20:58','keasramaan'),(4,'Pedoman Izin Tambahan Jam Kolaboratif','<p>Belum ada</p>',3,0,NULL,NULL,'2018-07-18 15:51:58','keasramaan','2018-07-18 16:21:06','keasramaan');

/*Table structure for table `askm_pembinaan` */

DROP TABLE IF EXISTS `askm_pembinaan`;

CREATE TABLE `askm_pembinaan` (
  `pembinaan_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`pembinaan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `askm_pembinaan` */

insert  into `askm_pembinaan`(`pembinaan_id`,`name`,`desc`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,'Dicatat/dinasihati/diberikan sanksi sesuai ketentuan pelanggaran','Melakukan satu pelanggaran ringan.',0,NULL,NULL,NULL,NULL,NULL,NULL),(2,'Dipanggil/dilakukan pendampingan atau konseling','Melakukan tindakan pelanggaran ringan hingga tiga kali;\r\nMelakukan pelangaran yang sama hingga dua kali dalam kurun waktu berdekatan;\r\nMencapai batas skor pelanggaran 10 poin.',0,NULL,NULL,NULL,NULL,NULL,NULL),(3,'Menulis Surat Pernyataan Komitmen ','Melakukan pelanggaran ringan yang sama dan berulang-ulang setelah dipanggil;\r\nMencapai batas skor pelanggaran 15 poin. ',0,NULL,NULL,NULL,NULL,NULL,NULL),(4,'Dibawa ke Lembaga Kemahasiswaan','Melakukan pelanggaran ringan yang sama dan berulang meski telah membuat surat pernyataan komitmen;\r\nMelakukan suatu pelanggaran sedang atau berat; \r\nMelakukan pelanggaran yang dianggap perlu oleh Pembina Asrama untuk diberikan kasusnya pada kemahasiswaan.',0,NULL,NULL,NULL,NULL,NULL,NULL),(5,'Diberi Surat Peringatan oleh Kemahasiswaan	Mencapai poin pelanggaran 30 karena beberapa kasus pelangaran.','Mencapai poin pelanggaran >15 atas beberapa pelanggaran;  \r\nTindakan lain yang dianggap perlu oleh Pembina Asrama untuk diberitahukan kasusnya pada orangtua.',0,NULL,NULL,NULL,NULL,NULL,NULL),(6,'Dibawa ke Komisi Disiplin','Jika melakukan pelangaran berat atau pelanggaran yang dianggap pantas oleh akademik/kemahasiswaan.',0,NULL,NULL,NULL,NULL,NULL,NULL),(7,'Dikeluarkan','Jika melakukan pelanggaran berat dengan poin 100.',0,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `askm_poin_kebaikan` */

DROP TABLE IF EXISTS `askm_poin_kebaikan`;

CREATE TABLE `askm_poin_kebaikan` (
  `kebaikan_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `desc` longtext,
  `penilaian_id` int(11) DEFAULT NULL,
  `pelanggaran_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`kebaikan_id`),
  KEY `pelanggaran_id` (`pelanggaran_id`),
  KEY `penilaian_id` (`penilaian_id`),
  CONSTRAINT `askm_poin_kebaikan_ibfk_1` FOREIGN KEY (`pelanggaran_id`) REFERENCES `askm_dim_pelanggaran` (`pelanggaran_id`),
  CONSTRAINT `askm_poin_kebaikan_ibfk_2` FOREIGN KEY (`penilaian_id`) REFERENCES `askm_dim_penilaian` (`penilaian_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Data for the table `askm_poin_kebaikan` */

insert  into `askm_poin_kebaikan`(`kebaikan_id`,`name`,`desc`,`penilaian_id`,`pelanggaran_id`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (12,'Membersihkan toilet gd. 5','Sungguh anak yang baik',24,25,0,NULL,NULL,'2018-08-10 14:41:43','keasramaan','2018-08-10 14:41:43','keasramaan'),(13,'Membersihkan toilet gd. 5','Baik dia ya',24,26,0,NULL,NULL,'2018-08-10 14:48:56','keasramaan','2018-08-10 14:48:56','keasramaan'),(14,'Membersihkan toilet gd. 7','Test',24,27,0,NULL,NULL,'2018-08-10 15:28:21','keasramaan','2018-08-10 15:28:21','keasramaan'),(15,'Membersihkan toilet gd. 5','',24,NULL,0,NULL,NULL,'2018-08-13 12:07:18','keasramaan','2018-08-13 12:07:18','keasramaan'),(16,'Test','Test',24,28,0,NULL,NULL,'2018-08-13 12:24:14','keasramaan','2018-08-13 12:24:14','keasramaan'),(17,'Test','test',26,31,0,NULL,NULL,'2018-08-21 14:40:59','dosen','2018-08-21 14:40:59','dosen'),(18,'Test','',26,31,0,NULL,NULL,'2018-08-21 14:41:19','dosen','2018-08-21 14:41:19','dosen'),(19,'test','test',26,30,0,NULL,NULL,'2018-08-21 14:41:32','dosen','2018-08-21 14:41:32','dosen');

/*Table structure for table `askm_poin_pelanggaran` */

DROP TABLE IF EXISTS `askm_poin_pelanggaran`;

CREATE TABLE `askm_poin_pelanggaran` (
  `poin_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `poin` int(11) DEFAULT NULL,
  `desc` text,
  `bentuk_id` int(11) DEFAULT NULL,
  `tingkat_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`poin_id`),
  KEY `tingkat_id` (`tingkat_id`),
  KEY `bentuk_id` (`bentuk_id`),
  CONSTRAINT `askm_poin_pelanggaran_ibfk_1` FOREIGN KEY (`tingkat_id`) REFERENCES `askm_tingkat_pelanggaran` (`tingkat_id`),
  CONSTRAINT `askm_poin_pelanggaran_ibfk_2` FOREIGN KEY (`bentuk_id`) REFERENCES `askm_bentuk_pelanggaran` (`bentuk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `askm_poin_pelanggaran` */

insert  into `askm_poin_pelanggaran`(`poin_id`,`name`,`poin`,`desc`,`bentuk_id`,`tingkat_id`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,'Terlambat bangun pagi.',1,'Belum ada',1,1,0,NULL,NULL,NULL,NULL,'2018-08-10 10:47:19','keasramaan'),(2,'Tidak berbaris.',2,'Belum ada',2,1,0,NULL,NULL,NULL,NULL,NULL,NULL),(3,'Terlambat masuk kelas.',2,'Belum ada',3,1,0,NULL,NULL,NULL,NULL,NULL,NULL),(4,'Berpakaian tidak sesuai dengan ketentuan',4,'Belum ada',4,1,0,NULL,NULL,NULL,NULL,NULL,NULL),(5,'Mengucapkan kata-kata yang tidak pantas',15,'Belum ada',5,2,0,NULL,NULL,NULL,NULL,NULL,NULL),(6,'Tidak membawa perlengkapan Ibadah',2,'Belum ada',6,1,0,NULL,NULL,NULL,NULL,NULL,NULL),(7,'Membuang ludah dan atau dahak di sembarang tempat',3,'Belum ada',7,1,0,NULL,NULL,NULL,NULL,NULL,NULL),(8,'Membawa dan menghisap rokok.',50,'Belum ada',8,3,0,NULL,NULL,NULL,NULL,NULL,NULL),(9,'Test',100,'<p>Test</p>',8,3,0,NULL,NULL,'2018-08-08 14:11:35','keasramaan','2018-08-08 14:11:35','keasramaan');

/*Table structure for table `askm_r_status_request` */

DROP TABLE IF EXISTS `askm_r_status_request`;

CREATE TABLE `askm_r_status_request` (
  `status_request_id` int(11) NOT NULL,
  `status_request` varchar(45) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`status_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `askm_r_status_request` */

insert  into `askm_r_status_request`(`status_request_id`,`status_request`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,'Menunggu',0,NULL,NULL,NULL,NULL,NULL,NULL),(2,'Disetujui',0,NULL,NULL,NULL,NULL,NULL,NULL),(3,'Ditolak',0,NULL,NULL,NULL,NULL,NULL,NULL),(4,'Dibatalkan',0,NULL,NULL,NULL,NULL,NULL,NULL),(5,'Sudah izin',0,NULL,NULL,NULL,NULL,NULL,NULL),(6,'Tanpa Keterangan',0,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `askm_sanksi_dim` */

DROP TABLE IF EXISTS `askm_sanksi_dim`;

CREATE TABLE `askm_sanksi_dim` (
  `sanksi_dim_id` int(11) NOT NULL AUTO_INCREMENT,
  `desc` text,
  `tanggal` date NOT NULL,
  `mulai_kerja` time NOT NULL,
  `selesai_kerja` time NOT NULL,
  `status_request_id` int(11) NOT NULL DEFAULT '1',
  `sanksi_sosial_id` int(11) NOT NULL,
  `pengawas_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`sanksi_dim_id`),
  KEY `sanksi_sosial_id` (`sanksi_sosial_id`),
  KEY `status_request_id` (`status_request_id`),
  KEY `askm_sanksi_dim_ibfk_4` (`pengawas_id`),
  CONSTRAINT `askm_sanksi_dim_ibfk_1` FOREIGN KEY (`sanksi_sosial_id`) REFERENCES `askm_sanksi_sosial` (`sanksi_sosial_id`),
  CONSTRAINT `askm_sanksi_dim_ibfk_3` FOREIGN KEY (`status_request_id`) REFERENCES `askm_r_status_request` (`status_request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Data for the table `askm_sanksi_dim` */

insert  into `askm_sanksi_dim`(`sanksi_dim_id`,`desc`,`tanggal`,`mulai_kerja`,`selesai_kerja`,`status_request_id`,`sanksi_sosial_id`,`pengawas_id`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (7,'Membersihkan toilet GD9','2019-04-09','05:00:00','06:00:00',6,3,243,0,NULL,NULL,'2019-04-09 09:24:29','keasramaan','2019-04-15 10:16:52','keasramaan'),(8,'Membersihkan pekarangan pos 2','2019-04-09','05:00:00','05:30:00',6,3,243,0,NULL,NULL,'2019-04-09 10:06:46','keasramaan','2019-04-15 10:16:52','keasramaan'),(9,'Membersihkan pos 1','2019-04-09','05:00:00','06:00:00',6,3,243,0,NULL,NULL,'2019-04-09 11:42:37','11S15027','2019-04-15 10:16:52','keasramaan'),(15,'','2019-04-15','00:00:00','00:00:00',6,3,243,0,NULL,NULL,'2019-04-09 14:38:10','11S15027','2019-04-23 13:35:36','keasramaan'),(16,'test','2019-04-09','05:00:00','06:00:00',6,4,243,0,NULL,NULL,'2019-04-09 14:38:10','11S15027','2019-04-15 10:16:52','keasramaan'),(17,'','2019-04-15','00:00:00','00:00:00',6,4,243,0,NULL,NULL,'2019-04-09 17:18:29','keasramaan','2019-04-23 13:35:36','keasramaan'),(19,'Menstempel kertas ujian uas','2019-04-10','01:00:00','05:00:00',2,5,243,0,NULL,NULL,NULL,NULL,NULL,NULL),(20,'test','2019-04-15','10:00:00','11:00:00',2,5,243,0,NULL,NULL,'2019-04-15 10:36:34','keasramaan','2019-04-15 10:40:28','keasramaan'),(21,'bersihkan sd','2019-04-23','04:30:00','06:00:00',5,3,243,0,NULL,NULL,'2019-04-23 13:37:01','keasramaan','2019-04-23 13:59:40','keasramaan'),(22,NULL,'2019-04-23','00:00:00','00:00:00',1,3,NULL,0,NULL,NULL,'2019-04-23 13:44:49','keasramaan','2019-04-23 13:44:49','keasramaan'),(23,NULL,'2019-04-23','00:00:00','00:00:00',1,4,NULL,0,NULL,NULL,'2019-04-23 13:44:49','keasramaan','2019-04-23 13:44:49','keasramaan'),(24,NULL,'2019-04-23','00:00:00','00:00:00',1,5,NULL,0,NULL,NULL,'2019-04-23 13:44:49','keasramaan','2019-04-23 13:44:49','keasramaan');

/*Table structure for table `askm_sanksi_sosial` */

DROP TABLE IF EXISTS `askm_sanksi_sosial`;

CREATE TABLE `askm_sanksi_sosial` (
  `sanksi_sosial_id` int(11) NOT NULL AUTO_INCREMENT,
  `desc` text NOT NULL,
  `tanggal` date NOT NULL,
  `jumlah_jam` double NOT NULL,
  `status_sanksi` tinyint(1) NOT NULL DEFAULT '0',
  `dim_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`sanksi_sosial_id`),
  KEY `dim_id` (`dim_id`),
  CONSTRAINT `askm_sanksi_sosial_ibfk_1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `askm_sanksi_sosial` */

insert  into `askm_sanksi_sosial`(`sanksi_sosial_id`,`desc`,`tanggal`,`jumlah_jam`,`status_sanksi`,`dim_id`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (3,'Test','2019-04-09',55,0,5,0,NULL,NULL,'2019-04-09 09:24:06','keasramaan','2019-04-23 13:35:36','keasramaan'),(4,'Test','2019-04-09',35,0,8,0,NULL,NULL,'2019-04-09 13:30:49','11S15027','2019-04-23 13:35:36','keasramaan'),(5,'Test','2019-04-09',0,1,6,0,NULL,NULL,NULL,NULL,'2019-04-15 10:40:28','keasramaan');

/*Table structure for table `askm_tingkat_pelanggaran` */

DROP TABLE IF EXISTS `askm_tingkat_pelanggaran`;

CREATE TABLE `askm_tingkat_pelanggaran` (
  `tingkat_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`tingkat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `askm_tingkat_pelanggaran` */

insert  into `askm_tingkat_pelanggaran`(`tingkat_id`,`name`,`desc`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,'Ringan','Merupakan pelanggaran ringan',0,NULL,NULL,NULL,NULL,'2018-08-10 10:49:11','keasramaan'),(2,'Sedang','<p>Merupakan pelanggaran sedang</p>',0,NULL,NULL,NULL,NULL,'2018-08-06 11:06:49','keasramaan'),(3,'Berat','<p>Merupakan pelanggaran berat</p>',0,NULL,NULL,NULL,NULL,'2018-08-06 11:06:54','keasramaan');

/*Table structure for table `cist_atasan_cuti_nontahunan` */

DROP TABLE IF EXISTS `cist_atasan_cuti_nontahunan`;

CREATE TABLE `cist_atasan_cuti_nontahunan` (
  `atasan_cuti_nontahunan_id` int(11) NOT NULL AUTO_INCREMENT,
  `permohonan_cuti_nontahunan_id` int(11) DEFAULT NULL,
  `pegawai_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`atasan_cuti_nontahunan_id`),
  KEY `FK_cist_atasan_cuti_nontahunan` (`permohonan_cuti_nontahunan_id`),
  CONSTRAINT `FK_cist_atasan_cuti_nontahunan` FOREIGN KEY (`permohonan_cuti_nontahunan_id`) REFERENCES `cist_permohonan_cuti_nontahunan` (`permohonan_cuti_nontahunan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

/*Data for the table `cist_atasan_cuti_nontahunan` */

insert  into `cist_atasan_cuti_nontahunan`(`atasan_cuti_nontahunan_id`,`permohonan_cuti_nontahunan_id`,`pegawai_id`,`name`,`deleted`,`deleted_at`,`deleted_by`,`created_by`,`created_at`,`updated_by`,`updated_at`) values (6,14,222,NULL,0,NULL,NULL,'dosen','2018-08-30 14:39:41','dosen','2018-08-30 14:39:41'),(7,15,222,NULL,0,NULL,NULL,'dosen','2018-08-30 15:36:29','dosen','2018-08-30 15:36:29'),(8,16,222,NULL,0,NULL,NULL,'dosen','2018-08-31 11:16:28','dosen','2018-08-31 11:16:28');

/*Table structure for table `cist_atasan_cuti_tahunan` */

DROP TABLE IF EXISTS `cist_atasan_cuti_tahunan`;

CREATE TABLE `cist_atasan_cuti_tahunan` (
  `atasan_cuti_tahunan_id` int(11) NOT NULL AUTO_INCREMENT,
  `permohonan_cuti_tahunan_id` int(11) DEFAULT NULL,
  `pegawai_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`atasan_cuti_tahunan_id`),
  KEY `FK_cist_atasan_cuti_tahunan` (`permohonan_cuti_tahunan_id`),
  CONSTRAINT `FK_cist_atasan_cuti_tahunan` FOREIGN KEY (`permohonan_cuti_tahunan_id`) REFERENCES `cist_permohonan_cuti_tahunan` (`permohonan_cuti_tahunan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

/*Data for the table `cist_atasan_cuti_tahunan` */

insert  into `cist_atasan_cuti_tahunan`(`atasan_cuti_tahunan_id`,`permohonan_cuti_tahunan_id`,`pegawai_id`,`name`,`deleted`,`deleted_at`,`deleted_by`,`created_by`,`created_at`,`updated_by`,`updated_at`) values (1,NULL,222,NULL,0,NULL,NULL,'dosen','2018-08-31 11:49:44','dosen','2018-08-31 11:49:44'),(2,NULL,222,NULL,0,NULL,NULL,'dosen','2018-08-31 12:00:21','dosen','2018-08-31 12:00:21'),(3,NULL,222,NULL,0,NULL,NULL,'dosen','2018-08-31 12:01:04','dosen','2018-08-31 12:01:04'),(4,NULL,222,NULL,0,NULL,NULL,'dosen','2018-08-31 12:02:25','dosen','2018-08-31 12:02:25'),(28,31,222,NULL,0,NULL,NULL,'dosen','2018-08-31 14:53:22','dosen','2018-08-31 14:53:22'),(29,32,222,NULL,0,NULL,NULL,'dosen','2018-08-31 14:54:14','dosen','2018-08-31 14:54:14'),(30,33,222,NULL,0,NULL,NULL,'dosen','2018-08-31 16:11:44','dosen','2018-08-31 16:11:44'),(31,34,222,NULL,0,NULL,NULL,'dosen','2018-08-31 16:39:02','dosen','2018-08-31 16:39:02'),(32,35,222,NULL,0,NULL,NULL,'dosen','2018-10-03 17:06:39','dosen','2018-10-03 17:06:39');

/*Table structure for table `cist_atasan_izin` */

DROP TABLE IF EXISTS `cist_atasan_izin`;

CREATE TABLE `cist_atasan_izin` (
  `atasan_izin_id` int(11) NOT NULL AUTO_INCREMENT,
  `permohonan_izin_id` int(11) DEFAULT NULL,
  `pegawai_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`atasan_izin_id`),
  KEY `FK_cist_atasan_izin` (`permohonan_izin_id`),
  CONSTRAINT `FK_cist_atasan_izin` FOREIGN KEY (`permohonan_izin_id`) REFERENCES `cist_permohonan_izin` (`permohonan_izin_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

/*Data for the table `cist_atasan_izin` */

insert  into `cist_atasan_izin`(`atasan_izin_id`,`permohonan_izin_id`,`pegawai_id`,`name`,`deleted`,`deleted_at`,`deleted_by`,`created_by`,`created_at`,`updated_by`,`updated_at`) values (47,40,222,NULL,0,NULL,NULL,'dosen','2018-08-27 14:56:08','dosen','2018-08-27 14:56:08'),(48,44,222,NULL,0,NULL,NULL,'dosen','2018-08-27 16:28:02','dosen','2018-08-27 16:28:02'),(49,45,222,NULL,0,NULL,NULL,'dosen','2018-08-28 12:29:54','dosen','2018-08-28 12:29:54'),(50,46,222,NULL,0,NULL,NULL,'dosen','2018-08-30 11:10:26','dosen','2018-08-30 11:10:26'),(51,47,222,NULL,0,NULL,NULL,'dosen','2018-08-31 11:15:00','dosen','2018-08-31 11:15:00'),(52,48,222,NULL,0,NULL,NULL,'dosen','2018-09-26 09:13:32','dosen','2018-09-26 09:13:32'),(53,49,222,NULL,0,NULL,NULL,'dosen','2018-10-05 09:53:35','dosen','2018-10-05 09:53:35');

/*Table structure for table `cist_atasan_surat_tugas` */

DROP TABLE IF EXISTS `cist_atasan_surat_tugas`;

CREATE TABLE `cist_atasan_surat_tugas` (
  `atasan_surat_tugas_id` int(11) NOT NULL AUTO_INCREMENT,
  `surat_tugas_id` int(11) DEFAULT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `perequest` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`atasan_surat_tugas_id`),
  KEY `FK_cist_atasan_surat_tugas` (`surat_tugas_id`),
  KEY `FK_cist_atasan_surat_tugas_2` (`perequest`),
  CONSTRAINT `FK_cist_atasan_surat_tugas` FOREIGN KEY (`surat_tugas_id`) REFERENCES `cist_surat_tugas` (`surat_tugas_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

/*Data for the table `cist_atasan_surat_tugas` */

insert  into `cist_atasan_surat_tugas`(`atasan_surat_tugas_id`,`surat_tugas_id`,`id_pegawai`,`perequest`,`deleted`,`deleted_at`,`deleted_by`,`updated_at`,`updated_by`,`created_at`,`created_by`) values (48,87,1,54,0,NULL,NULL,'2018-08-04 18:36:03','inte','2018-08-04 18:36:03','inte');

/*Table structure for table `cist_golongan_kuota_cuti` */

DROP TABLE IF EXISTS `cist_golongan_kuota_cuti`;

CREATE TABLE `cist_golongan_kuota_cuti` (
  `golongan_kuota_cuti_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_golongan` varchar(100) DEFAULT NULL,
  `min_tahun_kerja` int(11) DEFAULT NULL,
  `max_tahun_kerja` int(11) DEFAULT NULL,
  `kuota` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`golongan_kuota_cuti_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `cist_golongan_kuota_cuti` */

insert  into `cist_golongan_kuota_cuti`(`golongan_kuota_cuti_id`,`nama_golongan`,`min_tahun_kerja`,`max_tahun_kerja`,`kuota`,`deleted`,`deleted_at`,`deleted_by`,`created_by`,`created_at`,`updated_by`,`updated_at`) values (1,'Kontrak',NULL,NULL,12,0,NULL,NULL,'root','2018-07-25 01:51:00','root','2018-07-26 23:19:24'),(2,'1-3 Tahun',1,3,12,0,NULL,NULL,'root','2018-07-25 01:51:35','root','2018-07-27 15:41:50'),(3,'3-5 Tahun',3,5,15,0,NULL,NULL,'root','2018-07-25 01:51:53','root','2018-07-27 15:42:15'),(4,'>= 5 Tahun',5,999,18,0,NULL,NULL,'root','2018-07-25 01:52:06','root','2018-07-27 15:43:53');

/*Table structure for table `cist_kategori_cuti_nontahunan` */

DROP TABLE IF EXISTS `cist_kategori_cuti_nontahunan`;

CREATE TABLE `cist_kategori_cuti_nontahunan` (
  `kategori_cuti_nontahunan_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `lama_pelaksanaan` int(6) DEFAULT NULL,
  `satuan` int(1) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`kategori_cuti_nontahunan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

/*Data for the table `cist_kategori_cuti_nontahunan` */

insert  into `cist_kategori_cuti_nontahunan`(`kategori_cuti_nontahunan_id`,`name`,`lama_pelaksanaan`,`satuan`,`deleted`,`deleted_at`,`deleted_by`,`created_by`,`created_at`,`updated_by`,`updated_at`) values (3,'Istirahat Melahirkan/ Gugur Kandungan',90,NULL,0,NULL,NULL,'root','2018-06-25 10:02:07','root','2018-06-25 10:02:07'),(4,'Cuti Diluar Tanggungan',180,NULL,0,NULL,NULL,'root','2018-06-29 09:28:33','dosen','2018-08-29 09:49:57');

/*Table structure for table `cist_kategori_izin` */

DROP TABLE IF EXISTS `cist_kategori_izin`;

CREATE TABLE `cist_kategori_izin` (
  `kategori_izin_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`kategori_izin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

/*Data for the table `cist_kategori_izin` */

insert  into `cist_kategori_izin`(`kategori_izin_id`,`name`,`deleted`,`deleted_at`,`deleted_by`,`created_by`,`created_at`,`updated_by`,`updated_at`) values (1,'Gangguan Kesehatan/Sakit',0,NULL,NULL,'root','2018-07-28 21:41:16','root','2018-07-28 21:41:16'),(2,'Pernikahan Pegawai',0,NULL,NULL,'root','2018-07-28 21:41:31','root','2018-07-28 21:41:31'),(3,'Istri Melahirkan/Gugur Kandungan',0,NULL,NULL,'root','2018-07-28 21:41:46','root','2018-07-28 21:41:46'),(4,'Suami/Istri/Orang Tua/Mertua/Anak/Menantu Meninggal Dunia',0,NULL,NULL,'root','2018-07-28 21:41:56','root','2018-07-28 21:41:56'),(5,'Anak Pegawai Baptisan ',0,NULL,NULL,'root','2018-07-28 21:42:11','root','2018-07-28 21:42:11'),(6,'Anak Pegawai Wisuda di Luar Kota',0,NULL,NULL,'root','2018-07-28 21:42:24','root','2018-07-28 21:42:24'),(7,'Anggota Keluarga dalam Satu Rumah Meninggal',0,NULL,NULL,'root','2018-07-28 21:42:37','root','2018-07-28 21:42:37');

/*Table structure for table `cist_laporan_surat_tugas` */

DROP TABLE IF EXISTS `cist_laporan_surat_tugas`;

CREATE TABLE `cist_laporan_surat_tugas` (
  `laporan_surat_tugas_id` int(11) NOT NULL AUTO_INCREMENT,
  `surat_tugas_id` int(11) NOT NULL,
  `status_id` int(11) DEFAULT NULL,
  `nama_file` varchar(200) DEFAULT NULL,
  `lokasi_file` varchar(100) DEFAULT NULL,
  `tanggal_submit` date DEFAULT NULL,
  `batas_submit` datetime NOT NULL,
  `kode_laporan` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`laporan_surat_tugas_id`),
  KEY `fk_cist_laporan_surat_tugas_cist_surat_tugas1_idx` (`surat_tugas_id`),
  KEY `FK_status_laporan_surat_tugas` (`status_id`),
  CONSTRAINT `FK_cist_laporan_surat_tugas` FOREIGN KEY (`surat_tugas_id`) REFERENCES `cist_surat_tugas` (`surat_tugas_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_status_laporan_surat_tugas` FOREIGN KEY (`status_id`) REFERENCES `cist_r_status` (`status_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cist_laporan_surat_tugas` */

/*Table structure for table `cist_permohonan_cuti_nontahunan` */

DROP TABLE IF EXISTS `cist_permohonan_cuti_nontahunan`;

CREATE TABLE `cist_permohonan_cuti_nontahunan` (
  `permohonan_cuti_nontahunan_id` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_akhir` date DEFAULT NULL,
  `alasan_cuti` text,
  `lama_cuti` int(6) DEFAULT NULL,
  `kategori_id` int(11) NOT NULL,
  `pengalihan_tugas` tinytext,
  `status_cuti_nontahunan_id` int(11) DEFAULT NULL,
  `pegawai_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`permohonan_cuti_nontahunan_id`),
  KEY `FK_cist_permohonan_cuti_pegawai` (`pegawai_id`),
  KEY `FK_cist_permohonan_cuti_nontahunan` (`kategori_id`),
  KEY `cist_permohonan_cuti_nontahunan_ibfk_1` (`status_cuti_nontahunan_id`),
  CONSTRAINT `FK_cist_permohonan_cuti_nontahunan` FOREIGN KEY (`kategori_id`) REFERENCES `cist_kategori_cuti_nontahunan` (`kategori_cuti_nontahunan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_cist_permohonan_cuti_pegawai` FOREIGN KEY (`pegawai_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`),
  CONSTRAINT `cist_permohonan_cuti_nontahunan_ibfk_1` FOREIGN KEY (`status_cuti_nontahunan_id`) REFERENCES `cist_status_cuti_nontahunan` (`status_cuti_nontahunan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `cist_permohonan_cuti_nontahunan` */

insert  into `cist_permohonan_cuti_nontahunan`(`permohonan_cuti_nontahunan_id`,`tgl_mulai`,`tgl_akhir`,`alasan_cuti`,`lama_cuti`,`kategori_id`,`pengalihan_tugas`,`status_cuti_nontahunan_id`,`pegawai_id`,`deleted`,`deleted_at`,`deleted_by`,`created_by`,`created_at`,`updated_by`,`updated_at`) values (14,'2018-09-03','2018-09-28','test',180,4,'test',16,1,0,NULL,NULL,'dosen','2018-08-30 14:39:41','dosen','2018-08-30 14:39:41'),(15,'2018-12-31','2018-09-03','tes',180,4,'tes',17,1,0,NULL,NULL,'dosen','2018-08-30 15:36:29','dosen','2018-08-30 15:36:29'),(16,'2018-11-30','2018-08-31','tes',90,3,'tes',18,1,0,NULL,NULL,'dosen','2018-08-31 11:16:28','dosen','2018-08-31 11:16:28');

/*Table structure for table `cist_permohonan_cuti_tahunan` */

DROP TABLE IF EXISTS `cist_permohonan_cuti_tahunan`;

CREATE TABLE `cist_permohonan_cuti_tahunan` (
  `permohonan_cuti_tahunan_id` int(11) NOT NULL AUTO_INCREMENT,
  `waktu_pelaksanaan` varchar(500) NOT NULL,
  `alasan_cuti` text,
  `lama_cuti` int(6) DEFAULT NULL,
  `pengalihan_tugas` text,
  `status_izin_id` int(11) DEFAULT NULL,
  `pegawai_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`permohonan_cuti_tahunan_id`),
  KEY `FK_cist_permohonan_cuti_tahunan` (`pegawai_id`),
  KEY `cist_permohonan_cuti_tahunan_ibfk_1` (`status_izin_id`),
  CONSTRAINT `FK_cist_permohonan_cuti_tahunan` FOREIGN KEY (`pegawai_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`),
  CONSTRAINT `cist_permohonan_cuti_tahunan_ibfk_1` FOREIGN KEY (`status_izin_id`) REFERENCES `cist_status_cuti_tahunan` (`status_cuti_tahunan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

/*Data for the table `cist_permohonan_cuti_tahunan` */

insert  into `cist_permohonan_cuti_tahunan`(`permohonan_cuti_tahunan_id`,`waktu_pelaksanaan`,`alasan_cuti`,`lama_cuti`,`pengalihan_tugas`,`status_izin_id`,`pegawai_id`,`deleted`,`deleted_at`,`deleted_by`,`created_by`,`created_at`,`updated_by`,`updated_at`) values (31,'2018-09-12, 2018-09-19, 2018-09-26','tes',3,'tes',26,1,0,NULL,NULL,'dosen','2018-08-31 14:53:22','dosen','2018-08-31 14:53:22'),(32,'2018-09-14, 2018-09-19, 2018-09-20','tes',3,'tes',27,1,0,NULL,NULL,'dosen','2018-08-31 14:54:14','dosen','2018-08-31 14:54:14'),(33,'2018-09-03, 2018-09-04','teees',2,'teees',28,1,0,NULL,NULL,'dosen','2018-08-31 16:11:44','dosen','2018-08-31 16:11:44'),(34,'2018-09-03, 2018-09-11, 2018-09-20','testt',3,'testt',29,1,0,NULL,NULL,'dosen','2018-08-31 16:39:02','dosen','2018-08-31 16:39:02'),(35,'2018-10-04, 2018-10-11','hj',2,'sd',30,1,0,NULL,NULL,'dosen','2018-10-03 17:06:39','dosen','2018-10-03 17:06:39');

/*Table structure for table `cist_permohonan_izin` */

DROP TABLE IF EXISTS `cist_permohonan_izin`;

CREATE TABLE `cist_permohonan_izin` (
  `permohonan_izin_id` int(11) NOT NULL AUTO_INCREMENT,
  `waktu_pelaksanaan` varchar(500) NOT NULL,
  `alasan_izin` text,
  `pengalihan_tugas` text,
  `kategori_id` int(11) DEFAULT NULL,
  `lama_izin` int(6) NOT NULL,
  `file_surat` varchar(200) DEFAULT NULL,
  `status_izin_id` int(11) DEFAULT NULL,
  `atasan_id` int(11) NOT NULL,
  `pegawai_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`permohonan_izin_id`),
  KEY `FK_cist_permohonan_izin` (`pegawai_id`),
  KEY `FK_atasan_izin` (`atasan_id`),
  KEY `FK_cist_kategori_izin` (`kategori_id`),
  KEY `status_izin_id` (`status_izin_id`),
  CONSTRAINT `FK_cist_permohonan_izin` FOREIGN KEY (`pegawai_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`),
  CONSTRAINT `FK_cist_permohonan_izinnn` FOREIGN KEY (`kategori_id`) REFERENCES `cist_kategori_izin` (`kategori_izin_id`),
  CONSTRAINT `cist_permohonan_izin_ibfk_1` FOREIGN KEY (`status_izin_id`) REFERENCES `cist_status_izin` (`status_izin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

/*Data for the table `cist_permohonan_izin` */

insert  into `cist_permohonan_izin`(`permohonan_izin_id`,`waktu_pelaksanaan`,`alasan_izin`,`pengalihan_tugas`,`kategori_id`,`lama_izin`,`file_surat`,`status_izin_id`,`atasan_id`,`pegawai_id`,`deleted`,`deleted_at`,`deleted_by`,`created_by`,`created_at`,`updated_by`,`updated_at`) values (40,'2018-08-30, 2018-08-31','tes','tes',4,2,'',38,0,1,0,NULL,NULL,'dosen','2018-08-27 14:56:08','dosen','2018-08-27 14:56:08'),(44,'2018-08-30, 2018-08-31','test','test',2,2,'',42,0,1,0,NULL,NULL,'dosen','2018-08-27 16:28:02','dosen','2018-08-27 16:28:02'),(45,'2018-08-31, 2018-09-03','Tes','Tes',3,2,NULL,43,0,1,0,NULL,NULL,'dosen','2018-08-28 12:29:54','dosen','2018-08-28 12:29:54'),(46,'2018-09-03, 2018-09-04','test','test',5,2,NULL,44,0,1,0,NULL,NULL,'dosen','2018-08-30 11:10:26','dosen','2018-08-30 11:10:26'),(47,'2018-09-04, 2018-09-05, 2018-09-07','tes','tes',3,3,NULL,45,0,1,0,NULL,NULL,'dosen','2018-08-31 11:15:00','dosen','2018-08-31 11:15:00'),(48,'2018-09-27, 2018-09-28','sakit','asdos',1,2,NULL,46,0,1,0,NULL,NULL,'dosen','2018-09-26 09:13:32','dosen','2018-09-26 09:13:32'),(49,'2018-10-10, 2018-10-11','tes','asd',NULL,2,NULL,64,0,1,0,NULL,NULL,'dosen','2018-10-05 09:53:35','dosen','2018-10-05 09:53:35');

/*Table structure for table `cist_r_jenis_surat` */

DROP TABLE IF EXISTS `cist_r_jenis_surat`;

CREATE TABLE `cist_r_jenis_surat` (
  `jenis_surat_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_surat` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`jenis_surat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `cist_r_jenis_surat` */

insert  into `cist_r_jenis_surat`(`jenis_surat_id`,`jenis_surat`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,'Diluar Kampus',0,NULL,NULL,NULL,NULL,NULL,NULL),(2,'Didalam Kampus',0,NULL,NULL,NULL,NULL,NULL,NULL),(3,'Penugasan Luar Kampus',0,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `cist_r_jumlah_libur` */

DROP TABLE IF EXISTS `cist_r_jumlah_libur`;

CREATE TABLE `cist_r_jumlah_libur` (
  `jumlah_libur_id` int(11) NOT NULL AUTO_INCREMENT,
  `jumlah` int(3) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` datetime NOT NULL,
  `deleted_by` varchar(32) NOT NULL,
  `created_by` varchar(32) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(32) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`jumlah_libur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cist_r_jumlah_libur` */

/*Table structure for table `cist_r_kuota_cuti_tahunan` */

DROP TABLE IF EXISTS `cist_r_kuota_cuti_tahunan`;

CREATE TABLE `cist_r_kuota_cuti_tahunan` (
  `kuota_cuti_tahunan_id` int(11) NOT NULL AUTO_INCREMENT,
  `pegawai_id` int(11) NOT NULL,
  `kuota` int(6) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`kuota_cuti_tahunan_id`),
  KEY `FK_cist_kuota_cuti_tahunan` (`pegawai_id`),
  CONSTRAINT `FK_cist_kuota_cuti_tahunan` FOREIGN KEY (`pegawai_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=485 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

/*Data for the table `cist_r_kuota_cuti_tahunan` */

insert  into `cist_r_kuota_cuti_tahunan`(`kuota_cuti_tahunan_id`,`pegawai_id`,`kuota`,`deleted`,`deleted_at`,`deleted_by`,`created_by`,`created_at`,`updated_by`,`updated_at`) values (364,1,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(365,2,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(366,7,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(367,15,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(368,16,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(369,22,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(370,25,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(371,35,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(372,36,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(373,41,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(374,54,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(375,55,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(376,63,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(377,66,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(378,70,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(379,80,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(380,81,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(381,86,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(382,88,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(383,91,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(384,92,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(385,96,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(386,103,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(387,105,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(388,106,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(389,111,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(390,113,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(391,115,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(392,116,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(393,118,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(394,119,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(395,120,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(396,121,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(397,122,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(398,126,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(399,128,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(400,132,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(401,133,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(402,135,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(403,136,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(404,137,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(405,138,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(406,141,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(407,148,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(408,152,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(409,159,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(410,160,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(411,161,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(412,162,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(413,163,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(414,164,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(415,165,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(416,166,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(417,171,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(418,172,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(419,173,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(420,174,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(421,176,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(422,177,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(423,178,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(424,179,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(425,181,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(426,182,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(427,183,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(428,185,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(429,186,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(430,187,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(431,188,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(432,189,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(433,191,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(434,192,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(435,193,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(436,194,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(437,195,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(438,196,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(439,197,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(440,198,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(441,199,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(442,201,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(443,202,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(444,203,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(445,204,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(446,205,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(447,206,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(448,207,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(449,208,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(450,209,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(451,210,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(452,211,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(453,212,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(454,213,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(455,214,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(456,215,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(457,221,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(458,222,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(459,223,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(460,225,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(461,226,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(462,229,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(463,230,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(464,231,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(465,233,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(466,235,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(467,236,14,0,NULL,NULL,NULL,NULL,NULL,NULL),(468,237,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(469,238,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(470,239,17,0,NULL,NULL,NULL,NULL,NULL,NULL),(471,240,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(472,242,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(473,243,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(474,244,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(475,247,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(476,248,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(477,249,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(478,250,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(479,251,11,0,NULL,NULL,NULL,NULL,NULL,NULL),(480,252,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(481,253,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(482,254,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(483,255,0,0,NULL,NULL,NULL,NULL,NULL,NULL),(484,256,0,0,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `cist_r_status` */

DROP TABLE IF EXISTS `cist_r_status`;

CREATE TABLE `cist_r_status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `cist_r_status` */

insert  into `cist_r_status`(`status_id`,`name`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,'Memohon',0,NULL,NULL,NULL,NULL,NULL,NULL),(2,'Review',0,NULL,NULL,NULL,NULL,NULL,NULL),(3,'Diterbitkan',0,NULL,NULL,NULL,NULL,NULL,NULL),(4,'Ditolak',0,NULL,NULL,NULL,NULL,NULL,NULL),(5,'Dibatalkan',0,NULL,NULL,NULL,NULL,NULL,NULL),(6,'Diterima',0,NULL,NULL,NULL,NULL,NULL,NULL),(7,'Laporan telah dimasukkan',0,NULL,NULL,NULL,NULL,NULL,NULL),(8,'Laporan belum dimasukkan',0,NULL,NULL,NULL,NULL,NULL,NULL),(9,'Laporan diterima',0,NULL,NULL,NULL,NULL,NULL,NULL),(10,'Laporan ditolak',0,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `cist_r_waktu_generate_kuota_cuti` */

DROP TABLE IF EXISTS `cist_r_waktu_generate_kuota_cuti`;

CREATE TABLE `cist_r_waktu_generate_kuota_cuti` (
  `waktu_generate_kuota_cuti_id` int(11) NOT NULL AUTO_INCREMENT,
  `waktu_generate_terakhir` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` datetime NOT NULL,
  `deleted_by` varchar(32) NOT NULL,
  `created_by` varchar(32) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` varchar(32) NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`waktu_generate_kuota_cuti_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `cist_r_waktu_generate_kuota_cuti` */

insert  into `cist_r_waktu_generate_kuota_cuti`(`waktu_generate_kuota_cuti_id`,`waktu_generate_terakhir`,`deleted`,`deleted_at`,`deleted_by`,`created_by`,`created_at`,`updated_by`,`updated_at`) values (1,'2018-10-05 06:23:46',0,'0000-00-00 00:00:00','','','0000-00-00 00:00:00','','0000-00-00 00:00:00');

/*Table structure for table `cist_status_cuti_nontahunan` */

DROP TABLE IF EXISTS `cist_status_cuti_nontahunan`;

CREATE TABLE `cist_status_cuti_nontahunan` (
  `status_cuti_nontahunan_id` int(11) NOT NULL AUTO_INCREMENT,
  `permohonan_cuti_nontahunan_id` int(11) NOT NULL,
  `status_by_hrd` int(11) DEFAULT '1',
  `status_by_atasan` int(11) DEFAULT '1',
  `status_by_wr2` int(11) DEFAULT '1',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`status_cuti_nontahunan_id`),
  KEY `FK_cist_r_status_cuti_nontahunan` (`permohonan_cuti_nontahunan_id`),
  KEY `status_by_hrd` (`status_by_hrd`),
  KEY `status_by_atasan` (`status_by_atasan`),
  KEY `status_by_wr2` (`status_by_wr2`),
  CONSTRAINT `FK_cist_r_status_cuti_nontahunan` FOREIGN KEY (`permohonan_cuti_nontahunan_id`) REFERENCES `cist_permohonan_cuti_nontahunan` (`permohonan_cuti_nontahunan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cist_status_cuti_nontahunan_ibfk_1` FOREIGN KEY (`status_by_hrd`) REFERENCES `cist_r_status` (`status_id`),
  CONSTRAINT `cist_status_cuti_nontahunan_ibfk_2` FOREIGN KEY (`status_by_atasan`) REFERENCES `cist_r_status` (`status_id`),
  CONSTRAINT `cist_status_cuti_nontahunan_ibfk_3` FOREIGN KEY (`status_by_wr2`) REFERENCES `cist_r_status` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

/*Data for the table `cist_status_cuti_nontahunan` */

insert  into `cist_status_cuti_nontahunan`(`status_cuti_nontahunan_id`,`permohonan_cuti_nontahunan_id`,`status_by_hrd`,`status_by_atasan`,`status_by_wr2`,`deleted`,`deleted_at`,`deleted_by`,`created_by`,`created_at`,`updated_by`,`updated_at`) values (16,14,6,6,6,0,NULL,NULL,'dosen','2018-08-30 14:39:41','dosen','2018-08-30 14:40:27'),(17,15,5,5,5,0,NULL,NULL,'dosen','2018-08-30 15:36:29','dosen','2018-08-30 16:56:58'),(18,16,5,5,5,0,NULL,NULL,'dosen','2018-08-31 11:16:28','dosen','2018-10-03 08:20:22');

/*Table structure for table `cist_status_cuti_tahunan` */

DROP TABLE IF EXISTS `cist_status_cuti_tahunan`;

CREATE TABLE `cist_status_cuti_tahunan` (
  `status_cuti_tahunan_id` int(11) NOT NULL AUTO_INCREMENT,
  `permohonan_cuti_tahunan_id` int(11) NOT NULL,
  `status_by_hrd` int(11) DEFAULT '1',
  `status_by_atasan` int(11) DEFAULT '1',
  `status_by_wr2` int(11) DEFAULT '1',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`status_cuti_tahunan_id`),
  KEY `FK_cist_r_status_cuti_tahunan` (`permohonan_cuti_tahunan_id`),
  KEY `status_by_hrd` (`status_by_hrd`),
  KEY `status_by_atasan` (`status_by_atasan`),
  KEY `status_by_wr2` (`status_by_wr2`),
  CONSTRAINT `FK_cist_r_status_cuti_tahunan` FOREIGN KEY (`permohonan_cuti_tahunan_id`) REFERENCES `cist_permohonan_cuti_tahunan` (`permohonan_cuti_tahunan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cist_status_cuti_tahunan_ibfk_1` FOREIGN KEY (`status_by_hrd`) REFERENCES `cist_r_status` (`status_id`),
  CONSTRAINT `cist_status_cuti_tahunan_ibfk_2` FOREIGN KEY (`status_by_atasan`) REFERENCES `cist_r_status` (`status_id`),
  CONSTRAINT `cist_status_cuti_tahunan_ibfk_3` FOREIGN KEY (`status_by_wr2`) REFERENCES `cist_r_status` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

/*Data for the table `cist_status_cuti_tahunan` */

insert  into `cist_status_cuti_tahunan`(`status_cuti_tahunan_id`,`permohonan_cuti_tahunan_id`,`status_by_hrd`,`status_by_atasan`,`status_by_wr2`,`deleted`,`deleted_at`,`deleted_by`,`created_by`,`created_at`,`updated_by`,`updated_at`) values (26,31,6,6,6,0,NULL,NULL,'dosen','2018-08-31 14:53:22','dosen','2018-08-31 14:53:22'),(27,32,6,6,6,0,NULL,NULL,'dosen','2018-08-31 14:54:14','dosen','2018-08-31 14:54:14'),(28,33,6,6,6,0,NULL,NULL,'dosen','2018-08-31 16:11:44','dosen','2018-08-31 16:21:36'),(29,34,5,5,5,0,NULL,NULL,'dosen','2018-08-31 16:39:02','dosen','2018-09-03 12:48:24'),(30,35,1,6,6,0,NULL,NULL,'dosen','2018-10-03 17:06:39','rektor','2018-10-17 13:14:12');

/*Table structure for table `cist_status_izin` */

DROP TABLE IF EXISTS `cist_status_izin`;

CREATE TABLE `cist_status_izin` (
  `status_izin_id` int(11) NOT NULL AUTO_INCREMENT,
  `permohonan_izin_id` int(11) DEFAULT NULL,
  `status_by_atasan` int(11) DEFAULT '1',
  `status_by_wr2` int(11) DEFAULT '1',
  `status_by_hrd` int(11) DEFAULT '1',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`status_izin_id`),
  KEY `status_by_atasan` (`status_by_atasan`),
  KEY `status_by_wr2` (`status_by_wr2`),
  KEY `status_by_hrd` (`status_by_hrd`),
  KEY `permohonan_izin_id` (`permohonan_izin_id`),
  CONSTRAINT `cist_status_izin_ibfk_1` FOREIGN KEY (`status_by_atasan`) REFERENCES `cist_r_status` (`status_id`),
  CONSTRAINT `cist_status_izin_ibfk_2` FOREIGN KEY (`status_by_wr2`) REFERENCES `cist_r_status` (`status_id`),
  CONSTRAINT `cist_status_izin_ibfk_3` FOREIGN KEY (`status_by_hrd`) REFERENCES `cist_r_status` (`status_id`),
  CONSTRAINT `cist_status_izin_ibfk_4` FOREIGN KEY (`permohonan_izin_id`) REFERENCES `cist_permohonan_izin` (`permohonan_izin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

/*Data for the table `cist_status_izin` */

insert  into `cist_status_izin`(`status_izin_id`,`permohonan_izin_id`,`status_by_atasan`,`status_by_wr2`,`status_by_hrd`,`deleted`,`deleted_at`,`deleted_by`,`created_by`,`created_at`,`updated_by`,`updated_at`) values (38,40,5,5,5,0,NULL,NULL,'dosen','2018-08-27 14:56:07','dosen','2018-08-27 15:02:34'),(42,44,4,4,6,0,NULL,NULL,'dosen','2018-08-27 16:28:02','rektor','2018-08-27 21:56:37'),(43,45,4,4,4,0,NULL,NULL,'dosen','2018-08-28 12:29:54','dosen','2018-08-30 12:19:26'),(44,46,6,4,6,0,NULL,NULL,'dosen','2018-08-30 11:10:26','rektor','2018-08-31 11:11:00'),(45,47,6,6,6,0,NULL,NULL,'dosen','2018-08-31 11:15:00','dosen','2018-08-31 11:15:00'),(46,48,5,5,5,0,NULL,NULL,'dosen','2018-09-26 09:13:31','dosen','2018-10-05 09:55:55'),(47,NULL,1,1,1,0,NULL,NULL,'dosen','2018-10-03 16:25:28','dosen','2018-10-03 16:25:28'),(48,NULL,1,1,1,0,NULL,NULL,'dosen','2018-10-03 16:26:36','dosen','2018-10-03 16:26:36'),(49,NULL,1,1,1,0,NULL,NULL,'dosen','2018-10-03 16:31:28','dosen','2018-10-03 16:31:28'),(50,NULL,1,1,1,0,NULL,NULL,'dosen','2018-10-03 16:32:54','dosen','2018-10-03 16:32:54'),(51,NULL,1,1,1,0,NULL,NULL,'dosen','2018-10-03 16:33:28','dosen','2018-10-03 16:33:28'),(52,NULL,1,1,1,0,NULL,NULL,'dosen','2018-10-03 16:33:56','dosen','2018-10-03 16:33:56'),(53,NULL,1,1,1,0,NULL,NULL,'dosen','2018-10-03 16:36:40','dosen','2018-10-03 16:36:40'),(54,NULL,1,1,1,0,NULL,NULL,'dosen','2018-10-03 16:46:11','dosen','2018-10-03 16:46:11'),(55,NULL,1,1,1,0,NULL,NULL,'dosen','2018-10-03 16:46:51','dosen','2018-10-03 16:46:51'),(56,NULL,1,1,1,0,NULL,NULL,'dosen','2018-10-03 16:47:27','dosen','2018-10-03 16:47:27'),(57,NULL,1,1,1,0,NULL,NULL,'dosen','2018-10-03 16:47:53','dosen','2018-10-03 16:47:53'),(58,NULL,1,1,1,0,NULL,NULL,'dosen','2018-10-03 16:49:08','dosen','2018-10-03 16:49:08'),(59,NULL,1,1,1,0,NULL,NULL,'dosen','2018-10-03 17:07:59','dosen','2018-10-03 17:07:59'),(60,NULL,1,1,1,0,NULL,NULL,'dosen','2018-10-05 09:22:12','dosen','2018-10-05 09:22:12'),(61,NULL,1,1,1,0,NULL,NULL,'dosen','2018-10-05 09:38:10','dosen','2018-10-05 09:38:10'),(62,NULL,1,1,1,0,NULL,NULL,'dosen','2018-10-05 09:42:48','dosen','2018-10-05 09:42:48'),(63,NULL,1,1,1,0,NULL,NULL,'dosen','2018-10-05 09:53:13','dosen','2018-10-05 09:53:13'),(64,49,1,1,1,0,NULL,NULL,'dosen','2018-10-05 09:53:35','dosen','2018-10-05 09:53:35');

/*Table structure for table `cist_surat_tugas` */

DROP TABLE IF EXISTS `cist_surat_tugas`;

CREATE TABLE `cist_surat_tugas` (
  `surat_tugas_id` int(11) NOT NULL AUTO_INCREMENT,
  `perequest` int(11) NOT NULL,
  `no_surat` varchar(45) DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `tanggal_berangkat` datetime DEFAULT NULL,
  `tanggal_kembali` datetime DEFAULT NULL,
  `tanggal_mulai` datetime DEFAULT NULL,
  `tanggal_selesai` datetime DEFAULT NULL,
  `agenda` varchar(100) DEFAULT NULL,
  `pengalihan_tugas` text,
  `review_surat` text,
  `transportasi` text,
  `desc_surat_tugas` text,
  `catatan` text,
  `jenis_surat_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`surat_tugas_id`),
  KEY `fk_cist_surat_tugas_cist_pegawai1_idx` (`perequest`),
  KEY `FK_cist_status` (`status_id`),
  KEY `FK_jenis_surat` (`jenis_surat_id`),
  CONSTRAINT `FK_cist_status` FOREIGN KEY (`status_id`) REFERENCES `cist_r_status` (`status_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_cist_surat_tugas_perequest` FOREIGN KEY (`perequest`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_jenis_surat` FOREIGN KEY (`jenis_surat_id`) REFERENCES `cist_r_jenis_surat` (`jenis_surat_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=latin1;

/*Data for the table `cist_surat_tugas` */

insert  into `cist_surat_tugas`(`surat_tugas_id`,`perequest`,`no_surat`,`tempat`,`tanggal_berangkat`,`tanggal_kembali`,`tanggal_mulai`,`tanggal_selesai`,`agenda`,`pengalihan_tugas`,`review_surat`,`transportasi`,`desc_surat_tugas`,`catatan`,`jenis_surat_id`,`status_id`,`deleted`,`deleted_at`,`deleted_by`,`updated_at`,`updated_by`,`created_at`,`created_by`) values (87,54,NULL,'tes','2018-08-15 09:45:00','2018-08-14 09:45:00','2018-08-07 18:25:00','2018-08-15 13:50:00','tes','<p>tes</p>',NULL,'<p>tes</p>','<p>tes</p>',NULL,1,6,0,NULL,NULL,'2018-08-04 18:36:50','tennov','2018-08-04 18:36:03','inte');

/*Table structure for table `cist_surat_tugas_assignee` */

DROP TABLE IF EXISTS `cist_surat_tugas_assignee`;

CREATE TABLE `cist_surat_tugas_assignee` (
  `surat_tugas_assignee_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pegawai` int(11) DEFAULT NULL,
  `surat_tugas_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`surat_tugas_assignee_id`),
  KEY `FK_cist_surat_tugas_assignee` (`id_pegawai`),
  KEY `FK_surat` (`surat_tugas_id`),
  CONSTRAINT `FK_cist_surat_tugas_assignee` FOREIGN KEY (`id_pegawai`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_surat` FOREIGN KEY (`surat_tugas_id`) REFERENCES `cist_surat_tugas` (`surat_tugas_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=latin1;

/*Data for the table `cist_surat_tugas_assignee` */

insert  into `cist_surat_tugas_assignee`(`surat_tugas_assignee_id`,`id_pegawai`,`surat_tugas_id`,`deleted`,`deleted_at`,`deleted_by`,`updated_at`,`updated_by`,`created_at`,`created_by`) values (110,54,87,0,NULL,NULL,'2018-08-04 18:36:04','inte','2018-08-04 18:36:04','inte');

/*Table structure for table `cist_surat_tugas_file` */

DROP TABLE IF EXISTS `cist_surat_tugas_file`;

CREATE TABLE `cist_surat_tugas_file` (
  `surat_tugas_file_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_file` varchar(100) DEFAULT NULL,
  `lokasi_file` varchar(100) DEFAULT NULL,
  `surat_tugas_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` date DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `kode_file` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`surat_tugas_file_id`),
  KEY `FK_cist_surat_tugas_file` (`surat_tugas_id`),
  CONSTRAINT `FK_cist_surat_tugas_file` FOREIGN KEY (`surat_tugas_id`) REFERENCES `cist_surat_tugas` (`surat_tugas_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

/*Data for the table `cist_surat_tugas_file` */

insert  into `cist_surat_tugas_file`(`surat_tugas_file_id`,`nama_file`,`lokasi_file`,`surat_tugas_id`,`deleted`,`deleted_at`,`deleted_by`,`updated_at`,`updated_by`,`created_at`,`created_by`,`kode_file`) values (49,'candle_light_1152x864.jpg',NULL,87,0,NULL,NULL,'2018-08-04','inte','2018-08-04','inte',NULL);

/*Table structure for table `cist_waktu_cuti_tahunan` */

DROP TABLE IF EXISTS `cist_waktu_cuti_tahunan`;

CREATE TABLE `cist_waktu_cuti_tahunan` (
  `waktu_cuti_tahunan_id` int(11) NOT NULL AUTO_INCREMENT,
  `permohonan_cuti_tahunan_id` int(11) DEFAULT NULL,
  `durasi` date DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`waktu_cuti_tahunan_id`),
  KEY `FK_cist_waktu_cuti_tahunan` (`permohonan_cuti_tahunan_id`),
  CONSTRAINT `FK_cist_waktu_cuti_tahunan` FOREIGN KEY (`permohonan_cuti_tahunan_id`) REFERENCES `cist_permohonan_cuti_tahunan` (`permohonan_cuti_tahunan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

/*Data for the table `cist_waktu_cuti_tahunan` */

/*Table structure for table `dimx_alumni` */

DROP TABLE IF EXISTS `dimx_alumni`;

CREATE TABLE `dimx_alumni` (
  `alumni_id` int(11) NOT NULL AUTO_INCREMENT,
  `ta` int(11) NOT NULL DEFAULT '0',
  `sem` int(11) NOT NULL DEFAULT '0',
  `nim` varchar(8) NOT NULL DEFAULT '',
  `status` varchar(50) NOT NULL DEFAULT '',
  `tanggal_lulus` date DEFAULT NULL,
  `sks_lulus` int(11) DEFAULT NULL,
  `ipk_lulus` float DEFAULT NULL,
  `no_sk_yudisium` varchar(30) DEFAULT NULL,
  `tanggal_sk` date DEFAULT NULL,
  `no_ijazah` varchar(40) DEFAULT NULL,
  `no_transkrip` varchar(255) NOT NULL DEFAULT '',
  `predikat_lulus` varchar(100) NOT NULL DEFAULT '',
  `judul_ta` text NOT NULL,
  `pembimbing1` varchar(20) DEFAULT NULL,
  `pembimbing2` varchar(20) DEFAULT NULL,
  `dosen_id_1` int(11) DEFAULT NULL,
  `dosen_id_2` int(11) DEFAULT NULL,
  `dim_id` int(11) NOT NULL,
  `n_toefl` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`alumni_id`),
  KEY `fk_t_alumni_t_dim1_idx` (`dim_id`),
  KEY `FK_dimx_alumni_dosen_1` (`dosen_id_1`),
  KEY `FK_dimx_alumni_dosen_2` (`dosen_id_2`),
  CONSTRAINT `FK_dimx_alumni_dosen_1` FOREIGN KEY (`dosen_id_1`) REFERENCES `hrdx_dosen` (`dosen_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_alumni_dosen_2` FOREIGN KEY (`dosen_id_2`) REFERENCES `hrdx_dosen` (`dosen_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_t_alumni_t_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dimx_alumni` */

/*Table structure for table `dimx_alumni_data` */

DROP TABLE IF EXISTS `dimx_alumni_data`;

CREATE TABLE `dimx_alumni_data` (
  `alumni_data_id` int(11) NOT NULL AUTO_INCREMENT,
  `nim` varchar(8) NOT NULL DEFAULT '',
  `alamat` varchar(100) NOT NULL DEFAULT '',
  `kota` varchar(255) DEFAULT NULL,
  `propinsi` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL DEFAULT '',
  `hp` varchar(20) NOT NULL DEFAULT '',
  `telepon` varchar(20) DEFAULT NULL,
  `alumni_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`alumni_data_id`),
  UNIQUE KEY `NIM_UNIQUE` (`nim`),
  KEY `fk_t_alumni_data_t_alumni1_idx` (`alumni_id`),
  CONSTRAINT `fk_t_alumni_data_t_alumni1` FOREIGN KEY (`alumni_id`) REFERENCES `dimx_alumni` (`alumni_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dimx_alumni_data` */

/*Table structure for table `dimx_alumni_pekerjaan` */

DROP TABLE IF EXISTS `dimx_alumni_pekerjaan`;

CREATE TABLE `dimx_alumni_pekerjaan` (
  `alumni_pekerjaan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nim` varchar(8) NOT NULL DEFAULT '',
  `tgl_start` date NOT NULL,
  `tgl_end` date DEFAULT NULL,
  `nama_perusahaan` varchar(200) NOT NULL DEFAULT '',
  `alamat_perusahaan` varchar(255) DEFAULT NULL,
  `bidang_perusahaan` varchar(255) DEFAULT NULL,
  `bidang_pekerjaan` varchar(100) NOT NULL DEFAULT '',
  `gaji` varchar(100) DEFAULT NULL,
  `alumni_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`alumni_pekerjaan_id`),
  KEY `fk_t_ALUMNI_PEKERJAAN_t_ALUMNI_DATA_idx` (`alumni_id`),
  CONSTRAINT `FK_dimx_alumni_pekerjaan` FOREIGN KEY (`alumni_id`) REFERENCES `dimx_alumni` (`alumni_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dimx_alumni_pekerjaan` */

/*Table structure for table `dimx_dim` */

DROP TABLE IF EXISTS `dimx_dim`;

CREATE TABLE `dimx_dim` (
  `dim_id` int(11) NOT NULL AUTO_INCREMENT,
  `nim` varchar(8) NOT NULL DEFAULT '',
  `no_usm` varchar(15) NOT NULL DEFAULT '',
  `jalur` varchar(20) DEFAULT NULL,
  `user_name` varchar(10) DEFAULT NULL,
  `kbk_id` varchar(20) DEFAULT NULL,
  `ref_kbk_id` int(11) DEFAULT NULL,
  `kpt_prodi` varchar(10) DEFAULT NULL,
  `id_kur` int(4) NOT NULL DEFAULT '0',
  `tahun_kurikulum_id` int(11) DEFAULT NULL,
  `nama` varchar(50) NOT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `gol_darah` char(2) DEFAULT NULL,
  `golongan_darah_id` int(11) DEFAULT NULL,
  `jenis_kelamin` char(1) DEFAULT NULL,
  `jenis_kelamin_id` int(11) DEFAULT NULL,
  `agama` varchar(30) DEFAULT NULL,
  `agama_id` int(11) DEFAULT NULL,
  `alamat` text,
  `kabupaten` varchar(50) DEFAULT NULL,
  `kode_pos` varchar(5) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telepon` varchar(50) DEFAULT NULL,
  `hp` varchar(50) DEFAULT NULL,
  `hp2` varchar(50) DEFAULT NULL,
  `no_ijazah_sma` varchar(100) DEFAULT NULL,
  `nama_sma` varchar(50) DEFAULT NULL,
  `asal_sekolah_id` int(11) DEFAULT NULL,
  `alamat_sma` text,
  `kabupaten_sma` varchar(100) DEFAULT NULL,
  `telepon_sma` varchar(50) DEFAULT NULL,
  `kodepos_sma` varchar(8) DEFAULT NULL,
  `thn_masuk` int(11) DEFAULT NULL,
  `status_akhir` varchar(50) DEFAULT 'Aktif',
  `nama_ayah` varchar(50) DEFAULT NULL,
  `nama_ibu` varchar(50) DEFAULT NULL,
  `no_hp_ayah` varchar(50) DEFAULT NULL,
  `no_hp_ibu` varchar(50) DEFAULT NULL,
  `alamat_orangtua` text,
  `pekerjaan_ayah` varchar(100) DEFAULT NULL,
  `pekerjaan_ayah_id` int(11) DEFAULT NULL,
  `keterangan_pekerjaan_ayah` text,
  `penghasilan_ayah` varchar(50) DEFAULT NULL,
  `penghasilan_ayah_id` int(11) DEFAULT NULL,
  `pekerjaan_ibu` varchar(100) DEFAULT NULL,
  `pekerjaan_ibu_id` int(11) DEFAULT NULL,
  `keterangan_pekerjaan_ibu` text,
  `penghasilan_ibu` varchar(50) DEFAULT NULL,
  `penghasilan_ibu_id` int(11) DEFAULT NULL,
  `nama_wali` varchar(50) DEFAULT NULL,
  `pekerjaan_wali` varchar(50) DEFAULT NULL,
  `pekerjaan_wali_id` int(11) DEFAULT NULL,
  `keterangan_pekerjaan_wali` text,
  `penghasilan_wali` varchar(50) DEFAULT NULL,
  `penghasilan_wali_id` int(11) DEFAULT NULL,
  `alamat_wali` text,
  `telepon_wali` varchar(20) DEFAULT NULL,
  `no_hp_wali` varchar(50) DEFAULT NULL,
  `pendapatan` varchar(50) DEFAULT NULL,
  `ipk` float DEFAULT '0',
  `anak_ke` tinyint(4) DEFAULT NULL,
  `dari_jlh_anak` tinyint(4) DEFAULT NULL,
  `jumlah_tanggungan` tinyint(4) DEFAULT NULL,
  `nilai_usm` float DEFAULT NULL,
  `score_iq` tinyint(4) DEFAULT NULL,
  `rekomendasi_psikotest` varchar(4) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `kode_foto` varchar(100) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`dim_id`),
  UNIQUE KEY `NIM_UNIQUE` (`nim`),
  KEY `NIM` (`nim`),
  KEY `FK_dimx_dim_thn_krkm` (`tahun_kurikulum_id`),
  KEY `FK_dimx_dim_user` (`user_id`),
  KEY `FK_dimx_dim_ref_kbk` (`ref_kbk_id`),
  KEY `FK_dimx_dim_asal_sekolah` (`asal_sekolah_id`),
  KEY `FK_dimx_dim_golongan_darah` (`golongan_darah_id`),
  KEY `FK_dimx_dim_jenis_kelamin` (`jenis_kelamin_id`),
  KEY `FK_dimx_dim_agama` (`agama_id`),
  KEY `FK_dimx_dim_pekerjaan_ayah` (`pekerjaan_ayah_id`),
  KEY `FK_dimx_dim_penghasilan_ayah` (`penghasilan_ayah_id`),
  KEY `FK_dimx_dim_pekerjaan_ibu` (`pekerjaan_ibu_id`),
  KEY `FK_dimx_dim_penghasilan_ibu` (`penghasilan_ibu_id`),
  KEY `FK_dimx_dim_pekerjaan_wali` (`pekerjaan_wali_id`),
  KEY `FK_dimx_dim_penghasilan_wali_id` (`penghasilan_wali_id`),
  CONSTRAINT `FK_dimx_dim_agama` FOREIGN KEY (`agama_id`) REFERENCES `mref_r_agama` (`agama_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_asal_sekolah` FOREIGN KEY (`asal_sekolah_id`) REFERENCES `mref_r_asal_sekolah` (`asal_sekolah_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_golongan_darah` FOREIGN KEY (`golongan_darah_id`) REFERENCES `mref_r_golongan_darah` (`golongan_darah_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_jenis_kelamin` FOREIGN KEY (`jenis_kelamin_id`) REFERENCES `mref_r_jenis_kelamin` (`jenis_kelamin_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_pekerjaan_ayah` FOREIGN KEY (`pekerjaan_ayah_id`) REFERENCES `mref_r_pekerjaan` (`pekerjaan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_pekerjaan_ibu` FOREIGN KEY (`pekerjaan_ibu_id`) REFERENCES `mref_r_pekerjaan` (`pekerjaan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_pekerjaan_wali` FOREIGN KEY (`pekerjaan_wali_id`) REFERENCES `mref_r_pekerjaan` (`pekerjaan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_penghasilan_ayah` FOREIGN KEY (`penghasilan_ayah_id`) REFERENCES `mref_r_penghasilan` (`penghasilan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_penghasilan_ibu` FOREIGN KEY (`penghasilan_ibu_id`) REFERENCES `mref_r_penghasilan` (`penghasilan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_penghasilan_wali_id` FOREIGN KEY (`penghasilan_wali_id`) REFERENCES `mref_r_penghasilan` (`penghasilan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_ref_kbk` FOREIGN KEY (`ref_kbk_id`) REFERENCES `inst_prodi` (`ref_kbk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_thn_krkm` FOREIGN KEY (`tahun_kurikulum_id`) REFERENCES `krkm_r_tahun_kurikulum` (`tahun_kurikulum_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_user` FOREIGN KEY (`user_id`) REFERENCES `sysx_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `dimx_dim` */

insert  into `dimx_dim`(`dim_id`,`nim`,`no_usm`,`jalur`,`user_name`,`kbk_id`,`ref_kbk_id`,`kpt_prodi`,`id_kur`,`tahun_kurikulum_id`,`nama`,`tgl_lahir`,`tempat_lahir`,`gol_darah`,`golongan_darah_id`,`jenis_kelamin`,`jenis_kelamin_id`,`agama`,`agama_id`,`alamat`,`kabupaten`,`kode_pos`,`email`,`telepon`,`hp`,`hp2`,`no_ijazah_sma`,`nama_sma`,`asal_sekolah_id`,`alamat_sma`,`kabupaten_sma`,`telepon_sma`,`kodepos_sma`,`thn_masuk`,`status_akhir`,`nama_ayah`,`nama_ibu`,`no_hp_ayah`,`no_hp_ibu`,`alamat_orangtua`,`pekerjaan_ayah`,`pekerjaan_ayah_id`,`keterangan_pekerjaan_ayah`,`penghasilan_ayah`,`penghasilan_ayah_id`,`pekerjaan_ibu`,`pekerjaan_ibu_id`,`keterangan_pekerjaan_ibu`,`penghasilan_ibu`,`penghasilan_ibu_id`,`nama_wali`,`pekerjaan_wali`,`pekerjaan_wali_id`,`keterangan_pekerjaan_wali`,`penghasilan_wali`,`penghasilan_wali_id`,`alamat_wali`,`telepon_wali`,`no_hp_wali`,`pendapatan`,`ipk`,`anak_ke`,`dari_jlh_anak`,`jumlah_tanggungan`,`nilai_usm`,`score_iq`,`rekomendasi_psikotest`,`foto`,`kode_foto`,`user_id`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`updated_at`,`created_by`,`updated_by`) values (5,'11316021','',NULL,'if316021',NULL,1,NULL,0,NULL,'Nathan Mora Tua Nainggolan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2016,'Aktif',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3098,0,NULL,NULL,NULL,NULL,NULL,NULL),(6,'11316005','',NULL,'11316021',NULL,1,NULL,0,NULL,'Mei Romauli Sagala',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2016,'Aktif',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(7,'11316025','',NULL,'11316021',NULL,1,NULL,0,NULL,'Eirene Hutasoit',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2016,'Aktif',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(8,'11S15027','',NULL,'11316021',NULL,3,NULL,0,NULL,'Jonathan Silalahi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2016,'Aktif',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3103,0,NULL,NULL,NULL,NULL,NULL,NULL),(9,'11S15024','',NULL,'11316021',NULL,3,NULL,0,NULL,'Ricky Tampubolon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2015,'Aktif',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(10,'11S15006','',NULL,'11316021',NULL,3,NULL,0,NULL,'Khairani Malau',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2015,'Aktif',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(11,'11416004','',NULL,'11416004',NULL,1,NULL,0,NULL,'Andre Sihombing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Aktif',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3107,0,NULL,NULL,NULL,NULL,NULL,NULL),(12,'11111111','',NULL,'11111111',NULL,1,NULL,0,NULL,'Alexander',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Aktif',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3108,0,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `dimx_dim_pmb` */

DROP TABLE IF EXISTS `dimx_dim_pmb`;

CREATE TABLE `dimx_dim_pmb` (
  `dim_pmb_id` int(11) NOT NULL AUTO_INCREMENT,
  `no_umpid` varchar(9) NOT NULL DEFAULT '',
  `tahun_ujian` int(11) NOT NULL DEFAULT '2004',
  `nim` varchar(8) DEFAULT NULL,
  `user_name` varchar(7) DEFAULT NULL,
  `kbk_id` varchar(20) DEFAULT 'N/A',
  `nama` varchar(50) NOT NULL DEFAULT '',
  `tgl_lahir` date DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `gol_darah` char(2) DEFAULT NULL,
  `jenis_kelamin` char(1) DEFAULT NULL,
  `agama` varchar(30) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `kabupaten` varchar(50) DEFAULT NULL,
  `kode_pos` varchar(5) DEFAULT NULL,
  `telepon` varchar(15) DEFAULT NULL,
  `hp` varchar(20) DEFAULT NULL,
  `nama_sma` varchar(50) DEFAULT NULL,
  `no_ijazah_sma` varchar(100) DEFAULT NULL,
  `alamat_sma` varchar(100) DEFAULT NULL,
  `kabupaten_sma` varchar(100) DEFAULT NULL,
  `kodepos_sma` varchar(8) DEFAULT NULL,
  `telepon_sma` varchar(50) DEFAULT NULL,
  `thn_masuk` int(11) DEFAULT NULL,
  `status_akhir` char(1) DEFAULT NULL,
  `nama_ayah` varchar(50) DEFAULT NULL,
  `nama_ibu` varchar(50) DEFAULT NULL,
  `pekerjaan_ayah` varchar(100) DEFAULT NULL,
  `pekerjaan_ibu` varchar(100) DEFAULT NULL,
  `nama_wali` varchar(50) DEFAULT NULL,
  `pekerjaan_wali` varchar(50) DEFAULT NULL,
  `alamat_wali` varchar(200) DEFAULT NULL,
  `telepon_wali` varchar(20) DEFAULT NULL,
  `pendapatan` varchar(50) DEFAULT NULL,
  `ipk` float DEFAULT NULL,
  `foto` longblob,
  `tgl_daftar_s` date DEFAULT NULL,
  `tgl_daftar_e` date DEFAULT NULL,
  `status_daftar` varchar(10) DEFAULT NULL,
  `n_pembangunan` int(11) DEFAULT NULL,
  `jumlah_pembangunan` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`dim_pmb_id`),
  KEY `fk_t_dim_pmb_t_dim1_idx` (`dim_id`),
  CONSTRAINT `fk_t_dim_pmb_t_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dimx_dim_pmb` */

/*Table structure for table `dimx_dim_pmb_daftar` */

DROP TABLE IF EXISTS `dimx_dim_pmb_daftar`;

CREATE TABLE `dimx_dim_pmb_daftar` (
  `dim_pmb_daftar_id` int(11) NOT NULL AUTO_INCREMENT,
  `no_umpid` varchar(9) NOT NULL DEFAULT '',
  `nim` varchar(8) DEFAULT NULL,
  `tgl_daftar` datetime DEFAULT NULL,
  `biaya_bayar` double DEFAULT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`dim_pmb_daftar_id`),
  UNIQUE KEY `no_umpid_UNIQUE` (`no_umpid`),
  KEY `fk_t_dim_pmb_daftar_t_dim1_idx` (`dim_id`),
  CONSTRAINT `fk_t_dim_pmb_daftar_t_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dimx_dim_pmb_daftar` */

/*Table structure for table `dimx_dim_trnon_lulus` */

DROP TABLE IF EXISTS `dimx_dim_trnon_lulus`;

CREATE TABLE `dimx_dim_trnon_lulus` (
  `dim_trnon_lulus_id` int(11) NOT NULL AUTO_INCREMENT,
  `nim` varchar(8) NOT NULL DEFAULT '',
  `sem_ta` tinyint(4) NOT NULL DEFAULT '0',
  `ta` int(11) NOT NULL DEFAULT '0',
  `status_akhir` varchar(50) NOT NULL DEFAULT 'Mengundurkan Diri',
  `periode_start` date DEFAULT NULL,
  `periode_end` date DEFAULT NULL,
  `no_sk` varchar(255) DEFAULT NULL,
  `tanggal_sk` date DEFAULT NULL,
  `keterangan` text,
  `dim_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`dim_trnon_lulus_id`),
  KEY `fk_t_dim_trnon_lulus_t_dim1_idx` (`dim_id`),
  CONSTRAINT `fk_t_dim_trnon_lulus_t_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dimx_dim_trnon_lulus` */

/*Table structure for table `dimx_dim_update` */

DROP TABLE IF EXISTS `dimx_dim_update`;

CREATE TABLE `dimx_dim_update` (
  `dim_id` int(11) NOT NULL AUTO_INCREMENT,
  `nim` varchar(8) DEFAULT NULL,
  `no_usm` varchar(15) DEFAULT NULL,
  `jalur` varchar(20) DEFAULT NULL,
  `user_name` varchar(10) DEFAULT NULL,
  `kbk_id` varchar(20) DEFAULT NULL,
  `ref_kbk_id` int(11) DEFAULT NULL,
  `kpt_prodi` varchar(10) DEFAULT NULL,
  `id_kur` int(4) DEFAULT '0',
  `tahun_kurikulum_id` int(11) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `gol_darah` char(2) DEFAULT NULL,
  `golongan_darah_id` int(11) DEFAULT NULL,
  `jenis_kelamin` char(1) DEFAULT NULL,
  `jenis_kelamin_id` int(11) DEFAULT NULL,
  `agama` varchar(30) DEFAULT NULL,
  `agama_id` int(11) DEFAULT NULL,
  `alamat` text,
  `kabupaten` varchar(50) DEFAULT NULL,
  `kode_pos` varchar(5) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telepon` varchar(50) DEFAULT NULL,
  `hp` varchar(50) DEFAULT NULL,
  `hp2` varchar(50) DEFAULT NULL,
  `no_ijazah_sma` varchar(100) DEFAULT NULL,
  `nama_sma` varchar(50) DEFAULT NULL,
  `asal_sekolah_id` int(11) DEFAULT NULL,
  `alamat_sma` text,
  `kabupaten_sma` varchar(100) DEFAULT NULL,
  `telepon_sma` varchar(50) DEFAULT NULL,
  `kodepos_sma` varchar(8) DEFAULT NULL,
  `thn_masuk` int(11) DEFAULT NULL,
  `status_akhir` varchar(50) DEFAULT NULL,
  `nama_ayah` varchar(50) DEFAULT NULL,
  `nama_ibu` varchar(50) DEFAULT NULL,
  `no_hp_ayah` varchar(50) DEFAULT NULL,
  `no_hp_ibu` varchar(50) DEFAULT NULL,
  `alamat_orangtua` text,
  `pekerjaan_ayah` varchar(100) DEFAULT NULL,
  `pekerjaan_ayah_id` int(11) DEFAULT NULL,
  `keterangan_pekerjaan_ayah` text,
  `penghasilan_ayah` varchar(50) DEFAULT NULL,
  `penghasilan_ayah_id` int(11) DEFAULT NULL,
  `pekerjaan_ibu` varchar(100) DEFAULT NULL,
  `pekerjaan_ibu_id` int(11) DEFAULT NULL,
  `keterangan_pekerjaan_ibu` text,
  `penghasilan_ibu` varchar(50) DEFAULT NULL,
  `penghasilan_ibu_id` int(11) DEFAULT NULL,
  `nama_wali` varchar(50) DEFAULT NULL,
  `pekerjaan_wali` varchar(50) DEFAULT NULL,
  `pekerjaan_wali_id` int(11) DEFAULT NULL,
  `keterangan_pekerjaan_wali` text,
  `penghasilan_wali` varchar(50) DEFAULT NULL,
  `penghasilan_wali_id` int(11) DEFAULT NULL,
  `alamat_wali` text,
  `telepon_wali` varchar(20) DEFAULT NULL,
  `no_hp_wali` varchar(50) DEFAULT NULL,
  `pendapatan` varchar(50) DEFAULT NULL,
  `ipk` float DEFAULT '0',
  `anak_ke` tinyint(4) DEFAULT NULL,
  `dari_jlh_anak` tinyint(4) DEFAULT NULL,
  `jumlah_tanggungan` tinyint(4) DEFAULT NULL,
  `nilai_usm` float DEFAULT NULL,
  `score_iq` tinyint(4) DEFAULT NULL,
  `rekomendasi_psikotest` varchar(4) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `kode_foto` varchar(100) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`dim_id`),
  UNIQUE KEY `NIM_UNIQUE` (`nim`),
  KEY `NIM` (`nim`),
  KEY `FK_dimx_dim_thn_krkm` (`tahun_kurikulum_id`),
  KEY `FK_dimx_dim_user` (`user_id`),
  KEY `FK_dimx_dim_ref_kbk` (`ref_kbk_id`),
  KEY `FK_dimx_dim_asal_sekolah` (`asal_sekolah_id`),
  KEY `FK_dimx_dim_golongan_darah` (`golongan_darah_id`),
  KEY `FK_dimx_dim_jenis_kelamin` (`jenis_kelamin_id`),
  KEY `FK_dimx_dim_agama` (`agama_id`),
  KEY `FK_dimx_dim_pekerjaan_ayah` (`pekerjaan_ayah_id`),
  KEY `FK_dimx_dim_penghasilan_ayah` (`penghasilan_ayah_id`),
  KEY `FK_dimx_dim_pekerjaan_ibu` (`pekerjaan_ibu_id`),
  KEY `FK_dimx_dim_penghasilan_ibu` (`penghasilan_ibu_id`),
  KEY `FK_dimx_dim_pekerjaan_wali` (`pekerjaan_wali_id`),
  KEY `FK_dimx_dim_penghasilan_wali_id` (`penghasilan_wali_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dimx_dim_update` */

/*Table structure for table `dimx_histori_prodi` */

DROP TABLE IF EXISTS `dimx_histori_prodi`;

CREATE TABLE `dimx_histori_prodi` (
  `histori_prodi_id` int(11) NOT NULL AUTO_INCREMENT,
  `dim_id` int(11) DEFAULT NULL,
  `nim_old` varchar(8) DEFAULT NULL,
  `ref_kbk_id_old` int(11) DEFAULT NULL,
  `username_old` varchar(20) DEFAULT NULL,
  `email_old` varchar(30) DEFAULT NULL,
  `tahun_pindah` varchar(4) NOT NULL,
  `sem_ta_pindah` int(11) NOT NULL,
  `tgl_pindah` date NOT NULL,
  `nim_new` varchar(8) NOT NULL,
  `ref_kbk_id_new` int(11) NOT NULL,
  `username_new` varchar(20) NOT NULL,
  `email_new` varchar(30) NOT NULL,
  `kelas_new` int(11) DEFAULT NULL,
  `wali_new` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`histori_prodi_id`),
  KEY `FK_dimx_histori_prodi` (`dim_id`),
  CONSTRAINT `FK_dimx_histori_prodi` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dimx_histori_prodi` */

/*Table structure for table `hrdx_dosen` */

DROP TABLE IF EXISTS `hrdx_dosen`;

CREATE TABLE `hrdx_dosen` (
  `dosen_id` int(11) NOT NULL AUTO_INCREMENT,
  `pegawai_id` int(11) DEFAULT NULL,
  `nidn` varchar(10) DEFAULT NULL,
  `prodi_id` int(11) DEFAULT NULL,
  `golongan_kepangkatan_id` int(11) DEFAULT NULL,
  `jabatan_akademik_id` int(11) DEFAULT NULL,
  `status_ikatan_kerja_dosen_id` int(11) DEFAULT NULL,
  `gbk_1` int(11) DEFAULT NULL,
  `gbk_2` int(11) DEFAULT NULL,
  `aktif_start` date DEFAULT '0000-00-00',
  `aktif_end` date DEFAULT '0000-00-00',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `temp_id_old` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`dosen_id`),
  KEY `FK_hrdx_dosen` (`golongan_kepangkatan_id`),
  KEY `FK_hrdx_dosen_jab` (`jabatan_akademik_id`),
  KEY `FK_hrdx_dosen_stts` (`status_ikatan_kerja_dosen_id`),
  KEY `FK_hrdx_dosen_gbk` (`gbk_1`),
  KEY `FK_hrdx_dosen_pegawai` (`pegawai_id`),
  KEY `FK_hrdx_dosen_gbk2` (`gbk_2`),
  KEY `FK_hrdx_dosen_prodi` (`prodi_id`),
  CONSTRAINT `FK_hrdx_dosen` FOREIGN KEY (`golongan_kepangkatan_id`) REFERENCES `mref_r_golongan_kepangkatan` (`golongan_kepangkatan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_dosen_gbk` FOREIGN KEY (`gbk_1`) REFERENCES `mref_r_gbk` (`gbk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_dosen_gbk2` FOREIGN KEY (`gbk_2`) REFERENCES `mref_r_gbk` (`gbk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_dosen_jab` FOREIGN KEY (`jabatan_akademik_id`) REFERENCES `mref_r_jabatan_akademik` (`jabatan_akademik_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_dosen_prodi` FOREIGN KEY (`prodi_id`) REFERENCES `inst_prodi` (`ref_kbk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_dosen_stts` FOREIGN KEY (`status_ikatan_kerja_dosen_id`) REFERENCES `mref_r_status_ikatan_kerja_dosen` (`status_ikatan_kerja_dosen_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `hrdx_dosen_ibfk_1` FOREIGN KEY (`pegawai_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hrdx_dosen` */

/*Table structure for table `hrdx_pegawai` */

DROP TABLE IF EXISTS `hrdx_pegawai`;

CREATE TABLE `hrdx_pegawai` (
  `pegawai_id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_old_id` varchar(20) DEFAULT NULL,
  `nama` varchar(135) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `nip` varchar(45) DEFAULT NULL,
  `kpt_no` varchar(10) DEFAULT NULL,
  `kbk_id` varchar(20) DEFAULT NULL,
  `ref_kbk_id` int(11) DEFAULT NULL,
  `alias` varchar(9) DEFAULT NULL,
  `posisi` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(60) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `agama_id` int(11) DEFAULT NULL,
  `jenis_kelamin_id` int(11) DEFAULT NULL,
  `golongan_darah_id` int(11) DEFAULT NULL,
  `hp` varchar(20) DEFAULT NULL,
  `telepon` varchar(45) DEFAULT NULL,
  `alamat` blob,
  `alamat_libur` varchar(100) DEFAULT NULL,
  `kecamatan` varchar(150) DEFAULT NULL,
  `kota` varchar(50) DEFAULT NULL,
  `kabupaten_id` int(11) DEFAULT NULL,
  `kode_pos` varchar(15) DEFAULT NULL,
  `no_ktp` varchar(255) DEFAULT NULL,
  `email` text,
  `ext_num` char(3) DEFAULT NULL,
  `study_area_1` varchar(50) DEFAULT NULL,
  `study_area_2` varchar(50) DEFAULT NULL,
  `jabatan` char(1) DEFAULT NULL,
  `jabatan_akademik_id` int(11) DEFAULT NULL,
  `gbk_1` int(11) DEFAULT NULL,
  `gbk_2` int(11) DEFAULT NULL,
  `status_ikatan_kerja_pegawai_id` int(11) DEFAULT NULL,
  `status_akhir` char(1) DEFAULT NULL,
  `status_aktif_pegawai_id` int(11) DEFAULT NULL,
  `tanggal_masuk` date DEFAULT '0000-00-00',
  `tanggal_keluar` date DEFAULT '0000-00-00',
  `nama_bapak` varchar(50) DEFAULT NULL,
  `nama_ibu` varchar(50) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `status_marital_id` int(11) DEFAULT NULL,
  `nama_p` varchar(50) DEFAULT NULL,
  `tgl_lahir_p` date DEFAULT NULL,
  `tmp_lahir_p` varchar(50) DEFAULT NULL,
  `pekerjaan_ortu` varchar(100) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`pegawai_id`),
  KEY `FK_hrdx_pegawai_JK` (`jenis_kelamin_id`),
  KEY `FK_hrdx_pegawai_agama` (`agama_id`),
  KEY `FK_hrdx_pegawai_golda` (`golongan_darah_id`),
  KEY `FK_hrdx_pegawai_kab` (`kabupaten_id`),
  KEY `FK_hrdx_pegawai_sts_aktf` (`status_aktif_pegawai_id`),
  KEY `FK_hrdx_pegawai_sts_iktn` (`status_ikatan_kerja_pegawai_id`),
  KEY `FK_hrdx_pegawai_sts_martl` (`status_marital_id`),
  KEY `FK_hrdx_pegawai_user` (`user_id`),
  KEY `FK_hrdx_pegawai_jabatan_akademik` (`jabatan_akademik_id`),
  KEY `FK_hrdx_pegawai_kbk` (`ref_kbk_id`),
  CONSTRAINT `FK_hrdx_pegawai_JK` FOREIGN KEY (`jenis_kelamin_id`) REFERENCES `mref_r_jenis_kelamin` (`jenis_kelamin_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_agama` FOREIGN KEY (`agama_id`) REFERENCES `mref_r_agama` (`agama_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_golda` FOREIGN KEY (`golongan_darah_id`) REFERENCES `mref_r_golongan_darah` (`golongan_darah_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_jabatan_akademik` FOREIGN KEY (`jabatan_akademik_id`) REFERENCES `mref_r_jabatan_akademik` (`jabatan_akademik_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_kab` FOREIGN KEY (`kabupaten_id`) REFERENCES `mref_r_kabupaten` (`kabupaten_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_kbk` FOREIGN KEY (`ref_kbk_id`) REFERENCES `inst_prodi` (`ref_kbk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_sts_aktf` FOREIGN KEY (`status_aktif_pegawai_id`) REFERENCES `mref_r_status_aktif_pegawai` (`status_aktif_pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_sts_iktn` FOREIGN KEY (`status_ikatan_kerja_pegawai_id`) REFERENCES `mref_r_status_ikatan_kerja_pegawai` (`status_ikatan_kerja_pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_sts_martl` FOREIGN KEY (`status_marital_id`) REFERENCES `mref_r_status_marital` (`status_marital_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_user` FOREIGN KEY (`user_id`) REFERENCES `sysx_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=258 DEFAULT CHARSET=latin1;

/*Data for the table `hrdx_pegawai` */

insert  into `hrdx_pegawai`(`pegawai_id`,`profile_old_id`,`nama`,`user_name`,`nip`,`kpt_no`,`kbk_id`,`ref_kbk_id`,`alias`,`posisi`,`tempat_lahir`,`tgl_lahir`,`agama_id`,`jenis_kelamin_id`,`golongan_darah_id`,`hp`,`telepon`,`alamat`,`alamat_libur`,`kecamatan`,`kota`,`kabupaten_id`,`kode_pos`,`no_ktp`,`email`,`ext_num`,`study_area_1`,`study_area_2`,`jabatan`,`jabatan_akademik_id`,`gbk_1`,`gbk_2`,`status_ikatan_kerja_pegawai_id`,`status_akhir`,`status_aktif_pegawai_id`,`tanggal_masuk`,`tanggal_keluar`,`nama_bapak`,`nama_ibu`,`status`,`status_marital_id`,`nama_p`,`tgl_lahir_p`,`tmp_lahir_p`,`pekerjaan_ortu`,`user_id`,`deleted`,`deleted_at`,`deleted_by`,`created_by`,`created_at`,`updated_by`,`updated_at`) values (1,'g6gq1dg98c8jvljigm71','Tennov Simanjuntak, S.T, M.Sc','tennov','0308010002','110117601','tfc83kdqpq6y42i3pyqa',2,'THS','Dosen','Pematang Siantar','1976-11-10',1,1,1,'081375860514','0632-331234','Jl. Medan Km. 14,5 RT/RW : 001/001 Kel.Sumber Jaya Kec.Siantar Martoba Kota Pematang Siantar\r\n','','Siantar Martoba','Pematang Siantar',2,'21137','1272061011760000','tennov@del.ac.id','225','DAME_COMBINE','','B',2,1,NULL,4,'A',1,'2001-08-13',NULL,'H. Simanjuntak(+)','S. Manurung','M',1,'','0000-00-00','','',3101,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 14:18:59'),(2,'g6gq1dg92c8jvljigm71','Yaya Setiadi, S.Si., MT ','yaya','0308010003','121097601','yxooba992hd2w78lyuc7',1,'YYS','Dosen','Kuningan Jawa Barat','1976-09-21',3,1,2,'08126324284','0632-331234','Jl. Batu Permata Raya RT/RW 001/001 Kel. Bah Kapul Kec. Siantar Sitalasari Kota. P.Siantar\r\n','Ds. Babakan Mulya Kec. Jalaksana Rt 13/03 Jawa Barat','Siantar Martoba','Kuningan',2,'21139','1272072109760005','yaya@del.ac.id, yaya.s@lycos.com','228','THIN','','B',2,2,NULL,4,'A',1,'2001-08-13',NULL,'Endo Suwenda','Inah Dainah','M',1,'','0000-00-00','','Pensiunan PNS',1382,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 14:21:21'),(3,'g6gq1dg98c8jvlj3gm71','Candra Taufik','candra','','115027701','yxooba992hd2w78lyuc7',1,'CTF','Dosen','Bandung','1977-02-15',3,1,4,'08126431174','0265321292',NULL,'JL. CONDONG 38 KP. NYOMPET 04/03 SETIANAGARA CIBEUREUM',NULL,'TASIKMALAYA',NULL,'46196','00151/2028/04/TS/2003','ctaufik@lycos.com, candra@del.ac.id','224','PROSEDE','','B',2,3,NULL,NULL,'K',3,'2001-08-13','0000-00-00','Saeful Rohman','Yayah Djuariah','M',1,'','0000-00-00','','WIRASWASTA',1383,0,NULL,NULL,NULL,NULL,NULL,NULL),(4,'g6gq1dg98c8jvlji4m71','Musthofa Lutfi','mlutfi','','16','tfc83kdqpq6y42i3pyqa',2,'MLF','Dosen','Gresik','0000-00-00',3,1,1,'(0310 3941489','',NULL,'Jl. Sadang Sari 11 No. 18 Sekoloa Bandung 40134',NULL,'',NULL,'','1.05007e15','mlutfi@del.ac.id, musthofal@yahoo.com','','','','',NULL,NULL,NULL,NULL,'K',3,'2001-08-13','2005-08-01','Ali Affandi','Sajidah','M',1,'','0000-00-00','','Guru',1384,0,NULL,NULL,NULL,NULL,NULL,NULL),(5,'g6gq1dg98c8svljigm71','Kurnia Djaja','kurnia','','12','fiy7zfcz29vmkeclc6hi',3,'KDJ','Dosen','Bandung','1974-08-01',3,1,1,'081320704690','022-7314849',NULL,'Jl. Situsari II No 18 Buah Batu',NULL,'Bandung',NULL,'40265','1.0501e15','kurnia@del.ac.id, kurenia_djaja@msn.com','223','','','',NULL,NULL,NULL,NULL,'K',3,'2001-08-13','2005-08-01','E. Djaja','Euis Rumanah (Alm)','M',1,'','0000-00-00','','Wiraswasta',1385,0,NULL,NULL,NULL,NULL,NULL,NULL),(6,'g6gq1dg98c8jfljigm71','James Benjamin Callen Lewis','','','25','',NULL,'','Dosen','Newport South Wales UK.','0000-00-00',1,1,NULL,'144796763523','',NULL,'22 Park Ave Abergavenny, Monmouthshire, S.Walen UK',NULL,'',NULL,'','32.0371.1004/474.4/5404379','urdosen@yahoo.com','','','','',NULL,NULL,NULL,NULL,'K',3,'2001-08-01','2003-08-01','','Mrs. Lawton','S',2,'','0000-00-00','','Perawat',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(7,'g6gq1dg98c8jgljigm71','Dr. Arnaldo Marulitua Sinaga, ST., M.InfoTech','aldo','0308010001','115017701','yxooba992hd2w78lyuc7',1,'AMS','Dosen','Tarutung','1977-01-15',1,1,1,'081317745457','0632-331234','Kampus Politeknik Informatika Del Jl. Sisingamangaraja Sitoluama Laguboti\r\n','Perumnas Pagar Beringin No. 241 Pagar Batu Sipoholon','Laguboti','Tarutung',3,'22381','1212021501770001','arnaldo.s@lycos.com, aldo@del.ac.id','','DAME_COMBINE','PROSEDE','B',2,1,3,4,'A',1,'2001-08-13',NULL,'+ B. Sinaga ','R. Sianturi','M',1,'','0000-00-00','','-',1386,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 14:14:12'),(8,'g6gq1dg98c8jvljigm7c','Vidya Vrat Agarwal','vidyavrat','','21','fiy7zfcz29vmkeclc6hi',3,'VVA','Dosen','Roorke, India','1975-05-08',4,1,NULL,'','001-91-13454068',NULL,'Basi Kiratpur, District Biynox UP PIN-246731',NULL,'India',NULL,'','','vidyavrat@rediffmail..com','220','','','',NULL,NULL,NULL,NULL,'K',3,'2002-09-02','2004-09-02','Sh. Ved Prakash','Smt. Sudha','S',2,'','0000-00-00','','Wiraswasta',1387,0,NULL,NULL,NULL,NULL,NULL,NULL),(9,'g6gq1dg98c8jvljigm7a','Abhishek Chadha','abhi','','1','fiy7zfcz29vmkeclc6hi',3,'ABC','Dosen','India','1977-09-07',4,1,NULL,'0091 657 407945','',NULL,'MA-60 New housing Colony, Adityapur,jamshedpur,India',NULL,'',NULL,'','','abhi@del.ac.id, abhishek_chadha_2000@yahoo.com','','','','',NULL,NULL,NULL,NULL,'K',3,'2002-09-02','2004-09-02','Ashok Chadha','Renu Chadha','S',2,'','0000-00-00','','Wiraswasta',1388,0,NULL,NULL,NULL,NULL,NULL,NULL),(10,'g6gq1dg98c8jvljigm72','Graham Neil Hornby','graham','','9','yxooba992hd2w78lyuc7',1,'GNH','Dosen','Hull, York Shire Inggris','1975-03-08',NULL,1,NULL,'','OO1441482706877',NULL,'33 Kildale Close, E. Yorkshire, England HU 8 9 NW',NULL,'England',NULL,'','passport 022593462','leeds2836@yahoo.com','223','','','',NULL,NULL,NULL,NULL,'K',3,'2002-09-02','2004-09-02','Kenneth','Susan','S',2,'','0000-00-00','','Wiraswasta',1389,0,NULL,NULL,NULL,NULL,NULL,NULL),(11,'g6gq1dg98c8jvljigm7g','Ramot Lubis','lubis','','130047801','fiy7zfcz29vmkeclc6hi',3,'RML','Dosen','Balige','1978-04-30',NULL,1,NULL,'(021) 7761964','','','Pardede Onan Tobasa','','Balige',NULL,'','32.0371.1004/474.4/5404379','lubis@del.ac.id, motssl@yahoo.com','227','DISCONET','PROSEDE','B',2,4,3,NULL,'K',3,'2002-09-02','0000-00-00','Albert Lubis','Tiur Helena Simanjuntak','S',2,'','0000-00-00','','',1390,0,NULL,NULL,NULL,NULL,'root','2016-01-20 09:57:08'),(12,'g6gq1dg98c8jvljigm7y','Ondy Dharma Indra Sukma','ondy','','127057901','yxooba992hd2w78lyuc7',1,'ODT','Dosen','Medan','1979-05-27',NULL,1,4,'(061) 8458351','',NULL,'Melur Kompleks Puskesmas Perumnas Helvetia',NULL,'Medan',NULL,'','02.5012.270579.0002','ondy_dharma@yahoo.com','221','PROSEDE','','A',1,3,NULL,NULL,'K',3,'2002-09-02','0000-00-00','Dr. Robert Tambun','Anny E. Siregar','S',2,'','0000-00-00','','Dokter',1391,0,NULL,NULL,NULL,NULL,NULL,NULL),(13,'g6gq1dg98c8jvljigm7j','Johannes Harungguan , ST., MT','johannes','0309020008','116047301','fiy7zfcz29vmkeclc6hi',3,'JHS','Dosen','Jambi','1973-04-16',1,1,1,'08126363667','0632-331234','MESS Perwira Jl. Utama No.15 RT/RW: 003/005 Kel. Srengseng Sawah Kec. Jagakarsa Kotamadya Jakarta Selatan\r\n','Jl. Bandar Pulau Karam No 13 C','Jagakarsa','Padang',42,'12640','1050101604733000','runggu@yahoo.com','','DISCONET','','B',2,4,NULL,4,'T',6,'2002-09-01',NULL,'+ C.F. Sianipar','P.B. Pardede','M',1,'','0000-00-00','','',1392,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 14:36:46'),(14,'g6gq1dg98c8jvljigm7k','Albert Sagala, S.T, M.T','albert','0305120056','104027801','fiy7zfcz29vmkeclc6hi',3,'ABS','Dosen','Samosir','1978-02-04',1,1,4,'081361493343','022-70785568','Babakan Ciseureuh Blk 364 No.2 RT 004/RW 004 Kel. Karasak Kec. Astanaanyar Kota Bandung\r\n','Jl. Moh. Toha Gg. BBK Ciseureuh RT 04/04 No 03','Astana anyar','Bandung',20,'40243','1050240402773003','albert_sagala@yahoo.com','229','DISCONET','GAME','B',2,4,6,4,'A',6,'2012-05-01',NULL,'M.Sagala','R.Sihaloho','M',1,'','0000-00-00','','',1393,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:56:31'),(15,'g6gq1dg98c8jvljigm7l','Good Fried Panggabean, ST., MT','good','0309020007','125097301','fiy7zfcz29vmkeclc6hi',3,'GFP','Dosen','Barus','1973-09-25',1,1,4,'082167174848','0632-331234','Jl. Bahagia No. 01 RT/RW: 011/004 Desa/Kel: Kristen Kec: Siantar Selatan, Kota Pematang Siantar\r\n','Jl. Bahagia No. 1','Siantar Selatan','Pematang Siantar',2,'21124','1272042509730002','goodfp@yahoo.com','226','DISCONET','','B',2,4,NULL,4,'A',1,'2002-09-02',NULL,'+ Alberto Panggabean','Posma Hutabarat','M',1,'','0000-00-00','','Pensiunan Guru',1394,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 14:32:21'),(16,'g6gq1dg98c8jvljigm7e','Dr. Arlinta Christy Barus, ST., M.InfoTech','arlinta','0311020009','117027901','tfc83kdqpq6y42i3pyqa',2,'ACB','Dosen','Medan','1979-02-17',1,2,4,'081375660081','0632-331234','Jl. Tri Dharma No 26 Kampus USU P.Bulan Medan\r\n','Jl. Tri Dharma No. 26\nKampus USU','Medan Baru','Medan',14,'20156','1271175702790003','arlinta014@yahoo.com','','DAME_COMBINE','','B',2,1,NULL,4,'A',1,'2002-11-29',NULL,'Prof. Dr. Tonel Barus','Dra. Tini Sembiring M.Sc','M',1,'','0000-00-00','','Dosen',1395,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 14:39:09'),(17,'g6gq1dg98c8jvljigm7p','Maria Emmanuelly Siahaan','maria','','109087601','tfc83kdqpq6y42i3pyqa',2,'MES','Dosen','Medan','1976-08-09',NULL,2,2,'','061-8223726',NULL,'Jl. Gitar no. 15',NULL,'Medan',NULL,'20155','','maria@del.ac.id, maria_emmanuelly@yahoo.com','229','LANGUAGE','','B',2,5,NULL,NULL,'K',3,'2001-09-15','0000-00-00','','','M',1,'','0000-00-00','','',1396,0,NULL,NULL,NULL,NULL,NULL,NULL),(18,'kh6fuu22djtxxijibrod','Mauritz Panggabean','mauritz','','112038001','fiy7zfcz29vmkeclc6hi',3,'MHP','Dosen','Banjarmasin','1980-03-12',NULL,1,1,'','',NULL,'',NULL,'',NULL,'','02.5012.120380.0001','','','DISCONET','','A',1,4,NULL,NULL,'K',3,'2004-04-01','0000-00-00','','','S',2,'','0000-00-00','','',1397,0,NULL,NULL,NULL,NULL,NULL,NULL),(19,'g6gq1Eg98c8jvljigm7p','Imelda R. Simanjuntak, ST, M.Sc','imelda','0304020006','108127801','tfc83kdqpq6y42i3pyqa',2,'IMS','Dosen','Tarutung','1978-12-08',1,2,4,'082166864244','0632-331234','Jl. Mawar Raya No. 222  kel: Helvetia Tengah Kec.Medan Helvetia\r\n','Jl. Complex Mesjid No. 107','Medan Helvetia','Tarutung',14,'20124','1271204812780004','imel_rwaty@lycos.com, imelda@del.ac.id','219','DAME_COMBINE','','B',2,1,NULL,4,'K',3,'2002-04-19','2012-02-07','T. Simanjuntak','S. Silitonga','S',1,'','0000-00-00','','Pensiunan PNS',1398,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 14:30:18'),(20,'g6gq1dgS8c8jwljigm71','Anastasia L. Rajagukguk','anastasia','','3','tfc83kdqpq6y42i3pyqa',2,'ALR','Dosen','Medan','1978-08-28',NULL,2,2,'','',NULL,'Jl. KB 12',NULL,'Sidikalang',NULL,'22211','4689/1877/094/D/2001','anastasia@del.ac.id','219','','','',NULL,NULL,NULL,NULL,'K',3,'2002-02-19','2005-08-01','Drs. G. Rajagukguk','B. Silaen','S',2,'','0000-00-00','','Pensiunan TNI AD',1399,0,NULL,NULL,NULL,NULL,NULL,NULL),(21,'g6gqSdg98c8jqljigm71','Deni Parlindungan  Lumbantoruan , ST, M.Eng','toruan','0303030010','114017901','fiy7zfcz29vmkeclc6hi',3,'DPL','Dosen','Siborong-borong','1979-01-14',1,1,2,'081298005540','0632-331234','Jl. Makmur Desa/Kel. Pasar Siborongborong Kec. Siborongborong Kab. Tapanuli Utara\r\n','Jl. Makmur No. 45','Siborong - borong','Siborong-borong',4,'22474','1050061401793000','dtoruan@yahoo.com, toruan@telecom.ee.itb.ac.id','227','DISCONET','','B',2,4,NULL,4,'A',6,'2003-03-10',NULL,'Manumpak Lumbantoruan','Tiambun Siregar','M',1,'','0000-00-00','','Bertani',1400,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 14:41:04'),(22,'r0nbfxbuvpycsfevht7b','Mariana Simanjuntak, S.S., M.Sc','anna','0309010004','','',NULL,'ANA','Dosen','Sigumpar Toba Samosir','1974-07-20',1,2,4,'08126452731','0632-331234','Sigumpar  Kec. Sigumpar Kab. Toba Samosir\r\n','Sigumpar, Kec. Silaen Toba Samosir\n','Sigumpar','Toba Samosir',3,'22381','1212116007740053','lisbeth_anna@yahoo.com, anna@del.ac.id','','','','A',1,NULL,NULL,4,'A',1,'2001-09-15',NULL,'Ganda Simanjuntak','T. Siagian','S',2,'','0000-00-00','','Wiraswasta',1401,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 14:24:25'),(23,'i9fkizsiomay64fp7wh3','Imelda Sity Nurbaya Pasaribu','','','','',NULL,'','Pustakawan','','0000-00-00',NULL,2,NULL,'','',NULL,'',NULL,'',NULL,'','','','','','','',NULL,NULL,NULL,NULL,'K',3,'2003-09-15','0000-00-00','','','S',2,'','0000-00-00','','',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(24,'24lwc5w0mhjx8czlqpkw','Rudolf Bernard Ginting','','','','fiy7zfcz29vmkeclc6hi',3,'','P.O. Keuangan','Batukarang','1973-12-01',NULL,1,NULL,'08126452832','',NULL,'Jl. Prof. H.M. Yamin Gg. Kitab No. 23 \n',NULL,'Medan',NULL,'20233','02.5020.140873.0001','rudolf_begin73@yahoo.com.sg, rudolf@del.ac.id','110','','','',NULL,NULL,NULL,NULL,'K',3,'2001-09-15','0000-00-00','D. Ginting, SH','S. Bangun','S',2,'','0000-00-00','','PNS',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(25,'ldinnzzbq1je1ztyprnh','Tiurma Lumban Gaol, SP., M.P','tiur','0309010005','108037601','yxooba992hd2w78lyuc7',1,'TLG','Pustakawan,Dosen','Merbau Selatan','1976-03-08',1,2,1,'081362778555','0632-331234','Pasar Silimbat Desa/Kel. Situatua Kec.Sigumpar Kab.Toba Samosir\r\n','PTPN III Perkebunan Merbau Selatan Labuhan Batu \n','Sigumpar','Rantau Prapat',3,'22381','1212114803760001','tiur@del.ac.id, tiurlg@yahoo.com','','LANGUAGE','','A',1,5,NULL,4,'A',1,'2001-09-15',NULL,'B. Lumban Gaol','L. Hutagalung','M',1,'','0000-00-00','','Karyawan',1402,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 14:26:24'),(26,'2sp34rh7q0u3ezmk4p8g','Yusniar Sijabat','yusniar','','','yxooba992hd2w78lyuc7',1,'','Inventaris/Pengadaan Barang','Medan','1970-11-27',NULL,2,2,'','061-7324153',NULL,'Jl. Enggang VI No. 494 \n',NULL,'Medan',NULL,'','','yusniar_mangisar@yahoo.com, yusniar@del.ac.id','','','','',NULL,NULL,NULL,NULL,'K',3,'2001-10-01','0000-00-00','Dj. Sijabat (Alm)','P. Nababan','S',2,'','0000-00-00','','Purn. ABRI',1403,0,NULL,NULL,NULL,NULL,NULL,NULL),(27,'ma82jkwegspzihs332qw','Moppo Tua Pangaribuan','','','','',NULL,'','Teknisi','Sitoluama','1977-12-03',NULL,1,NULL,'','',NULL,'Sitoluama\n',NULL,'',NULL,'','','','','','','',NULL,NULL,NULL,NULL,'K',3,'2001-11-01','0000-00-00','','','S',2,'','0000-00-00','','',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(28,'cd89jj486bnd80mksbkv','Oberlin Pangaribuan','','','','yxooba992hd2w78lyuc7',1,'','Teknisi','Sitoluama','1982-01-17',NULL,1,NULL,'','',NULL,'Sitoluama',NULL,'',NULL,'','','','','','','',NULL,NULL,NULL,NULL,'A',1,'2002-09-01','0000-00-00','','','S',2,'','0000-00-00','','',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(29,'pwzbd3ev5euj06hcbo03','Patar Manalu','patar','','','',NULL,'','Laboran','','0000-00-00',NULL,1,NULL,'','',NULL,'',NULL,'',NULL,'','','','','','','',NULL,NULL,NULL,NULL,'K',3,'0000-00-00','0000-00-00','','','S',2,'','0000-00-00','','',1404,0,NULL,NULL,NULL,NULL,NULL,NULL),(30,'nkghfh3p9lxfgomazpl8','Rahul Kaushik','rahul','','14','tfc83kdqpq6y42i3pyqa',2,'RHK','Dosen','India','1980-03-04',4,1,NULL,'','',NULL,'B-5/47-C Safdarjung Enclave',NULL,'New Delhi',NULL,'','','ralz_r@yahoo.com, rahul@del.ac.id','','','','',NULL,NULL,NULL,NULL,'K',3,'2003-08-01','2005-08-01','R.K Kaushik','Veena Kaushik','S',2,'','0000-00-00','','Advocate',1405,0,NULL,NULL,NULL,NULL,NULL,NULL),(31,'2gpdij1zp2796xefa34y','Prof. Dr. Ir. Saswinadi Sasmojo, M.Sc, Ph.D','saswinadi','030304H001','114103701','yxooba992hd2w78lyuc7',1,'SAS','Dosen','Pati','1937-10-14',3,1,NULL,'','','Jl.Sangkuriang No. 38 Kompleks Perumahan Dosen ITB  Bandung Jabar\r\n','','Coblong','',20,'40131','','','','THIN','SOBAT','E',5,2,7,2,'A',1,'2008-02-01',NULL,'','','M',1,'','0000-00-00','','',1406,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 10:58:52'),(32,'pwy47xwviixjlkouww65','Imelda Doharta Aritonang','doharta','','','',NULL,'IDA','dosen','Sibolga','1983-01-18',NULL,2,NULL,'081362415021','',NULL,'',NULL,'',NULL,'','','','','','','',NULL,NULL,NULL,NULL,'K',3,'2004-09-06','0000-00-00','','','S',2,'','0000-00-00','','',1407,0,NULL,NULL,NULL,NULL,NULL,NULL),(33,'7hhtgcb51fcmagdh7qfi','Gandhi Maruli Tua Manalu','gandhi','','118078201','tfc83kdqpq6y42i3pyqa',2,'GMM','Dosen','Tarutung','1982-07-18',NULL,1,2,'+6281311181032','-',NULL,'Jl. Cisadane II No 112 Depok Timur',NULL,'Depok',NULL,'16417','','gandhi.mtm@gmail.com','221','PROSEDE','THIN','A',1,3,2,NULL,'K',3,'2005-05-02','0000-00-00','','','S',2,'','0000-00-00','','',1408,0,NULL,NULL,NULL,NULL,NULL,NULL),(34,'x5z7iclp2xuf350fmzsh','Bonar Lumban Tobing','','030111H004','118125101','tfc83kdqpq6y42i3pyqa',2,'BLT','Dosen','Tarutung','1951-12-18',NULL,1,NULL,'','',NULL,'',NULL,'',NULL,'','18121951','','','','','A',1,NULL,NULL,NULL,'A',1,'0000-00-00','0000-00-00','','','S',2,'','0000-00-00','','',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(35,'eymxhiscq12v899d5iak','Ir. Partumpuan Naiborhu, M.M','','','108024001','yxooba992hd2w78lyuc7',1,'PN','Dosen','Silaen','1940-02-08',1,1,NULL,'08126556763','','','','','',NULL,'','250030802400001','','','','','A',1,NULL,NULL,4,'A',1,'0000-00-00','0000-00-00','','','M',1,'','0000-00-00','','',1936,0,NULL,NULL,NULL,NULL,'cori','2015-12-04 14:24:34'),(36,'18o13y7cox8eiyat7g69','Rentauli Mariah Silalahi, S.Pd, M.Ed','rentauli','0306050011','121018301','tfc83kdqpq6y42i3pyqa',2,'RMS','Dosen','Pematang Siantar','1983-01-21',2,2,NULL,'081362049752','0632-331234','Jl. Sadum No. 71 RT/RW : 001/005 Kel.Bane Kec. Siantar Utara Kota P.Siantar\r\n','','Siantar Utara','',2,'21142','1272036110183000','','','LANGUAGE','','A',1,5,NULL,4,'A',1,'2005-06-01',NULL,'','','M',1,'','0000-00-00','','',1409,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 14:43:37'),(37,'uy0cj9qp556fx8uz9cgp','Geoffrey Moss','gmoss','','','tfc83kdqpq6y42i3pyqa',2,'GMS','Dosen','Singapure','1948-11-28',NULL,1,NULL,'','061-103588864',NULL,'450 Queensberry St. North Melbournes Victoria 3051',NULL,'',NULL,'','','','','','','',NULL,NULL,NULL,NULL,'K',3,'2005-09-01','2006-09-01','','','M',1,'','0000-00-00','','',1410,0,NULL,NULL,NULL,NULL,NULL,NULL),(38,'w0dtx9mgp0bkzl6a1hsb','Henry Edison Sitorus','henry','','123048001','fiy7zfcz29vmkeclc6hi',3,'HES','Dosen','Medan','1980-04-23',NULL,1,1,'0811818512','0618360715',NULL,'Jl. Vanili Raya 94 Simalingkar',NULL,'Medan',NULL,'20141','250112304800003','henry@del.ac.id, henry_sitorus@yahoo.com','218','PROSEDE','DISCONET','A',1,3,4,NULL,'K',3,'2005-11-10','0000-00-00','S. Sitorus','T. Butar-Butar','S',2,'','0000-00-00','','',1411,0,NULL,NULL,NULL,NULL,NULL,NULL),(39,'mqsrilni8bs99t1g5xkh','Amrin Salomo Pandapotan Sinaga','amrin','','','',NULL,'ASP','Laboran','Salak','1984-06-07',NULL,1,2,'','',NULL,'',NULL,'',NULL,'','','','','','','',NULL,NULL,NULL,NULL,'K',3,'0000-00-00','0000-00-00','Ir. Sotan Sinaga, (Alm)','Tiur Dame Pasaribu','S',2,'','0000-00-00','','PNS',1412,0,NULL,NULL,NULL,NULL,NULL,NULL),(40,'kl9q35zpxksxyqrgqvtw','Fredy Sibarani','fredy','','111037301','yxooba992hd2w78lyuc7',1,'FRE','Dosen','Pasar Raja','1973-03-11',NULL,1,2,'081322228828','0632-41379',NULL,'Jl. Sisingamangaraja Porsea',NULL,'Tobasa',NULL,'','','fredy@del.ac.id,fredy_sibarani@yahoo.com','','GAME','','A',1,6,NULL,NULL,'K',3,'2006-02-08','0000-00-00','Oscar Sibarani','Alm. Laksi Hutagaol','M',1,'','0000-00-00','','Wiraswasta',1413,0,NULL,NULL,NULL,NULL,NULL,NULL),(41,'ed7rlmk49sok3a6fhs9v','Larisma Simanjuntak, S.Sos','larisma.s','0312060012','','',NULL,'LSM','Staff','Matio','1985-01-21',1,2,NULL,'081362214484','0632-331234','Matio, Desa Parsoburan Barat Kec. Habinsaran Kab. Toba Samosir\r\n','','Habinsaran','',3,'22383','1212046101850001','larisma@del.ac.id','','','','',NULL,NULL,NULL,4,'A',1,'2006-12-01',NULL,'','','S',2,'','0000-00-00','','',1414,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 14:46:30'),(42,'5fgendu8okq44tqsyb6m','Anna Florence Simanjuntak','','','','',NULL,'','Administrasi','','0000-00-00',NULL,2,NULL,'','',NULL,'',NULL,'',NULL,'','','','','','','',NULL,NULL,NULL,NULL,'K',3,'2006-05-23','0000-00-00','','','S',2,'','0000-00-00','','',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(43,'317z1osrl93mn3twviwg','Duma Imelda Panjaitan','','','','',NULL,'','Administrasi','','0000-00-00',NULL,1,NULL,'','',NULL,'',NULL,'',NULL,'','','','','','','',NULL,NULL,NULL,NULL,'K',3,'2005-05-23','0000-00-00','','','S',2,'','0000-00-00','','',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(44,'7ydlh8mskbsh3g0pnsp0','Ardin Dolok Saribu','','','','',NULL,'','Administrasi','','0000-00-00',NULL,1,NULL,'','',NULL,'',NULL,'',NULL,'','','','','','','',NULL,NULL,NULL,NULL,'K',3,'2005-05-23','0000-00-00','','','S',2,'','0000-00-00','','',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(45,'42tw92as76ahrnkn380k','Berlin Napitupulu','','','','',NULL,'','Teknisi','','0000-00-00',NULL,1,NULL,'','',NULL,'',NULL,'',NULL,'','','','','','','',NULL,NULL,NULL,NULL,'A',1,'2001-10-16','0000-00-00','','','S',2,'','0000-00-00','','',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(46,'s4x16wsbqgn2eosmqax3','Bangkit Marpaung','','','','',NULL,'','Teknisi','','0000-00-00',NULL,1,NULL,'','',NULL,'',NULL,'',NULL,'','','','','','','',NULL,NULL,NULL,NULL,'A',1,'2005-04-01','0000-00-00','','','S',2,'','0000-00-00','','',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(47,'ppqqbtnl7ullc12nql7s','Rudi Silalahi','','','','',NULL,'','Teknisi','','0000-00-00',NULL,1,NULL,'','',NULL,'',NULL,'',NULL,'','','','','','','',NULL,NULL,NULL,NULL,'A',1,'2004-12-02','0000-00-00','','','S',2,'','0000-00-00','','',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(48,'ct7dg9um0svd7frwwbmz','Parlinggoman Napitupulu','','','','',NULL,'','Teknisi','','0000-00-00',NULL,1,NULL,'','',NULL,'',NULL,'',NULL,'','','','','','','',NULL,NULL,NULL,NULL,'A',1,'2001-10-01','0000-00-00','','','S',2,'','0000-00-00','','',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(49,'dsmgb7hkvfizbfpmrzak','Leonard (Lenny) M. Shores','lennyshores','','','',NULL,'LMS','dosen','Jacksonuilk Florida','1946-06-03',NULL,1,NULL,'','',NULL,'',NULL,'',NULL,'','','','','','','',NULL,NULL,NULL,NULL,'K',3,'2007-01-15','0000-00-00','','','M',1,'','0000-00-00','','',1415,0,NULL,NULL,NULL,NULL,NULL,NULL),(50,'y9zihvw4otw5cpwh56lv','Georgeanna Victoria Shores','gashores','','','',NULL,'GVS','dosen','Ft. Benning Georgia','0000-00-00',NULL,2,NULL,'','',NULL,'',NULL,'',NULL,'','','','','','','',NULL,NULL,NULL,NULL,'K',3,'2007-01-15','0000-00-00','','','M',1,'','0000-00-00','','',1416,0,NULL,NULL,NULL,NULL,NULL,NULL),(51,'05q9c19tz0smy48sdi27','Humasak Tomy Argo Simanjuntak, ST, M.ISD','humasak','0302070013','126048301','tfc83kdqpq6y42i3pyqa',2,'HTS','dosen','Pematangsiantar','1983-04-26',1,1,3,'081362408544','0632-331234','Jl. S.M.Raja Kel.Sitoluama Kec. Laguboti Kab. Toba Samosir\r\n','','Laguboti','',3,'22381','1212022604830001','','','DAME_COMBINE','PROSEDE','A',1,1,3,4,'A',6,'2007-02-01',NULL,'','','S',1,'','0000-00-00','','',1417,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 14:49:22'),(52,'qw5zufmy00w1d7z4ab6a','Rosni Lumbantoruan, ST, M.ISD','rosni','0303070016','','',NULL,'RSL','Dosen','Sosunggulon','1982-10-24',2,2,4,'08216502741','0632-331234','Lumban Maradong Kel.Sosunggulan Kec. Tarutung Kab. Tapanuli Utara\r\n','','Tarutung','',4,'22411','1212026410820001','','','','','',NULL,NULL,NULL,4,'A',6,'2007-03-20',NULL,'Dohir Lumbantoruan','Rospita Hutabarat','M',1,'','0000-00-00','','Purnawirawan POLRI',1418,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 14:57:42'),(53,'0i92cu1g0m3xgyh91qqs','Elisa Margareth Sibarani, ST, M.ISD','elisa','0303070017','123118301','yxooba992hd2w78lyuc7',1,'EMS','dosen','Medan','1983-11-23',1,2,NULL,'085221874357','0632-331234','Jl. Bambu I No. 78/27 RT 009/RW 009 Kel. Durian Kec. Medan Timur Kota Medan\r\n','','Medan Timur','',14,'20235','250065811830003','','','DAME_COMBINE','PROSEDE','A',1,1,3,4,'A',6,'2007-03-20',NULL,'','','M',1,'','0000-00-00','','',1419,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 14:59:27'),(54,'wub8vso27k6df1fwi6st','Inte Christinawati Bu\'ulolo, ST., M.T.I','inte','0303070015','118118201','yxooba992hd2w78lyuc7',1,'ICB','dosen','Gunungsitoli','1982-11-18',1,2,NULL,'082133003002','0632-331234','Jl. M.Hatta II No. 1 Afilaza Kel. Pasar Gunung Sitoli Kec. Gunung Sitoli  Kota Gunung Sitoli\r\n','','Gunung Sitoli','',20,'22811','1204015711820001','','','PROSEDE','','A',1,3,NULL,4,'A',1,'2007-03-20',NULL,'','','M',1,'','0000-00-00','','',1420,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 14:55:12'),(55,'e269zu67q4em0u3zn6pc','Marojahan MT. Sigiro, ST., M.Sc','marojahan','0303070014','108098301','fiy7zfcz29vmkeclc6hi',3,'MMS','dosen','Lumban Sigiro','1983-09-08',1,1,4,'081265991465','0632-331234','Tomok, Desa Tomok Kec. Simanindo Kab. Samosir\r\n','','Simanindo','',25,'22395','1217090809830002','marojahan@gmail.com','','DISCONET','','A',1,4,NULL,4,'A',1,'2007-03-20',NULL,'','','S',2,'','0000-00-00','','',1421,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 14:51:47'),(56,'58oz935e5hw6k9lbyu2f','Drs. Tahan Hutahaean, M.Ed','tahan','0309070018','114044202','tfc83kdqpq6y42i3pyqa',2,'THT','Dosen','Laguboti','1942-04-14',1,1,NULL,'081361132851','0632-331234','Jl. Asahan No. 4 RT/RW 000/000 Kel. Siopat Suhu Kec. Siantar Timur Kota .P.Siantar\r\n','','Siantar Timur','Siantar',2,'21136','0253031404420001','','','LANGUAGE','','A',1,5,NULL,1,'K',3,'2007-09-17','2013-01-16','','','M',1,'','0000-00-00','','',1422,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 15:04:08'),(57,'h8ghdu34o24p4twzqepd','Dr. Inggriani Liem','inge','-','130796176','',NULL,'IL','Dosen','Blitar','1953-01-16',2,2,NULL,'0811234558','','','','','',NULL,'','','','','','','',NULL,NULL,NULL,3,'A',1,'2007-11-24','0000-00-00','','','M',NULL,'','0000-00-00','','',1423,0,NULL,NULL,NULL,NULL,'cori','2015-12-04 14:21:59'),(58,'r3q3b1d7yg9ifqy56jk3','Bony Parulian Josaphat Marbun','bony','','118018001','yxooba992hd2w78lyuc7',1,'BPM','Dosen','JAKARTA','1980-01-18',NULL,1,NULL,'','',NULL,'',NULL,'',NULL,'','','bony@del.ac.id','','THIN','PROSEDE','A',1,2,3,NULL,'K',3,'2008-05-05','0000-00-00','','','S',2,'','0000-00-00','','',1424,0,NULL,NULL,NULL,NULL,NULL,NULL),(59,'filk21qp1gmcybmjnhj5','Herry DS Pasaribu','','111105701','','',NULL,'HDP','dosen,Wakil Direktur','','0000-00-00',NULL,1,NULL,'','',NULL,'',NULL,'',NULL,'','','herrypasaribu@del.ac.id','','SOBAT','','C',3,7,NULL,NULL,'K',3,'2008-01-01','0000-00-00','','','M',1,'','0000-00-00','','',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(60,'mic0oz5k9pcdy4ecmahl','Kisno Shinoda','kisno-x','','116108501','fiy7zfcz29vmkeclc6hi',3,'KIS','Dosen','Pematangsiantar','1985-10-16',3,1,NULL,'081264563707','0622-28742',NULL,'Jl. Jogjakarta No. 09\nSimalungun/ Pematangsiantar',NULL,'Pematangsiantar',NULL,'21118','851071800047','kisno@del.ac.id; d-shinoda@lycos.com','','LANGUAGE','','A',1,5,NULL,NULL,'K',3,'2008-07-21','0000-00-00','','','S',2,'','0000-00-00','','',1425,1,NULL,NULL,NULL,NULL,NULL,NULL),(61,'olk431ph714fztv15t1a','Rina Sibuea','rina','','','tfc83kdqpq6y42i3pyqa',2,'RNS','Dosen','','0000-00-00',NULL,2,NULL,'','',NULL,'',NULL,'Pematang Siantar',NULL,'','','rina@del.ac.id','','DAME_COMBINE','PROSEDE','A',1,1,3,NULL,'K',3,'2009-06-08','0000-00-00','','','S',2,'','0000-00-00','','',1426,0,NULL,NULL,NULL,NULL,NULL,NULL),(62,'060bkqaax4arnj6v3xrt','Roberto','roberto','','102098601','yxooba992hd2w78lyuc7',1,'REA','dosen','Malang','1986-09-02',NULL,1,4,'','',NULL,'',NULL,'',NULL,'','','roberto@del.ac.id','','PROSEDE','','A',1,3,NULL,NULL,'K',3,'2009-08-26','0000-00-00','','','S',2,'','0000-00-00','','',1427,0,NULL,NULL,NULL,NULL,NULL,NULL),(63,'1aohqbuyq1uvfc38ci1o','Eka Stephani Sinambela, SST., M.Sc','eka','0309080019','9931000091','fiy7zfcz29vmkeclc6hi',3,'EKA','Dosen','Pematang Siantar','1987-07-17',1,2,3,'081397026205','0632-331234','Jl. Kisaran No. 19 RT/RW 001/001 Kel. Kristen Kec. Siantar Selatan Kota Pematang Siantar\r\n','','Siantar Selatan','',2,'21124','1272045707870001','','','DISCONET','','A',1,4,NULL,4,'A',1,'2008-09-15',NULL,'','','S',2,'','0000-00-00','','',1428,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 15:18:26'),(64,'p3gyberw4uamk5ilp56w','Eka Trisno Samosir','ekatrisno','','110098601','yxooba992hd2w78lyuc7',1,'ETS','dosen, asisten','Mela','1986-09-10',NULL,1,2,'','',NULL,'',NULL,'',NULL,'','','','','PROSEDE','','A',1,3,NULL,NULL,'K',3,'2009-08-28','0000-00-00','','','S',2,'','0000-00-00','','',1429,0,NULL,NULL,NULL,NULL,NULL,NULL),(65,'b6fluozwy4jr8kaypafy','Irma Sari Barus','irma','','125078701','tfc83kdqpq6y42i3pyqa',2,'ISB','dosen, asisten','Kabanjahe','1987-07-25',NULL,1,2,'','',NULL,'',NULL,'',NULL,'','','','','DAME_COMBINE','','A',1,1,NULL,NULL,'K',3,'2009-08-28','0000-00-00','','','S',2,'','0000-00-00','','',1430,0,NULL,NULL,NULL,NULL,NULL,NULL),(66,'90z5nw934znln53vkmpy','Roy Deddy Hasiholan Lumban Tobing, S.T., M.ICT','roy.deddy','0309090020','121038401','yxooba992hd2w78lyuc7',1,'RDT','Dosen','Tarutung','1984-03-21',1,1,3,'081396846620','0632-331234','Jl. D.I. Panjaitan No. 1 Kel. Hutabarangan Kec. Sibolga Utara Kota Sibolga\r\n','','Sibolga Utara','',1,'22514','0255022103840001','roy.deddy@del.ac.id','','PROSEDE','','A',1,3,NULL,4,'A',1,'2009-09-28',NULL,'','','M',2,'','0000-00-00','','',1431,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 15:21:34'),(67,'yypq41jmtkritm238t2o','Sanhenra Sinaga','sanhenra','','125108701','fiy7zfcz29vmkeclc6hi',3,'SAN','dosen, asisten','Mandoge','1987-10-25',NULL,1,4,'','',NULL,'',NULL,'',NULL,'','','','','DISCONET','','A',1,4,NULL,NULL,'K',3,'2009-10-01','0000-00-00','','','S',2,'','0000-00-00','','',1432,0,NULL,NULL,NULL,NULL,NULL,NULL),(68,'ybisw0fsxtvx8vnuhpuv','Yosi Trianggono','yosi','','117118501','fiy7zfcz29vmkeclc6hi',3,'YST','dosen, asisten','Solo','1985-11-17',NULL,1,NULL,'','',NULL,'',NULL,'',NULL,'','','yosi@del.ac.id','','DISCONET','','A',1,4,NULL,NULL,'K',3,'0000-00-00','0000-00-00','','','S',2,'','0000-00-00','','',1433,0,NULL,NULL,NULL,NULL,NULL,NULL),(69,'rnpgi8g5dkbnri3f4b01','Grace Dona Harlita Tarihoran','grace','','121098901','yxooba992hd2w78lyuc7',1,'GDT','Dosen, Asisten','Tarutung','1989-09-21',NULL,1,NULL,'','',NULL,'',NULL,'',NULL,'','','','','PROSEDE','','A',1,3,NULL,NULL,'K',3,'2009-10-01','0000-00-00','','','S',2,'','0000-00-00','','',1434,0,NULL,NULL,NULL,NULL,NULL,NULL),(70,'kjvh0amq0z3r9x8787d8','Mario Elyezer Subekti Simaremare, S.Kom., M.Sc','mario','0310090021','128058801','yxooba992hd2w78lyuc7',1,'MSS','Dosen','Solo','1988-05-14',1,1,4,'085262211248','0632-331234','Jl. P. Misol VIIA No. 11A Dusun BR. Sumuh, Desa/Kelurahan Dauh Puri Kauh/80113 Kec. Denpasar Barat Kota Denpasar Bali\r\n','','Denpasar Barat','',20,'80113','517103280588002','mario@del.ac.id','','PROSEDE','','A',1,3,NULL,4,'A',1,'2009-10-01',NULL,'','','S',1,'','0000-00-00','','',1435,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 15:29:15'),(71,'2jcvleh92zblcmm5a9q8','Timbang Pangaribuan','timbang','','121026401','fiy7zfcz29vmkeclc6hi',3,'TRP','Dosen','Laguboti','1964-02-21',NULL,1,NULL,'','',NULL,'',NULL,'',NULL,'','','','','','','A',1,NULL,NULL,NULL,'K',3,'2009-11-19','0000-00-00','','','M',1,'','0000-00-00','','',1436,0,NULL,NULL,NULL,NULL,NULL,NULL),(72,'zv9cnho17zjx2xeg06l1','Doni Arzinal','doni','','103031001','yxooba992hd2w78lyuc7',1,'DNA','dosen','','0000-00-00',3,1,NULL,'','',NULL,'',NULL,'',NULL,'','','doni@del.ac.id','','PROSEDE','','A',1,3,NULL,NULL,'K',3,'0000-00-00','0000-00-00','','','S',2,'','0000-00-00','','',1437,0,NULL,NULL,NULL,NULL,NULL,NULL),(73,'o7s2x7darsnz699ct77p','Indra Siregar','indra.siregar','','108018301','yxooba992hd2w78lyuc7',1,'IS','dosen','Laguboti','1983-09-08',NULL,1,NULL,'','',NULL,'',NULL,'',NULL,'','','indra@del.ac.id','','PROSEDE','','A',1,3,NULL,NULL,'K',3,'1983-08-04','0000-00-00','','','S',2,'','0000-00-00','','',1438,0,NULL,NULL,NULL,NULL,NULL,NULL),(74,'wej0kxrv84h0iesiuax0','Kristonny Panjaitan','kristonny','','','',NULL,'','','','0000-00-00',NULL,1,NULL,'','',NULL,'',NULL,'',NULL,'','','','','LANGUAGE','','A',1,5,NULL,NULL,'K',3,'0000-00-00','0000-00-00','','','S',2,'','0000-00-00','','',1439,0,NULL,NULL,NULL,NULL,NULL,NULL),(75,'f6qe7wwin1gtbtfax89u','Hernawati Susanti Samosir, SST','hernawati','0309100028','','',NULL,'HER','Asisten Dosen','Porsea','1989-09-24',1,2,NULL,'081370869163','0632-331234','Komp. Tanah Lapang Patane III Kec. Porsea Kab. Toba Samosir\r\n','','Porsea','',3,'22384','1212070640989006','','','','','',NULL,NULL,NULL,4,'S',6,'2010-09-20',NULL,'','','S',2,'','0000-00-00','','',1440,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:06:45'),(76,'0987tqkxtzoc7x2t8jlt','Perdana Martinus Situmorang','','','','',NULL,'PMS','','Binjohara','1992-09-11',NULL,1,NULL,'085359144246','',NULL,'',NULL,'',NULL,'','1201051109920003','perdana.situmorang@del.ac.id','','','','',NULL,NULL,NULL,NULL,'A',1,'2014-09-07','0000-00-00','','','S',2,'','0000-00-00','','',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(77,'7e44linbs4fbzy7ekaa2','Masto Sitorus, Amd','masto','0308110042','','',NULL,'MAS','','Balige','1990-02-25',1,1,4,'085762379790','0632-331234','Jl. Sutomo Kel. Sangkarnihuta Kec. Balige Kab. Toba Samosir\r\n','','Balige','',3,'22312','1212012502900025','','','','','',NULL,NULL,NULL,4,'K',3,'2011-08-23','2014-03-31','','','S',2,'','0000-00-00','','',1441,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:31:16'),(78,'p5ic0bf9r8pi07pt9m1o','Jogi Silalahi','jogi','','','fiy7zfcz29vmkeclc6hi',3,'','','','0000-00-00',NULL,1,NULL,'','',NULL,'',NULL,'',NULL,'','','','','DISCONET','','A',1,4,NULL,NULL,'K',3,'0000-00-00','0000-00-00','','','S',2,'','0000-00-00','','',1442,0,NULL,NULL,NULL,NULL,NULL,NULL),(79,'emqapwl6wxkpz873t1ii','Sabar M. Tampubolon , AMd','sabar','0309100027','','fiy7zfcz29vmkeclc6hi',3,'SBT','Asisten Dosen','Balige','1989-03-11',1,1,NULL,'085275545608','0632-331234','Sibolahotang, Kel.Sibolahotangsas Kec. Kab. Balige Toba Samosir\r\n','','Balige','',3,'22312','1212011103890004','','','PROSEDE','','A',1,3,NULL,4,'K',3,'2010-09-20','2012-08-01','','','S',2,'','0000-00-00','','',1443,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:04:30'),(80,'xjknxg61on31y6xikbqt','Togu Novriansyah Turnip, S.S.T., M.IM','togu','0309100024','9931000090','yxooba992hd2w78lyuc7',1,'TNT','Asisten Dosen','Pematang Siantar','1989-11-29',1,1,4,'085210573009','0632-331234','Jl. Jend.A.Yani Gg. Setia No. 26 RT 014/005 Kel. Asuhan Kec. Siantar Timur Kota Pematang Siantar\r\n','','Siantar Timur         ','',2,'21136','1272012911890002','','','PROSEDE','','A',1,3,NULL,4,'A',1,'2010-09-20',NULL,'','','S',2,'','0000-00-00','','',1444,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 15:48:37'),(81,'b0dpf40byp3p4lo4aobl','Riyanthi Angrainy Sianturi, S.Sos, M.Ds','riyanthi','0303110035','9901114483','',NULL,'RIS','Dosen','Tarutung','1985-05-21',1,2,2,'081375215693','0632-331234','Jl. Dr. TB. Simatupang Desa Hutatoruan VII Kec. Tarutung Kab. Tapanuli Utara  \r\n','Jl. Dr TB Simatupang, Hutabaginda, Tarutung','Tarutung','Tapanuli Utara',4,'22411','1202016105850002','riyanthi@del.ac.id','','GAME','DAME_COMBINE','A',1,6,1,4,'A',1,'2011-03-01',NULL,'','','S',2,'','0000-00-00','','',1445,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:19:40'),(82,'by8a7nxuskx5012juh4o','Ellida Damanik','elida_1','','','',NULL,'ELD','Dosen','','0000-00-00',NULL,2,NULL,'','',NULL,'',NULL,'',NULL,'','','elida@del.ac.id','','','','',NULL,NULL,NULL,NULL,'K',3,'0000-00-00','0000-00-00','','','M',1,'','0000-00-00','','',1446,0,NULL,NULL,NULL,NULL,NULL,NULL),(83,'7cskd0x4jfrcctnc3ub0','Ecko Fernando Basarah Manalu','','','','',NULL,'EFM','dosen','Medan','1985-01-05',NULL,1,NULL,'','',NULL,'',NULL,'',NULL,'','','','','','','',NULL,NULL,NULL,NULL,'K',3,'0000-00-00','0000-00-00','','','S',2,'','0000-00-00','','',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(84,'77h6c6623p19p8jm40sy','Frederick B Situmeang, M.Sc','','0312090022','','',NULL,'FBS','Dosen','Medan','1984-04-26',1,1,NULL,'031207799070','0614420878','Komp. Taman Setia Budi Indah Blok CC No. 6 Sunggal Medan 20122\r\n','','Sunggal Medan','',14,'20128','1212022604830026','','','','','',NULL,NULL,NULL,4,'S',3,'2009-12-17',NULL,'','','M',1,'','0000-00-00','','',1981,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 15:36:35'),(85,'th9p32gv43tk5in76ift','Genesis S.G. Banjarnahor, AMd','genesis','0309100026','','fiy7zfcz29vmkeclc6hi',3,'GEN','Asisten Dosen','Sarang Giting','1989-06-06',1,1,NULL,'085270060781','0632-331234','Desa Sarang Giting Kec. Dolok Masihul Kab. Serdang Bedagai\r\n','','Dolok Masihul','',26,'20991','1218080606890006','','','','','',NULL,NULL,NULL,4,'K',3,'2010-09-20','2012-03-01','','','S',2,'','0000-00-00','','',1447,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:00:09'),(86,'w6esafxsmyp6hqefzdtc','Tulus Pardamean Simanjuntak, SST','tulus','0309110046','','fiy7zfcz29vmkeclc6hi',3,'TLS','asisten dosen','Tarutung','1990-03-07',1,1,NULL,'085361045737','0632-331234','Jl. Raja Johannes Hutabarat Kel. Parbajutoruan Kec. Tarutung Kab. Tapanuli Utara\r\n','','Tarutung','',4,'22416','1202010703900002','','','','','',NULL,NULL,NULL,4,'S',1,'2011-09-05',NULL,'','','S',2,'','0000-00-00','','',1448,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:35:53'),(87,'n5gz79pzn3lu4g7731vx','Eko A. Silitonga, AMd','eko','309100025','','fiy7zfcz29vmkeclc6hi',3,'EKO','Asisten Dosen','P.Siantar','1990-02-04',1,1,NULL,'085275841094','0632-331234','Jl. F.Pasaribu Gg J.Kelutuk 10 Sukamaju  Kec. Siantar Marihat Kota  Pematang Siantar\r\n','','Siantar Marihat','',2,'21127','1272050402900002','','','','','',NULL,NULL,NULL,4,'K',3,'2010-09-20','2014-01-31','','','S',2,'','0000-00-00','','',1449,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 15:56:36'),(88,'jq37gv8o3xesemtyv0zm','Gerry Italiano Wowiling, S.Tr.Kom','gerry','0309110047','','',NULL,'GIW','Asisten Dosen','Solo','1990-05-25',1,1,2,'085275347835','0632-331234','Jl. Rajawali Rt/Rw 002/001 Kel. Batang Bungo Kec. Pasar Muara Bungo Kab. Bungo Jambi\r\n','Jl. Lintas Sumatera RT I/Rw2 Kec. Pasar Muara Bungo','Pasar Muara Bungo','Bungo Jambi',20,'37211','1508032505900001','gerry@del.ac.id','','','','',NULL,NULL,NULL,4,'S',1,'2011-09-05',NULL,'','','S',2,'','0000-00-00','','Wiraswasta',1450,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:38:23'),(89,'rawbw0l7sa7sg21gjmp1','Edwin Swandi Sijabat, SST','edwin','0308110041','','',NULL,'ESJ','Asisten Dosen','Laras II','1990-05-01',1,1,2,'087892287567','0632-331234','Huta Lumban Buntu Kelurahan Laras Dua Kec. Siantar Kab. Simalungun\r\n','','Siantar','',27,'21151','1208010105900002','','','','','',NULL,NULL,NULL,4,'A',6,'2011-08-23',NULL,'','','S',2,'','0000-00-00','','',1451,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:28:46'),(90,'azoykic25y69lhwpy6eb','Artati C.R.Tampubolon, AMd','artati','0308110040','','yxooba992hd2w78lyuc7',1,'ACT','asisten dosen','Jakarta','1991-04-18',1,2,1,'085361624913','0632-331234','Jl. Pematangsiantar Kel.Sibolahotangsas Kec.Balige Kab Toba Samosir\r\n','','Balige','',3,'22312','1212015804910058','','','','','',NULL,NULL,NULL,NULL,'K',3,'2011-08-23','2012-02-07','','','S',2,'','0000-00-00','','',1452,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:26:39'),(91,'ep4fwza6r00mlvwfzolu','Wiwin Sry Adinda Banjarnahor, S.Kom, M.Sc','wiwin','0301120049','','',NULL,'WSA','','Medan','1985-07-20',1,2,2,'081316704408','0632-331234','Jl. Pagar Merbau No. 14  Komp PNS II Pagar Merbau Lubuk Pakam Deli Serdang\r\n','Jl. Pagar Merbau No.14 Komp. Perumahan Pemda Lubuk Pakam','Lubuk Pakam','DELI SERDANG',8,'20515','1207285007850005','wiwin@del.ac.id','','','','',NULL,NULL,NULL,4,'A',1,'2012-01-10',NULL,'','','S',2,'','0000-00-00','','',1453,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:40:26'),(92,'44etlq0gwt1bng49fjum','Verawaty Situmorang, S.Kom., M.T.I','verawaty','0302120051','','',NULL,'VES','','Balige','1985-05-12',1,2,NULL,'081376373273','0632-331234','Jl. Tandang Buhit Gg Cemara Desa/Kel. Pardede Onan Kec.Balige Kab. Tobasamosir\r\n','Jl. Pardede Onan Balige ','Balige','TOBA SAMOSIR ',3,'22312','1212015205850001','verawaty@del.ac.id','','','','',NULL,NULL,NULL,4,'A',1,'2012-02-03',NULL,'','','M',1,'','0000-00-00','','',1454,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:47:22'),(93,'0lqo64xv7tqd13phpjpo','Mariani Simatupang, SPd','mariani','0302100023','','',NULL,'MAR','dosen','Pematangsiantar','1985-03-13',1,2,NULL,'085276072213','0632-331234','Jl. Manunggal Karya Kel. Simarimbun Kec. Siantar Marimbun Kota P.Siantar\r\n','','Siantar Marimbun','',2,'21129','1272055303880000','mariani_simatupang@yahoo.com','','LANGUAGE','','A',1,5,NULL,1,'K',3,'2010-02-15','2013-01-31','','','S',2,'','0000-00-00','','',1455,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 15:46:10'),(94,'150sw85r9du622fm8344','Kristina Gloria Simanjuntak, S.Sos','kristina','0303120052','9901114491','tfc83kdqpq6y42i3pyqa',2,'KGS','Dosen','Medan','1985-10-03',1,2,4,'085297038523','0632-331234','Jl. Menteng III Gg. Silaturahim 16 Kel. Binjai Kec. Medan Denai Kota Medan\r\n','Jl. BAhagia By Pass Puri BAhagia No. 1 Medan Kel.Sudirejo 1 Kec. Medan Kota','Medan Denai','Medan',14,'20228','1271044310850003','','','DAME_COMBINE','','A',1,1,NULL,4,'K',3,'2012-03-01','2013-06-30','','','S',2,'','0000-00-00','','',1456,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:50:13'),(95,'8jgvxiccn3x2993vp1k4','Elida Lamria Siahaan, S.Kom','elida','0308120064','','',NULL,'','Staff','Pararung','1983-03-21',1,2,1,'085261771995','0632-331234','Jl. Pararung\r\n','','Porsea','',3,'22384','1212076103830002',' my_aero182@yahoo.com','','','','',NULL,NULL,NULL,4,'K',3,'2012-08-01','2015-01-23','','','S',2,'','0000-00-00','','',1457,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 17:06:56'),(96,'vgs2crh8plywp21t9kfy','Fidelis Haposan Silalahi, SH., MH','fidelis','0311100031','','',NULL,'FHS','Staf Human Resources','Kisaran','1972-04-30',2,1,2,'081370444418','0632-331234','Perumahan Griya Rumah Tengah Jl. Bunga Turi I Blok C9 No. 23 Simalingkar A -Kec.Pancurbatu  20353 Kab.Deli Serdang\r\n','','Pancur Batu','',8,'20353','1207053004720001','','','','','',NULL,NULL,NULL,4,'A',1,'2010-11-01',NULL,'','','M',1,'','0000-00-00','','',1458,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:15:52'),(97,'v4zr5whd6m277dnqjxkb','Dorlan Malau, SE','dorlan','0302100029','','',NULL,'','Staff','Pargambiran ','1985-10-13',1,2,NULL,'085270523591','0632-331234','Dusun I. Desa Pargambiran, Kec. Sumbul Kab. Dairi\r\n','','Sumbul','',7,'22281','1211025310850001','dorlan@del.ac.id','','','','',NULL,NULL,NULL,4,'A',7,'2010-02-01',NULL,'','','M',1,'','0000-00-00','','',1459,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:09:58'),(98,'f3wuwxqrfanygsbuikzb','Debora Putri Tambunan, A.Md','','0309140128','','',NULL,'DPT','','Dolok Tala','1992-12-07',1,2,NULL,'085275055649','0632-331234','Silaen, Desa Silaen Kecamatan Silaen Kab. Tobasamosir\r\n','','Silaen','',32,'22382','1212034712920003','debora.tambunan@del.ac.id','','','','',NULL,NULL,NULL,1,'A',3,'2014-09-22','2016-08-31','','','S',2,'','0000-00-00','','',1930,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:08:10'),(99,'liciohwvep1semibph3a','Danang Junaedi, ST, MT','danang','309120065','415117801','op5whgldgbzt3in6b98k',4,'DNJ','Dosen','Yogyakarta','1978-11-15',3,1,4,'08562303178','0227210409','Jl. Pasir Leutik No. 137 Padasuka RT/RW 006/003 Kel. Padasuka Kec. Cimenyan Kab.Bandung\r\n','Pasirleutik No.137 RT.06/03 Padasuka','Cimenyan','Bandung',20,'40192','3204061511780002','danang@del.ac.id','','PROSEDE','','B',2,3,NULL,1,'K',3,'2012-09-01','2014-07-23','Nana Bin Wihatma','Sri Warsilah','S',2,'','0000-00-00','','Wiraswata',1460,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 17:10:32'),(100,'cj3qn2vo79c0tc1582ih','Ester Afriany Simatupang, A.Md','ester','309120066','','',NULL,'EAS','Asisten Dosen','Tarutung','1991-04-12',1,2,NULL,'085361146981','0632-331234','J. Balige  Gang Naipospos No. 3\r\n','','Sipoholon','',4,'22452','1202045204910006','','','','','',NULL,NULL,NULL,1,'K',3,'2012-09-01','2013-08-31','','','S',2,'','0000-00-00','','',1461,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 08:43:03'),(101,'y5qpo1ie17uyponfzpz1','Hanna Theresia Lumban Tobing, A.Md','hanna','0309120067','','',NULL,'HNT','asisten dosen','Tarutung','1991-08-22',1,2,NULL,'085762917081','0632-331234','Jl. Sisingamaraja No. 51\r\n','','Tarutung','',4,'22411','1202016208910001','','','','','',NULL,NULL,NULL,1,'K',3,'2012-09-01','2013-09-09','','','S',2,'','0000-00-00','','',1462,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 08:45:20'),(102,'9xywrdxx2rbgrbhao2xt','Sumiati Hutagalung, A.Md','sumiati','0309120068','','',NULL,'SHG','asisten dosen','Balige','1991-03-19',1,2,NULL,'085270818707','0632-331234','Jl.  Samosir\r\n','','Balige','',3,'22312','1212015903910001','','','','','',NULL,NULL,NULL,1,'K',3,'2012-09-01','2013-10-25','','','S',2,'','0000-00-00','','',1463,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 08:47:33'),(103,'sopl8hdk2zi8g20dtx98','Cori Sarma Natalia  Lumban Gaol, S.S','cori','0307120058','','',NULL,'CSN','Asisten Dosen,Staff','Kembang Seri','1987-12-02',1,2,4,'085297252005','0632-331234','Jl. Parkit  Raya III No. 300\r\n','Jln Parkit Raya 3 No 300, Perumnas Mandala, Medan, Sumatera Utara','Percut Sei Tuan','Medan',8,'22351','1207264212870006','corinathalia@gmail.com','','','','',NULL,NULL,NULL,4,'A',1,'2012-07-05',NULL,'D. Lumban Gaol (+)','N. Lumban Toruan','S',1,'','0000-00-00','','Guru',1464,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:58:39'),(104,'ddv3mj2crkccsdkh01cp','Depni Situmorang','depni','030812H011','','',NULL,'DLS','Asisten Dosen','Bagot Puluan','1988-03-18',1,2,NULL,'085276753497','0632-331234','Bagot Puloan, Kel. Siatasan Kec.Dolok Panribuan Kab. Simalungun\r\n','','Dolok Panribuan','',27,'21173','1208135803880000','','','','','',NULL,NULL,NULL,2,'K',3,'2012-08-01','2012-12-21','','','S',2,'','0000-00-00','','',1465,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 08:56:37'),(105,'mx88epu5htch4rjmet11','Yoke Aprilia Purba, A.Md','yoke','0301120050','','',NULL,'YAP','Staff','Medan','1990-04-27',1,2,3,'085310414897','0632-331234','Jl. Puskesmas Gg Sekolah  No. 14 RT 033/010 Kelurahan Lalang Medan Sunggal Kota Medan\r\n','Jln. Binjai KM 7.8 BLKI Pendidikan No. 5\nLalang, Medan, Sunggal','Medan Sungggal','Medan, SUMUT',14,'20127','1271026704900002','','','','','',NULL,NULL,NULL,4,'A',1,'2012-01-12',NULL,'','','S',2,'','0000-00-00','','',1466,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:43:47'),(106,'cnaproqs7s716tzljmi3','Santy Munthe, AP','','0311140129','','',NULL,'SMT','Pustakawan','Hutatua','1992-03-03',1,2,NULL,'087748134611','0632-331234','Huta Tua Desa/Kel Bonani Onan Kec.Doloksanggul Kab.Humbang Hasundutan\r\n','','Doloksanggul','',9,'22381','1216064303920003','','','','','',NULL,NULL,NULL,1,'A',1,'2012-10-05',NULL,'','','S',2,'','0000-00-00','','',1939,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 08:49:19'),(107,'dc0i7r69s8nzgugotnse','Silvia Yulianti, A.Md','silvia','031011H007','','',NULL,'sys','Staf Administrasi Akademik dan Kemahasiswaan','Jakarta','1976-07-27',1,2,2,'082110154356','021-7272538','MESS Perwira Jl. Utama No.15 RT/RW: 003/005 Kel. Srengseng Sawah Kec. Jagakarsa Kotamadya Jakarta Selatan\r\n','Jl. Utama No. 15\nSrengseng Sawah, Jagakarsa','Jagakarsa','Jakarta Selatan',42,'12640','0953096707760023','silviayulianti.ori@gmail.com','','','','',NULL,NULL,NULL,2,'A',3,'2011-10-21','2013-09-17','Mattheus Pardi','Caesilia Sutjiati','M',1,'','0000-00-00','','Purnawirawan/Ibu Rumah Tangga',1467,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 08:54:03'),(108,'ffoab5c6ilz5br39ivn5','Kisno, S.Pd, M.Pd','kisno','0302150133','','',NULL,'KIS','Dosen','Pematangsiantar','1985-10-16',3,1,4,'082174449291','','Jl. Jurung Gg Selar RT/011/RW 005 Kel. PardomuanKota Pematangsiantar\r\n','','Siantar Timur','',2,'21118','1272011610860005','kisno@del.ac.id','','','','',NULL,NULL,NULL,2,'A',3,'2013-01-28',NULL,'','','S',2,'','0000-00-00','','',1468,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 10:57:18'),(109,'tvr68zzuzam7gu9pemuv','Rymelda Naiborhu, S.Sos','rymelda','-','','',NULL,'RN','Staff','Medan','1975-07-31',1,2,NULL,'085297940023','','','','','',NULL,'','','rymelda@del.ac.id','','','','',NULL,NULL,NULL,NULL,'A',1,'2001-01-01','0000-00-00','','','S',2,'','0000-00-00','','',1469,0,NULL,NULL,NULL,NULL,'cori','2015-09-16 10:52:53'),(110,'scl051wehnquv3ka1ab6','Immanuel Panjaitan, S.Kom','immanuel','0303130070','','',NULL,'IMP','','Medan','1987-06-13',1,1,4,'082133003001','0632-331234','Kampus Politeknik Informatika Del Jl. Sisingamangaraja Sitoluama Laguboti\r\n','','Laguboti','',3,'22381','1271031306870001','immanuel@del.ac.id','','','','',NULL,NULL,NULL,4,'A',6,'2013-03-01',NULL,'','','M',1,'','0000-00-00','','',1470,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 09:08:28'),(111,'rmo4bn3s5w0tm1w1mgjd','Larima Simanjuntak, S.Sos','larisma','0312060012','','',NULL,'LSM','Pustakawan','Matio','1985-01-21',2,2,NULL,'081362214484','','','','','',NULL,'','1212046101850001','larisma@del.ac.id','','LANGUAGE','','',NULL,5,NULL,4,'A',1,'2006-12-01','0000-00-00','','','S',1,'','0000-00-00','','',1471,0,NULL,NULL,NULL,NULL,'cori','2015-09-16 09:54:30'),(112,'8nmaaoug8dvyr5wdb1z1','Dewi Simanjuntak','dewisimanjuntak','','','',NULL,'DES','Guru SMU Unggul, Dosen','Sibolga','1988-09-17',1,2,4,'','081370947243','','Sirongit','','Toba Samosir',NULL,'22381','','sarudik_88@yahoo.com','','LANGUAGE','','A',1,5,NULL,2,'A',7,'0000-00-00','0000-00-00','','','S',2,'','0000-00-00','','',1472,0,NULL,NULL,NULL,NULL,'cori','2015-09-15 15:48:43'),(113,'ggun5mue868w94le7m2l','Rumondang Miranda Marsaulina, S.P, M.Si','rumondang.naiborhu','0308130076','','',NULL,'RMM','Dosen','Jakarta','1976-05-08',1,2,1,'08128970747','0632-331234','Lembah Pinang Blok  I 15 No. 57-58 RT 011/009\r\n','Jl. Sei Besitang No.25 ','Duren Sawit','Medan',42,'20119','3175074805760003','rumondang.naiborhu@del.ac.id','','LANGUAGE','','B',2,5,NULL,1,'A',1,'2013-08-26',NULL,'Partumpuan Naiborhu','Riama Ida Sianipar','S',2,'','0000-00-00','','Dosen',1473,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 09:20:29'),(114,'irncyqfd8mbfb3ec09je','Batara Parada Siahaan, A.Md','','0309130077','','',NULL,'BPS','','Janji Maria','1992-06-22',1,1,3,'085360648802','0632-331234','Jl. Balige No. 87 Parparean  III Kec.Porsea Kab.Toba Samosir\r\n','Jl. Balige Porsea ','Porsea','Toba Samosir',3,'22384','1212072206920001','batara.siahaan@del.ac.id','','','','',NULL,NULL,NULL,1,'A',6,'2013-08-29',NULL,'Wilson Siahaan','Bertua Marpaung','S',2,'','0000-00-00','','',1927,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 09:22:40'),(115,'l2jz5gsjt7wpoodjz48y','Rini Juliana Sipahutar, A.Md','rini.sipahutar','0309130080','','',NULL,'RJS','Asisten Dosen','Kabanjahe','1992-03-10',1,2,4,'085760034945','0632-331234','Jembatan Sigeok, Desa Lumban Gaol Kec. Balige Toba Samosir\r\n','','Balige','',3,'22312','1212015003920002','rini.sipahutar@del.ac.id','','','','',NULL,NULL,NULL,1,'A',1,'2013-08-29',NULL,'','','S',2,'','0000-00-00','','',1474,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 09:29:01'),(116,'2lqs56uticqmequigap5','Goklas Henry Agus Panjaitan, A.Md','goklas.panjaitan','0309130081','','',NULL,'GHP','Asisten Dosen','Tarutung ','1991-08-19',1,1,4,'085262451014','0632-331234','Huta Harambir Desa Hutatoruan I Tarutung\r\n','','Tarutung','',4,'22411','1202011708910003','goklas.panjaitan@del.ac.id;goklasif10029@gmail.com','','','','',NULL,NULL,NULL,1,'A',1,'2013-08-29',NULL,'','','S',2,'','0000-00-00','','',1475,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 09:30:49'),(117,'fkb6zvqssrb7c3e07ng6','Rony Friendy Tampubolon, A.Md','','0309130078','','',NULL,'RFT','Asisten Dosen','Parapat','1993-03-23',1,1,4,'085762478186','0632-331234','Jl. Merdeka Gang Pangasean Parapat-Simalungun\r\n','','Girsangsipanganbolon','',27,'21174','1208162303930001','rony.tampubolon@del.ac.id','','','','',NULL,NULL,NULL,1,'A',3,'2013-08-29','2015-09-06','Nelson W Tampubolon','(Alm.) Juliana Siadari','S',2,'','0000-00-00','','',1979,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 09:24:50'),(118,'eyva85lcg276z1vfj1mn','Olga Minar Viona Sianturi, A.Md','','0309130079','','',NULL,'OMS','Asisten Dosen','Pematang Siantar','2013-06-26',1,2,1,'085362009616','0632-331234','Jl. Dr. Ferdinan Lumbantobing No. 73 Tarutung\r\n','','Tarutung','',4,'22411','1202016606920003','olga.sianturi@del.ac.id','','','','',NULL,NULL,NULL,1,'A',1,'2013-08-29',NULL,'Drs Binton Sianturi','Dra Damenna Tampubolon','S',2,'','0000-00-00','','',1934,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 09:26:50'),(119,'4lrylsbfl4qww1a51g6n','Jhon Batara Lelan Siahaan, S.Kom','jhon.siahaan','0305130071','','',NULL,'JBS','Staff','Tarutung','1987-01-20',1,1,3,'087868685050','081269876986','Siahaan Parhuta\r\n','Siahaan Parhuta, Silimbat','Sigumpar','Toba Samosir',3,'22381','1212112001870020','jhon.siahaan@del.ac.id','','','','',NULL,NULL,NULL,4,'A',1,'2013-03-01',NULL,'Saur Siahaan','Ratna Doloksaribu','S',2,'','0000-00-00','','',1476,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 09:13:57'),(120,'fu226u37ecvry56f870h','Santi Agustina Manalu, S.S., M.Pd','santi.manalu','0309130082','','',NULL,'SAM','Dosen','Tarutung','1983-08-05',1,2,1,'081375761247','0632-331234','Kp. Galuga RT/RW 002/005 Kel Binong Kec. Curug Kab Tangerang\r\n','Jl. Di. Panjaitan No. 27 Hutabarat','Curug','Tarutung',20,'15811','1202014508830004','santi.manalu@gmail.com','','LANGUAGE','','',NULL,5,NULL,4,'A',1,'2013-09-17',NULL,'Lukman Manalu','Rouli Sitanggang','S',2,'','0000-00-00','','Guru',3102,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 09:33:38'),(121,'7rsnyctjmrikp1xsye1y','Parmonangan Rotua Togatorop, S.Kom., M.T.I','','0309130086','','',NULL,'PAT','Asisten Dosen','Laguboti','1985-05-30',1,2,1,'082125606962','0632-331234','Lumban Atas, Sariburaya, Janji Maria, Kec Balige Tobasa\r\n','Muara','Balige','Tapanuli Utara',3,'22312','1212017005850007','mona.togatorop@gmail.com, mona.togatorop@del.ac.id','','','','',NULL,NULL,NULL,1,'A',1,'2013-09-17',NULL,'Japuddin Togatorop','Manuria Sinaga','M',1,'','0000-00-00','','PNS',1935,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 09:48:19'),(122,'modx8jtnb9t0ew1ums4w','Arie Satia Dharma, S.T, M.Kom','ariesatia','0309130087','927028002','yxooba992hd2w78lyuc7',1,'ASD','Dosen','Palembang','1980-02-27',3,1,4,'085296154949','0632-331234','Jl. Eka Rasmi Komp.Springville No.  19  LK VII Kel Gedung Johor Medan Johor Kota Medan\r\n','Jl. Eka Rasmi komp. Springfield No 19','Medan Johor','Medan',14,'20144','1271212702800004','ariesatia@gmail.com','','PROSEDE','DISCONET','B',2,3,4,1,'A',1,'2013-09-19',NULL,'Abdul Karim','Maryati','M',1,'','0000-00-00','','Pensiunan BUMN',1478,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 09:50:01'),(123,'1n9ix0ho7z01u4klhz7x','Denni Prima Putra Roli Sembiring  A.Md','','30913088','','',NULL,'DNS','Asisten Dosen','Porsea','1992-06-19',1,1,4,'082370844835','0632-331234','Town Site B TPL Kel/Desa Banjar Ganjang Kecamatan Parmaksian Kabupaten Toba Samosir\r\n','Kompleks PT TPL No 617 Kec Parmaksian','Parmaksian','',3,'22384','1212241906920002','denni.sembiring@del.ac.id','','','','',NULL,NULL,NULL,1,'A',3,'2013-09-17','2015-05-08','','','S',2,'','0000-00-00','','',1978,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 09:43:38'),(124,'y9w3slv614bkjj7smf5l','Markus Mulia Marsada Panjaitan ','','30913089','','',NULL,'MMP','Asisten Dosen','Medan','1991-09-21',NULL,1,4,'081376591916','',NULL,' Jl Melur III No 20',NULL,'Medan',NULL,'20132','','markus.panjaitan@del.ac.id','','','','',NULL,NULL,NULL,NULL,'K',3,'2013-09-17','0000-00-00','','','S',2,'','0000-00-00','','',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(125,'l24wujesmi93aku3jstl','Ade Candra Simamora, A.Md','ade.immanuel','0301140099','','',NULL,'ACS','','Doloksanggul','1990-11-26',1,1,2,'08974690137','063331802','Jl. Kompol M.Simamora Kel.Pasar Doloksanggul Kec.Doloksanggul Kab. Humbanghasundutan\r\n','Doloksanggul','Doloksanggul','Humbang Hasundutan',9,'33457','1216062611900003','if09072@gmail.com, ade.immanuel@del.ac.id','','','','',NULL,NULL,NULL,1,'K',3,'2014-01-20','2015-01-19','Patar Simamora','Geser Pane','S',2,'','0000-00-00','','PNS',1479,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 10:08:02'),(126,'uqpqs0m5adnc7v7pzj08','Lit Malem Ginting, S.Si, MT','litmalem','0301140091','118097802','yxooba992hd2w78lyuc7',1,'LMG','Dosen','Medan','1978-09-18',1,1,3,'081362799552','0632-331234','Jl. Palas II No. 44  Kel Simpang Selayang Kec.Medan Tuntungan Kota Medan\r\n','Jl. Pales II No.44 Simpang Selayang MEdan','Medan Tuntungan','Medan',14,'20135','1271071809780001','litmalem.ginting@del.ac.id, litmalem@gmail.com','','PROSEDE','DAME_COMBINE','A',1,3,1,4,'A',1,'2014-01-07',NULL,'Mesti Ginting','Nani Sembiring','M',1,'','0000-00-00','','Pensiunan PNS',1480,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 09:55:25'),(127,'hsl3pvxh7loqd6yo2q5e','Daniel Sitorus','','','','',NULL,'DST','Asisten Dosen','Medan','1992-02-06',NULL,1,NULL,'081397592120','',NULL,'',NULL,'',NULL,'','','daniel.sitorus@del.ac.id','','','','',NULL,NULL,NULL,NULL,'A',1,'0000-00-00','0000-00-00','','','S',2,'','0000-00-00','','',NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),(128,'2uvwskyd9glzib73j0sk','Prof.Dr. Roberd Saragih, MT','','0309130088','','',NULL,'RST','Dosen','Aeknauli','1962-12-27',1,1,2,'08122137708','','Jl. Sanggar Kencana XII No. 3 RT/RW 006/002 Kel. Jatisari Kec. Buah Batu Kota Bandung\r\n','','Buah Batu','',20,'40286','3273222712620001','','','','','',NULL,NULL,NULL,1,'A',1,'2013-08-20',NULL,'','','S',1,'','0000-00-00','','',1926,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 14:09:03'),(129,'zu51l8q9blexsogedql2','Irmandes Roy M Tambunan, A.Md','irmandes','0309150147','','',NULL,'IRT','Asisten Dosen','Balige','1992-12-10',1,1,NULL,'085358622739','0632-331234','Jl. Aek Bolon Kel/Desa Balige III\r\n','','Balige','',3,'22312','1212011012920000','','','','','',NULL,NULL,NULL,1,'A',3,'2015-09-01',NULL,'','','S',2,'','0000-00-00','','',1481,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 11:28:21'),(130,'9k6b3w7quhi8lb4b36ka','Hj. Latifah Sudarmy, S.Ag.','','','','',NULL,'LTS','Dosen','','0000-00-00',3,2,NULL,'','',NULL,'',NULL,'',NULL,'','','','','','','',NULL,NULL,NULL,NULL,'A',1,'2014-02-03','0000-00-00','','','M',1,'','0000-00-00','','',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(131,'773qbcokclgw97wf3ow4','ujicoba','','','','',NULL,'','','','0000-00-00',NULL,1,NULL,'','',NULL,'',NULL,'',NULL,'','','','','','','',NULL,NULL,NULL,NULL,'A',1,'0000-00-00','0000-00-00','','','S',2,'','0000-00-00','','',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(132,'yzlw07m8llujkuwfbk3v','Adelina Manurung, S.Si, M.Sc','adelina.manurung','0301140092','','jljj1y9xvqb0pd191uqw',8,'ANM','Dosen','Laguboti','1983-03-05',1,2,3,'082167244711','0632-331234','Jl. Tombak No. 48 Medan Kel Sidorejo Hilir Kecamatan Medan Tembung Kota Medan\r\n','Jl. Tombak No.48 Medan','Medan Tembung','Medan',14,'20222','1271144503830005','adelina.manurung@del.ac.id,manurungadelina@google.com','','','','A',1,NULL,NULL,4,'A',1,'2014-01-20',NULL,'Edison Manurung','Kamaida Pangaribuan','M',1,'','0000-00-00','','PNS',1482,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 10:01:05'),(133,'p8xf1xfyzu098ei4pz9c','Ricardo Chandra Situmeang, S.Psi, M.A','ricardo.situmeang','0304140107','','',NULL,'RCS','Dosen','Medan','1981-02-21',1,1,4,'081265455756','0632-331234','Jl. Bromo Gg Rukun No. 5 Tegal Sari Marindal III Medan Denai Kota Medan\r\n','Jl. Balige No.88 Sipoholon ','Medan Denai','Tapanuli Utara',14,'20218','1271042102810005','ricardo.situmeang@del.ac.id,ricardositumeang@gmail.com','','','','',NULL,NULL,NULL,1,'A',1,'2014-04-07',NULL,'R. Situmeang','M. Hutauruh','S',2,'','0000-00-00','','PNS',1483,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 10:18:57'),(134,'ko3z8y8oqyqa45ne1llj','Lusiana Parhusip, A.Md','lusiana.parhusip','0306140100','','',NULL,'LSP','Asisten Dosen','Nainggolan','1987-10-08',1,2,1,'081265966618','0632-331234','Jl. Melanthon Siregar Gg Barito Blok 6 B No. 21  RT 009/004 Desa/Kel Marihat Jaya Siantar Marimbun Kota Siantar\r\n','','Siantar Marimbun','',2,'21128','1217054810870001','lusiana.parhusip@del.ac.id','','','','',NULL,NULL,NULL,1,'A',3,'2014-06-10','2016-02-01','Marulak Parhusip','Dorta Sinaga','M',1,'','0000-00-00','','Wiraswasta',1484,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 10:21:56'),(135,'fmc7y9wu9jfny1wwl5ms','Rizal Horas Manahan Sinaga, S.Si., M.T','rizal.sinaga','0308140102','','',NULL,'RZS','Dosen','Tarutung','1982-05-15',1,1,1,'081397026770','0632-331234','Jl. H. Briti Sanggrahan RT/RW 002/009 Kel. Kembangan Selatan Kec Kembangan Jakarta Barat\r\n','Jl. TD Pardede Gg Dame No 455 Tarutung','Kembangan','Tapanuli Utara',42,'11610','3173081505820013','rizal.sinaga@del.ac.id','81','','','',NULL,NULL,NULL,1,'A',1,'2014-08-01',NULL,'Asmin Sinaga, Alm','Mideria Simanjuntak','S',2,'','0000-00-00','','Pensiuanan Guru PNS',1485,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 10:27:34'),(136,'1jz9a0q298fwd6qme1aw','Fitriani Tupa Ronauli Silalahi, S.Si, M.Si','fitri.silalahi','0308140103','','',NULL,'FIS','Dosen','Sintanauli','1990-03-12',1,2,1,'085262566677','0632-331234','Sianjur, Desa/Kel Tong Marimbun Kec.Siantar Marimbun Kota Pematangsiantar\r\n','Jl. Sukamulia Depan Kantor Lurah Tong Marimbun Kel. Bintang Marinbun, Kec. Siantar Marihat. P. Siant','Siantar Marimbun','P. Siantar',2,'21129','1272085203900001','fitri.silalahi@del.ac.id','85','','','',NULL,NULL,NULL,1,'A',1,'2014-08-04',NULL,'Jonni Silalahi','Megawati Sirait','S',1,'','0000-00-00','','Guru/wiraswasta',1486,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 10:24:09'),(137,'wd4w63qt13cixcwy3gfc','Dra. Roga Florida Kembaren, M.Si','roga.kembaren','0301140094','','',NULL,'RFK','Dosen','Kabanjahe','1966-01-06',1,2,3,'0815132736','0227279542','Jl. Kalijati Indah No. 42 RT/RW/005/005 Kel. Antapani Kulon Kota Bandung\r\n','Jl Kalijati Indah No 42 Antapani Bandung','Antapani','Bandung',20,'40291','3273204601660002','roga.kembaren@del.ac.id','83','','','',NULL,NULL,NULL,1,'A',1,'2014-01-20',NULL,'Kumpul S Kembaren','Hormat perangin-angin','S',2,'','0000-00-00','','',1487,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 10:10:08'),(138,'jrwb8rv3hg5v6woctrpj','Dr. Merry Meryam Martgrita, S.Si, M.Si','merry.martgrita','0301140095','','',NULL,'MMM','Dosen','Bandung','1970-03-12',1,2,4,'087825663415','0632-331234','Jl. Permata II O-4 No. 10 RT/RW 005/008 Kel Tani Mulya Kec.Ngamprah Kab. Bandung Barat\r\n','Jl. Salam no. 16','Ngamprah','Bandung',20,'40552','3217065203700010','merry.martgrita@del.ac.id','','','','',NULL,NULL,NULL,1,'A',1,'2014-01-20',NULL,'Oeoeng T. Poespotaroeno (Alm)','Flora Helena Christina','S',2,'','0000-00-00','','Ibu Rumah Tangga',1488,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 10:13:03'),(139,'gnvbolb53u1n3f1y6qq6','Poppy Fedora','poppy.sibarani','0309140114','','',NULL,'PFS','Asisten Dosen','Medan','1994-09-01',1,2,4,'085296745657','0632-331234','','','','',NULL,'','1271014109940004','poppy.sibarani@del.ac.id','','','','',NULL,NULL,NULL,2,'A',3,'2012-07-01',NULL,'','','S',2,'','0000-00-00','','',1489,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 15:54:40'),(140,'ntsvhiop48lqy9b8v3ne','Laborawaty Rajagukguk, A.Md','labora.rajagukguk','0309140111','','',NULL,'LAR','Asisten Dosen','Tarutung','1993-08-10',1,2,3,'082165224746','0632-331234','Jl. Jeruk No. 29 Perumnas Pagar Beringin, Kel. Pagar Batu , Kec.Sipoholon, Kab Tapanuli Utara\r\n','Jl. Jeruk No 209 Perumnas','Sipoholon','Tarutung',4,'22452','1202045008930001','labora.rajagukguk@del.ac.id','','','','',NULL,NULL,NULL,1,'A',3,'2014-09-08','2016-09-07','Lauren Rajagukguk','Tiamsah Simorangkir','S',2,'','0000-00-00','','PNS',1490,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 15:44:38'),(141,'79irygrwhwgasbjfsk7j','Perdana Situmorang, A.Md','perdana.situmorang','0309140123','','',NULL,'PMS','Asisten Dosen','Binjohara','1992-09-11',2,1,NULL,'085359144246','','','','','',NULL,'','1201051109920003','perdana.situmorang@del.ac.id','','','','',NULL,NULL,NULL,1,'A',1,'2014-09-08','0000-00-00','','','S',2,'','0000-00-00','','',1491,0,NULL,NULL,NULL,NULL,'cori','2015-12-04 14:29:35'),(142,'gx7hhwxlw0j8nik2dz4o','Novalina Hutabarat, A.Md','novalina.hutabarat','0309140122','','',NULL,'NOH','Asisten Dosen','Tarutung','1993-10-30',1,1,NULL,'085358379798','0632-331234','Sosorpadang , Kel/Desa Hapoltahan Kec. Tarutung Kab.tapanuli Utara\r\n','','Hapoltahan','',4,'22411','1202017010930003','novalina.hutabarat@del.ac.id','','','','',NULL,NULL,NULL,1,'A',3,'2014-09-08','2016-12-07','','','S',2,'','0000-00-00','','',1492,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 15:49:32'),(143,'bembn4pgck7re10a66k7','Sahat Maruhum Gultom, A.Md','sahat.gultom','0309140115','','',NULL,'SMG','Asisten Dosen','Nainggolan','1993-10-08',1,1,NULL,'085296409010','0632-331234','Nainggolan, Desa Sirumahombar, Kec. Nainggolan,Kab.  Samosir\r\n','','Nainggolan','',25,'22394','1217050810930001','sahat.gultom@del.ac.id','','','','',NULL,NULL,NULL,1,'A',3,'2014-09-08','2016-09-07','','','S',2,'','0000-00-00','','',1493,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:00:31'),(144,'iuihlb751fxa7hot66j1','Dian Ira Putri Hutasoit, A.Md','dian.hutasoit','0308140105','','',NULL,'DIH','Asisten Dosen','Pematangsiantar','1994-03-22',1,2,NULL,'089669420234','0632-331234','Jl. Farel Pasaribu no. 9 RT/RW/ 001/001 Kel. Pardamean,Kec. Siantar Marihat Kota Pematangsiantar\r\n','','Siantar Marihat','',2,'21128','1272056203940001','dian.hutasoit@del.ac.id','','','','',NULL,NULL,NULL,1,'A',3,'2014-09-08','2016-09-07','','','S',2,'','0000-00-00','','',1494,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 10:48:35'),(145,'9h2fqg4uizma5ku378vw','Elfira Utami Gultom, A.Md','elfira.gultom','0309140116','','',NULL,'EUG','Asisten Dosen','Medan','1993-07-26',1,2,NULL,'082276657954','0632-331234','Jl.Prof. Mr.M Hajairin Ling. I Kel Sibuluan Raya Kec. Pandan Kab. Tapanuli Tengah\r\n','','Pandan','',29,'22616','1201036607930004','elfira.gultom@del.ac.id','','','','',NULL,NULL,NULL,1,'A',3,'2014-09-15','2016-06-30','','','S',2,'','0000-00-00','','',1495,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:03:55'),(146,'y4dc882au2xn81b5ch7p','Rominda Gurning, A.Md','','0309140129','','',NULL,'RAG','','Lumban Gurning','1993-07-25',1,2,NULL,'082369585025','0632-331234','Lumban Bagot, Desa Lumban Gurning,Kec. Porsea Kab. Toba Samosir\r\n','','Porsea ','',3,'22384','1212076507930001','rominda.gurning@del.ac.id','','','','',NULL,NULL,NULL,1,'A',3,'2014-09-22','2016-08-31','','','S',2,'','0000-00-00','','',1937,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:10:43'),(147,'ovowecg2hdeqvzqfvhct','Nengsi Napitupulu, A.Md','','0309140117','','',NULL,'NIN','','Balige','1993-07-09',1,2,NULL,'081262915494','0632-331234','Jl. Napitupulu Bagasan No. 24 Desa/Kel Napitupulu Kec. Balige Kab. Tobasamosir\r\n\r\n','','Balige','',3,'22311','1212014907930015','nengsi.napitupulu@del.ac.id','','','','',NULL,NULL,NULL,1,'A',3,'2014-09-22','2017-04-01','','','S',2,'','0000-00-00','','',1933,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:06:15'),(148,'eduztjrmrijsete6ealy','Ellyas Alga Nainggolan, S.TP, M.Sc','ellyas.nainggolan','0310140127','','',NULL,'EAN','Dosen','Sidamanik','1990-07-10',1,1,2,'081361381167','0632-331234','Sarimatondang, Desa/Kel Sarimatondang Kec Sidamanik Kab. Simalungun\r\n','Jl Besar Sidamanik No 392 Sari Matontang\nKec. Sidamanik ','Sidamanik','Simalungun',27,'21171','120809100790002','ellyas.nainggolan@del.ac.id','','','','',NULL,NULL,NULL,1,'A',1,'2014-10-07','0002-12-02','Dingot Raja Nainggolan (Alm)','Saur Meriati Sidauruk','S',2,'','0000-00-00','','Guru',1496,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:30:42'),(149,'z90jaggrkjf77y4iml0f','Goretti Anna Maria Situmorang, A.Md','goretti.situmorang','0309140129','','',NULL,'GMS','Asisten Dosen','Balige','1993-12-05',1,2,4,'085373747268','0632-331234','Jl. Tandang Buhit Gg Cemara Desa/Kel. Pardede Onan Kec.Balige Kab. Tobasamosir\r\n','balige','Balige','tobasa',32,'22313','1212014512930002','gorettis.situmorang@del.ac.id','','','','',NULL,NULL,NULL,1,'A',3,'2014-10-07','2016-08-31','Sahala Situmorang','Darma A. Simanjuntak','S',2,'','0000-00-00','','Sopir',1497,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:28:10'),(150,'32az6z44jsggfehdobzj','Dr. Yosef Barita Sar Manik, S.T, M.Sc','yosef.manik','0310140126','','',NULL,'YMA','Dosen','Jambi','1975-05-20',2,1,1,'0813 1635 47','0632-331234','Jl. Setia 1-S No. 47 RT/RW 008/008 Desa Kel Jati Cempaka Kec. Pondok Gede Kota Bekasi\r\n','Jl. Matahari I No 51 \n','Pondok Gede','Jambi',38,'17111','3275082005750025','yosef.manik@del.ac.id','81','','','',NULL,NULL,NULL,NULL,'A',1,'2014-10-27','0002-12-02','Sabar Manik (Alm)','Elisabeth  Silitonga','M',1,'','0000-00-00','','Pensiunan Guru',1498,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:42:48'),(151,'jlsgroyx12gmmel5vz6i','Rotua Cyntia Dame Panjaitan, S.Kom, M.Pd','rotua.panjaitan','0311140130','','',NULL,'RCP','Dosen','Jakarta','1979-07-04',1,2,1,'081210673317','02129406144','Rawa Buaya, RT/RW 009/006 Kel. Duri Kosambi Kec. Cengkareng Jakarta Barat\r\n','Jl. Puskesmas I No 28 Kelurahan Duri Kosandi ','Cengkareng ','Jakarta Barat',42,'11750','3173014407790007','rotua.panjaitan@del.ac.id','','','','',NULL,NULL,NULL,1,'A',3,'2014-11-17','2016-01-31','Raden Panjaitan','Rumintang Naiborhu','S',2,'','0000-00-00','','-/Ibu Rumah Tangga',1499,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 10:51:56'),(152,'def6g70d3qxf6kr4hm68','I Gde Eka Dirgayussa, S.Pd,M.Si','eka.dirgayussa','0311140131','','',NULL,'GDE','Dosen','Lampung','1989-05-03',5,1,1,'085221347738','0632-331234','Dusun III RT/RW 016/003 Desa Mulyo Sari Kec. Pasir Sakti Kabupaten  Lampung Timur\r\n','Dusun III RT 016 RW 008 Mulyo Sari pasir Sakti ','Pasir Sakti','Lampung Timur',20,'34387','1807190305890005','eka.dirgayussa@del.ac.id','','','','',NULL,NULL,NULL,1,'A',1,'2014-11-25',NULL,'I Made Muliarsa','Sutarti','S',2,'','0000-00-00','','Wiraswasta/PNS Guru SD',1500,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 10:53:53'),(153,'i9vm3tu0qbyjehulngeo','Kamaruddin Manullang, A.Md','','0309130090','','',NULL,'KAM','Asisten Dosen','Dairi Bagasan','1992-04-21',1,1,4,'085270523621','0632-331234','Jl. Dairi Bagasan Desa/Kel Narumonda VIII Kec.Siantar Narumonda Kab.Toba Samosir\r\n','Dairi Bagasan, Narumonda VII, Kec.Siantar Narumonda ','Siantar Narumonda','TOBASA',3,'22384','1212142104920001','kama.manullang@del.ac.id','','','','',NULL,NULL,NULL,1,'A',3,'2013-09-17','2015-10-12','Ferdinand Manullang','Bontur Silaen','S',2,'','0000-00-00','','Petani',1932,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 09:46:02'),(154,'ep81qk6nh6kpk1qk6j8d','Josua Putra Silitonga, A.Md','','0309140114','','',NULL,'JPS','Asisten Dosen','Pematangsiantar','1993-08-25',1,1,NULL,'085262774694','0632-331234','Jl. Farel Pasaribu G.G Jambu Bol 2 RT/RW 002/003 Kel/Desa Sikamaju Kec. Siantar Marimbun Kota Pematangsinatar\r\n','Jl. Farel Pasaribu Gg. Jambu Bol No 2','Siantar Marimbun','Pematangsiantar',16,'21127','1272052508930003','josua.silitonga@del.ac.id','','','','',NULL,NULL,NULL,1,'A',3,'2014-10-01','2015-12-31','Nauli Mangihut Silitonga','Lusiana Sitorus','S',2,'','0000-00-00','','Wiraswasta',1931,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 16:21:40'),(155,'2tiuxelh8une9c1m8plg','Franky Parulian Silalahi, A.Md','','0309140111','','',NULL,'FRS','Asisten Dosen','Laguboti','1993-09-19',NULL,1,4,'085362613811','08126-0448993','','Sirajadeang Toruan Laguboti','','Toba Samosir',NULL,'22381','1212021909930001','franky.silalahi@del.ac.id','','','','',NULL,NULL,NULL,1,'A',3,'2014-10-01','2015-08-03','Runggu Silalahi','Udur Hutahaen','S',2,'','0000-00-00','','Petani',1980,0,NULL,NULL,NULL,NULL,'cori','2016-12-08 16:28:23'),(156,'qzux0hjbyxfrje94xywp','Anita Emi Kurniawati, S.Pd, M.Si','anita.kurniawati','0302150134','','',NULL,'EMI','Dosen','Sukoharjo','1989-05-11',3,2,4,'082328545303','','Ngemplak, RT/RW 003/003 Kel/Desa Gedangan Kec. Grogol Kab.Sukoharjo Jawa Tengah\r\n','Ngemplak RT/Rw1/1 Mayang Gatak Sukoharjo','Grogol ','Sukoharjo',20,'57552','3311095105890004','anita.kurniawati@del.ac.id','','','','',NULL,NULL,NULL,1,'A',3,'2015-02-02','2016-01-31','Sukardi Kardi Darsono','Sri Suwarni','M',1,'','0000-00-00','','Wiraswasta/Ibu Rumah Tangga',1501,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 10:55:54'),(157,'wyvtgntfod9vz3ljm54b','Juli Loisiana Butarbutar','juli.butarbutar','','','',NULL,'JLB','Dosen','Pematangsiantar','1987-07-28',NULL,2,2,'083840862352','085373240888','','Jl Kuala Tanjung No 9 Perdagangan Kec. Bandar','','Simalungun',NULL,'21184','1208236807870002','juli.butarbutar@del.ac.id','','','','',NULL,NULL,NULL,NULL,'A',3,'2015-02-02','0000-00-00','Alosius Butarbutar','Marsaulina Manurung','S',2,'','0000-00-00','','BUMN (Pens)/Ibu Rumah Tangga',1502,0,NULL,NULL,NULL,NULL,'cori','2016-04-13 11:48:31'),(158,'nmd83cdjg2jcpsk18762','Daniel Sitorus, A.Md','daniel.sitorus','0309130086','','',NULL,'DJR','Asisten Dosen','Medan','1992-02-06',1,1,2,'081397592120','0632-331234','Silaen Desa/Kel Sinta Dame Kecamatan Silaen Kabupaten Toba Samsoir\r\n','Jl Parsoburan Silaen','Silaen','Toba Samosir',3,'22382','1212030602920002','daniel.sitorus@del.ac.id','','','','',NULL,NULL,NULL,1,'A',3,'2013-09-17','2015-05-08','Godlife Sitorus','Meryati Situmeang [Alm]','S',2,'','0000-00-00','','Petani',1503,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 09:36:07'),(159,'2fh405v8rexzz7h72kib','Jonni Fridles Silaban, A.Md','jonni','0308140103','','',NULL,'JNS','Asisten Dosen','Sijaba','1989-11-08',1,1,4,'081396480648','0632-331234','Sijaba, Desa/Kel Siborongborong II Kec. Siborongborong Kab. Tapanuli Utara\r\n','Desa Siborongborong II Kec Siborongborong','Siborongborong','Tapanuli utara',4,'22474','1202090811890002','jonni@del.ac.id','','','','',NULL,NULL,NULL,1,'A',1,'2014-09-01',NULL,'Alusman Silaban','Helderia Togatorop','S',2,'','0000-00-00','','Petani',1504,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 10:44:11'),(160,'xuoqtlmikeq0i4v43tor','Santi Febri Arianti, S.Si, M.Sc','santi.febri','0302150135','','',NULL,'SFA','Dosen','Bandung','1985-02-04',3,2,4,'081263806626','0632-331234','Jl. Mendut V No. 51 RT/RW 002/018 Kel Melong Kec. Cimahi Selatan Kota Cimahi Jawa Barat\r\n','Jl. Mendut 5 No 51\nCimahi Selatan','Cimahi Selatan','Cimahi',20,'40535','3277014402850024','santi.febri@del.ac.id','','','','',NULL,NULL,NULL,1,'A',1,'2015-02-17',NULL,'Tedi Tjahyadi Patah','Silviana Agustami','S',2,'','0000-00-00','','Wiraswasta/Dosen',1505,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 10:59:26'),(161,'bm608y00vlg75luwph9s','Pandapotan Siagian, ST, M.Eng','pandapotan.siagian','0302150136','','',NULL,'PDS','Dosen','Huta Gurgur Selatan','1974-03-18',1,1,3,'085266060168','0632-331234','Mekar Jaya RT 010 Kel/Desa Mekar Jaya Kec. Sungai Gelam Kab. Muaro Jambi \r\n','Jl. Kepodang V Perumnas Kota Baru No 128',' Sungai Gelam','Jambi',20,'36373','1505081803730004','pandapotan.siagian@del.ac.id','','','','',NULL,NULL,NULL,1,'A',1,'2015-02-27',NULL,'Sirus Siagian','Dingin Hutagaol','M',1,'','0000-00-00','','Wiraswasta',1506,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 11:03:09'),(162,'tv4axtv5no3xa6e84n4d','Indra Sarito Lumban Tobing, S.Pd','indra','0307120060','','',NULL,'IST','','Tarutung','1988-04-17',1,2,3,'085261482254','0632-331234','Jl. DI Panjaitan No. 97 \r\n','Kampus Del','Sibolga Utara','',1,'22514','1273011704880001','Indra@del.ac.id','','','','',NULL,NULL,NULL,4,'A',1,'2012-07-09',NULL,'','','S',2,'','0000-00-00','','',1507,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-11 17:01:43'),(163,'af2uamwpb982ehcin0s2','Rusneni Vitaria Purba, S.Hut','','0310130090','','',NULL,'RVP','','Kabanjahe','1986-05-20',1,2,2,'085262288543','0632-331234','Soposurung Kel Sangkar Nihuta Kecamatan Balige Kab tobasamosir\r\n','Kabanjahe','Balige','Tanah Karo',3,'22312','1212016005860003','rusneni.vitaria@del.ac.id','','','','',NULL,NULL,NULL,1,'A',1,'2013-10-24',NULL,'','','S',2,'','0000-00-00','','',1938,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 09:53:38'),(164,'dsds98okvlb044n2r09p','Anthon  Roberto Tampubolon, S.Kom, M.T','anthon.roberto','0307150142','','',NULL,'ART','Dosen','Kupang','1981-09-19',1,1,4,'081231079870','','Jl. Perintis Kemerdekaan RT 026/RW 011 Kel. Kelapa Lima Kec. Kelapa Lima Kota Kupang\r\n','Jl Perintis Kemerdekaan Kelapalima Kupang',' Kelapa Lima','Kupang',20,'85221','5371031909810004','bang.anthon@gmail.com, anthon.roberto@del.ac.id','','','','',NULL,NULL,NULL,1,'A',1,'2015-07-01',NULL,'Charles Tampubolon','Magdalena Djami','M',1,'','0000-00-00','','PNS Dinas PU/Ibu Rumah Tangga',1508,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 11:12:04'),(165,'vbtv6for4h55hbwx6djv','Dr. Sony Adhi Susanto, S.Si, M.Sc','sony.susanto','0307150141','','',NULL,'SSU','Dosen','Jakarta','1979-10-07',2,1,1,'081282170370','','Jl. Wijaya Kusuma II/1 RT 001/RW 014 Kel. Cilendek Barat Kec. Kota Bogor Barat Kota Bogor Jawa Barat\r\n','Jl wijaya Kusuma II/I Cilendek Barat Bogor',' Kota Bogor Barat','Bogor',20,'16112','3271040710790016','sonyadhisusanto@outlook.com, sony.susanto@del.ac.id','','','','',NULL,NULL,NULL,1,'A',1,'2015-07-01',NULL,'Johannes Edy Subagio','Veronika Utari','M',1,'','0000-00-00','','Pega Pertamina/Ibu Rumah tangga',1509,0,NULL,NULL,NULL,NULL,'melva.hutagalung','2018-07-12 11:10:01'),(166,NULL,'Deni Elvin Hutagaol, ST',NULL,'0307120061',NULL,NULL,NULL,'DEH',NULL,'Medan','2015-08-15',1,1,2,'081362463626','0632-331234','Jl. Sisingamaraja No. 95  \r\n',NULL,'Laguboti',NULL,32,'22381','1212121008800016',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,NULL,1,'2012-07-09',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,1918,0,NULL,NULL,'cori','2015-08-24 14:08:27','melva.hutagalung','2018-07-11 17:02:58'),(167,NULL,'Alexander Lumbantobing, A.Md',NULL,'0309140116',NULL,NULL,NULL,'ALT',NULL,'SIBOLGA','1993-09-22',1,1,4,'085361663999','0632-331234','Jl. Sibolga Baru No. 53 Kel/Desa Pancuran Kerambil, Kec. Sibolga Sambas Kota Sibolga\r\n',NULL,'Pancuran Kerambi',NULL,1,'22531','1273042209930001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,3,'2014-10-01','2016-06-30',NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1922,0,NULL,NULL,'root','2015-08-25 13:41:39','melva.hutagalung','2018-07-11 16:24:33'),(168,NULL,'Ruth Hutabarat, A.Md',NULL,'0309140115',NULL,NULL,NULL,'RMH',NULL,'Pematangsiantar','2015-03-17',1,2,4,'082168583281','0632-331234','Jl. Seribodolok RT/RW 014/005 Desa/Kel Nagahuta Kec. Siantar Marimbun Kota Pematangsiantar\r\n',NULL,'Siantar Marimbun',NULL,2,'21129','1272055703930001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,3,'2014-10-01','2016-01-31',NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1920,0,NULL,NULL,'root','2015-08-25 13:46:06','melva.hutagalung','2018-07-11 16:23:11'),(169,NULL,'Rudy Samuel Pardosi, A.Md',NULL,'0309140112',NULL,NULL,NULL,'RSP',NULL,'Dili','1993-06-06',1,1,4,'081362163184','0632-331234','Lumban Sianipar, Kel/Desa Pardede Onan Kec.Balige Kab.Toba Samosir\r\n\r\n\r\n',NULL,'Balige',NULL,32,'22313','1212010606930001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,3,'2014-09-15','2016-06-30',NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1921,0,NULL,NULL,'root','2015-08-25 13:53:41','melva.hutagalung','2018-07-11 16:17:16'),(170,NULL,'Elni Enita Manurung, A.Md',NULL,'0309140113',NULL,NULL,NULL,'EEM',NULL,'Balige','1993-10-21',1,2,4,'082168583219','0632-331234','Sosor Gurugur, Kel/Desa Pardede Onan Kec.Balige Kab Toba Samosir\r\n\r\n',NULL,'Balige',NULL,3,'22313','1212016110930004',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,3,'2014-09-15','2016-01-31',NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1923,0,NULL,NULL,'root','2015-08-25 13:57:02','melva.hutagalung','2018-07-11 16:19:15'),(171,NULL,'Ernie Bertha Nababan, S.Pd, M.Pd',NULL,'0308150145',NULL,NULL,NULL,'EBN',NULL,'Siambolas','1986-08-30',1,2,4,'085262950312','','Siambolas, Desa/Kel Paniaran,Kec Siborongborong Kab Tapanuli Utara\r\n',NULL,'Siborongborong',NULL,4,'22381','1202097005860004',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2015-08-04',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1919,0,NULL,NULL,'cori','2015-08-27 11:54:04','melva.hutagalung','2018-07-12 11:23:59'),(172,NULL,'Joki Andi Nababan, A.Md',NULL,'0310140136',NULL,NULL,NULL,'JAN',NULL,'Pearaja','1988-03-18',1,1,4,'085276516795','','Pearaja, Kel/Desa Sitabotabo, Kec Siborongborong Kab. Taapnuli  Utara\r\n',NULL,'Siborongborong',NULL,4,'22381','120209180388.000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2014-10-02',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1924,0,NULL,NULL,'root','2015-08-27 16:59:13','cori','2015-09-16 09:34:43'),(173,NULL,'Yuniarta Basani, S.Si, M.Si',NULL,'0309150148',NULL,NULL,NULL,'YBN',NULL,'Palangkaraya','1989-06-17',1,2,4,'085247399127','0632-331234','Jl. Rajawali II/B GG Sejahtera No. 51 RT 005/ RW 002\r\n',NULL,'Jekan Raya',NULL,37,'73112','6271035706860002',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2015-09-07',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1925,0,NULL,NULL,'cori','2015-09-08 13:10:12','melva.hutagalung','2018-07-12 11:29:59'),(174,NULL,'Nora Nastity Simanjuntak, S.Pd',NULL,'0307120059',NULL,NULL,NULL,'NNS',NULL,'Balige','1986-01-12',1,2,NULL,'081375287672','0632-331234','Jl. Bungalow No. 22 B Simpang RSUP HAM\r\n',NULL,'Medan Timur',NULL,14,'20235','1207264212870006',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,NULL,1,'2012-07-05',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,1940,0,NULL,NULL,'cori','2015-09-21 09:26:43','melva.hutagalung','2018-07-11 17:00:08'),(175,NULL,'Anthony L Tampubolon, S.Pd',NULL,'0308110039',NULL,NULL,NULL,'ANT',NULL,'Jakarta','1988-07-08',1,1,NULL,'082366502238','0632-331234','Sitampulak Desa sibolahotongas Kec. Balige Kab. Toba Samosir\r\n',NULL,'Balige',NULL,3,'22312','1212010807880001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,NULL,3,'2011-08-10','2016-11-01',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,1941,0,NULL,NULL,'cori','2015-09-21 10:06:53','melva.hutagalung','2018-07-11 16:23:12'),(176,NULL,'Melva Sabar Maria Hutagalung, S.Sos',NULL,'0304120054',NULL,NULL,NULL,'MEL',NULL,'Medan','1983-05-27',1,2,NULL,'081260373330','0632-331234','Jl. Sawit 1 No. 11 P.Simalingkar Kota Medan\r\n',NULL,'Medan',NULL,14,'20135','1271076705830000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,NULL,1,'2012-04-03',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,1942,0,NULL,NULL,'cori','2015-09-21 10:10:19','melva.hutagalung','2018-07-11 16:52:56'),(177,NULL,'Rintha Meylisa Gulo, SE',NULL,'0304140096',NULL,NULL,NULL,'RMG',NULL,'Batangserangan','1990-05-17',1,2,NULL,'085270049535','0632-331234','Jl. Dusun IV Sei Tasik Kel/Desa Sei Litur Tasik Kec. Sawit Seberang Kabupaten Langkat\r\n',NULL,'Sawit Seberang',NULL,21,'20811','1205205705900001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2014-04-01',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1943,0,NULL,NULL,'cori','2015-09-21 10:12:49','melva.hutagalung','2018-07-12 10:16:12'),(178,NULL,'Osin Verawati Nainggolan, S.Sos ',NULL,'0310130089',NULL,NULL,NULL,'OVN',NULL,'Medan','1989-07-29',2,2,NULL,'085763649545','0632-331234','Jl. Pintu Air IV No 316 Kelurahan Kwala Bekala Kecamatan Medan Johor Kota Medan\r\n',NULL,'Medan Johor',NULL,14,'20142','1271116007890003',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2013-10-21',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1944,0,NULL,NULL,'cori','2015-09-21 10:22:04','melva.hutagalung','2018-07-12 09:51:57'),(179,NULL,'Friska Silaban, A.Md',NULL,'0308140102',NULL,NULL,NULL,'FKS',NULL,'Siborongborong','1992-02-16',1,2,NULL,'085362286316','0632-331234','Silaitlait Jompo Kel Silaitlait Kec. Siborongborong Kab Tapanuli Utara\r\n',NULL,'Siborongborong',NULL,4,'22474','1202095602920006',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2014-09-01',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1945,0,NULL,NULL,'cori','2015-09-21 10:28:04','melva.hutagalung','2018-07-12 10:45:22'),(180,NULL,'Ernawati Yohana Doloksaribu, S.Th',NULL,'0305150140',NULL,NULL,NULL,'EYD',NULL,'Laguboti','1991-03-28',1,2,NULL,'085275840209','','Jl. Sangnawaluh No. 06 Komplek STT HKBP Kel Pahlawan Siantar Timur Kota Pematangsiantar\r\n',NULL,'Siantar Timur',NULL,2,'21132','1272016803910005',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,3,'2015-05-01',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1946,0,NULL,NULL,'cori','2015-09-21 10:31:00','melva.hutagalung','2018-07-12 11:08:20'),(181,NULL,'Pdt. Sufrando Simanjuntak, S.Th',NULL,'0308140104',NULL,NULL,NULL,'SSK',NULL,'Pematangsiantar','1988-01-29',1,1,NULL,'081263560821','0632-331234','Jl. Permosi No. 21 RT/RW 029/014 Kel. Siopat Suhu Kec. Siantar Timur Kota Pematangsiantar\r\n',NULL,'Siantar Timur',NULL,2,'21139','1272012801880004',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2014-09-02',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1947,0,NULL,NULL,'cori','2015-09-21 11:03:36','melva.hutagalung','2018-07-12 10:46:54'),(182,NULL,'Pdt. Rini Hutajulu, S.Th',NULL,'0305130072',NULL,NULL,NULL,'RNH',NULL,'Laguboti','1981-06-08',1,2,NULL,'081370859456','0632-331234','Dusun II Gagang Abadi No. 15 D\r\n',NULL,'Sunggal',NULL,8,'20351','1207234806860001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2013-05-21',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,1948,0,NULL,NULL,'cori','2015-09-21 11:09:22','melva.hutagalung','2018-07-12 09:17:06'),(183,NULL,'Pdt. Nelson Situmeang, S.Th',NULL,'0305140098',NULL,NULL,NULL,'NLS',NULL,'Jakarta','1991-01-12',1,1,NULL,'081398679697','0632-331234','Jl. Sangnawaluh No. 06 Komplek STT HKBP Kel Pahlawan Siantar Timur Kota Pematangsiantar\r\n',NULL,'Siantar Timur',NULL,2,'21132','1272011201910008',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2014-08-18',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1949,0,NULL,NULL,'cori','2015-09-21 11:13:03','melva.hutagalung','2018-07-12 10:37:22'),(184,NULL,'Dairi Ivan Rinaldi Sihombing, S.Th',NULL,'0308140104',NULL,NULL,NULL,'DIS',NULL,'Sidikalang','1991-09-30',1,1,NULL,'085361713435','0632-331234','Jl. Gereja No. 29 Kel. Sei Agul Kec. Medan Barat Kota Medan\r\n',NULL,'Medan Barat',NULL,14,'20117','1271053009910002',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,3,'2014-08-04',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1950,0,NULL,NULL,'cori','2015-09-21 11:16:24','melva.hutagalung','2018-07-12 10:33:39'),(185,NULL,'Vita Sani Adelina Hutapea, S.Th',NULL,'0305140099',NULL,NULL,NULL,'VSH',NULL,'Pematangsiantar','1992-01-14',1,2,NULL,'081361056721','0632-331234','Jl. Sangnawaluh No. 06 Komplek STT HKBP Kel Pahlawan Siantar Timur Kota Pematangsiantar\r\n',NULL,'Siantar Timur',NULL,2,'21132','1272015401920001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2014-05-05',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1951,0,NULL,NULL,'cori','2015-09-21 11:18:33','melva.hutagalung','2018-07-12 10:20:38'),(186,NULL,'Pdt. Ria Rapina Dodent Simanjuntak, S.Th',NULL,'0307150144',NULL,NULL,NULL,'RRS',NULL,'Sungai Guntung','1990-04-01',1,2,NULL,'085278670262','','Jl. Air Bersih Gg Bawal No. 2 RT/RW 15 Kel. Teluk Binjai Kec. Dumai Timur Kota Dumai\r\n',NULL,'Dumai Timur',NULL,20,' 2881','1472024104900001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2015-07-29',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1952,0,NULL,NULL,'cori','2015-09-21 11:22:04','melva.hutagalung','2018-07-12 11:17:33'),(187,NULL,'Pdt. Hendra Siregar, S.Th.',NULL,'0308150146',NULL,NULL,NULL,'HDS',NULL,'Afd A Tobasari','1986-10-26',1,1,NULL,'081260597380','','Afd A Tobasari\r\n',NULL,'Pematang Sidamanik',NULL,27,'21171','1208102610880001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2015-08-19',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1953,0,NULL,NULL,'cori','2015-09-21 11:25:20','melva.hutagalung','2018-07-12 11:26:24'),(188,NULL,'Berlian Simanjuntak, S.Si',NULL,'0306140100',NULL,NULL,NULL,'BLS',NULL,'Bosar Majawa','1989-07-06',1,2,NULL,'085270958519','0632-331234','Jl. Lancang Kuning RT/RW 002/006 Kel/Desa Bagan Batu Kec. Bagan Sinembah Kab Rokan Hilir\r\n',NULL,'Bagan Sinembah',NULL,20,'','1407054607890004',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2014-09-01',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1955,0,NULL,NULL,'root','2015-09-21 11:37:07','melva.hutagalung','2018-07-12 10:41:44'),(189,NULL,'Yohanssen Pratama, S.Si, M.T',NULL,'0309150151',NULL,NULL,NULL,'YHP',NULL,'Bandung','1987-05-21',1,1,2,'085710000043','0632-331234','AH Nasution No. 55 Jamaras RT 002, RW 002, Jatihandap',NULL,'Mandalajati',NULL,NULL,'40195','3273302105870003',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2015-09-29',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1956,0,NULL,NULL,'cori','2015-10-01 10:58:09','melva.hutagalung','2018-07-12 11:33:45'),(190,NULL,'Eka Trisno Samosir, S.Kom, PGDip',NULL,'0310150153',NULL,NULL,NULL,'ETS',NULL,'Mela','1986-08-10',1,1,2,'081293694603','0632-331234','Dusun III, Aek Lobu',NULL,'TAPIAN NAULI',NULL,29,'22618','1201071009860002',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,4,'2015-10-05',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1957,0,NULL,NULL,'cori','2015-10-13 08:49:38','melva.hutagalung','2018-07-12 11:36:31'),(191,NULL,'Mukhammad Solikhin, S.Si, M.Si',NULL,'0311150160',NULL,NULL,NULL,'MSL',NULL,'Pasuruan','1990-08-31',3,1,2,'085731191411','','Simo Mulyo Baru/ 2-E/23\r\n',NULL,'SUKOMANUNGGAL',NULL,20,'60181','3578273108900001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2015-11-16',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,1958,0,NULL,NULL,'cori','2015-11-18 09:25:53','melva.hutagalung','2018-07-12 11:45:53'),(192,NULL,'Pandapotan Napitupulu, S.T, M.T ',NULL,'0301160162',NULL,NULL,NULL,'PDN',NULL,'SIDIKALANG','1982-12-30',1,1,2,'081221726666','','KP. MANGLID MARGAHAYU SELATAN RT 006/ RW 010, DESA MARGAHAYU SELATAN',NULL,'MARGAHAYU',NULL,20,'40226','3204093012820003',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2016-01-18',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,1959,0,NULL,NULL,'cori','2016-01-25 14:46:45','melva.hutagalung','2018-07-12 11:54:00'),(193,NULL,'Andy Trirakhmadi, S.T, M.T',NULL,'0310150154',NULL,NULL,NULL,'ATR',NULL,'Rumbai','1989-12-11',3,1,1,'085294311196','0632-331234','Ujungberung Indah A 9 A, RT 007/RW 011, Desa Cigending',NULL,'Ujungberung',NULL,20,'40611','3273261112890001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2015-10-19',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1962,0,NULL,NULL,'cori','2016-02-01 15:29:52','melva.hutagalung','2018-07-12 11:38:41'),(194,NULL,'Hadi Sutanto Saragi, S.T, M.Eng',NULL,'0302160165',NULL,NULL,NULL,'HSS',NULL,'Tarutung','1991-09-09',2,1,3,'085295001691','','Jl. DR.TD. Pardede Lorong IV A, Hutatoruan VII',NULL,'Tarutung',NULL,4,'22413','1202010909910002',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2016-02-01',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1961,0,NULL,NULL,'cori','2016-02-01 17:09:45','melva.hutagalung','2018-07-12 13:13:13'),(195,NULL,'Rien Rakhmana, S.Si, M.T',NULL,'0311150155',NULL,NULL,NULL,'RIR',NULL,'Dumai','1987-05-21',3,2,4,'081220841987','0632-331234','Jl. Pemuda Laut No. 8 A, RT 003\r\n',NULL,'Dumai Barat',NULL,20,'28824','1472016105870021',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2015-11-09',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1964,0,NULL,NULL,'cori','2016-02-02 11:59:08','melva.hutagalung','2018-07-12 11:41:43'),(196,NULL,'Rikardo Simanjuntak, S.Si',NULL,'0302160164',NULL,NULL,NULL,'RKS',NULL,'Bosar Majawa','1987-11-07',1,1,4,'081365220715','','Jl. Lancang Kuning, Rt 002/ RW 006, Bagan Batu',NULL,'Bagan Sinembah',NULL,20,'28992','1407050711870002',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2016-02-01',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1963,0,NULL,NULL,'cori','2016-02-02 14:12:57','melva.hutagalung','2018-07-12 13:12:02'),(197,NULL,'Olnes Yosefa Hutajulu, S.Pd., M.Eng',NULL,'0303160166',NULL,NULL,NULL,'OYH',NULL,'Gondang Rejo','1989-08-30',1,1,NULL,'082166436925','','Komp Address Cempaka Madani (ACM) Blok 1 No. 12, Tanjung Gusta',NULL,'Medan Helvetia',NULL,22,'20125','1271153008890005',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2016-03-01',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1976,0,NULL,NULL,'cori','2016-03-01 14:22:01','melva.hutagalung','2018-07-12 13:14:14'),(198,NULL,'Ike Fitriyaningsih, S.Si., M.Si',NULL,'0303160167',NULL,NULL,NULL,'IFY',NULL,'Lamongan','1990-04-09',3,2,2,'081231523483','','Kembangbahu, RT 003/RW 003, Kembangbahu',NULL,'Kembangbahu',NULL,20,'62282','3524194904900001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2016-03-01',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1977,0,NULL,NULL,'cori','2016-03-01 14:33:54','melva.hutagalung','2018-07-12 13:16:27'),(199,NULL,'Christin Erniati Panjaitan, ST., M.Sc',NULL,'0303160168',NULL,NULL,NULL,'CEP',NULL,'Sungai Pakning','1988-01-14',1,2,NULL,'082277742995','','Jl. Setia LK III No. 8 C, Tanjung Gusta',NULL,'Medan Helvetia',NULL,22,'20125','127103540188000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2016-03-15',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1982,0,NULL,NULL,'cori','2016-03-18 09:15:00','melva.hutagalung','2018-07-12 13:17:38'),(200,NULL,'Letkol Arh. Drs. Japorman Purba, M.M.',NULL,'030416K030',NULL,NULL,NULL,'JAP',NULL,'Dolok Sanggul','1962-07-13',1,1,NULL,'08126812555','','Jl. Sisingamangaraja',NULL,'Sibolga Selatan',NULL,17,'22533','1273031307620001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,3,'2016-04-11','2017-03-01',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,1984,0,NULL,NULL,'cori','2016-04-12 15:51:46','melva.hutagalung','2018-07-12 13:19:15'),(201,NULL,'Devis Wawan Saputra, ST., MBA',NULL,'0305160170',NULL,NULL,NULL,'DWS',NULL,'Pematang Siantar','1986-06-30',1,1,4,'081293217351','','Dusun III Desa Sei Rotan RT/RW \r\n',NULL,'Percut Sei Tuan ',NULL,8,'0','1207263006860006',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2016-05-23',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1985,0,NULL,NULL,'cori','2016-06-01 10:57:22','melva.hutagalung','2018-07-12 13:28:20'),(202,NULL,'Indra Hartarto Tambunan, Ph.D.',NULL,'0306160171',NULL,NULL,NULL,'IHT',NULL,'Muara','1984-04-28',1,1,NULL,'081213830680','','Jl. Pacuan RT/RW 000/000 \r\n',NULL,'Siborongborong',NULL,4,'22461','1202092804840002',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2016-06-06',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1986,0,NULL,NULL,'cori','2016-06-27 14:25:42','melva.hutagalung','2018-07-12 13:30:17'),(203,NULL,'Pdt. Freddy Sahputra Simaremare, S.Th',NULL,'0308160174',NULL,NULL,NULL,'FSS',NULL,'Dolok Masihul','1989-06-22',1,1,NULL,'082240110672','','Dusun 1 , Batu 12, ',NULL,'Dolok Masihul',NULL,26,'20991','1218092206890003',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2016-08-03',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1990,0,NULL,NULL,'cori','2016-08-19 09:01:10','melva.hutagalung','2018-07-12 13:33:14'),(204,NULL,'Pdt. Nurni Matogu Siahaan, S.Th',NULL,'0308160175',NULL,NULL,NULL,'NMS',NULL,'Bahkata','1988-02-14',1,2,NULL,'085361182107','','Bahkata, Janggir Leto, ',NULL,'Panei',NULL,27,'21161','1208045402880001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2016-08-03',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1988,0,NULL,NULL,'cori','2016-08-19 09:09:27','melva.hutagalung','2018-07-12 13:34:18'),(205,NULL,'Pdt. Dian Paulus H.N. Tambunan, S.Th',NULL,'0308160176',NULL,NULL,NULL,'DPT',NULL,'Bandung','1991-05-14',1,1,NULL,'081370352167','','Jl. Sangnawaluh, No 6 Komplek STT, Pahlawan',NULL,'Siantar Timur',NULL,16,'21151','1272010405910005',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2016-08-03',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1989,0,NULL,NULL,'cori','2016-08-19 10:18:45','melva.hutagalung','2018-07-12 13:37:42'),(206,NULL,'Pdt. Eska Hastuty Silitonga, S.Th',NULL,'0308160177',NULL,NULL,NULL,'EHS',NULL,'Sipahutar','1990-09-11',1,2,NULL,'085361059600','','Jl. Sangnawaluh, No 6 Komplek STT, Pahlawan',NULL,'Siantar Timur',NULL,16,'21151','1272015109900004',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2016-08-03',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,1987,0,NULL,NULL,'cori','2016-08-19 10:28:32','melva.hutagalung','2018-07-12 13:38:38'),(207,NULL,'Evi Rahmadani Ompusunggu, S.Pd',NULL,'0309160180',NULL,NULL,NULL,'ERO',NULL,'Medan','1993-08-09',1,2,4,'082165413640','','Jl. Bantan No 129 Medan\r\n',NULL,'Medan Tembung',NULL,14,'20224','1271144908930003',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2016-09-01',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,2557,0,NULL,NULL,'cori','2016-09-05 11:48:18','melva.hutagalung','2018-07-12 13:42:46'),(208,NULL,'Yulisa Lestari., S.Si, M.T',NULL,'0306160172',NULL,NULL,NULL,'YUL',NULL,'Sicincin','1986-03-17',3,2,4,'085263597739','','Komp Kharismatama Permai Blok C No. 1\r\n',NULL,'Lubuk Buaya',NULL,20,'25172','1371115703860002',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2016-06-20',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,2556,0,NULL,NULL,'cori','2016-09-05 12:02:55','melva.hutagalung','2018-07-12 13:31:44'),(209,NULL,'Nenni Mona Aruan, S.Pd., M.Si',NULL,'0309160179',NULL,NULL,NULL,'NMA',NULL,'Aruan','1990-10-28',1,2,1,'081322706176','','Desa Aruan Pangasean',NULL,'Laguboti',NULL,3,'22381','1212026810900002',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2016-09-01',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,2555,0,NULL,NULL,'cori','2016-09-05 12:07:07','melva.hutagalung','2018-07-12 13:41:50'),(210,NULL,'Dr. Ir. Bambang S.P. Abednego',NULL,'0309160181',NULL,NULL,NULL,'BSP',NULL,'Yogyakarta','1949-03-08',1,1,NULL,'081222397708','','Gegerkalong Tonggoh 1 No 11-A, RT/RW 002/008 , Geger Kalong',NULL,'Sukasari',NULL,20,'40153','3273010106500003',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2016-09-01',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,2558,0,NULL,NULL,'cori','2016-09-05 16:51:54','melva.hutagalung','2018-07-12 13:43:28'),(211,NULL,'Fernando Mula Tua Nainggolan, S.Si',NULL,'0309160182',NULL,NULL,NULL,'FMN',NULL,'Medan','1992-11-16',1,1,NULL,'085361610797','','Jl. Taduan No. 105, Sidorejo',NULL,'Medan Tembung',NULL,22,'22024','1271141611920002',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2016-09-13',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,2622,0,NULL,NULL,'cori','2016-09-13 17:45:12','melva.hutagalung','2018-07-12 13:44:04'),(212,NULL,'Laba Berutu, A.Md',NULL,'0308160178',NULL,NULL,NULL,'LBB',NULL,'Lae Langge','1991-02-18',1,1,NULL,'085363569124','','Lae  Langge, Lae Langge Namuseng',NULL,'Sitellu Tali Urang Julu',NULL,20,'22272','1215041802910001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2016-08-22',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,2623,0,NULL,NULL,'cori','2016-09-19 15:03:35','melva.hutagalung','2018-07-12 13:40:53'),(213,NULL,'Frengki Sardion Simatupang, A.Md',NULL,'0309160183',NULL,NULL,NULL,'FST',NULL,'Sosor Gabetua','1994-10-20',1,1,NULL,'082360456288','','Desa Purba Manalu',NULL,'Dolok Sanggul',NULL,9,'22457','121606201094000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2016-09-19',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,2621,0,NULL,NULL,'cori','2016-09-20 17:34:19','melva.hutagalung','2018-07-12 13:45:22'),(214,NULL,'Ritcan Maruli Pandapotan Hutahaean, A.Md',NULL,'0309160184',NULL,NULL,NULL,'RMH',NULL,'Bandung','1995-11-03',1,1,2,'081377001025','','Gompar Patuan, Simatibung, Laguboti, Toba Samosir, Sumatera Utara\r\n',NULL,'Laguboti',NULL,3,'22381','1212020311950001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2016-09-22',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,3099,0,NULL,NULL,'cori','2016-09-22 09:29:22','melva.hutagalung','2018-07-12 13:46:38'),(215,NULL,'Christoper Janwar Saputra Sinaga',NULL,'',NULL,NULL,NULL,'CJS',NULL,'Muara Enim','1988-01-13',1,1,2,'081221339033','','Institut Teknologi Del\r\nJl. Sisingamangaraja Sitoluama',NULL,'Laguboti',NULL,3,'22381','1603021301880001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2016-11-16',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,2628,0,NULL,NULL,'christoper.sinaga','2017-01-05 06:59:34','christoper.sinaga','2017-01-05 06:59:34'),(216,NULL,'Sondang Yunita Malau, SE',NULL,'0310160223',NULL,NULL,NULL,'SYM',NULL,'Pontianak','1987-05-22',1,2,NULL,'081376650690','','Jl. Setiabudi Ps II GG Melati No. 30\r\n',NULL,'Medan Selayang',NULL,14,'','121708620587000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,3,'2016-10-10','2018-04-10',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,2627,0,NULL,NULL,'melva.hutagalung','2017-01-18 15:56:56','melva.hutagalung','2018-07-11 09:15:50'),(217,NULL,'Joas Putra Saragih, S.Tr.Kom',NULL,'0310160222',NULL,NULL,NULL,'JPS',NULL,'Pematang Siantar','1993-10-16',1,1,NULL,'081370983747','','Jl. Sarinembah No. 60 RT/RW 011/005\r\n',NULL,'Siantar Selatan',NULL,16,'21125','1272041610930001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,3,'2016-10-03','2017-09-02',NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,2630,0,NULL,NULL,'melva.hutagalung','2017-01-18 16:36:54','melva.hutagalung','2018-07-11 09:10:52'),(218,NULL,'M. Yusuf Hakim Widianto,  M.Si, M.Sc.',NULL,'0302170240',NULL,NULL,NULL,'YUS',NULL,'Lamongan','1991-06-20',3,1,3,'082236201006','','TAMBAKBOYO, RT 003/001, TAMBAKRIGADUNG',NULL,'TIKUNG',NULL,20,'62281','3524232006910003',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,3,'2017-02-01','2018-02-01',NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,2631,0,NULL,NULL,'cori','2017-02-03 08:40:00','melva.hutagalung','2018-07-11 09:45:48'),(219,NULL,'Maisevli Harika, M.T, M.Eng.',NULL,'0302170241',NULL,NULL,NULL,'MAI',NULL,'PADANG','2017-04-21',3,1,4,'081320158305','','KP. KALAWI LB. LINTAH, RT/RW 001/005, LUBUK LINTAH',NULL,'KURANJI',NULL,20,'25153','1371092104860011',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,3,'2017-02-01','2018-02-01',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,2632,0,NULL,NULL,'cori','2017-02-03 08:52:13','melva.hutagalung','2018-07-11 09:49:43'),(220,NULL,'Rismal, S.Pd., M.Si',NULL,'0302170242',NULL,NULL,NULL,'RML',NULL,'Belawa','1990-04-28',3,1,2,'','082299091782','Wele II RT/RW 002/002\r\n',NULL,'Belawa',NULL,20,'90953','7313072804900001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,4,'2017-02-06','2018-01-29',NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,2633,0,NULL,NULL,'fidelis','2017-02-07 14:42:45','melva.hutagalung','2018-07-11 09:52:01'),(221,NULL,'Charla Tri Selda Manik, ST., M.Eng',NULL,'',NULL,NULL,NULL,'CTM',NULL,'Medan','1988-10-03',1,2,NULL,'085370396452','','Jl. Tempirai Lestari IX No. 190 Blok V, Griya Martubung',NULL,'',NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2017-02-09',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,2634,0,NULL,NULL,'melva.hutagalung','2017-02-10 09:47:58','melva.hutagalung','2017-02-10 09:47:58'),(222,NULL,'Prof.Ir. Togar M. Simatupang, M.Tech., Ph.D.',NULL,'010212K008',NULL,NULL,NULL,'TMS',NULL,'Pematangsiantar','1968-12-31',1,1,3,'0811205815','0632-331234','Jl. Sukajadi No. 107 RT/RW 002/009 Kel. Cipedes Kec. Sukajadi Kota Bandung\r\n',NULL,'Sukajadi',NULL,20,'40162','3273073112680008',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2016-11-01',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,3104,0,NULL,NULL,'cori','2017-04-10 12:15:29','melva.hutagalung','2018-07-12 09:58:19'),(223,NULL,'Hotma Sinaga',NULL,'0306120272',NULL,NULL,NULL,'HOT',NULL,'sibolga','1985-04-14',2,1,1,'085263011082','','Sibuea\r\n',NULL,'Laguboti',NULL,3,'22381','1201201404850001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,NULL,1,'2012-06-12',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,2642,0,NULL,NULL,'melva.hutagalung','2017-07-17 18:42:09','melva.hutagalung','2018-07-11 10:18:48'),(224,NULL,'Ahmad Fauzi, S.Si., M.T.I.',NULL,'0307170255',NULL,NULL,NULL,'AHF',NULL,'Bogor','1988-05-09',3,1,2,'085256086446','085694303506','Jl. H. Amat No.3 RT/RW 005/001\r\n',NULL,'Beji',NULL,20,'','3276060905880005',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,3,'2017-07-10','2018-10-01',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,2643,0,NULL,NULL,'melva.hutagalung','2017-07-26 17:51:37','melva.hutagalung','2018-07-11 09:59:27'),(225,NULL,'Monita Pasaribu, S.Si., MT.',NULL,'0307170254',NULL,NULL,NULL,'MNP',NULL,'Rantauprapat','1986-08-17',1,2,4,'081322843286','','Jl. Urip Sumodiharjo Gg. Bogor RT/RW 002/001\r\n',NULL,'Rantau Utara',NULL,19,'','1210015708860004',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2017-07-11',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,2644,0,NULL,NULL,'melva.hutagalung','2017-07-26 17:58:31','melva.hutagalung','2018-07-11 09:56:25'),(226,NULL,'Berlin Napitupulu',NULL,'0309010161',NULL,NULL,NULL,'',NULL,'',NULL,1,1,NULL,'','','',NULL,'',NULL,NULL,'','121211100981001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,NULL,1,'2001-08-01',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,2645,0,NULL,NULL,'melva.hutagalung','2017-08-03 14:14:07','melva.hutagalung','2018-01-26 17:47:18'),(227,NULL,'Rizal Pangaribuan',NULL,'0305060172',NULL,NULL,NULL,'',NULL,'',NULL,1,1,NULL,'','','',NULL,'',NULL,NULL,'','1212020807790001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,NULL,NULL,'2005-05-05',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,2646,0,NULL,NULL,'melva.hutagalung','2017-08-03 14:15:49','melva.hutagalung','2018-07-11 08:47:56'),(228,NULL,'Rudi Silalahi',NULL,'0312030167',NULL,NULL,NULL,'',NULL,'',NULL,1,1,NULL,'','','PUNTUMANDA',NULL,'Laguboti',NULL,3,'22381','1212021106780001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,NULL,NULL,'2003-08-04',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,2647,0,NULL,NULL,'melva.hutagalung','2017-08-03 14:18:19','melva.hutagalung','2017-08-03 14:18:19'),(229,NULL,'Samuel Indra Gunawan Situmeang',NULL,'0308170258',NULL,NULL,NULL,'SGS',NULL,'Medan','1992-02-10',1,1,NULL,'082170709369','','Jl. Danau Jempang Lk.I No. 64A\r\n',NULL,'BINJAI TIMUR',NULL,13,'20734','1275041002920001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2017-08-16',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,2649,0,NULL,NULL,'melva.hutagalung','2017-08-30 12:07:56','melva.hutagalung','2018-07-11 10:02:20'),(230,NULL,'Teamsar Muliadi Panggabean',NULL,'0308170259',NULL,NULL,NULL,'TMP',NULL,'Banda Aceh','1988-09-01',1,1,NULL,'081263105978','','Jl. Karet Belakang No.10 RT/RW 012/004\r\n',NULL,'SETIA BUDI',NULL,42,'','1208010109880001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2017-08-28',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,3078,0,NULL,NULL,'melva.hutagalung','2017-08-30 16:54:43','melva.hutagalung','2018-07-11 10:05:29'),(231,NULL,'Joyce Rotua Natalia Manurung',NULL,'',NULL,NULL,NULL,'JNM',NULL,'Parapat','1995-12-09',1,2,2,'081269617959','','Jl. Merdeka No. 40 A\r\nParapat',NULL,'Girsang Sipangan Bolon',NULL,27,'','1208164912950002',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2017-09-11',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,3080,0,NULL,NULL,'melva.hutagalung','2017-09-15 10:10:57','melva.hutagalung','2017-09-15 10:10:57'),(232,NULL,'Flora Hutahean',NULL,'',NULL,NULL,NULL,'FLH',NULL,'',NULL,1,2,NULL,'','','',NULL,'',NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3082,0,NULL,NULL,'arlinta','2017-09-27 09:55:32','jonathan.tambun','2017-09-27 10:03:28'),(233,NULL,'Niko Saripson P. Simamora',NULL,'123234654663',NULL,NULL,NULL,'NSS',NULL,'Sidikalang','2018-02-16',1,1,4,'08116321160','','Jl. Haur Pancuh II No. 38B\r\nRT/RW 02/04, Lebak Gede\r\nBandung, Jawa Barat',NULL,'lebak gede',NULL,20,'40133','1211011602890001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2018-01-15',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,3087,0,NULL,NULL,'melva.hutagalung','2018-01-26 15:57:06','melva.hutagalung','2018-01-26 15:57:06'),(234,NULL,'Mayor Arwan Sembiring',NULL,'030817K036',NULL,NULL,NULL,'ARS',NULL,'Sibolangit','1963-12-13',1,1,NULL,'082168363036','','Asrama Yonkav 6-Serbu Medan\r\n',NULL,'Medan Selayang',NULL,14,'','1271211312630001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,NULL,1,'2017-08-03',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,3089,0,NULL,NULL,'melva.hutagalung','2018-01-26 17:25:13','melva.hutagalung','2018-07-11 10:53:58'),(235,NULL,'Anwar Pangaribuan',NULL,'1231655465354',NULL,NULL,NULL,'ANP',NULL,'',NULL,1,1,2,'081372174196','','',NULL,'',NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,NULL,1,'2009-09-02',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,3090,0,NULL,NULL,'melva.hutagalung','2018-01-26 17:26:52','melva.hutagalung','2018-01-26 17:46:31'),(236,NULL,'Yohanes Roche',NULL,'2154232312',NULL,NULL,NULL,'YOR',NULL,'',NULL,1,1,3,'081298982088','','',NULL,'',NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,NULL,1,'2014-06-01',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,2626,0,NULL,NULL,'melva.hutagalung','2018-01-26 17:29:17','melva.hutagalung','2018-01-26 17:29:17'),(237,NULL,'Bettina Simanjuntak',NULL,'132444646',NULL,NULL,NULL,'BTS',NULL,'',NULL,1,2,4,'081362398204','','',NULL,'',NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,NULL,1,'2009-10-01',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,3091,0,NULL,NULL,'melva.hutagalung','2018-01-26 17:31:57','melva.hutagalung','2018-01-26 17:31:57'),(238,NULL,'Sabar Simanjuntak',NULL,'2142332313',NULL,NULL,NULL,'SBS',NULL,'',NULL,1,1,2,'085261631941','','',NULL,'',NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2005-01-29',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,3092,0,NULL,NULL,'melva.hutagalung','2018-01-26 17:34:46','melva.hutagalung','2018-01-26 17:34:46'),(239,NULL,'Anggraini Sitepu',NULL,'23543254312324',NULL,NULL,NULL,'AJS',NULL,'',NULL,1,2,1,'081314881413','','',NULL,'',NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2007-10-23',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,3093,0,NULL,NULL,'melva.hutagalung','2018-01-26 17:36:25','melva.hutagalung','2018-01-26 17:36:25'),(240,NULL,'Metilova Sitorus',NULL,'0301180278',NULL,NULL,NULL,'MTS',NULL,'Balige','1991-09-28',1,2,NULL,'082113013520','','Jl Sutomo \r\n',NULL,'Balige',NULL,3,'','1212016809910001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2018-01-10',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,3088,0,NULL,NULL,'melva.hutagalung','2018-02-14 13:31:52','melva.hutagalung','2018-07-11 10:13:33'),(241,NULL,'Jonathan Borisman Tambun,A.Md',NULL,'031014K028',NULL,NULL,NULL,'JBT',NULL,'Jakarta','1992-12-25',1,1,1,'081381301302','0632-331234','Visar Indah Pratama Blok VGI No. 21 RT/RW 008/002 Kel. Cibinong Kec.Cibinong Kab. Bogor\r\n \r\n',NULL,'Cibinong',NULL,39,'16911','3201012512920020',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,NULL,1,'2017-03-01',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,2637,0,NULL,NULL,'melva.hutagalung','2018-02-14 13:37:26','melva.hutagalung','2018-07-11 16:35:33'),(242,NULL,'Hery Voltama Hutauruk',NULL,'0',NULL,NULL,NULL,'HVH',NULL,'lumban julu','1985-05-08',1,1,1,'0823678965','','jl. Vilgram',NULL,'balige',NULL,3,'','1212010805950001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2018-04-09',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,3098,0,NULL,NULL,'melva.hutagalung','2018-04-17 15:44:46','melva.hutagalung','2018-04-17 15:44:46'),(243,NULL,'Dedy Anwar, ST., MT',NULL,'03041245',NULL,NULL,NULL,'DDA',NULL,'Padangsidempuan','2018-08-08',3,1,1,'0811640107','','Jl. BM. Muda No. 21\r\nDesa Padang Matinggi Lestari, ',NULL,'Padangsidimpuan Selatan',NULL,15,'0','1277020308900007',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2018-04-17',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,3100,0,NULL,NULL,'melva.hutagalung','2018-04-24 09:46:34','melva.hutagalung','2018-04-24 09:46:34'),(244,NULL,'Sari Muthia Silalahi, S.Pd., M.Ed',NULL,'0304180277',NULL,NULL,NULL,'SML',NULL,'Medan','1993-10-17',1,2,4,'082166078272','','Dusun Negara\r\n',NULL,'STM Hilir',NULL,8,'','1207085710930003',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2018-04-04','2018-09-02',NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,3099,0,NULL,NULL,'melva.hutagalung','2018-06-21 14:30:56','melva.hutagalung','2018-07-11 10:23:01'),(245,NULL,'Elisabeth Manurung',NULL,'',NULL,NULL,NULL,'',NULL,'',NULL,1,NULL,NULL,'','','',NULL,'',NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,3096,0,NULL,NULL,'root','2018-07-13 15:03:53','root','2018-07-13 15:03:53'),(246,NULL,'',NULL,'',NULL,NULL,NULL,'',NULL,'',NULL,NULL,NULL,NULL,'','','',NULL,'',NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2639,0,NULL,NULL,'melva.hutagalung','2018-07-18 14:41:47','melva.hutagalung','2018-07-18 14:41:47'),(247,NULL,'Khoirun Nasoha',NULL,'0309170261',NULL,NULL,NULL,'',NULL,'Sidoharjo','1990-09-06',1,1,NULL,'081393889679','','Kota Wisata Sentra Eropa Blok D 46 RT/RW 001/036\r\n',NULL,'Gunung Putri',NULL,42,'','1801240609900002',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2017-09-11',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,3084,0,NULL,NULL,'melva.hutagalung','2018-07-20 09:36:25','melva.hutagalung','2018-07-20 09:50:19'),(248,NULL,'Yerni Maryuna Talan',NULL,'0309170262',NULL,NULL,NULL,'',NULL,'Kiuubat','1992-07-26',1,2,NULL,'082357773370','','Tarus RT/RW 015/006\r\n',NULL,'Kupang Tengah',NULL,20,'','5301086607920001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2017-09-05',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,3079,0,NULL,NULL,'melva.hutagalung','2018-07-20 09:51:28','melva.hutagalung','2018-07-20 10:17:57'),(249,NULL,'Elita Sihotang',NULL,'0309170265',NULL,NULL,NULL,'',NULL,'Sihabonghabong','1991-07-28',1,2,NULL,'082188536391','','Sihabonghabong Toruan\r\n',NULL,'Parlilitan',NULL,9,'','1216016807910002',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2017-09-15',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,3081,0,NULL,NULL,'melva.hutagalung','2018-07-20 09:59:08','melva.hutagalung','2018-07-20 09:59:08'),(250,NULL,'Parlindoan Simangunsong',NULL,'0301180277',NULL,NULL,NULL,'',NULL,'Cinta Rakyat','1991-06-17',1,1,NULL,'082161288464','','Huta II Cinta Rakyat\r\n',NULL,'Tanah Jawa',NULL,27,'','1208112706910001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2018-01-18',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,3095,0,NULL,NULL,'melva.hutagalung','2018-07-20 10:04:06','melva.hutagalung','2018-07-20 10:04:06'),(251,NULL,'Ery Ricardo Nababan',NULL,'0309170260',NULL,NULL,NULL,'',NULL,'Marihat Dolok','1988-04-11',1,1,NULL,'085218986935','','Dusun II\r\n',NULL,'Bintang Bayu',NULL,26,'','1218171104880003',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2017-09-07',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,3100,0,NULL,NULL,'melva.hutagalung','2018-07-20 11:22:32','melva.hutagalung','2018-07-20 11:22:32'),(252,NULL,'Andreas Yudistira',NULL,'0307180282',NULL,NULL,NULL,'',NULL,'Jakarta','1993-05-09',1,1,1,'082110000917','','Komp. GBA 3 Blok 0-9 No. 23\r\n',NULL,'Bojongsoang',NULL,20,'','3204080905930006',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2018-07-05',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,3105,0,NULL,NULL,'melva.hutagalung','2018-07-26 10:51:41','melva.hutagalung','2018-07-26 10:59:52'),(253,NULL,'Jason Tambun',NULL,'0307180284',NULL,NULL,NULL,'',NULL,'Jakarta','1994-06-01',1,1,4,'082114165353','','Visar Indah Pratama Blok VG 1 No.21 Cibinong – Bogor ',NULL,'Cibinong',NULL,20,'16911','3201010106940003',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2018-07-05',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,3104,0,NULL,NULL,'melva.hutagalung','2018-07-26 11:08:16','melva.hutagalung','2018-07-26 11:08:16'),(254,NULL,'Cronika Hutapea',NULL,'0307180283',NULL,NULL,NULL,'',NULL,'Tarutung','2018-08-03',1,2,4,'085297654357','','Naga Timbul Pardede Onan, Balige',NULL,'Pardede Onan',NULL,3,'','1212014308890003',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2018-07-05',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,3103,0,NULL,NULL,'melva.hutagalung','2018-07-26 11:19:52','melva.hutagalung','2018-07-26 11:19:52'),(255,NULL,'Daniel Surya Parapat,SE',NULL,'0302180273',NULL,NULL,NULL,'DSP',NULL,'Medan','2018-02-27',1,1,1,'0858334081','','Jl. Tombak GG Kasan No.12\r\nKel. Sidorejo Hilir. \r\n',NULL,'Medan Tembung',NULL,14,'','1271142702960009',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2018-02-01',NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,3094,0,NULL,NULL,'melva.hutagalung','2018-08-08 14:12:12','melva.hutagalung','2018-08-08 14:12:12'),(256,NULL,'Monalisa Pasaribu, SS., M.Ed (TESOL)',NULL,'0306180281',NULL,NULL,NULL,'MPR',NULL,'Medan','1986-11-13',1,2,1,'+62813961569','','Jl. Sei Sahang Demang\r\nLebar Daun No. 08\r\nLorok Pakjo , Ilir barat 1\r\nPalembang',NULL,'Lorok Pakjo',NULL,20,'','1671045311860015',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,'2018-06-25',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,3102,0,NULL,NULL,'melva.hutagalung','2018-08-21 09:33:51','melva.hutagalung','2018-08-21 09:33:51'),(257,NULL,'Istas Manalu',NULL,'',NULL,NULL,NULL,'ISM',NULL,'',NULL,1,1,NULL,'','','',NULL,'',NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3579,0,NULL,NULL,'root','2018-08-23 10:45:50','root','2018-08-23 10:45:50');

/*Table structure for table `hrdx_pendidikan` */

DROP TABLE IF EXISTS `hrdx_pendidikan`;

CREATE TABLE `hrdx_pendidikan` (
  `pendidikan_id` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(20) NOT NULL DEFAULT '',
  `no` int(11) NOT NULL,
  `jenjang` varchar(40) DEFAULT NULL,
  `gelar` varchar(10) DEFAULT NULL,
  `universitas` varchar(100) DEFAULT NULL,
  `progdi` varchar(200) DEFAULT NULL,
  `bidang` varchar(200) DEFAULT NULL,
  `thn_masuk` date DEFAULT NULL,
  `thn_lulus` date DEFAULT NULL,
  `judul_ta` varchar(255) DEFAULT NULL,
  `ipk` float DEFAULT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`pendidikan_id`),
  KEY `fk_t_pendidikan_t_profile1_idx` (`profile_id`),
  CONSTRAINT `fk_t_pendidikan_t_profile1` FOREIGN KEY (`profile_id`) REFERENCES `hrdx_profile` (`profile_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hrdx_pendidikan` */

/*Table structure for table `hrdx_pengajar` */

DROP TABLE IF EXISTS `hrdx_pengajar`;

CREATE TABLE `hrdx_pengajar` (
  `pengajar_id` int(11) NOT NULL AUTO_INCREMENT,
  `ta` int(4) NOT NULL DEFAULT '0',
  `id_kur` int(4) NOT NULL DEFAULT '0',
  `kode_mk` varchar(11) NOT NULL,
  `id` varchar(20) NOT NULL,
  `role` char(1) NOT NULL DEFAULT '',
  `kurikulum_id` int(11) DEFAULT NULL,
  `pegawai_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`pengajar_id`),
  KEY `fk_t_pengajar_t_kurikulum1_idx` (`kurikulum_id`),
  KEY `FK_hrdx_pengajar` (`pegawai_id`),
  CONSTRAINT `FK_hrdx_pengajar_pegawai` FOREIGN KEY (`pegawai_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_t_pengajar_t_kurikulum1` FOREIGN KEY (`kurikulum_id`) REFERENCES `krkm_kuliah` (`kuliah_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hrdx_pengajar` */

/*Table structure for table `hrdx_profile` */

DROP TABLE IF EXISTS `hrdx_profile`;

CREATE TABLE `hrdx_profile` (
  `profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(20) NOT NULL DEFAULT '',
  `nip` varchar(20) NOT NULL DEFAULT '',
  `kpt_no` varchar(10) NOT NULL DEFAULT '',
  `user_name` varchar(20) DEFAULT NULL,
  `nama` varchar(50) NOT NULL DEFAULT '',
  `posisi` varchar(100) DEFAULT NULL,
  `alias` varchar(5) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `jenis_kelamin` char(1) NOT NULL DEFAULT '',
  `gol_darah` char(2) DEFAULT NULL,
  `tgl_masuk` date DEFAULT NULL,
  `tgl_keluar` date NOT NULL,
  `agama` varchar(30) DEFAULT NULL,
  `kbk_id` varchar(20) DEFAULT NULL,
  `ext_num` char(3) DEFAULT NULL,
  `hp` varchar(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `alamat_libur` varchar(100) DEFAULT NULL,
  `kota` varchar(50) DEFAULT NULL,
  `kode_pos` varchar(5) DEFAULT NULL,
  `telepon` varchar(15) DEFAULT NULL,
  `ktp` varchar(255) DEFAULT NULL,
  `pendidikan` varchar(255) DEFAULT NULL,
  `jabatan` varchar(20) NOT NULL DEFAULT '',
  `pendidikan_tertinggi` varchar(20) NOT NULL DEFAULT 'S1',
  `study_area1` varchar(50) NOT NULL DEFAULT '',
  `study_area2` varchar(50) NOT NULL DEFAULT '',
  `status` char(1) NOT NULL DEFAULT 'S',
  `nama_bapak` varchar(50) DEFAULT NULL,
  `nama_ibu` varchar(50) DEFAULT NULL,
  `pekerjaan_ortu` varchar(100) DEFAULT NULL,
  `nama_p` varchar(50) DEFAULT NULL,
  `tmp_lahir_p` varchar(50) DEFAULT NULL,
  `tgl_lahir_p` date NOT NULL,
  `ket` text NOT NULL,
  `status_akhir` varchar(5) NOT NULL DEFAULT 'A',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`profile_id`),
  KEY `ID` (`profile_id`),
  KEY `NAMA` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hrdx_profile` */

/*Table structure for table `hrdx_r_staf_role` */

DROP TABLE IF EXISTS `hrdx_r_staf_role`;

CREATE TABLE `hrdx_r_staf_role` (
  `staf_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`staf_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hrdx_r_staf_role` */

/*Table structure for table `hrdx_riwayat_pendidikan` */

DROP TABLE IF EXISTS `hrdx_riwayat_pendidikan`;

CREATE TABLE `hrdx_riwayat_pendidikan` (
  `riwayat_pendidikan_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenjang_id` int(11) DEFAULT NULL,
  `universitas` varchar(100) DEFAULT NULL,
  `jurusan` varchar(200) DEFAULT NULL,
  `thn_mulai` varchar(50) DEFAULT NULL,
  `thn_selesai` varchar(50) DEFAULT NULL,
  `ipk` varchar(15) DEFAULT NULL,
  `gelar` varchar(15) DEFAULT NULL,
  `judul_ta` varchar(255) DEFAULT NULL,
  `pegawai_id` int(11) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `jenjang` varchar(40) DEFAULT NULL,
  `id_old` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`riwayat_pendidikan_id`),
  KEY `fk_t_pendidikan_t_profile1_idx` (`profile_id`),
  KEY `FK_hrdx_riwayat_pendidikan_new_pegawai` (`pegawai_id`),
  KEY `FK_hrdx_riwayat_pendidikan_new_jenjang` (`jenjang_id`),
  CONSTRAINT `FK_hrdx_riwayat_pendidikan_new_jenjang` FOREIGN KEY (`jenjang_id`) REFERENCES `mref_r_jenjang` (`jenjang_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_riwayat_pendidikan_new_pegawai` FOREIGN KEY (`pegawai_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hrdx_riwayat_pendidikan` */

/*Table structure for table `hrdx_riwayat_pendidikan_old` */

DROP TABLE IF EXISTS `hrdx_riwayat_pendidikan_old`;

CREATE TABLE `hrdx_riwayat_pendidikan_old` (
  `riwayat_pendidikan_id` int(11) DEFAULT NULL,
  `jenjang_id` int(11) DEFAULT NULL,
  `universitas` varchar(180) DEFAULT NULL,
  `jurusan` varchar(150) DEFAULT NULL,
  `thn_mulai` varchar(150) DEFAULT NULL,
  `thn_selesai` varchar(150) DEFAULT NULL,
  `ipk` varchar(15) DEFAULT NULL,
  `gelar` varchar(15) DEFAULT NULL,
  `dosen_id` int(11) DEFAULT NULL,
  `staf_id` int(11) DEFAULT NULL,
  `judul_ta` blob,
  `website` varchar(765) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(96) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(96) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(96) DEFAULT NULL,
  KEY `FK_hrdx_riwayat_pendidikan_dosen` (`dosen_id`),
  KEY `FK_hrdx_riwayat_pendidikan_staf` (`staf_id`),
  KEY `FK_hrdx_riwayat_pendidikan_jenjang` (`jenjang_id`),
  CONSTRAINT `FK_hrdx_riwayat_pendidikan_dosen` FOREIGN KEY (`dosen_id`) REFERENCES `hrdx_dosen` (`dosen_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_riwayat_pendidikan_jenjang` FOREIGN KEY (`jenjang_id`) REFERENCES `mref_r_jenjang` (`jenjang_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_riwayat_pendidikan_staf` FOREIGN KEY (`staf_id`) REFERENCES `hrdx_staf` (`staf_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hrdx_riwayat_pendidikan_old` */

/*Table structure for table `hrdx_staf` */

DROP TABLE IF EXISTS `hrdx_staf`;

CREATE TABLE `hrdx_staf` (
  `staf_id` int(11) NOT NULL AUTO_INCREMENT,
  `pegawai_id` int(11) DEFAULT NULL,
  `prodi_id` int(11) DEFAULT NULL,
  `staf_role_id` int(11) DEFAULT NULL,
  `aktif_start` date DEFAULT '0000-00-00',
  `aktif_end` date DEFAULT '0000-00-00',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `temp_id_old` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`staf_id`),
  KEY `FK_hrdx_staf_pegawai` (`pegawai_id`),
  KEY `FK_hrdx_staf_prodi` (`prodi_id`),
  KEY `FK_hrdx_staf_role` (`staf_role_id`),
  CONSTRAINT `FK_hrdx_staf_pegawai` FOREIGN KEY (`pegawai_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_staf_prodi` FOREIGN KEY (`prodi_id`) REFERENCES `inst_prodi` (`ref_kbk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_staf_role` FOREIGN KEY (`staf_role_id`) REFERENCES `hrdx_r_staf_role` (`staf_role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hrdx_staf` */

/*Table structure for table `inst_instansi` */

DROP TABLE IF EXISTS `inst_instansi`;

CREATE TABLE `inst_instansi` (
  `instansi_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `inisial` varchar(45) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varbinary(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`instansi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `inst_instansi` */

insert  into `inst_instansi`(`instansi_id`,`name`,`inisial`,`desc`,`deleted`,`deleted_at`,`deleted_by`,`updated_at`,`updated_by`,`created_at`,`created_by`) values (4,'Institut Teknologi Del','IT Del','Unit perguruan tinggi di bawah naungan Yayasan Del',0,NULL,NULL,'2017-09-27 09:57:01','arlinta','2017-09-27 09:57:01','arlinta');

/*Table structure for table `inst_pejabat` */

DROP TABLE IF EXISTS `inst_pejabat`;

CREATE TABLE `inst_pejabat` (
  `pejabat_id` int(11) NOT NULL AUTO_INCREMENT,
  `pegawai_id` int(11) DEFAULT NULL,
  `struktur_jabatan_id` int(11) DEFAULT NULL,
  `awal_masa_kerja` date DEFAULT NULL,
  `akhir_masa_kerja` date DEFAULT NULL,
  `no_sk` varchar(45) DEFAULT NULL,
  `file_sk` text,
  `kode_file` varchar(200) DEFAULT NULL,
  `status_aktif` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varbinary(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`pejabat_id`),
  KEY `FK_pejabat_struktur_jabatan_idx` (`struktur_jabatan_id`),
  KEY `FK_inst_pejabat_pegawai` (`pegawai_id`),
  CONSTRAINT `FK_inst_pejabat_pegawai` FOREIGN KEY (`pegawai_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_pejabat_struktur_jabatan` FOREIGN KEY (`struktur_jabatan_id`) REFERENCES `inst_struktur_jabatan` (`struktur_jabatan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

/*Data for the table `inst_pejabat` */

insert  into `inst_pejabat`(`pejabat_id`,`pegawai_id`,`struktur_jabatan_id`,`awal_masa_kerja`,`akhir_masa_kerja`,`no_sk`,`file_sk`,`kode_file`,`status_aktif`,`deleted`,`deleted_at`,`deleted_by`,`updated_at`,`updated_by`,`created_at`,`created_by`) values (19,222,82,'2016-11-01','2018-11-01','00000','SKDosen.pdf','5647d3036e0b46f6ccc1b2ccae21d8e4',1,0,NULL,NULL,'2018-01-26 15:11:18','jonathan.tambun','2018-01-26 15:11:18','jonathan.tambun'),(20,36,118,'2017-11-01','2018-07-18','00000','SKDosen.pdf','0a47ef50f2c4400fa53f1915d485a36c',0,0,NULL,NULL,'2018-07-18 14:06:23','melva.hutagalung','2018-01-26 15:12:54','jonathan.tambun'),(21,36,136,'2017-11-01','2018-07-18','00000','SKDosen.pdf','9d87a2c12b003b57f284d45e2919ad9a',0,0,NULL,NULL,'2018-07-18 14:47:41','melva.hutagalung','2018-01-26 15:14:10','jonathan.tambun'),(22,16,83,'2017-11-01','2019-10-31','00000','SKDosen.pdf','1dff3a84638d5b62678fb106b22d6f1d',1,0,NULL,NULL,'2018-01-26 15:15:21','jonathan.tambun','2018-01-26 15:15:21','jonathan.tambun'),(23,1,109,'2017-11-01','2018-10-31','00000','SKDosen.pdf','a068466f595a7f0553b8c32c7ef4f58e',1,0,NULL,NULL,'2018-01-26 15:16:42','jonathan.tambun','2018-01-26 15:16:42','jonathan.tambun'),(24,161,112,'2017-11-01','2018-03-11','00000','SKDosen.pdf','9a48a0125c66171fcb9701ea5cc13107',0,0,NULL,NULL,'2018-07-18 14:36:35','melva.hutagalung','2018-01-26 15:17:55','jonathan.tambun'),(25,164,113,'2017-11-01','2019-11-01','00000','SKDosen.pdf','eaa40c2ae8d37973da5e0cf90399a544',1,0,NULL,NULL,'2018-01-26 15:19:03','jonathan.tambun','2018-01-26 15:19:03','jonathan.tambun'),(26,66,114,'2017-11-01','2018-03-11','00000','SKDosen.pdf','33cfc4b4b5aba464201b258392918dca',0,0,NULL,NULL,'2018-07-18 15:34:08','melva.hutagalung','2018-01-26 15:20:06','jonathan.tambun'),(27,2,115,'2017-11-01','2018-03-10','023/ITDel/Rek/SK/SDM/III/18','SKDosen.pdf','57f41a7b598d011bfe8a7c93db14482f',0,0,NULL,NULL,'2018-07-18 14:19:49','melva.hutagalung','2018-01-26 15:21:01','jonathan.tambun'),(28,70,116,'2017-11-01','2019-11-01','00000','SKDosen.pdf','b7e5d25e4e667928ba766e020115d391',1,0,NULL,NULL,'2018-01-26 15:22:20','jonathan.tambun','2018-01-26 15:22:20','jonathan.tambun'),(29,202,117,'2017-02-01','2019-02-11','00000','SKDosen.pdf','af4b480d4264b44d45fe429d5bd0d3ef',1,0,NULL,NULL,'2018-07-18 14:34:55','melva.hutagalung','2018-01-26 15:23:21','jonathan.tambun'),(30,138,110,'2017-11-03','2019-11-03','00000','SKDosen.pdf','023da9a24c9d316a76c4db0eaf1e9fbb',1,0,NULL,NULL,'2018-01-26 15:25:00','jonathan.tambun','2018-01-26 15:25:00','jonathan.tambun'),(31,201,133,'2017-11-01','2019-11-01','00000','SKDosen.pdf','9f5ac4fee5c07fe7c9c0480eabddc9a4',1,0,NULL,NULL,'2018-01-26 15:26:31','jonathan.tambun','2018-01-26 15:26:31','jonathan.tambun'),(32,150,111,'2017-11-03','2019-11-03','00000','SKDosen.pdf','ebd8b531ea62214b7793fc50e901c3eb',1,0,NULL,NULL,'2018-01-26 15:27:26','jonathan.tambun','2018-01-26 15:27:26','jonathan.tambun'),(33,132,129,'2017-11-01','2018-03-10','00000','SKDosen.pdf','ee92698b629ca63df9ad62712aac6b22',0,0,NULL,NULL,'2018-07-18 15:30:31','melva.hutagalung','2018-01-26 15:28:20','jonathan.tambun'),(34,22,87,'2017-07-01','2018-05-31','00000','SKDosen.pdf','26543808cd10acdfe63318fe26539c68',0,0,NULL,NULL,'2018-07-18 15:53:01','melva.hutagalung','2018-01-26 15:29:19','jonathan.tambun'),(35,120,89,'2018-01-01','2019-12-31','00000','SKDosen.pdf','eee8b0fe26526be0675a01aa3c4523f2',1,0,NULL,NULL,'2018-01-26 15:30:15','jonathan.tambun','2018-01-26 15:30:15','jonathan.tambun'),(36,92,88,'2018-01-26','2019-01-26','00000','SKDosen.pdf','f4496bc85dfe00669f054da88b7135da',1,0,NULL,NULL,'2018-01-26 15:30:57','jonathan.tambun','2018-01-26 15:30:57','jonathan.tambun'),(37,186,90,'2017-08-01','2018-07-31','00000','SKDosen.pdf','eb62de8e81ec9a5c09081733af2fe8be',1,0,NULL,NULL,'2018-01-26 15:31:59','jonathan.tambun','2018-01-26 15:31:59','jonathan.tambun'),(38,25,93,'2018-01-26','2019-01-26','00000','SKDosen.pdf','982f4573b812792e0b9a32419d6a4abc',1,0,NULL,NULL,'2018-01-26 15:32:53','jonathan.tambun','2018-01-26 15:32:53','jonathan.tambun'),(39,55,95,'2018-01-26','2019-01-26','00000','SKDosen.pdf','63df8a7aa512b37d1495ef344455d62f',1,0,NULL,NULL,'2018-01-26 15:33:40','jonathan.tambun','2018-01-26 15:33:40','jonathan.tambun'),(40,215,84,'2017-11-01','2018-04-30','00000','SKDosen.pdf','62a94b1095b71fe794cd9eb1fe2028c5',0,0,NULL,NULL,'2018-07-19 08:52:05','melva.hutagalung','2018-01-26 15:34:38','jonathan.tambun'),(41,45,103,'2017-12-25','2018-03-31','00000','SKDosen.pdf','f8325538f88a4ca6bf81c497b870c40e',1,0,NULL,NULL,'2018-01-26 15:42:43','jonathan.tambun','2018-01-26 15:42:43','jonathan.tambun'),(42,210,85,'2016-09-01','2018-09-01','00000','SKDosen.pdf','c798e0259dc95114a403ac0c79b487ae',1,0,NULL,NULL,'2018-01-26 15:45:27','jonathan.tambun','2018-01-26 15:45:27','jonathan.tambun'),(43,173,106,'2018-01-01','2018-05-31','00000','SKDosen.pdf','dddfb3a7d07efa3382087eaa8d8b4068',0,0,NULL,NULL,'2018-07-18 15:47:15','melva.hutagalung','2018-01-26 15:46:11','jonathan.tambun'),(44,133,107,'2018-01-26','2019-01-26','00000','SKDosen.pdf','00f3243993382ec5c0a59902a342c178',1,0,NULL,NULL,'2018-01-26 15:46:54','jonathan.tambun','2018-01-26 15:46:54','jonathan.tambun'),(45,80,108,'2017-11-01','2019-10-31','00000','SKDosen.pdf','5f26b0ec3797fa27cdd88e3273517fa3',1,0,NULL,NULL,'2018-01-26 15:48:04','jonathan.tambun','2018-01-26 15:48:04','jonathan.tambun'),(46,229,140,'2017-11-01','2019-10-31','00000','SKDosen.pdf','5ef985b3956ca020408335573df08003',1,0,NULL,NULL,'2018-01-26 15:49:00','jonathan.tambun','2018-01-26 15:49:00','jonathan.tambun'),(47,81,91,'2017-04-13','2018-03-31','00000','SKDosen.pdf','2cc3aadc34d8f788caeed49e9d186fb3',0,0,NULL,NULL,'2018-07-18 15:41:51','melva.hutagalung','2018-01-26 15:51:59','jonathan.tambun'),(48,63,142,'2017-11-01','2018-05-31','00000','SKDosen.pdf','4362d6e143ec1941dda38a3bc5bf81e6',0,0,NULL,NULL,'2018-07-18 15:55:48','melva.hutagalung','2018-01-26 15:55:05','jonathan.tambun'),(49,234,97,'2018-01-26','2019-01-26','00000','SKDosen.pdf','b4a0c8f1322d7c8535df7f7cb7dd75be',1,0,NULL,NULL,'2018-01-26 17:28:48','jonathan.tambun','2018-01-26 17:28:48','jonathan.tambun'),(50,236,101,'2017-10-11','2018-10-11','00000','SKDosen.pdf','91cc48acbbd3464c797bfab9fb488b36',1,0,NULL,NULL,'2018-01-26 17:38:53','jonathan.tambun','2018-01-26 17:38:53','jonathan.tambun'),(51,237,102,'2017-08-01','2018-07-31','00000','SKDosen.pdf','e3f5fb03277c7ac0e0847abc02995a9c',1,0,NULL,NULL,'2018-01-26 17:40:03','jonathan.tambun','2018-01-26 17:40:03','jonathan.tambun'),(52,238,104,'2017-11-01','2018-01-31','00000','SKDosen.pdf','9e0c3862d3aff9b1fb951918a3d5c90a',1,0,NULL,NULL,'2018-01-26 17:43:39','jonathan.tambun','2018-01-26 17:43:39','jonathan.tambun'),(53,239,96,'2017-10-23','2018-04-22','00000','SKDosen.pdf','9977d71bb79fc47ee5e7a44448371253',1,0,NULL,NULL,'2018-01-26 17:44:41','jonathan.tambun','2018-01-26 17:44:41','jonathan.tambun'),(54,235,100,'2017-08-01','2018-07-31','00000','SKDosen.pdf','7ea855973667b62f1ef32b05e82a50d7',1,0,NULL,NULL,'2018-01-26 17:47:46','jonathan.tambun','2018-01-26 17:47:46','jonathan.tambun'),(56,36,92,'2018-04-09','2019-04-09','123456789','SKDosen.pdf','5374d7c34e276897d8b9f357445bc177',1,0,NULL,NULL,'2018-04-09 09:09:43','jonathan.tambun','2018-04-09 09:09:43','jonathan.tambun'),(57,138,94,'2018-04-09','2019-04-09','123456789','SKDosen.pdf','bfec740219a3cb9404dffc842a97871d',1,0,NULL,NULL,'2018-04-09 09:10:46','jonathan.tambun','2018-04-09 09:10:46','jonathan.tambun'),(58,54,115,'2018-03-31','2020-04-01','023/ITDel/Rek/SK/SDM/III/18','023_SK_Rek_Perpanjangan_Jabatan_KaProdi_2018-2022.pdf','e0c0d100bdb85ceb8ed54cdd1ba5d5ca',1,0,NULL,NULL,'2018-07-18 15:25:39','melva.hutagalung','2018-07-18 15:25:39','melva.hutagalung'),(59,132,129,'2018-03-12','2020-03-11','023/ITDel/Rek/SK/SDM/III/18','023_SK_Rek_Perpanjangan_Jabatan_KaProdi_2018-2022.pdf','538c9c5c39b039741f3b1400ebc575f2',1,0,NULL,NULL,'2018-07-18 15:31:53','melva.hutagalung','2018-07-18 15:31:53','melva.hutagalung'),(60,66,114,'2018-03-12','2020-03-11','023/ITDel/Rek/SK/SDM/III/18','023_SK_Rek_Perpanjangan_Jabatan_KaProdi_2018-2022.pdf','7d6f6777fee0bba25548f9bd02477de3',1,0,NULL,NULL,'2018-07-18 15:35:01','melva.hutagalung','2018-07-18 15:35:01','melva.hutagalung'),(61,160,136,'2018-04-19','2020-04-18','028/ITDel/Rek/SK/SDM/IV/18','028_Rek_SK_Pemberhentian_dan_Pengangkatan_Ka._SPM.pdf','06b773f8bec57cf85f87d71ff1a29425',1,0,NULL,NULL,'2018-07-18 15:38:11','melva.hutagalung','2018-07-18 15:38:11','melva.hutagalung'),(62,81,91,'2018-04-01','2020-03-31','047/ITDel/Rek/SK/SDM/V/18','048_SK_Rektor_RIS__Sekr_PPKA.pdf','c6819fc1d7484b8773c44cad9a73c4d7',1,0,NULL,NULL,'2018-07-18 15:46:33','melva.hutagalung','2018-07-18 15:43:48','melva.hutagalung'),(63,173,106,'2018-06-01','2018-11-30','070/ITDel/Rek/SK/SDM/V/18','070_Rek_SK_Perpanjangan_Sekretaris_LPPM.pdf','152ca1f89690ff3dcdad75d7399136fa',1,0,NULL,NULL,'2018-07-18 15:49:05','melva.hutagalung','2018-07-18 15:48:52','melva.hutagalung'),(64,15,84,'2018-07-01','2019-06-30','063/ITDel/Rek/SK/SDM/VII/18','063_Rek_SK_Pengangkatan_sebagai_WR2.pdf','14d6096665b81ebc73b7dcb5bea1909f',1,0,NULL,NULL,'2018-07-18 15:52:05','melva.hutagalung','2018-07-18 15:51:53','melva.hutagalung'),(65,63,142,'2018-06-01','2018-11-30','072/ITDel/REK/SK/SDM/V/18','072_SK_Rek__SekreProdiTK_ESS_perpanjangan_revCJS_new.pdf','5c045b19cacd6e5f0a356a7437a6d788',1,0,NULL,NULL,'2018-07-18 15:58:22','melva.hutagalung','2018-07-18 15:58:00','melva.hutagalung'),(66,16,143,'2018-04-01','2018-12-31','050/ITDel/Rek/SK/SDM/V/18','050_SK_Rektor_ACB_Bendahara_Direktorat_PPM.pdf','84a446140794c1bcead95f2be7576893',1,0,NULL,NULL,'2018-07-18 16:22:12','melva.hutagalung','2018-07-18 16:22:01','melva.hutagalung'),(67,22,87,'2018-06-01','2019-05-31','071/ITDel/Rek/SK/SDM/V/18','071_Rek_SK_Perpanjangan_Masa_Jabatan_Direktur_Pendidikan.pdf','19b52a7007b66e21ea155e72c79db7dc',1,0,NULL,NULL,'2018-07-18 16:25:57','melva.hutagalung','2018-07-18 16:25:46','melva.hutagalung');

/*Table structure for table `inst_prodi` */

DROP TABLE IF EXISTS `inst_prodi`;

CREATE TABLE `inst_prodi` (
  `ref_kbk_id` int(11) NOT NULL AUTO_INCREMENT,
  `kbk_id` varchar(20) DEFAULT NULL,
  `kpt_id` varchar(10) DEFAULT NULL,
  `jenjang_id` int(11) DEFAULT NULL,
  `kbk_ind` varchar(100) DEFAULT NULL,
  `singkatan_prodi` varchar(50) DEFAULT NULL,
  `kbk_ing` varchar(100) DEFAULT NULL,
  `nama_kopertis_ind` varchar(255) DEFAULT NULL,
  `nama_kopertis_ing` varchar(255) DEFAULT NULL,
  `short_desc_ind` varchar(255) DEFAULT NULL,
  `short_desc_ing` varchar(255) DEFAULT NULL,
  `desc_ind` text,
  `desc_ing` text,
  `status` tinyint(1) DEFAULT '1',
  `is_jenjang_all` tinyint(1) DEFAULT '1',
  `is_public` tinyint(1) DEFAULT '1',
  `is_hidden` tinyint(1) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ref_kbk_id`),
  UNIQUE KEY `KBK_ID_UNIQUE` (`kbk_id`),
  KEY `FK_krkm_r_kbk` (`jenjang_id`),
  CONSTRAINT `FK_krkm_r_kbk` FOREIGN KEY (`jenjang_id`) REFERENCES `inst_r_jenjang` (`jenjang_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `inst_prodi` */

insert  into `inst_prodi`(`ref_kbk_id`,`kbk_id`,`kpt_id`,`jenjang_id`,`kbk_ind`,`singkatan_prodi`,`kbk_ing`,`nama_kopertis_ind`,`nama_kopertis_ing`,`short_desc_ind`,`short_desc_ing`,`desc_ind`,`desc_ing`,`status`,`is_jenjang_all`,`is_public`,`is_hidden`,`updated_at`,`created_by`,`updated_by`,`deleted`,`deleted_at`,`deleted_by`,`created_at`) values (1,'yxooba992hd2w78lyuc7','10802',1,'Teknik Informatika','TI','Informatics Engineering','','','Graduates should be able to design and implement high quality software.','Graduates should be able to design and implement high quality software.','Graduates should be able to design and implement high quality software.','Graduates should be able to design and implement high quality software.',1,0,0,0,NULL,NULL,NULL,0,NULL,NULL,NULL),(2,'tfc83kdqpq6y42i3pyqa','10804',1,'Manajemen Informatika','MI','Information System','','','Graduates should be able to develop an information system, using appropriate techniques and tools and make a complete Information System documentation','Graduates should be able to develop an information system, using appropriate techniques and tools and make a complete Information System documentation','Graduates should be able to develop an information system, using appropriate techniques and tools and make a complete Information System documentation','Graduates should be able to develop an information system, using appropriate techniques and tools and make a complete Information System documentation',1,0,0,0,NULL,NULL,NULL,0,NULL,NULL,NULL),(3,'fiy7zfcz29vmkeclc6hi','10803',1,'Teknik Komputer','TK','Computer Network Technology','','','Graduates should be able to design, develop and manage Local Area Network (LAN) system including server software such as Web, FTP, DNS, DHCP and Mail Server under Windows and Linux platform and also have a basic knowledge in design, develop and manage Wid','Graduates should be able to design, develop and manage Local Area Network (LAN) system including server software such as Web, FTP, DNS, DHCP and Mail Server under Windows and Linux platform and also have a basic knowledge in design, develop and manage Wid','Graduates should be able to design, develop and manage Local Area Network (LAN) system including server software such as Web, FTP, DNS, DHCP and Mail Server under Windows and Linux platform and also have a basic knowledge in design, develop and manage Wide Are Network (WAN)','Graduates should be able to design, develop and manage Local Area Network (LAN) system including server software such as Web, FTP, DNS, DHCP and Mail Server under Windows and Linux platform and also have a basic knowledge in design, develop and manage Wide Are Network (WAN)',1,0,0,0,NULL,NULL,NULL,0,NULL,NULL,NULL),(4,'op5whgldgbzt3in6b98k','',2,'Teknik Informatika','TI','Informatics Engineering','','','','','','',1,0,0,0,NULL,NULL,NULL,0,NULL,NULL,NULL),(5,'z0wds2phzbbgfaxx0hkv','',4,'Teknik Komputer dan Jaringan','TKJ','Teknik Komputer dan Jaringan','','','','','','',1,0,0,0,NULL,NULL,NULL,0,NULL,NULL,NULL),(6,'4oxcu32gmr7hipnm1c0d','',3,'Teknik Informatika','TI','Informatics Engineering','','','','','','',1,0,0,0,NULL,NULL,NULL,0,NULL,NULL,NULL),(7,'r47fejtetg2xblqkvnc4','',3,'Teknik Elektro','TE','Electrical Engineering','','','','','','',1,0,0,0,NULL,NULL,NULL,0,NULL,NULL,NULL),(8,'jljj1y9xvqb0pd191uqw','',3,'Teknik Bioproses','TB','Bioprocess Engineering','','','','','','',1,0,0,0,NULL,NULL,NULL,0,NULL,NULL,NULL),(9,'ts7adgjygeeyt1hjmy4e','',3,'Sistem Informasi','SI','Information System','','','','','','',1,0,0,0,NULL,NULL,NULL,0,NULL,NULL,NULL),(10,'nqowzjb0h560zwvypse6','',3,'Manajemen Rekayasa','MR','Industrial Engineering Management','','','','','','',1,0,0,0,NULL,NULL,NULL,0,NULL,NULL,NULL),(11,'all','',NULL,'Semua Prodi',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,1,0,1,1,NULL,NULL,NULL,0,NULL,NULL,NULL),(12,'d3all','',1,'Semua Prodi',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,1,1,0,1,NULL,NULL,NULL,0,NULL,NULL,NULL),(13,'d4all','',2,'Semua Prodi',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,1,1,0,1,NULL,NULL,NULL,0,NULL,NULL,NULL),(14,'s1all','',3,'Semua Prodi',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,1,1,0,1,NULL,NULL,NULL,0,NULL,NULL,NULL);

/*Table structure for table `inst_r_jenjang` */

DROP TABLE IF EXISTS `inst_r_jenjang`;

CREATE TABLE `inst_r_jenjang` (
  `jenjang_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(15) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`jenjang_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `inst_r_jenjang` */

insert  into `inst_r_jenjang`(`jenjang_id`,`nama`,`desc`,`deleted`,`deleted_by`,`deleted_at`,`updated_at`,`updated_by`,`created_at`,`created_by`) values (1,'DIII','Diploma III',0,NULL,NULL,NULL,NULL,NULL,NULL),(2,'DIV','Diploma IV',0,NULL,NULL,NULL,NULL,NULL,NULL),(3,'S1','Strata 1',0,NULL,NULL,'2015-09-21 08:41:26','1',NULL,NULL),(4,'DI','Diploma I',0,NULL,NULL,NULL,NULL,NULL,NULL),(5,'S2','Strata 2',0,NULL,NULL,'2015-09-21 08:37:58','1','2015-09-21 08:37:58','1'),(6,'S3','Strata 3',0,NULL,NULL,'2015-09-21 08:41:07','1','2015-09-21 08:41:07','1');

/*Table structure for table `inst_struktur_jabatan` */

DROP TABLE IF EXISTS `inst_struktur_jabatan`;

CREATE TABLE `inst_struktur_jabatan` (
  `struktur_jabatan_id` int(11) NOT NULL AUTO_INCREMENT,
  `instansi_id` int(11) DEFAULT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `inisial` varchar(45) DEFAULT NULL,
  `is_multi_tenant` tinyint(1) DEFAULT '0',
  `mata_anggaran` tinyint(1) DEFAULT '0',
  `laporan` tinyint(1) DEFAULT '0',
  `unit_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varbinary(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`struktur_jabatan_id`),
  KEY `FK_struktur_jabatan_instansi_idx` (`instansi_id`),
  KEY `FK_struktur_jabatan_struktur_jabatan_idx` (`parent`),
  KEY `FK_struktur_jabatan_unit_idx` (`unit_id`),
  CONSTRAINT `FK_struktur_jabatan_instansi` FOREIGN KEY (`instansi_id`) REFERENCES `inst_instansi` (`instansi_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_struktur_jabatan_struktur_jabatan` FOREIGN KEY (`parent`) REFERENCES `inst_struktur_jabatan` (`struktur_jabatan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_struktur_jabatan_unit` FOREIGN KEY (`unit_id`) REFERENCES `inst_unit` (`unit_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=latin1;

/*Data for the table `inst_struktur_jabatan` */

insert  into `inst_struktur_jabatan`(`struktur_jabatan_id`,`instansi_id`,`jabatan`,`parent`,`inisial`,`is_multi_tenant`,`mata_anggaran`,`laporan`,`unit_id`,`deleted`,`deleted_at`,`deleted_by`,`updated_at`,`updated_by`,`created_at`,`created_by`) values (77,4,'REKTOR',NULL,'Rektor',0,1,1,NULL,1,'2018-01-26 09:19:27','jonathan.tambun','2018-01-26 09:13:00','jonathan.tambun','2018-01-26 09:13:00','jonathan.tambun'),(78,4,'Wakil Rektor Bidang Akademik dan Kemahasiswaan',77,'WR AM',0,1,1,NULL,1,'2018-01-26 09:19:27','jonathan.tambun','2018-01-26 09:14:46','jonathan.tambun','2018-01-26 09:14:46','jonathan.tambun'),(79,4,'Wakil Rektor Bidang Perencanaan, Keuangan, dan Sumber Daya',77,'WR RUS',0,1,1,NULL,1,'2018-01-26 09:19:27','jonathan.tambun','2018-01-26 09:16:14','jonathan.tambun','2018-01-26 09:16:14','jonathan.tambun'),(80,4,'Wakil Rektor Bidang Penelitian, Pengabdian, Kemitraan, Inovasi dan Usaha',77,'WR PPKIU',0,1,1,NULL,1,'2018-01-26 09:19:27','jonathan.tambun','2018-01-26 09:17:13','jonathan.tambun','2018-01-26 09:17:13','jonathan.tambun'),(81,4,'Ketua Senat Akademik',NULL,'SA',0,1,1,78,0,NULL,NULL,'2018-01-26 11:10:23','jonathan.tambun','2018-01-26 09:20:02','jonathan.tambun'),(82,4,'REKTOR',NULL,'Rektor',0,1,1,NULL,0,NULL,NULL,'2018-08-05 20:25:22','jonathan.tambun','2018-01-26 09:20:23','jonathan.tambun'),(83,4,'Wakil Rektor Bidang Akademik dan Kemahasiswaan',82,'WR AM',0,1,1,NULL,0,NULL,NULL,'2018-01-26 09:20:53','jonathan.tambun','2018-01-26 09:20:53','jonathan.tambun'),(84,4,'Wakil Rektor Bidang Perencanaan, Keuangan, dan Sumber Daya',82,'WR RUS',0,1,1,NULL,0,NULL,NULL,'2018-01-26 09:22:11','jonathan.tambun','2018-01-26 09:22:11','jonathan.tambun'),(85,4,'Wakil Rektor Bidang Penelitian, Pengabdian, Kemitraan, Inovasi dan Usaha',82,'WR PPKIU',0,1,1,NULL,0,NULL,NULL,'2018-07-19 08:43:40','melva.hutagalung','2018-01-26 09:22:32','jonathan.tambun'),(86,4,'Anggota Senat Akademik',81,'ASA',1,0,0,78,0,NULL,NULL,'2018-01-26 11:17:11','jonathan.tambun','2018-01-26 09:24:39','jonathan.tambun'),(87,4,'Direktur Pendidikan',83,'DP',0,1,1,57,0,NULL,NULL,'2018-01-26 10:32:58','jonathan.tambun','2018-01-26 09:27:44','jonathan.tambun'),(88,4,'Sekretaris Direktorat Bidang Administrasi dan Akademik',87,'SDBAA',0,1,1,57,0,NULL,NULL,'2018-01-26 11:38:03','jonathan.tambun','2018-01-26 09:29:33','jonathan.tambun'),(89,4,'Sekretaris Direktorat Bidang Kemahasiswaan',87,'SDBK',0,1,1,57,0,NULL,NULL,'2018-01-26 11:38:17','jonathan.tambun','2018-01-26 09:30:40','jonathan.tambun'),(90,4,'Koordinator Pembinaan Keasramaan',87,'KPA',0,1,1,57,0,NULL,NULL,'2018-01-26 11:38:42','jonathan.tambun','2018-01-26 09:31:20','jonathan.tambun'),(91,4,'Sekretaris Pusat Pengembangan Karir dan Alumni',83,'SPPKA',0,1,1,58,0,NULL,NULL,'2018-01-26 10:34:23','jonathan.tambun','2018-01-26 09:32:44','jonathan.tambun'),(92,4,'Kepala UPT Pusat Bahasa',83,'KPB',0,1,1,59,0,NULL,NULL,'2018-01-26 10:35:54','jonathan.tambun','2018-01-26 09:33:58','jonathan.tambun'),(93,4,'Kepala UPT Perpustakaan',83,'KP',0,1,1,60,0,NULL,NULL,'2018-01-26 10:36:47','jonathan.tambun','2018-01-26 09:34:40','jonathan.tambun'),(94,4,'Kepala UPT Pembelajaran Sains dan Matematika',83,'KPSM',0,1,1,61,0,NULL,NULL,'2018-01-26 10:38:30','jonathan.tambun','2018-01-26 09:35:23','jonathan.tambun'),(95,4,'Kepala UPT Teknologi Informasi dan Komunikasi',83,'KTIK',0,1,1,62,0,NULL,NULL,'2018-01-26 10:40:04','jonathan.tambun','2018-01-26 09:36:21','jonathan.tambun'),(96,4,'Supervisor Keuangan',84,'SK',0,1,1,64,0,NULL,NULL,'2018-01-26 10:44:31','jonathan.tambun','2018-01-26 09:37:23','jonathan.tambun'),(97,4,'Kepala Pusat Fasilitas',84,'KPF',0,1,1,66,0,NULL,NULL,'2018-01-26 10:48:54','jonathan.tambun','2018-01-26 09:38:13','jonathan.tambun'),(98,4,'Koordinator Biro Administrasi Umum',96,'BAU',0,1,1,64,0,NULL,NULL,'2018-01-26 11:39:29','jonathan.tambun','2018-01-26 09:40:51','jonathan.tambun'),(99,4,'Koordinator Logistik',96,'KL',0,1,1,64,0,NULL,NULL,'2018-01-26 11:39:45','jonathan.tambun','2018-01-26 09:41:16','jonathan.tambun'),(100,4,'Koordinator Keamanan dan Keselamatan',97,'KKK',0,1,1,66,0,NULL,NULL,'2018-01-26 11:41:02','jonathan.tambun','2018-01-26 09:42:09','jonathan.tambun'),(101,4,'Koordinator Kerumahtanggaan',97,'KK',0,1,1,66,0,NULL,NULL,'2018-01-26 11:41:19','jonathan.tambun','2018-01-26 09:42:56','jonathan.tambun'),(102,4,'Koordinator Pertamanan',97,'KPt',0,1,1,66,0,NULL,NULL,'2018-01-26 11:42:40','jonathan.tambun','2018-01-26 09:44:08','jonathan.tambun'),(103,4,'Koordinator Sarana dan Prasarana',97,'KSP',0,1,1,66,0,NULL,NULL,'2018-01-26 11:42:56','jonathan.tambun','2018-01-26 09:44:55','jonathan.tambun'),(104,4,'Koordinator Transportasi',97,'KT',0,1,1,66,0,NULL,NULL,'2018-01-26 11:43:12','jonathan.tambun','2018-01-26 09:45:27','jonathan.tambun'),(105,4,'Kepala Pusat Perencanaan',84,'KPP',0,1,1,63,0,NULL,NULL,'2018-01-26 10:43:08','jonathan.tambun','2018-01-26 09:46:49','jonathan.tambun'),(106,4,'Sekretaris Lembaga Penelitian dan Pengabdian Kepada Masyarakat',85,'SLPPKM',0,1,1,69,0,NULL,NULL,'2018-01-26 10:54:43','jonathan.tambun','2018-01-26 09:49:16','jonathan.tambun'),(107,4,'Sekretaris Lembaga Kemitraan dan Hubungan Masyarakat',85,'SLKHM',0,1,1,70,0,NULL,NULL,'2018-01-26 10:56:00','jonathan.tambun','2018-01-26 09:50:32','jonathan.tambun'),(108,4,'Sekretaris Pusat Inovasi dan Usaha',85,'SPU',0,1,1,71,0,NULL,NULL,'2018-01-26 10:59:57','jonathan.tambun','2018-01-26 09:51:18','jonathan.tambun'),(109,4,'Dekan Fakultas Teknologi Informatika dan Elektro',82,'DFTIE',0,1,1,74,0,NULL,NULL,'2018-01-26 11:05:49','jonathan.tambun','2018-01-26 09:54:30','jonathan.tambun'),(110,4,'Dekan Fakultas Bioteknologi',82,'DFB',0,1,1,75,0,NULL,NULL,'2018-01-26 11:06:22','jonathan.tambun','2018-01-26 09:55:09','jonathan.tambun'),(111,4,'Dekan Fakultas Teknologi Industri',82,'DFTI',0,1,1,76,0,NULL,NULL,'2018-01-26 11:07:11','jonathan.tambun','2018-01-26 09:55:50','jonathan.tambun'),(112,4,'Ketua Program Studi D3 Teknik Komputer',109,'KPSTK',0,1,1,90,0,NULL,NULL,'2018-01-26 11:26:08','jonathan.tambun','2018-01-26 09:56:42','jonathan.tambun'),(113,4,'Ketua Program Studi D3 Teknik Informatika',109,'KPSTI',0,1,1,88,0,NULL,NULL,'2018-01-26 11:24:34','jonathan.tambun','2018-01-26 09:57:20','jonathan.tambun'),(114,4,'Ketua Program Studi D4 Teknik Informatika',109,'KPSD4TI',0,1,1,86,0,NULL,NULL,'2018-01-26 11:22:43','jonathan.tambun','2018-01-26 09:58:08','jonathan.tambun'),(115,4,'Ketua Program Studi S1 Teknik Informatika',109,'KPSS1TI',0,1,1,80,0,NULL,NULL,'2018-01-26 11:12:33','jonathan.tambun','2018-01-26 09:58:45','jonathan.tambun'),(116,4,'Ketua Program Studi S1 Sistem Informasi',109,'KPSS1SI',0,1,1,82,0,NULL,NULL,'2018-01-26 11:14:19','jonathan.tambun','2018-01-26 09:59:33','jonathan.tambun'),(117,4,'Ketua Program Studi S1 Teknik Elektro',109,'KPSS1TE',0,1,1,84,0,NULL,NULL,'2018-01-26 11:20:57','jonathan.tambun','2018-01-26 10:00:18','jonathan.tambun'),(118,4,'Kepala Satuan Pengendalian Internal',82,'SPI',0,1,1,55,0,NULL,NULL,'2018-01-26 10:29:44','jonathan.tambun','2018-01-26 10:03:06','jonathan.tambun'),(119,4,'Kepala Senat Fakultas FTIE',109,'SFFTIE',0,1,1,77,0,NULL,NULL,'2018-01-26 11:09:11','jonathan.tambun','2018-01-26 10:05:55','jonathan.tambun'),(120,4,'Kepala Gugus Jaminan Mutu',109,'KGJM',0,1,1,79,0,NULL,NULL,'2018-01-26 11:11:34','jonathan.tambun','2018-01-26 10:06:50','jonathan.tambun'),(121,4,'Kepala Gugus Bidang Kajian S1TI',115,'GBKS1TI',0,1,1,81,0,NULL,NULL,'2018-01-26 11:13:34','jonathan.tambun','2018-01-26 10:08:27','jonathan.tambun'),(122,4,'Kepala Gugus Bidang Kajian Sistem Informasi',116,'GBKS1SI',0,1,1,83,0,NULL,NULL,'2018-01-26 11:20:22','jonathan.tambun','2018-01-26 10:09:25','jonathan.tambun'),(123,4,'Kepala Gugus Bidang Kajian D3TK',112,'GBKD3TK',0,1,1,91,0,NULL,NULL,'2018-01-26 11:26:51','jonathan.tambun','2018-01-26 10:10:36','jonathan.tambun'),(124,4,'Kepala Gugus Bidang Kajian D3TI',113,'GBKD3TI',0,1,1,89,0,NULL,NULL,'2018-01-26 11:25:09','jonathan.tambun','2018-01-26 10:11:09','jonathan.tambun'),(125,4,'Kepala Gugus Bidang Kajian D4TI',114,'GBKD4TI',0,1,1,87,0,NULL,NULL,'2018-01-26 11:23:23','jonathan.tambun','2018-01-26 10:11:42','jonathan.tambun'),(126,4,'Kepala Gugus Bidang Kajian S1TE',117,'GBKS1TE',0,1,1,85,0,NULL,NULL,'2018-01-26 11:21:46','jonathan.tambun','2018-01-26 10:13:49','jonathan.tambun'),(127,4,'Kepala Senat FB',110,'KSFB',0,1,1,92,0,NULL,NULL,'2018-01-26 11:27:35','jonathan.tambun','2018-01-26 10:15:03','jonathan.tambun'),(128,4,'Kepala Gugus Jaminan Mutu FB',110,'GJMFB',0,1,1,93,0,NULL,NULL,'2018-01-26 11:28:04','jonathan.tambun','2018-01-26 10:15:43','jonathan.tambun'),(129,4,'Ketua Program Studi Sarjana Teknik Bioproses',110,'KPSTB',0,1,1,94,0,NULL,NULL,'2018-01-26 11:28:47','jonathan.tambun','2018-01-26 10:16:49','jonathan.tambun'),(130,4,'Kepala Gugus Bidang Kajian TB',129,'GBKTB',0,1,1,95,0,NULL,NULL,'2018-01-26 11:29:36','jonathan.tambun','2018-01-26 10:17:35','jonathan.tambun'),(131,4,'Kepala Senat Fakultas FTI',111,'SKFTI',0,1,1,96,0,NULL,NULL,'2018-01-26 11:30:16','jonathan.tambun','2018-01-26 10:18:28','jonathan.tambun'),(132,4,'Kepala Gugus Jaminan Mutu FTI',111,'GJFTI',0,1,1,97,0,NULL,NULL,'2018-01-26 11:31:04','jonathan.tambun','2018-01-26 10:19:06','jonathan.tambun'),(133,4,'Ketua Program Studi Sarjana Manajemen Rekayasa',111,'KPSMR',0,1,1,98,0,NULL,NULL,'2018-01-26 11:32:00','jonathan.tambun','2018-01-26 10:20:03','jonathan.tambun'),(134,4,'Kepala Gugus Bidang Kajian FB',110,'GBKFB',0,1,1,NULL,1,'2018-01-26 10:21:16','jonathan.tambun','2018-01-26 10:21:02','jonathan.tambun','2018-01-26 10:21:02','jonathan.tambun'),(135,4,'Kepala Gugus Bidang Kajian MR',133,'GBKMR',0,1,1,99,0,NULL,NULL,'2018-01-26 11:32:51','jonathan.tambun','2018-01-26 10:22:04','jonathan.tambun'),(136,4,'Kepala Satuan Pengendalian Mutu',82,'KSPM',0,1,1,56,0,NULL,NULL,'2018-01-26 10:31:16','jonathan.tambun','2018-01-26 10:30:21','jonathan.tambun'),(137,4,'Kepala Pusat Modal Manusia',84,'KPMM',0,1,1,65,0,NULL,NULL,'2018-01-26 10:47:54','jonathan.tambun','2018-01-26 10:45:39','jonathan.tambun'),(138,4,'Petugas Kantin',84,'PK',1,0,0,67,0,NULL,NULL,'2018-01-26 10:51:37','jonathan.tambun','2018-01-26 10:50:28','jonathan.tambun'),(139,4,'Petugas Klinik',84,'PKl',1,0,0,68,0,NULL,NULL,'2018-01-26 10:52:39','jonathan.tambun','2018-01-26 10:50:55','jonathan.tambun'),(140,4,'Sekretaris Pusat Penelitian dan Pengembangan',85,'SPPP',0,1,1,72,0,NULL,NULL,'2018-01-26 11:00:57','jonathan.tambun','2018-01-26 10:58:33','jonathan.tambun'),(141,4,'Kepala UPT Inkubator bisnis',108,'UPT-IB',0,1,1,73,0,NULL,NULL,'2018-01-26 11:03:46','jonathan.tambun','2018-01-26 11:02:28','jonathan.tambun'),(142,4,'Sekretaris Prodi D3TK',112,'SD3TK',0,1,1,90,0,NULL,NULL,'2018-01-26 15:54:08','jonathan.tambun','2018-01-26 15:54:08','jonathan.tambun'),(143,4,'Ex Oficio Bendahara Direktorat Penelitian dan Pengabdian kepada Masyarakat',106,'BDPPM',0,1,1,NULL,0,NULL,NULL,'2018-07-19 08:43:40','melva.hutagalung','2018-07-18 16:19:56','melva.hutagalung');

/*Table structure for table `inst_unit` */

DROP TABLE IF EXISTS `inst_unit`;

CREATE TABLE `inst_unit` (
  `unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `instansi_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `inisial` varchar(45) DEFAULT NULL,
  `desc` text,
  `kepala` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varbinary(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`unit_id`),
  KEY `FK_unit_struktur_jabatan_idx` (`kepala`),
  KEY `FK_unit_instansi_idx` (`instansi_id`),
  CONSTRAINT `FK_unit_instansi` FOREIGN KEY (`instansi_id`) REFERENCES `inst_instansi` (`instansi_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_unit_struktur_jabatan` FOREIGN KEY (`kepala`) REFERENCES `inst_struktur_jabatan` (`struktur_jabatan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;

/*Data for the table `inst_unit` */

insert  into `inst_unit`(`unit_id`,`instansi_id`,`name`,`inisial`,`desc`,`kepala`,`deleted`,`deleted_at`,`deleted_by`,`updated_at`,`updated_by`,`created_at`,`created_by`) values (55,4,'Satuan Pengawas Internal','SPI','Unit Untuk Mengawasi Proses Yang berjalan di Instansi IT Del',118,0,NULL,NULL,'2018-01-26 10:27:30','jonathan.tambun','2018-01-26 10:27:01','jonathan.tambun'),(56,4,'Satuan Penjaminan Mutu','SPM','Unit yang bertanggung jawab dalam penjaminan mutu instansi IT Del',136,0,NULL,NULL,'2018-01-26 10:31:16','jonathan.tambun','2018-01-26 10:31:16','jonathan.tambun'),(57,4,'Direktorat Pendidikan','DP','Unit yang bertanggung jawab dalam bidang akademik atau pendidikan di Instansi IT Del',87,0,NULL,NULL,'2018-01-26 10:32:58','jonathan.tambun','2018-01-26 10:32:58','jonathan.tambun'),(58,4,'Pusat Pengembangan Karir dan Alumni','PPKA','Unit ini bertanggung jawab dalam bidang pengembangan karir di IT Del dan hubungan Instansi IT Del dengan alumni.',91,0,NULL,NULL,'2018-01-26 10:34:23','jonathan.tambun','2018-01-26 10:34:23','jonathan.tambun'),(59,4,'UPT Pusat Bahasa','UPT-B','Unit ini bertanggung jawab dalam pengembangan bahasa di instansi IT Del',92,0,NULL,NULL,'2018-01-26 10:35:54','jonathan.tambun','2018-01-26 10:35:54','jonathan.tambun'),(60,4,'UPT Perpustakaan','UPT-P','Unit ini bertanggung jawab dalam pelayanan perpustakaan di instansi IT Del',93,0,NULL,NULL,'2018-01-26 10:36:47','jonathan.tambun','2018-01-26 10:36:47','jonathan.tambun'),(61,4,'UPT Pembelajaran Sains dan Matematika ','UPT-PSM','Unit ini bertanggung jawab dalam pengembangan ilmu pengetahuan dibidang sains dan matematika di Instansi IT Del',94,0,NULL,NULL,'2018-01-26 10:38:30','jonathan.tambun','2018-01-26 10:38:30','jonathan.tambun'),(62,4,'UPT Teknologi Informasi dan Komunikasi','UPT-TIK','Unit ini bertanggung jawab dalam pengembangan ilmu pengetahuan dalam bidang teknologi dan informasi di Instansi IT Del.',95,0,NULL,NULL,'2018-01-26 10:40:04','jonathan.tambun','2018-01-26 10:40:04','jonathan.tambun'),(63,4,'Pusat Perencanaan','PP','Unit ini bertanggung jawab untuk mengatur perencanaan perencanaan terkait pengembangan Instansi IT Del',105,0,NULL,NULL,'2018-01-26 10:43:08','jonathan.tambun','2018-01-26 10:43:08','jonathan.tambun'),(64,4,'Direktorat Keuangan dan Akuntansi','DKA','Unit ini bertangggung jawab untuk mengkoordinasikan keuangan dan akuntansi di instansi IT Del',96,0,NULL,NULL,'2018-01-26 10:44:31','jonathan.tambun','2018-01-26 10:44:31','jonathan.tambun'),(65,4,'Pusat Modal Manusia','PMM','Unit untuk mengelola sumber daya manusia di IT Del',137,0,NULL,NULL,'2018-01-26 10:47:54','jonathan.tambun','2018-01-26 10:47:54','jonathan.tambun'),(66,4,'Pusat Fasilitas','PF','Unit yang bertanggung jawab untuk pengelolaan fasilitas di IT Del',97,0,NULL,NULL,'2018-01-26 10:48:54','jonathan.tambun','2018-01-26 10:48:54','jonathan.tambun'),(67,4,'UPT Kantin','UPT-K','Unit ini bertanggung jawab untuk  mengelola kantin IT Del',138,0,NULL,NULL,'2018-01-26 10:51:37','jonathan.tambun','2018-01-26 10:51:37','jonathan.tambun'),(68,4,'UPT Klinik','UPT-Kl','Unit ini bertanggung jawab untuk mengelola klinik IT Del',139,0,NULL,NULL,'2018-01-26 10:52:39','jonathan.tambun','2018-01-26 10:52:39','jonathan.tambun'),(69,4,'Lembaga Penelitian dan Pengabdian Kepada Masyarakat ','LPPM','Unit ini bertanggung jawab untuk penelitian dan pengabdian kepada masyarakat oleh instansi IT Del',106,0,NULL,NULL,'2018-01-26 10:54:43','jonathan.tambun','2018-01-26 10:54:43','jonathan.tambun'),(70,4,'Lembaga Kemitraan dan Hubungan Masyarakat','LKHM','Unit ini bertanggung jawab dalam mengurus kemitraan dan hubungan masyarakat  IT Del',107,0,NULL,NULL,'2018-01-26 10:56:00','jonathan.tambun','2018-01-26 10:56:00','jonathan.tambun'),(71,4,'Pusat Inovasi dan Usaha','PIU','Unit ini bertanggung jawab dalam pengembangan usaha dan inovasi di instansi IT Del',108,0,NULL,NULL,'2018-01-26 10:59:57','jonathan.tambun','2018-01-26 10:59:57','jonathan.tambun'),(72,4,'Pusat Penelitian dan Pengembangan','PPP','Unit ini bertanggung jawab dalam penelitian dan pengembangan instansi IT Del',140,0,NULL,NULL,'2018-01-26 11:00:57','jonathan.tambun','2018-01-26 11:00:57','jonathan.tambun'),(73,4,'UPT Inkubator Bisnis','UPT-IB','Unit yang menyelenggarakan inkubasi terhadap bisnis baru yang sedang berkembang di IT Del',141,0,NULL,NULL,'2018-01-26 11:03:46','jonathan.tambun','2018-01-26 11:03:46','jonathan.tambun'),(74,4,'Fakultas Teknologi Informatika dan Elektro','FTIE','Fakultas Teknologi Informatika dan Elektro',109,0,NULL,NULL,'2018-01-26 11:05:48','jonathan.tambun','2018-01-26 11:05:48','jonathan.tambun'),(75,4,'Fakultas Bioteknologi','FB','Fakultas Bioteknologi',110,0,NULL,NULL,'2018-01-26 11:06:22','jonathan.tambun','2018-01-26 11:06:22','jonathan.tambun'),(76,4,'Fakultas Teknologi Industri','FTI','Fakultas Teknologi Industri',111,0,NULL,NULL,'2018-01-26 11:07:11','jonathan.tambun','2018-01-26 11:07:11','jonathan.tambun'),(77,4,'Senat Fakultas FTIE','SF FTIE','Senat Fakultas FTIE',119,0,NULL,NULL,'2018-01-26 11:08:08','jonathan.tambun','2018-01-26 11:08:08','jonathan.tambun'),(78,4,'Senat Akademik','SA','Senat Akademik IT Del',81,0,NULL,NULL,'2018-01-26 11:10:23','jonathan.tambun','2018-01-26 11:10:23','jonathan.tambun'),(79,4,'Gugus Jaminan Mutu FTIE','GJMFTIE','Gugus Jaminan Mutu FTIE',120,0,NULL,NULL,'2018-01-26 11:11:34','jonathan.tambun','2018-01-26 11:11:34','jonathan.tambun'),(80,4,'Program Studi Sarjana Teknik Informatika','S1TI','Program Studi Sarjana Teknik Informatika',115,0,NULL,NULL,'2018-07-18 14:15:18','melva.hutagalung','2018-01-26 11:12:33','jonathan.tambun'),(81,4,'Gugus Bidang Kajian S1TI','GBKS1TI','Gugus Bidang Kajian S1TI',121,0,NULL,NULL,'2018-01-26 11:13:34','jonathan.tambun','2018-01-26 11:13:34','jonathan.tambun'),(82,4,'Program Studi Sarjana Sistem Informasi','S1SI','Program Studi Sarjana Sistem Informasi',116,0,NULL,NULL,'2018-01-26 11:14:19','jonathan.tambun','2018-01-26 11:14:19','jonathan.tambun'),(83,4,'Gugus Bidang Kajian S1 Sistem Informasi','GBK SI','Gugus Bidang Kajian S1 Sistem Informasi',122,0,NULL,NULL,'2018-01-26 11:20:21','jonathan.tambun','2018-01-26 11:20:21','jonathan.tambun'),(84,4,'Sarjana Teknik Elektro','S1TE','Sarjana Teknik Elektro',117,0,NULL,NULL,'2018-01-26 11:20:56','jonathan.tambun','2018-01-26 11:20:56','jonathan.tambun'),(85,4,'Gugus Bidang Kajian S1 TE','GBK TE','Gugus Bidang Kajian S1 TE',126,0,NULL,NULL,'2018-01-26 11:21:46','jonathan.tambun','2018-01-26 11:21:46','jonathan.tambun'),(86,4,'Program Studi Diploma IV Teknik Informatika','D4TI','Program Studi Diploma IV Teknik Informatika',114,0,NULL,NULL,'2018-01-26 11:22:43','jonathan.tambun','2018-01-26 11:22:43','jonathan.tambun'),(87,4,'Gugus Bidang Kajian D4TI','GBK D4TI','Gugus Bidang Kajian D4TI',125,0,NULL,NULL,'2018-01-26 11:23:23','jonathan.tambun','2018-01-26 11:23:23','jonathan.tambun'),(88,4,'Program Studi Diploma III Teknik Informatika','D3TI','Program Studi Diploma III Teknik Informatika',113,0,NULL,NULL,'2018-01-26 11:24:34','jonathan.tambun','2018-01-26 11:24:34','jonathan.tambun'),(89,4,'Gugus Bidang Kajian D3TI','GBK D3TI','Gugus Bidang Kajian D3TI',124,0,NULL,NULL,'2018-01-26 11:25:09','jonathan.tambun','2018-01-26 11:25:09','jonathan.tambun'),(90,4,'Program Studi Diploma III Teknik Komputer','D3TK','Program Studi Diploma III Teknik Komputer',112,0,NULL,NULL,'2018-01-26 11:26:08','jonathan.tambun','2018-01-26 11:26:08','jonathan.tambun'),(91,4,'Gugus Bidang Kajian D3TK','GBKB D3TK','Gugus Bidang Kajian D3TK',123,0,NULL,NULL,'2018-01-26 11:26:51','jonathan.tambun','2018-01-26 11:26:51','jonathan.tambun'),(92,4,'Senat Fakultas FB','SF FB','Senat Fakultas FB',127,0,NULL,NULL,'2018-01-26 11:27:35','jonathan.tambun','2018-01-26 11:27:35','jonathan.tambun'),(93,4,'Gugus Jaminan Mutu FB','GJM FB','Gugus Jaminan Mutu FB',128,0,NULL,NULL,'2018-01-26 11:28:04','jonathan.tambun','2018-01-26 11:28:04','jonathan.tambun'),(94,4,'Program Studi Sarjana teknik Bioproses','TB','Program Studi Sarjana teknik Bioproses',129,0,NULL,NULL,'2018-01-26 11:28:47','jonathan.tambun','2018-01-26 11:28:47','jonathan.tambun'),(95,4,'Gugus Bidang Kajian TB','GBK TB','Gugus Bidang Kajian TB',130,0,NULL,NULL,'2018-01-26 11:29:36','jonathan.tambun','2018-01-26 11:29:36','jonathan.tambun'),(96,4,'Senat Fakultas FTI','SF FTI','Senat Fakultas FTI',131,0,NULL,NULL,'2018-01-26 11:30:16','jonathan.tambun','2018-01-26 11:30:16','jonathan.tambun'),(97,4,'Gugus Jaminan Mutu FTI','GJM FTI','Gugus Jaminan Mutu FTI',132,0,NULL,NULL,'2018-01-26 11:31:04','jonathan.tambun','2018-01-26 11:31:04','jonathan.tambun'),(98,4,'Program Studi Sarjana Manajemen Rekayasa','S1MR','Program Studi Sarjana Manajemen Rekayasa',133,0,NULL,NULL,'2018-01-26 11:32:00','jonathan.tambun','2018-01-26 11:32:00','jonathan.tambun'),(99,4,'Gugus Bidang Kajian S1MR','GBK MR','Gugus Bidang Kajian S1MR',135,0,NULL,NULL,'2018-01-26 11:32:51','jonathan.tambun','2018-01-26 11:32:51','jonathan.tambun'),(100,4,'Direktorat Penelitian dan Pengabdian kepada Masyarakat','DPPM','Direktorat Penelitian dan Pengabdian kepada Masyarakat',85,1,'2018-07-19 08:43:40','melva.hutagalung','2018-07-18 16:07:45','melva.hutagalung','2018-07-18 16:07:45','melva.hutagalung');

/*Table structure for table `invt_arsip_vendor` */

DROP TABLE IF EXISTS `invt_arsip_vendor`;

CREATE TABLE `invt_arsip_vendor` (
  `arsip_vendor_id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) DEFAULT NULL,
  `judul_arsip` varchar(150) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`arsip_vendor_id`),
  KEY `FK_invt_arsip_vendor` (`vendor_id`),
  CONSTRAINT `FK_invt_arsip_vendor` FOREIGN KEY (`vendor_id`) REFERENCES `invt_r_vendor` (`vendor_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invt_arsip_vendor` */

/*Table structure for table `invt_barang` */

DROP TABLE IF EXISTS `invt_barang`;

CREATE TABLE `invt_barang` (
  `barang_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_barang` varchar(200) DEFAULT NULL,
  `serial_number` varchar(100) DEFAULT NULL,
  `jenis_barang_id` int(11) DEFAULT NULL,
  `kategori_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT '0',
  `supplier` varchar(150) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `harga_per_barang` decimal(10,0) DEFAULT NULL,
  `total_harga` decimal(10,0) DEFAULT '0',
  `tanggal_masuk` date DEFAULT NULL,
  `satuan_id` int(11) DEFAULT NULL,
  `desc` text,
  `kapasitas` varchar(50) DEFAULT NULL,
  `nama_file` varchar(200) DEFAULT NULL,
  `kode_file` varchar(200) DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`barang_id`),
  KEY `FK_invt_barang` (`jenis_barang_id`),
  KEY `FK_invt_barang_kategori` (`kategori_id`),
  KEY `FK_invt_barang_satuan` (`satuan_id`),
  KEY `FK_invt_barang_unit` (`unit_id`),
  KEY `FK_invt_barang_brand` (`brand_id`),
  KEY `FK_invt_barang_vendor` (`vendor_id`),
  CONSTRAINT `FK_invt_barang` FOREIGN KEY (`jenis_barang_id`) REFERENCES `invt_r_jenis_barang` (`jenis_barang_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_invt_barang_brand` FOREIGN KEY (`brand_id`) REFERENCES `invt_r_brand` (`brand_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_invt_barang_kategori` FOREIGN KEY (`kategori_id`) REFERENCES `invt_r_kategori` (`kategori_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_invt_barang_satuan` FOREIGN KEY (`satuan_id`) REFERENCES `invt_r_satuan` (`satuan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_invt_barang_unit` FOREIGN KEY (`unit_id`) REFERENCES `invt_r_unit` (`unit_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_invt_barang_vendor` FOREIGN KEY (`vendor_id`) REFERENCES `invt_r_vendor` (`vendor_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invt_barang` */

/*Table structure for table `invt_detail_peminjaman_barang` */

DROP TABLE IF EXISTS `invt_detail_peminjaman_barang`;

CREATE TABLE `invt_detail_peminjaman_barang` (
  `detail_peminjaman_barang_id` int(11) NOT NULL AUTO_INCREMENT,
  `peminjaman_barang_id` int(11) DEFAULT NULL,
  `barang_id` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `jumlah_rusak` int(11) DEFAULT '0',
  PRIMARY KEY (`detail_peminjaman_barang_id`),
  KEY `FK_invt_detail_peminjaman_barang` (`peminjaman_barang_id`),
  KEY `FK_invt_detail_peminjaman_barang_barang` (`barang_id`),
  CONSTRAINT `FK_invt_detail_peminjaman_barang` FOREIGN KEY (`peminjaman_barang_id`) REFERENCES `invt_peminjaman_barang` (`peminjaman_barang_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_invt_detail_peminjaman_barang_barang` FOREIGN KEY (`barang_id`) REFERENCES `invt_barang` (`barang_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invt_detail_peminjaman_barang` */

/*Table structure for table `invt_file_vendor` */

DROP TABLE IF EXISTS `invt_file_vendor`;

CREATE TABLE `invt_file_vendor` (
  `file_vendor_id` int(11) NOT NULL AUTO_INCREMENT,
  `arsip_vendor_id` int(11) DEFAULT NULL,
  `nama_file` varchar(250) DEFAULT NULL,
  `kode_file` varchar(250) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`file_vendor_id`),
  KEY `FK_invt_file_vendor` (`arsip_vendor_id`),
  CONSTRAINT `FK_invt_file_vendor` FOREIGN KEY (`arsip_vendor_id`) REFERENCES `invt_arsip_vendor` (`arsip_vendor_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invt_file_vendor` */

/*Table structure for table `invt_keterangan_pengeluaran` */

DROP TABLE IF EXISTS `invt_keterangan_pengeluaran`;

CREATE TABLE `invt_keterangan_pengeluaran` (
  `keterangan_pengeluaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_keluar` date NOT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `keterangan` text NOT NULL,
  `total_barang_keluar` int(11) DEFAULT '0',
  `oleh` int(11) DEFAULT NULL,
  `lokasi_distribusi` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`keterangan_pengeluaran_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invt_keterangan_pengeluaran` */

/*Table structure for table `invt_pelaporan_barang_rusak` */

DROP TABLE IF EXISTS `invt_pelaporan_barang_rusak`;

CREATE TABLE `invt_pelaporan_barang_rusak` (
  `pelaporan_barang_rusak` int(11) NOT NULL AUTO_INCREMENT,
  `barang_id` int(11) DEFAULT NULL,
  `kode_barang` varchar(150) DEFAULT NULL,
  `pelapor` int(11) DEFAULT NULL,
  `jumlah_rusak` int(11) DEFAULT '0',
  `tgl_lapor` date DEFAULT NULL,
  `deskripsi` text,
  `nama_file` varchar(200) DEFAULT NULL,
  `status_perbaikan` tinyint(1) DEFAULT '0',
  `tgl_perbaikan` date DEFAULT NULL,
  `kode_file` varchar(200) DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`pelaporan_barang_rusak`),
  KEY `FK_invt_barang_rusak` (`barang_id`),
  KEY `FK_invt_barang_rusak_pelapor` (`pelapor`),
  KEY `FK_invt_pelaporan_barang_rusak_unit` (`unit_id`),
  CONSTRAINT `FK_invt_barang_rusak` FOREIGN KEY (`barang_id`) REFERENCES `invt_barang` (`barang_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_invt_barang_rusak_pelapor` FOREIGN KEY (`pelapor`) REFERENCES `sysx_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_invt_pelaporan_barang_rusak_unit` FOREIGN KEY (`unit_id`) REFERENCES `invt_r_unit` (`unit_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invt_pelaporan_barang_rusak` */

/*Table structure for table `invt_pemindahan_barang` */

DROP TABLE IF EXISTS `invt_pemindahan_barang`;

CREATE TABLE `invt_pemindahan_barang` (
  `pemindahan_barang_id` int(11) NOT NULL AUTO_INCREMENT,
  `pengeluaran_barang_id` int(11) DEFAULT NULL,
  `lokasi_awal_id` int(11) DEFAULT NULL,
  `kode_inventori_awal` varchar(50) DEFAULT NULL,
  `lokasi_akhir_id` int(11) DEFAULT NULL,
  `kode_inventori` varchar(50) DEFAULT NULL,
  `tanggal_pindah` date DEFAULT NULL,
  `oleh` int(11) DEFAULT NULL,
  `status_transaksi` varchar(50) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`pemindahan_barang_id`),
  KEY `FK_invt_pemindahan_barang` (`pengeluaran_barang_id`),
  CONSTRAINT `FK_invt_pemindahan_barang` FOREIGN KEY (`pengeluaran_barang_id`) REFERENCES `invt_pengeluaran_barang` (`pengeluaran_barang_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invt_pemindahan_barang` */

/*Table structure for table `invt_peminjaman_barang` */

DROP TABLE IF EXISTS `invt_peminjaman_barang`;

CREATE TABLE `invt_peminjaman_barang` (
  `peminjaman_barang_id` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_pinjam` date DEFAULT NULL,
  `tgl_kembali` date DEFAULT NULL,
  `oleh` int(11) DEFAULT NULL,
  `deskripsi` text,
  `unit_id` int(11) DEFAULT NULL,
  `status_approval` int(1) DEFAULT '0' COMMENT '0: belum; 1:sudah; 2:reject',
  `approved_by` int(11) DEFAULT NULL,
  `status_kembali` tinyint(1) DEFAULT '0',
  `tgl_realisasi_kembali` date DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`peminjaman_barang_id`),
  KEY `FK_invt_peminjaman_approved_by` (`approved_by`),
  KEY `FK_invt_peminjaman_oleh` (`oleh`),
  KEY `FK_invt_peminjaman_barang_unit` (`unit_id`),
  KEY `FK_invt_peminjaman_barang_status_approval` (`status_approval`),
  CONSTRAINT `FK_invt_peminjaman_approved_by` FOREIGN KEY (`approved_by`) REFERENCES `sysx_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_invt_peminjaman_barang_unit` FOREIGN KEY (`unit_id`) REFERENCES `invt_r_unit` (`unit_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_invt_peminjaman_oleh` FOREIGN KEY (`oleh`) REFERENCES `sysx_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invt_peminjaman_barang` */

/*Table structure for table `invt_pengeluaran_barang` */

DROP TABLE IF EXISTS `invt_pengeluaran_barang`;

CREATE TABLE `invt_pengeluaran_barang` (
  `pengeluaran_barang_id` int(11) NOT NULL AUTO_INCREMENT,
  `keterangan_pengeluaran_id` int(11) DEFAULT NULL,
  `barang_id` int(11) DEFAULT NULL,
  `kode_inventori` varchar(120) DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT '0',
  `lokasi_id` int(11) DEFAULT NULL,
  `tgl_keluar` date DEFAULT NULL,
  `status_akhir` varchar(50) DEFAULT 'DISTRIBUSI' COMMENT '0:distribusi, 1:pindah, 2: pinjam, 3: rusak, 4: musnah',
  `is_has_pic` tinyint(1) DEFAULT '0' COMMENT '0: no, 1: yes',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varbinary(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`pengeluaran_barang_id`),
  KEY `FK_invt_detail_pengeluaran_barang_barang` (`barang_id`),
  KEY `FK_invt_pengeluaran_barang` (`lokasi_id`),
  KEY `FK_invt_pengeluaran_barang_STATUS` (`status_akhir`),
  KEY `FK_invt_pengeluaran_barang_keterangan` (`keterangan_pengeluaran_id`),
  CONSTRAINT `FK_invt_detail_pengeluaran_barang_barang` FOREIGN KEY (`barang_id`) REFERENCES `invt_barang` (`barang_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_invt_pengeluaran_barang` FOREIGN KEY (`lokasi_id`) REFERENCES `invt_r_lokasi` (`lokasi_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_invt_pengeluaran_barang_STATUS` FOREIGN KEY (`status_akhir`) REFERENCES `invt_r_status` (`nama`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_invt_pengeluaran_barang_keterangan` FOREIGN KEY (`keterangan_pengeluaran_id`) REFERENCES `invt_keterangan_pengeluaran` (`keterangan_pengeluaran_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invt_pengeluaran_barang` */

/*Table structure for table `invt_pic_barang` */

DROP TABLE IF EXISTS `invt_pic_barang`;

CREATE TABLE `invt_pic_barang` (
  `pic_barang_id` int(11) NOT NULL AUTO_INCREMENT,
  `pengeluaran_barang_id` int(11) DEFAULT NULL COMMENT 'id distribusi barang',
  `pegawai_id` int(11) DEFAULT NULL COMMENT 'pegawai PIC barang',
  `tgl_assign` date DEFAULT NULL,
  `keterangan` text,
  `is_unassign` tinyint(1) DEFAULT '0',
  `tgl_unassign` date DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`pic_barang_id`),
  KEY `FK_invt_pic_barang` (`pengeluaran_barang_id`),
  KEY `FK_invt_pic_barang_pegawai` (`pegawai_id`),
  CONSTRAINT `FK_invt_pic_barang` FOREIGN KEY (`pengeluaran_barang_id`) REFERENCES `invt_pengeluaran_barang` (`pengeluaran_barang_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_invt_pic_barang_pegawai` FOREIGN KEY (`pegawai_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invt_pic_barang` */

/*Table structure for table `invt_pic_barang_file` */

DROP TABLE IF EXISTS `invt_pic_barang_file`;

CREATE TABLE `invt_pic_barang_file` (
  `pic_barang_file_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_file` varchar(250) DEFAULT NULL,
  `kode_file` varchar(250) DEFAULT NULL,
  `pic_barang_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`pic_barang_file_id`),
  KEY `FK_invt_pic_barang_file` (`pic_barang_id`),
  CONSTRAINT `FK_invt_pic_barang_file` FOREIGN KEY (`pic_barang_id`) REFERENCES `invt_pic_barang` (`pic_barang_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invt_pic_barang_file` */

/*Table structure for table `invt_r_brand` */

DROP TABLE IF EXISTS `invt_r_brand`;

CREATE TABLE `invt_r_brand` (
  `brand_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invt_r_brand` */

/*Table structure for table `invt_r_jenis_barang` */

DROP TABLE IF EXISTS `invt_r_jenis_barang`;

CREATE TABLE `invt_r_jenis_barang` (
  `jenis_barang_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`jenis_barang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invt_r_jenis_barang` */

/*Table structure for table `invt_r_kategori` */

DROP TABLE IF EXISTS `invt_r_kategori`;

CREATE TABLE `invt_r_kategori` (
  `kategori_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`kategori_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invt_r_kategori` */

/*Table structure for table `invt_r_lokasi` */

DROP TABLE IF EXISTS `invt_r_lokasi`;

CREATE TABLE `invt_r_lokasi` (
  `lokasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0',
  `nama_lokasi` varchar(50) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`lokasi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invt_r_lokasi` */

/*Table structure for table `invt_r_satuan` */

DROP TABLE IF EXISTS `invt_r_satuan`;

CREATE TABLE `invt_r_satuan` (
  `satuan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`satuan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invt_r_satuan` */

/*Table structure for table `invt_r_status` */

DROP TABLE IF EXISTS `invt_r_status`;

CREATE TABLE `invt_r_status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) NOT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`status_id`),
  UNIQUE KEY `status_unique` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invt_r_status` */

/*Table structure for table `invt_r_unit` */

DROP TABLE IF EXISTS `invt_r_unit`;

CREATE TABLE `invt_r_unit` (
  `unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`unit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invt_r_unit` */

/*Table structure for table `invt_r_vendor` */

DROP TABLE IF EXISTS `invt_r_vendor`;

CREATE TABLE `invt_r_vendor` (
  `vendor_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) DEFAULT NULL,
  `telp` varchar(15) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `alamat` varchar(150) NOT NULL,
  `link` varchar(250) DEFAULT NULL,
  `contact_person` varchar(200) DEFAULT NULL,
  `telp_contact_person` varchar(15) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`vendor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invt_r_vendor` */

/*Table structure for table `invt_summary_jumlah` */

DROP TABLE IF EXISTS `invt_summary_jumlah`;

CREATE TABLE `invt_summary_jumlah` (
  `summary_jumlah_id` int(11) NOT NULL AUTO_INCREMENT,
  `barang_id` int(11) NOT NULL,
  `kategori_id` int(11) NOT NULL,
  `total_jumlah` int(11) DEFAULT NULL,
  `jumlah_distribusi` int(11) DEFAULT '0',
  `jumlah_gudang` int(11) DEFAULT '0',
  `jumlah_rusak` int(11) DEFAULT '0',
  `jumlah_pinjam` int(11) DEFAULT '0',
  `jumlah_musnah` int(11) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`summary_jumlah_id`),
  KEY `FK_invt_summary_jumlah` (`barang_id`),
  CONSTRAINT `FK_invt_summary_jumlah` FOREIGN KEY (`barang_id`) REFERENCES `invt_barang` (`barang_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invt_summary_jumlah` */

/*Table structure for table `invt_unit_charged` */

DROP TABLE IF EXISTS `invt_unit_charged`;

CREATE TABLE `invt_unit_charged` (
  `unit_charged_id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`unit_charged_id`),
  KEY `FK_invt_unit_user_unit` (`unit_id`),
  KEY `FK_invt_unit_user_user` (`user_id`),
  CONSTRAINT `FK_invt_unit_user_unit` FOREIGN KEY (`unit_id`) REFERENCES `invt_r_unit` (`unit_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_invt_unit_user_user` FOREIGN KEY (`user_id`) REFERENCES `sysx_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `invt_unit_charged` */

/*Table structure for table `kategori` */

DROP TABLE IF EXISTS `kategori`;

CREATE TABLE `kategori` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `kategori` */

insert  into `kategori`(`id`,`nama_kategori`) values (1,'Makanan'),(2,'Minuman');

/*Table structure for table `kmhs_detail_kasus` */

DROP TABLE IF EXISTS `kmhs_detail_kasus`;

CREATE TABLE `kmhs_detail_kasus` (
  `detail_kasus_id` int(11) NOT NULL AUTO_INCREMENT,
  `nim` varchar(8) NOT NULL DEFAULT '',
  `dim_id` int(11) NOT NULL,
  `tgl_kasus` date NOT NULL,
  `jenis_kasus` varchar(20) NOT NULL DEFAULT '',
  `deskripsi` text,
  `no_form` varchar(20) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`detail_kasus_id`),
  KEY `fk_t_detail_kasus_t_dim1_idx` (`dim_id`),
  CONSTRAINT `fk_t_detail_kasus_t_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kmhs_detail_kasus` */

/*Table structure for table `kmhs_master_kasus` */

DROP TABLE IF EXISTS `kmhs_master_kasus`;

CREATE TABLE `kmhs_master_kasus` (
  `master_kasus` int(11) NOT NULL AUTO_INCREMENT,
  `nim` varchar(8) NOT NULL DEFAULT '',
  `no_sp1` varchar(50) NOT NULL DEFAULT '',
  `tgl_sp1` date NOT NULL,
  `uraian_sp1` text,
  `no_sp2` varchar(50) DEFAULT NULL,
  `tgl_sp2` date DEFAULT NULL,
  `uraian_sp2` text,
  `no_sk` varchar(50) DEFAULT NULL,
  `tgl_sk` date DEFAULT NULL,
  `uraian_sk` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `dim_id` int(11) NOT NULL,
  PRIMARY KEY (`master_kasus`),
  UNIQUE KEY `NIM_UNIQUE` (`nim`),
  KEY `fk_t_master_kasus_t_dim1_idx` (`dim_id`),
  CONSTRAINT `fk_t_master_kasus_t_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kmhs_master_kasus` */

/*Table structure for table `kmhs_nilai_perilaku` */

DROP TABLE IF EXISTS `kmhs_nilai_perilaku`;

CREATE TABLE `kmhs_nilai_perilaku` (
  `nilai_perilaku_id` int(11) NOT NULL AUTO_INCREMENT,
  `ta` varchar(30) NOT NULL,
  `sem_ta` int(11) NOT NULL,
  `bulan` int(11) NOT NULL,
  `nim` varchar(8) NOT NULL,
  `kriteria` varchar(4) NOT NULL,
  `nilai` int(11) DEFAULT NULL,
  `kriteria_nilai_perilaku_id` int(11) DEFAULT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`nilai_perilaku_id`),
  KEY `KRITERIA` (`kriteria`),
  KEY `NIM` (`nim`),
  KEY `fk_t_nilai_perilaku_t_kriteria_nilai_perilaku1_idx` (`kriteria_nilai_perilaku_id`),
  KEY `fk_t_nilai_perilaku_t_dim1_idx` (`dim_id`),
  CONSTRAINT `fk_t_nilai_perilaku_t_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_t_nilai_perilaku_t_kriteria_nilai_perilaku1` FOREIGN KEY (`kriteria_nilai_perilaku_id`) REFERENCES `kmhs_r_kriteria_nilai_perilaku` (`kriteria_nilai_perilaku_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kmhs_nilai_perilaku` */

/*Table structure for table `kmhs_nilai_perilaku_arsip` */

DROP TABLE IF EXISTS `kmhs_nilai_perilaku_arsip`;

CREATE TABLE `kmhs_nilai_perilaku_arsip` (
  `nilai_perilaku_arsip_id` int(11) NOT NULL AUTO_INCREMENT,
  `ta` varchar(30) NOT NULL,
  `sem_ta` int(11) NOT NULL,
  `bulan` int(11) NOT NULL,
  `nim` varchar(8) NOT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `kriteria` varchar(4) NOT NULL,
  `kriteria_nilai_perilaku_id` int(11) DEFAULT NULL,
  `nilai` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`nilai_perilaku_arsip_id`),
  KEY `KRITERIA` (`kriteria`),
  KEY `NIM` (`nim`),
  KEY `fk_t_nilai_perilaku_arsip_t_kriteria_nilai_perilaku1_idx` (`kriteria_nilai_perilaku_id`),
  KEY `fk_t_nilai_perilaku_arsip_t_dim1_idx` (`dim_id`),
  CONSTRAINT `fk_t_nilai_perilaku_arsip_t_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_t_nilai_perilaku_arsip_t_kriteria_nilai_perilaku1` FOREIGN KEY (`kriteria_nilai_perilaku_id`) REFERENCES `kmhs_r_kriteria_nilai_perilaku` (`kriteria_nilai_perilaku_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kmhs_nilai_perilaku_arsip` */

/*Table structure for table `kmhs_nilai_perilaku_as` */

DROP TABLE IF EXISTS `kmhs_nilai_perilaku_as`;

CREATE TABLE `kmhs_nilai_perilaku_as` (
  `nilai_perilaku_as_id` int(11) NOT NULL AUTO_INCREMENT,
  `ta` varchar(30) NOT NULL,
  `sem_ta` int(11) NOT NULL,
  `nim` varchar(8) NOT NULL,
  `k1` varchar(11) DEFAULT NULL COMMENT 'Kebersihan Kamar TIdur/Tempat Tidur',
  `k2` varchar(11) DEFAULT NULL COMMENT 'Kerapian Kamar/Tempat Tidur',
  `k3` varchar(11) DEFAULT NULL COMMENT 'Kerapian Almari',
  `k4` varchar(11) DEFAULT NULL COMMENT 'Ketepatan Waktu Keluar/Masuk Kampus',
  `k5` varchar(11) DEFAULT NULL COMMENT 'Ketepatan Waktu Hadir Kuliah',
  `K6` varchar(11) DEFAULT NULL COMMENT 'Kehadiran Saat Makan Pagi',
  `k7` varchar(11) DEFAULT NULL COMMENT 'Kehadiran Saat Makan Siang',
  `k8` varchar(11) DEFAULT NULL COMMENT 'Kehadiran Saat Makan Malam',
  `k9` varchar(11) DEFAULT NULL COMMENT 'Kedisiplinan',
  `k10` varchar(11) DEFAULT NULL COMMENT 'Ketertiban',
  `k11` varchar(11) DEFAULT NULL COMMENT 'Intens Pemanggilan Karena Bermasalah',
  `k12` varchar(11) DEFAULT NULL COMMENT 'Perilaku/Sikap',
  `na` varchar(11) DEFAULT NULL,
  `grade` varchar(2) DEFAULT NULL,
  `catatan` text,
  `dim_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`nilai_perilaku_as_id`),
  KEY `fk_t_nilai_perilaku_as_t_dim1_idx` (`dim_id`),
  CONSTRAINT `fk_t_nilai_perilaku_as_t_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Nilai perilaku akhir semester';

/*Data for the table `kmhs_nilai_perilaku_as` */

/*Table structure for table `kmhs_nilai_perilaku_summary` */

DROP TABLE IF EXISTS `kmhs_nilai_perilaku_summary`;

CREATE TABLE `kmhs_nilai_perilaku_summary` (
  `nilai_perilaku_summary_id` int(11) NOT NULL AUTO_INCREMENT,
  `ta` varchar(30) NOT NULL,
  `sem_ta` int(11) NOT NULL,
  `nim` varchar(8) NOT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `na` int(11) DEFAULT NULL,
  `grade` varchar(2) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`nilai_perilaku_summary_id`),
  KEY `fk_t_nilai_perilaku_summary_t_dim1_idx` (`dim_id`),
  CONSTRAINT `fk_t_nilai_perilaku_summary_t_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kmhs_nilai_perilaku_summary` */

/*Table structure for table `kmhs_nilai_perilaku_ts` */

DROP TABLE IF EXISTS `kmhs_nilai_perilaku_ts`;

CREATE TABLE `kmhs_nilai_perilaku_ts` (
  `nilai_perilaku_ts` int(11) NOT NULL AUTO_INCREMENT,
  `ta` varchar(30) NOT NULL,
  `sem_ta` int(11) NOT NULL,
  `nim` varchar(8) NOT NULL,
  `k1` varchar(11) DEFAULT NULL COMMENT 'Kebersihan',
  `k2` varchar(11) DEFAULT NULL COMMENT 'Kerapian Kamar',
  `k3` varchar(11) DEFAULT NULL COMMENT 'Kerapian Almari',
  `k4` varchar(11) DEFAULT NULL COMMENT 'Ketepatan Waktu Keluar/Masuk Kampus',
  `k5` varchar(11) DEFAULT NULL COMMENT 'Kejujuran',
  `k6` varchar(11) DEFAULT NULL COMMENT 'Kehadiran saat makan pagi',
  `k7` varchar(11) DEFAULT NULL COMMENT 'Kehadiran saat makan siang',
  `k8` varchar(11) DEFAULT NULL COMMENT 'Kehadiran saat makan malam',
  `k9` varchar(11) DEFAULT NULL COMMENT 'Kedisiplinan',
  `k10` varchar(11) DEFAULT NULL COMMENT 'Ketertiban',
  `k11` varchar(11) DEFAULT NULL COMMENT 'Intens',
  `k12` varchar(11) DEFAULT NULL COMMENT 'Perilaku/Sikap',
  `na` int(11) DEFAULT NULL,
  `grade` varchar(2) DEFAULT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`nilai_perilaku_ts`),
  KEY `fk_t_nilai_perilaku_ts_t_dim1_idx` (`dim_id`),
  CONSTRAINT `fk_t_nilai_perilaku_ts_t_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Nilai perilaku tengah semester';

/*Data for the table `kmhs_nilai_perilaku_ts` */

/*Table structure for table `kmhs_r_kasus` */

DROP TABLE IF EXISTS `kmhs_r_kasus`;

CREATE TABLE `kmhs_r_kasus` (
  `ref_kasus_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kasus` varchar(20) NOT NULL DEFAULT '',
  `deskripsi` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ref_kasus_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kmhs_r_kasus` */

/*Table structure for table `kmhs_r_kriteria_nilai_perilaku` */

DROP TABLE IF EXISTS `kmhs_r_kriteria_nilai_perilaku`;

CREATE TABLE `kmhs_r_kriteria_nilai_perilaku` (
  `kriteria_nilai_perilaku_id` int(11) NOT NULL AUTO_INCREMENT,
  `kriteria` varchar(4) NOT NULL,
  `deskripsi` varchar(50) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`kriteria_nilai_perilaku_id`),
  UNIQUE KEY `KRITERIA_UNIQUE` (`kriteria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kmhs_r_kriteria_nilai_perilaku` */

/*Table structure for table `kntn_dim_meja` */

DROP TABLE IF EXISTS `kntn_dim_meja`;

CREATE TABLE `kntn_dim_meja` (
  `dim_meja_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_dim_meja` tinyint(1) DEFAULT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `meja_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`dim_meja_id`),
  KEY `meja_id` (`meja_id`),
  KEY `dim_id` (`dim_id`),
  CONSTRAINT `kntn_dim_meja_ibfk_1` FOREIGN KEY (`meja_id`) REFERENCES `kntn_meja` (`meja_id`),
  CONSTRAINT `kntn_dim_meja_ibfk_2` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `kntn_dim_meja` */

insert  into `kntn_dim_meja`(`dim_meja_id`,`status_dim_meja`,`dim_id`,`meja_id`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (2,0,5,11,0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(3,0,6,2,0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(4,0,8,2,0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(5,0,9,3,0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(6,0,10,3,0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(7,0,11,9,0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(8,0,12,12,0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL);

/*Table structure for table `kntn_izin_makan` */

DROP TABLE IF EXISTS `kntn_izin_makan`;

CREATE TABLE `kntn_izin_makan` (
  `izin_makan_id` int(11) NOT NULL AUTO_INCREMENT,
  `jam_makan` varchar(45) NOT NULL,
  `tanggal` date NOT NULL,
  `desc` text,
  `status_by_keasramaan` int(11) DEFAULT NULL,
  `status_by_dosen` int(11) DEFAULT NULL,
  `jenis_izin_id` int(11) DEFAULT NULL,
  `jenis_kegiatan_id` int(11) DEFAULT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `dosen_id` int(11) DEFAULT NULL,
  `keasramaan_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`izin_makan_id`),
  KEY `dim_id` (`dim_id`),
  KEY `dosen_id` (`dosen_id`),
  KEY `keasramaan_id` (`keasramaan_id`),
  KEY `jenis_kegiatan_id` (`jenis_kegiatan_id`),
  KEY `jenis_izin_id` (`jenis_izin_id`),
  KEY `status_by_keasramaan` (`status_by_keasramaan`),
  KEY `status_by_dosen` (`status_by_dosen`),
  CONSTRAINT `kntn_izin_makan_ibfk_1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`),
  CONSTRAINT `kntn_izin_makan_ibfk_2` FOREIGN KEY (`dosen_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`),
  CONSTRAINT `kntn_izin_makan_ibfk_3` FOREIGN KEY (`keasramaan_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`),
  CONSTRAINT `kntn_izin_makan_ibfk_4` FOREIGN KEY (`jenis_kegiatan_id`) REFERENCES `kntn_r_jenis_kegiatan` (`jenis_kegiatan_id`),
  CONSTRAINT `kntn_izin_makan_ibfk_5` FOREIGN KEY (`jenis_izin_id`) REFERENCES `kntn_r_jenis_izin` (`jenis_izin_id`),
  CONSTRAINT `kntn_izin_makan_ibfk_6` FOREIGN KEY (`status_by_keasramaan`) REFERENCES `kntn_r_request` (`request_id`),
  CONSTRAINT `kntn_izin_makan_ibfk_7` FOREIGN KEY (`status_by_dosen`) REFERENCES `kntn_r_request` (`request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `kntn_izin_makan` */

insert  into `kntn_izin_makan`(`izin_makan_id`,`jam_makan`,`tanggal`,`desc`,`status_by_keasramaan`,`status_by_dosen`,`jenis_izin_id`,`jenis_kegiatan_id`,`dim_id`,`dosen_id`,`keasramaan_id`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,'Pagi','2019-04-29','Izin untuk penelitian',1,1,3,1,5,NULL,NULL,0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(2,'Pagi','2019-05-28','TA',2,2,1,1,5,1,243,0,NULL,NULL,'2019-05-27 16:56:30','iss16022','2019-05-27 16:58:30','keasramaan'),(3,'Pagi','2019-05-28','ga papa',2,NULL,3,2,5,NULL,242,0,NULL,NULL,'2019-05-27 17:22:27','iss16022','2019-05-27 17:23:21','iss16022'),(4,'Malam','2019-05-28','sakit',1,NULL,3,2,5,NULL,NULL,0,NULL,NULL,'2019-05-27 17:37:36','iss16022','2019-05-27 17:37:36','iss16022'),(5,'Malam','2019-05-28','coba',2,NULL,1,2,11,NULL,242,0,NULL,NULL,'2019-05-27 18:25:11','andre','2019-05-27 18:25:33','iss16022'),(6,'Pagi','2019-05-28','Tidak enak',1,NULL,3,2,5,NULL,NULL,0,NULL,NULL,'2019-05-27 21:33:36','iss16022','2019-05-27 21:33:36','iss16022'),(7,'Pagi','2019-05-28','asdf',2,1,2,1,12,NULL,NULL,0,NULL,NULL,'2019-05-27 21:38:07','itok','2019-05-27 21:38:07','itok');

/*Table structure for table `kntn_kantin` */

DROP TABLE IF EXISTS `kntn_kantin`;

CREATE TABLE `kntn_kantin` (
  `kantin_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `kapasitas` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`kantin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `kntn_kantin` */

insert  into `kntn_kantin`(`kantin_id`,`name`,`kapasitas`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,'Kantin Baru 1 Lantai 1',100,0,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(2,'Kantin Baru 1 Lantai 2',100,0,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(3,'Kantin Baru 2 Lantai 1',100,0,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(4,'Kantin Baru 2 Lantai 2',100,0,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(5,'Kantin Baru Tengah Lantai 1',50,0,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(6,'Kantin Baru Tengah Lantai 2',50,0,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL);

/*Table structure for table `kntn_meja` */

DROP TABLE IF EXISTS `kntn_meja`;

CREATE TABLE `kntn_meja` (
  `meja_id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(32) DEFAULT NULL,
  `kantin_id` int(11) DEFAULT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`meja_id`),
  KEY `kantin_id` (`kantin_id`),
  KEY `dim_id` (`dim_id`),
  CONSTRAINT `kntn_meja_ibfk_1` FOREIGN KEY (`kantin_id`) REFERENCES `kntn_kantin` (`kantin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `kntn_meja` */

insert  into `kntn_meja`(`meja_id`,`kode`,`kantin_id`,`dim_id`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,'KB11-01',1,NULL,0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(2,'KB11-01',1,NULL,0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(3,'KB11-01',1,NULL,0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(4,'KB11-01',1,NULL,0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(5,'KB11-01',1,NULL,0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(6,'KB11-01',1,NULL,0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(7,'KB11-01',1,NULL,0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(8,'KB11-01',1,NULL,0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(9,'KB11-01',1,NULL,0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(10,'KB11-01',1,NULL,0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(11,'KB11-01',1,NULL,0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(12,'KB11-01',1,NULL,0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL);

/*Table structure for table `kntn_r_jenis_izin` */

DROP TABLE IF EXISTS `kntn_r_jenis_izin`;

CREATE TABLE `kntn_r_jenis_izin` (
  `jenis_izin_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_izin` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`jenis_izin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `kntn_r_jenis_izin` */

insert  into `kntn_r_jenis_izin`(`jenis_izin_id`,`jenis_izin`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,'Izin Telat Makan',0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(2,'Izin Terlambat Makan',0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(3,'Izin Tidak Makan',0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL);

/*Table structure for table `kntn_r_jenis_kegiatan` */

DROP TABLE IF EXISTS `kntn_r_jenis_kegiatan`;

CREATE TABLE `kntn_r_jenis_kegiatan` (
  `jenis_kegiatan_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_kegiatan` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`jenis_kegiatan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `kntn_r_jenis_kegiatan` */

insert  into `kntn_r_jenis_kegiatan`(`jenis_kegiatan_id`,`jenis_kegiatan`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,'Akademik',0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(2,'Non Akademik',0,NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL);

/*Table structure for table `kntn_r_request` */

DROP TABLE IF EXISTS `kntn_r_request`;

CREATE TABLE `kntn_r_request` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT,
  `request` varchar(100) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `kntn_r_request` */

insert  into `kntn_r_request`(`request_id`,`request`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,'Menunggu',0,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(2,'Disetujui',0,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(3,'Ditolak',0,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL),(4,'Dibatalkan',0,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',NULL);

/*Table structure for table `krkm_course_group` */

DROP TABLE IF EXISTS `krkm_course_group`;

CREATE TABLE `krkm_course_group` (
  `course_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(20) NOT NULL DEFAULT '',
  `eng` varchar(255) NOT NULL DEFAULT '',
  `ina` varchar(255) DEFAULT NULL,
  `kpt_kode` varchar(10) NOT NULL DEFAULT '',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`course_group_id`),
  UNIQUE KEY `ID_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `krkm_course_group` */

/*Table structure for table `krkm_kuliah` */

DROP TABLE IF EXISTS `krkm_kuliah`;

CREATE TABLE `krkm_kuliah` (
  `kuliah_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kur` int(4) NOT NULL DEFAULT '2001',
  `kode_mk` varchar(11) NOT NULL,
  `nama_kul_ind` varchar(255) DEFAULT NULL,
  `nama_kul_ing` varchar(255) DEFAULT NULL,
  `short_name` varchar(20) DEFAULT NULL,
  `kbk_id` varchar(20) DEFAULT '0',
  `course_group` varchar(20) DEFAULT NULL,
  `sks` smallint(6) DEFAULT NULL,
  `sem` smallint(6) DEFAULT NULL,
  `urut_dlm_sem` smallint(6) DEFAULT NULL,
  `sifat` smallint(6) DEFAULT NULL,
  `meetings` varchar(100) DEFAULT NULL,
  `tipe` varchar(25) DEFAULT NULL,
  `level` varchar(15) DEFAULT NULL,
  `key_topics_ind` text,
  `key_topics_ing` text,
  `objektif_ind` text,
  `objektif_ing` text,
  `lab_hour` tinyint(4) DEFAULT NULL,
  `tutorial_hour` tinyint(4) DEFAULT NULL,
  `course_hour` tinyint(4) DEFAULT NULL,
  `course_hour_in_week` tinyint(4) DEFAULT NULL,
  `lab_hour_in_week` tinyint(4) DEFAULT NULL,
  `number_week` tinyint(4) DEFAULT NULL,
  `other_activity` varchar(50) DEFAULT '..............',
  `other_activity_hour` tinyint(4) DEFAULT NULL,
  `knowledge` tinyint(4) DEFAULT NULL,
  `skill` tinyint(4) DEFAULT NULL,
  `attitude` tinyint(4) DEFAULT NULL,
  `uts` tinyint(4) DEFAULT NULL,
  `uas` tinyint(4) DEFAULT NULL,
  `tugas` tinyint(4) DEFAULT NULL,
  `quiz` tinyint(4) DEFAULT NULL,
  `whiteboard` char(1) DEFAULT NULL,
  `lcd` char(1) DEFAULT NULL,
  `courseware` char(1) DEFAULT NULL,
  `lab` char(1) DEFAULT NULL,
  `elearning` char(1) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `prerequisites` text,
  `course_description` text,
  `course_objectives` text,
  `learning_outcomes` text,
  `course_format` text,
  `grading_procedure` text,
  `course_content` text,
  `ref_kbk_id` int(11) DEFAULT NULL,
  `course_group_id` int(11) DEFAULT NULL,
  `tahun_kurikulum_id` int(11) DEFAULT NULL,
  `web_page` varchar(150) DEFAULT NULL,
  `ekstrakurikuler` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`kuliah_id`),
  KEY `KODE_MK_2` (`kode_mk`),
  KEY `NAMA_KUL_IND` (`nama_kul_ind`),
  KEY `NAMA_KUL_ING` (`nama_kul_ing`),
  KEY `fk_t_kurikulum_t_ref_kbk2_idx` (`ref_kbk_id`),
  KEY `FK_krkm_kurikulum` (`tahun_kurikulum_id`),
  KEY `FK_krkm_kurikulum_cg` (`course_group_id`),
  CONSTRAINT `FK_krkm_kurikulum` FOREIGN KEY (`tahun_kurikulum_id`) REFERENCES `krkm_r_tahun_kurikulum` (`tahun_kurikulum_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_krkm_kurikulum_cg` FOREIGN KEY (`course_group_id`) REFERENCES `krkm_course_group` (`course_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_t_kurikulum_t_ref_kbk2` FOREIGN KEY (`ref_kbk_id`) REFERENCES `inst_prodi` (`ref_kbk_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `krkm_kuliah` */

/*Table structure for table `krkm_kuliah_prodi` */

DROP TABLE IF EXISTS `krkm_kuliah_prodi`;

CREATE TABLE `krkm_kuliah_prodi` (
  `krkm_kuliah_prodi_id` int(11) NOT NULL AUTO_INCREMENT,
  `kuliah_id` int(11) DEFAULT NULL,
  `ref_kbk_id` int(11) DEFAULT NULL,
  `semester` int(11) DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`krkm_kuliah_prodi_id`),
  KEY `FK_krkm_kuliah_prodi` (`kuliah_id`),
  KEY `FK_krkm_kuliah_prodi_ref_kbk` (`ref_kbk_id`),
  CONSTRAINT `FK_krkm_kuliah_prodi` FOREIGN KEY (`kuliah_id`) REFERENCES `krkm_kuliah` (`kuliah_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_krkm_kuliah_prodi_ref_kbk` FOREIGN KEY (`ref_kbk_id`) REFERENCES `inst_prodi` (`ref_kbk_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `krkm_kuliah_prodi` */

/*Table structure for table `krkm_kurikulum_prodi` */

DROP TABLE IF EXISTS `krkm_kurikulum_prodi`;

CREATE TABLE `krkm_kurikulum_prodi` (
  `kurikulum_prodi_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kur` int(4) NOT NULL DEFAULT '2012',
  `kode_mk` varchar(11) NOT NULL,
  `kbk_id` varchar(255) NOT NULL DEFAULT '',
  `kurikulum_id` int(11) DEFAULT NULL,
  `ref_kbk_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`kurikulum_prodi_id`),
  KEY `fk_t_kurikulum_prodi_t_kurikulum1_idx` (`kurikulum_id`),
  KEY `fk_t_kurikulum_prodi_t_ref_kbk1_idx` (`ref_kbk_id`),
  CONSTRAINT `fk_t_kurikulum_prodi_t_kurikulum1` FOREIGN KEY (`kurikulum_id`) REFERENCES `krkm_kuliah` (`kuliah_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_t_kurikulum_prodi_t_ref_kbk1` FOREIGN KEY (`ref_kbk_id`) REFERENCES `inst_prodi` (`ref_kbk_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `krkm_kurikulum_prodi` */

/*Table structure for table `krkm_prerequisite_courses` */

DROP TABLE IF EXISTS `krkm_prerequisite_courses`;

CREATE TABLE `krkm_prerequisite_courses` (
  `prerequisite_courses_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kur` int(4) DEFAULT '0',
  `kode_mk` varchar(11) DEFAULT NULL,
  `kuliah_id` int(11) DEFAULT NULL,
  `id_kur_pre` int(4) DEFAULT '0',
  `kode_mk_pre` varchar(10) DEFAULT NULL,
  `kuliah_pre_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`prerequisite_courses_id`),
  KEY `FK_krkm_prerequisite_courses_kuri` (`kuliah_id`),
  KEY `FK_krkm_prerequisite_courses_kuri_pre` (`kuliah_pre_id`),
  CONSTRAINT `FK_krkm_prerequisite_courses_kuri` FOREIGN KEY (`kuliah_id`) REFERENCES `krkm_kuliah` (`kuliah_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_krkm_prerequisite_courses_kuri_pre` FOREIGN KEY (`kuliah_pre_id`) REFERENCES `krkm_kuliah` (`kuliah_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `krkm_prerequisite_courses` */

/*Table structure for table `krkm_r_tahun_kurikulum` */

DROP TABLE IF EXISTS `krkm_r_tahun_kurikulum`;

CREATE TABLE `krkm_r_tahun_kurikulum` (
  `tahun_kurikulum_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`tahun_kurikulum_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `krkm_r_tahun_kurikulum` */

/*Table structure for table `krkm_sifat_kuliah` */

DROP TABLE IF EXISTS `krkm_sifat_kuliah`;

CREATE TABLE `krkm_sifat_kuliah` (
  `sifat_kuliah_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`sifat_kuliah_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `krkm_sifat_kuliah` */

/*Table structure for table `kursi` */

DROP TABLE IF EXISTS `kursi`;

CREATE TABLE `kursi` (
  `id_kursi` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_kursi`),
  KEY `fk_kursi_user` (`id_user`),
  KEY `fk_kursi_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Data for the table `kursi` */

insert  into `kursi`(`id_kursi`,`kode`,`status`,`id_user`) values (1,'K11',2,1),(2,'K12',1,2),(3,'K13',1,3),(4,'K14',1,4),(5,'K15',1,5),(6,'K21',1,1),(7,'K22',2,NULL),(8,'K23',1,1),(9,'K24',1,NULL),(10,'K25',2,NULL),(12,'K31',1,NULL),(13,'K32',1,NULL),(14,'K33',1,NULL),(15,'K34',1,NULL),(16,'K35',1,NULL),(17,'K41',1,NULL),(18,'K42',1,NULL),(19,'K43',1,NULL),(20,'K44',1,NULL),(21,'K45',1,NULL),(22,'K51',1,NULL),(23,'K52',1,NULL),(24,'K53',1,NULL),(25,'K54',1,NULL),(26,'K55',1,NULL);

/*Table structure for table `lppm_penelitian` */

DROP TABLE IF EXISTS `lppm_penelitian`;

CREATE TABLE `lppm_penelitian` (
  `penelitian_id` int(11) NOT NULL AUTO_INCREMENT,
  `judul_penelitian` varchar(500) DEFAULT NULL,
  `tahun` varchar(10) DEFAULT NULL,
  `biaya` varchar(50) DEFAULT NULL,
  `sumber_dana` varchar(100) DEFAULT NULL,
  `skema` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`penelitian_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `lppm_penelitian` */

/*Table structure for table `lppm_penelitian_dosen` */

DROP TABLE IF EXISTS `lppm_penelitian_dosen`;

CREATE TABLE `lppm_penelitian_dosen` (
  `penelitian_dosen_id` int(11) NOT NULL AUTO_INCREMENT,
  `penelitian_id` int(11) DEFAULT NULL,
  `dosen_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`penelitian_dosen_id`),
  KEY `dosen_id` (`dosen_id`),
  KEY `lppm_penelitian_dosen_ibfk_3` (`penelitian_id`),
  CONSTRAINT `lppm_penelitian_dosen_ibfk_2` FOREIGN KEY (`dosen_id`) REFERENCES `hrdx_dosen` (`dosen_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lppm_penelitian_dosen_ibfk_3` FOREIGN KEY (`penelitian_id`) REFERENCES `lppm_penelitian` (`penelitian_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `lppm_penelitian_dosen` */

/*Table structure for table `lppm_r_karyailmiah` */

DROP TABLE IF EXISTS `lppm_r_karyailmiah`;

CREATE TABLE `lppm_r_karyailmiah` (
  `karyailmiah_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(20) NOT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`karyailmiah_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `lppm_r_karyailmiah` */

/*Table structure for table `lppm_r_subkaryailmiah` */

DROP TABLE IF EXISTS `lppm_r_subkaryailmiah`;

CREATE TABLE `lppm_r_subkaryailmiah` (
  `subkaryailmiah_id` int(11) NOT NULL AUTO_INCREMENT,
  `karyailmiah_id` int(11) NOT NULL,
  `jenis` varchar(50) NOT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`subkaryailmiah_id`),
  KEY `FK_lppm_r_subkaryailmiah` (`karyailmiah_id`),
  CONSTRAINT `FK_lppm_r_subkaryailmiah` FOREIGN KEY (`karyailmiah_id`) REFERENCES `lppm_r_karyailmiah` (`karyailmiah_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `lppm_r_subkaryailmiah` */

/*Table structure for table `lppm_t_author_publikasi` */

DROP TABLE IF EXISTS `lppm_t_author_publikasi`;

CREATE TABLE `lppm_t_author_publikasi` (
  `author_publikasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `publikasi_id` int(11) DEFAULT NULL,
  `nama_author` varchar(255) DEFAULT NULL,
  `institusi` varchar(100) DEFAULT '255',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`author_publikasi_id`),
  KEY `FK_lppm_t_author_publikasi` (`publikasi_id`),
  CONSTRAINT `FK_lppm_t_author_publikasi` FOREIGN KEY (`publikasi_id`) REFERENCES `lppm_t_publikasi` (`publikasi_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `lppm_t_author_publikasi` */

/*Table structure for table `lppm_t_ketua_gbk` */

DROP TABLE IF EXISTS `lppm_t_ketua_gbk`;

CREATE TABLE `lppm_t_ketua_gbk` (
  `ketuagbk_id` int(11) NOT NULL AUTO_INCREMENT,
  `gbk_id` int(11) NOT NULL,
  `dosen_id` int(11) DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ketuagbk_id`),
  KEY `FK_lppm_t_ketua_gbk_dosen` (`dosen_id`),
  KEY `FK_lppm_t_ketua_gbk_gbk` (`gbk_id`),
  CONSTRAINT `FK_lppm_t_ketua_gbk_dosen` FOREIGN KEY (`dosen_id`) REFERENCES `hrdx_dosen` (`dosen_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_lppm_t_ketua_gbk_gbk` FOREIGN KEY (`gbk_id`) REFERENCES `mref_r_gbk` (`gbk_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `lppm_t_ketua_gbk` */

/*Table structure for table `lppm_t_logreview` */

DROP TABLE IF EXISTS `lppm_t_logreview`;

CREATE TABLE `lppm_t_logreview` (
  `logreview_id` int(11) NOT NULL AUTO_INCREMENT,
  `publikasi_id` int(11) NOT NULL,
  `pegawai_id` int(11) DEFAULT NULL,
  `catatanperbaikanreview` text,
  `review` int(1) DEFAULT '0',
  `status` int(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`logreview_id`),
  KEY `FK_lppm_t_logreview_pegawai` (`pegawai_id`),
  KEY `FK_lppm_t_logreview_publikasi` (`publikasi_id`),
  CONSTRAINT `FK_lppm_t_logreview_pegawai` FOREIGN KEY (`pegawai_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_lppm_t_logreview_publikasi` FOREIGN KEY (`publikasi_id`) REFERENCES `lppm_t_publikasi` (`publikasi_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `lppm_t_logreview` */

/*Table structure for table `lppm_t_publikasi` */

DROP TABLE IF EXISTS `lppm_t_publikasi`;

CREATE TABLE `lppm_t_publikasi` (
  `publikasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `pegawai_id` int(11) DEFAULT NULL,
  `subkaryailmiah_id` int(11) DEFAULT NULL,
  `konferensi` varchar(50) DEFAULT NULL,
  `review` int(11) DEFAULT '0',
  `gbk_id` int(11) DEFAULT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `abstrak` text,
  `deadline` date DEFAULT NULL,
  `tanggal_publish` date DEFAULT NULL,
  `keterangan` varchar(1000) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `reward` int(11) DEFAULT '0',
  `approved_ketuagbk` int(11) DEFAULT '0',
  `website` varchar(50) DEFAULT NULL,
  `path_uploaddokumen` varchar(255) DEFAULT NULL,
  `kode_file` varchar(50) DEFAULT NULL,
  `is_published` tinyint(1) DEFAULT '0',
  `pesan` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`publikasi_id`),
  KEY `FK_lppm_t_publikasi_pegawai` (`pegawai_id`),
  KEY `FK_lppm_t_publikasi_sub_karya_ilmiah` (`subkaryailmiah_id`),
  KEY `FK_lppm_t_publikasi_gbk` (`gbk_id`),
  CONSTRAINT `FK_lppm_t_publikasi_gbk` FOREIGN KEY (`gbk_id`) REFERENCES `mref_r_gbk` (`gbk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_lppm_t_publikasi_pegawai` FOREIGN KEY (`pegawai_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_lppm_t_publikasi_sub_karya_ilmiah` FOREIGN KEY (`subkaryailmiah_id`) REFERENCES `lppm_r_subkaryailmiah` (`subkaryailmiah_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `lppm_t_publikasi` */

/*Table structure for table `lppm_t_registrasi_file` */

DROP TABLE IF EXISTS `lppm_t_registrasi_file`;

CREATE TABLE `lppm_t_registrasi_file` (
  `registrasifile_id` int(11) NOT NULL AUTO_INCREMENT,
  `registrasipublikasi_id` int(11) DEFAULT NULL,
  `nama_file` varchar(255) DEFAULT NULL,
  `kode_file` varchar(255) DEFAULT NULL,
  `index_file` int(11) DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`registrasifile_id`),
  KEY `FK_lppm_t_registrasi_file` (`registrasipublikasi_id`),
  CONSTRAINT `FK_lppm_t_registrasi_file` FOREIGN KEY (`registrasipublikasi_id`) REFERENCES `lppm_t_registrasipublikasi` (`registrasipublikasi_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `lppm_t_registrasi_file` */

/*Table structure for table `lppm_t_registrasi_jurnal` */

DROP TABLE IF EXISTS `lppm_t_registrasi_jurnal`;

CREATE TABLE `lppm_t_registrasi_jurnal` (
  `registrasi_jurnal_id` int(11) NOT NULL AUTO_INCREMENT,
  `publikasi_id` int(11) DEFAULT NULL,
  `biaya_registrasi` decimal(11,0) DEFAULT NULL,
  `status_approved` int(11) DEFAULT NULL,
  `keterangan_approved` text,
  `catatan` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`registrasi_jurnal_id`),
  KEY `FK_lppm_t_registrasi_jurnal` (`publikasi_id`),
  CONSTRAINT `FK_lppm_t_registrasi_jurnal` FOREIGN KEY (`publikasi_id`) REFERENCES `lppm_t_publikasi` (`publikasi_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `lppm_t_registrasi_jurnal` */

/*Table structure for table `lppm_t_registrasi_jurnal_file` */

DROP TABLE IF EXISTS `lppm_t_registrasi_jurnal_file`;

CREATE TABLE `lppm_t_registrasi_jurnal_file` (
  `registrasi_jurnal_file_id` int(11) NOT NULL AUTO_INCREMENT,
  `registrasi_jurnal_id` int(11) DEFAULT NULL,
  `nama_file` varchar(200) DEFAULT NULL,
  `kode_file` varchar(200) DEFAULT NULL,
  `index_file` int(11) DEFAULT NULL,
  `keterangan` text,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`registrasi_jurnal_file_id`),
  KEY `FK_lppm_t_registrasi_jurnal_file` (`registrasi_jurnal_id`),
  CONSTRAINT `FK_lppm_t_registrasi_jurnal_file` FOREIGN KEY (`registrasi_jurnal_id`) REFERENCES `lppm_t_registrasi_jurnal` (`registrasi_jurnal_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `lppm_t_registrasi_jurnal_file` */

/*Table structure for table `lppm_t_registrasi_prosiding` */

DROP TABLE IF EXISTS `lppm_t_registrasi_prosiding`;

CREATE TABLE `lppm_t_registrasi_prosiding` (
  `registrasi_prosiding_id` int(11) NOT NULL AUTO_INCREMENT,
  `publikasi_id` int(11) DEFAULT NULL,
  `waktu_mulai` date DEFAULT NULL,
  `waktu_selesai` date DEFAULT NULL,
  `rute_transport_udara` varchar(255) DEFAULT NULL,
  `rute_transport_laut` varchar(255) DEFAULT NULL,
  `rute_transport_darat` varchar(255) DEFAULT NULL,
  `status_approved` int(11) DEFAULT NULL,
  `keterangan_approved` text,
  `catatan` text,
  `biaya_pendaftaran` decimal(11,0) DEFAULT NULL,
  `biaya_transport_darat` decimal(11,0) DEFAULT NULL,
  `biaya_transport_laut` decimal(11,0) DEFAULT NULL,
  `biaya_transport_udara` decimal(11,0) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`registrasi_prosiding_id`),
  KEY `FK_lppm_t_registrasi_prosiding` (`publikasi_id`),
  CONSTRAINT `FK_lppm_t_registrasi_prosiding` FOREIGN KEY (`publikasi_id`) REFERENCES `lppm_t_publikasi` (`publikasi_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `lppm_t_registrasi_prosiding` */

/*Table structure for table `lppm_t_registrasi_prosiding_file` */

DROP TABLE IF EXISTS `lppm_t_registrasi_prosiding_file`;

CREATE TABLE `lppm_t_registrasi_prosiding_file` (
  `registrasi_prosiding_file_id` int(11) NOT NULL AUTO_INCREMENT,
  `registrasi_prosiding_id` int(11) DEFAULT NULL,
  `nama_file` varchar(200) DEFAULT NULL,
  `kode_file` varchar(200) DEFAULT NULL,
  `index_file` int(11) DEFAULT NULL,
  `keterangan` text,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`registrasi_prosiding_file_id`),
  KEY `FK_lppm_t_registrasi_prosiding_file` (`registrasi_prosiding_id`),
  CONSTRAINT `FK_lppm_t_registrasi_prosiding_file` FOREIGN KEY (`registrasi_prosiding_id`) REFERENCES `lppm_t_registrasi_prosiding` (`registrasi_prosiding_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `lppm_t_registrasi_prosiding_file` */

/*Table structure for table `lppm_t_registrasipublikasi` */

DROP TABLE IF EXISTS `lppm_t_registrasipublikasi`;

CREATE TABLE `lppm_t_registrasipublikasi` (
  `registrasipublikasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `publikasi_id` int(11) NOT NULL,
  `biaya_pendaftaran` decimal(11,0) DEFAULT NULL,
  `biaya_transport` decimal(11,0) DEFAULT NULL,
  `keterangan_transport` varchar(255) DEFAULT NULL,
  `biaya_transport2` decimal(11,0) DEFAULT NULL,
  `keterangan_transport2` varchar(255) DEFAULT NULL,
  `biaya_penginapan` decimal(11,0) DEFAULT NULL,
  `tanggal_berangkat` date DEFAULT NULL,
  `tanggal_pulang` date DEFAULT NULL,
  `status_approved` int(11) DEFAULT '0',
  `keterangan_approved` varchar(255) DEFAULT NULL,
  `catatan` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`registrasipublikasi_id`),
  KEY `FK_lppm_t_registrasipublikasi_publikasi` (`publikasi_id`),
  CONSTRAINT `FK_lppm_t_registrasipublikasi_publikasi` FOREIGN KEY (`publikasi_id`) REFERENCES `lppm_t_publikasi` (`publikasi_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `lppm_t_registrasipublikasi` */

/*Table structure for table `lppm_t_review_file` */

DROP TABLE IF EXISTS `lppm_t_review_file`;

CREATE TABLE `lppm_t_review_file` (
  `reviewfile_id` int(11) NOT NULL AUTO_INCREMENT,
  `logreview_id` int(11) DEFAULT NULL,
  `nama_file` varchar(255) DEFAULT NULL,
  `kode_file` varchar(255) DEFAULT NULL,
  `index_file` int(11) DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`reviewfile_id`),
  KEY `FK_lppm_t_review_file_log_review` (`logreview_id`),
  CONSTRAINT `FK_lppm_t_review_file_log_review` FOREIGN KEY (`logreview_id`) REFERENCES `lppm_t_logreview` (`logreview_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `lppm_t_review_file` */

/*Table structure for table `lppm_t_reward_file` */

DROP TABLE IF EXISTS `lppm_t_reward_file`;

CREATE TABLE `lppm_t_reward_file` (
  `rewardfile_id` int(11) NOT NULL AUTO_INCREMENT,
  `rewardpublikasi_id` int(11) DEFAULT NULL,
  `nama_file` varchar(255) DEFAULT NULL,
  `kode_file` varchar(255) DEFAULT NULL,
  `index_file` int(11) DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`rewardfile_id`),
  KEY `FK_lppm_t_reward_file` (`rewardpublikasi_id`),
  CONSTRAINT `FK_lppm_t_reward_file` FOREIGN KEY (`rewardpublikasi_id`) REFERENCES `lppm_t_rewardpublikasi` (`rewardpublikasi_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `lppm_t_reward_file` */

/*Table structure for table `lppm_t_reward_jurnal` */

DROP TABLE IF EXISTS `lppm_t_reward_jurnal`;

CREATE TABLE `lppm_t_reward_jurnal` (
  `reward_jurnal_id` int(11) NOT NULL AUTO_INCREMENT,
  `publikasi_id` int(11) DEFAULT NULL,
  `issn` varchar(50) DEFAULT NULL,
  `volume` varchar(50) DEFAULT NULL,
  `nomor` varchar(50) DEFAULT NULL,
  `halaman_awal` int(11) DEFAULT NULL,
  `halaman_akhir` int(11) DEFAULT NULL,
  `status_reward` int(11) DEFAULT NULL,
  `keterangan_reward` text,
  `jumlah` decimal(11,0) DEFAULT NULL,
  `catatan` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`reward_jurnal_id`),
  KEY `FK_lppm_t_reward_jurnal` (`publikasi_id`),
  CONSTRAINT `FK_lppm_t_reward_jurnal` FOREIGN KEY (`publikasi_id`) REFERENCES `lppm_t_publikasi` (`publikasi_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `lppm_t_reward_jurnal` */

/*Table structure for table `lppm_t_reward_jurnal_file` */

DROP TABLE IF EXISTS `lppm_t_reward_jurnal_file`;

CREATE TABLE `lppm_t_reward_jurnal_file` (
  `reward_jurnal_file_id` int(11) NOT NULL AUTO_INCREMENT,
  `reward_jurnal_id` int(11) DEFAULT NULL,
  `nama_file` varchar(200) DEFAULT NULL,
  `kode_file` varchar(200) DEFAULT NULL,
  `index_file` int(11) DEFAULT NULL,
  `keterangan` text,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`reward_jurnal_file_id`),
  KEY `FK_lppm_t_reward_jurnal_file` (`reward_jurnal_id`),
  CONSTRAINT `FK_lppm_t_reward_jurnal_file` FOREIGN KEY (`reward_jurnal_id`) REFERENCES `lppm_t_reward_jurnal` (`reward_jurnal_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `lppm_t_reward_jurnal_file` */

/*Table structure for table `lppm_t_reward_prosiding` */

DROP TABLE IF EXISTS `lppm_t_reward_prosiding`;

CREATE TABLE `lppm_t_reward_prosiding` (
  `reward_prosiding_id` int(11) NOT NULL AUTO_INCREMENT,
  `publikasi_id` int(11) DEFAULT NULL,
  `institusi_penyelenggara` varchar(150) DEFAULT NULL,
  `tempat_pelaksanaan` varchar(200) DEFAULT NULL,
  `status_reward` int(11) DEFAULT NULL,
  `keterangan_reward` text,
  `catatan` text,
  `jumlah` decimal(11,0) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`reward_prosiding_id`),
  KEY `FK_lppm_t_reward_prosiding` (`publikasi_id`),
  CONSTRAINT `FK_lppm_t_reward_prosiding` FOREIGN KEY (`publikasi_id`) REFERENCES `lppm_t_publikasi` (`publikasi_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `lppm_t_reward_prosiding` */

/*Table structure for table `lppm_t_reward_prosiding_file` */

DROP TABLE IF EXISTS `lppm_t_reward_prosiding_file`;

CREATE TABLE `lppm_t_reward_prosiding_file` (
  `reward_prosiding_file_id` int(11) NOT NULL AUTO_INCREMENT,
  `reward_prosiding_id` int(11) DEFAULT NULL,
  `nama_file` varchar(200) DEFAULT NULL,
  `kode_file` varchar(200) DEFAULT NULL,
  `index_file` int(11) DEFAULT NULL,
  `keterangan` text,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`reward_prosiding_file_id`),
  KEY `FK_lppm_t_reward_prosiding_file` (`reward_prosiding_id`),
  CONSTRAINT `FK_lppm_t_reward_prosiding_file` FOREIGN KEY (`reward_prosiding_id`) REFERENCES `lppm_t_reward_prosiding` (`reward_prosiding_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `lppm_t_reward_prosiding_file` */

/*Table structure for table `lppm_t_rewardpublikasi` */

DROP TABLE IF EXISTS `lppm_t_rewardpublikasi`;

CREATE TABLE `lppm_t_rewardpublikasi` (
  `rewardpublikasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `publikasi_id` int(11) NOT NULL,
  `status_reward` int(11) DEFAULT NULL,
  `keterangan_reward` varchar(255) DEFAULT NULL,
  `catatan` varchar(255) DEFAULT NULL,
  `jumlah` decimal(10,0) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`rewardpublikasi_id`),
  KEY `FK_lppm_t_rewardpublikasi` (`publikasi_id`),
  CONSTRAINT `FK_lppm_t_rewardpublikasi` FOREIGN KEY (`publikasi_id`) REFERENCES `lppm_t_publikasi` (`publikasi_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `lppm_t_rewardpublikasi` */

/*Table structure for table `migration` */

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `migration` */

insert  into `migration`(`version`,`apply_time`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values ('m180228_081214_init_cis_lite',1520346816,0,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `mref_r_agama` */

DROP TABLE IF EXISTS `mref_r_agama`;

CREATE TABLE `mref_r_agama` (
  `agama_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) NOT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`agama_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_agama` */

/*Table structure for table `mref_r_asal_sekolah` */

DROP TABLE IF EXISTS `mref_r_asal_sekolah`;

CREATE TABLE `mref_r_asal_sekolah` (
  `asal_sekolah_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) NOT NULL,
  `alamat` text,
  `provinsi_id` int(11) DEFAULT NULL,
  `kabupaten_id` int(11) DEFAULT NULL,
  `kodepos` varchar(8) DEFAULT NULL,
  `telepon` varchar(20) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`asal_sekolah_id`),
  KEY `FK_mref_r_asal_sekolah` (`kabupaten_id`),
  KEY `FK_mref_r_asal_sekolah_provinsi` (`provinsi_id`),
  CONSTRAINT `FK_mref_r_asal_sekolah` FOREIGN KEY (`kabupaten_id`) REFERENCES `mref_r_kabupaten` (`kabupaten_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_mref_r_asal_sekolah_provinsi` FOREIGN KEY (`provinsi_id`) REFERENCES `mref_r_provinsi` (`provinsi_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_asal_sekolah` */

/*Table structure for table `mref_r_bidang_pekerjaan` */

DROP TABLE IF EXISTS `mref_r_bidang_pekerjaan`;

CREATE TABLE `mref_r_bidang_pekerjaan` (
  `bidang_pekerjaan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`bidang_pekerjaan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_bidang_pekerjaan` */

/*Table structure for table `mref_r_bidang_perusahaan` */

DROP TABLE IF EXISTS `mref_r_bidang_perusahaan`;

CREATE TABLE `mref_r_bidang_perusahaan` (
  `bidang_perusahaan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`bidang_perusahaan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_bidang_perusahaan` */

/*Table structure for table `mref_r_gbk` */

DROP TABLE IF EXISTS `mref_r_gbk`;

CREATE TABLE `mref_r_gbk` (
  `gbk_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) NOT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`gbk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_gbk` */

/*Table structure for table `mref_r_gelombang_pmb` */

DROP TABLE IF EXISTS `mref_r_gelombang_pmb`;

CREATE TABLE `mref_r_gelombang_pmb` (
  `gelombang_pmb_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `deskripsi` varchar(150) DEFAULT NULL,
  `lokasi` varchar(25) DEFAULT NULL,
  `tanggal_awal` datetime DEFAULT NULL,
  `tanggal_akhir` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(25) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(25) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`gelombang_pmb_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_gelombang_pmb` */

/*Table structure for table `mref_r_golongan_darah` */

DROP TABLE IF EXISTS `mref_r_golongan_darah`;

CREATE TABLE `mref_r_golongan_darah` (
  `golongan_darah_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`golongan_darah_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_golongan_darah` */

/*Table structure for table `mref_r_golongan_kepangkatan` */

DROP TABLE IF EXISTS `mref_r_golongan_kepangkatan`;

CREATE TABLE `mref_r_golongan_kepangkatan` (
  `golongan_kepangkatan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) NOT NULL,
  `desc` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`golongan_kepangkatan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_golongan_kepangkatan` */

/*Table structure for table `mref_r_jabatan_akademik` */

DROP TABLE IF EXISTS `mref_r_jabatan_akademik`;

CREATE TABLE `mref_r_jabatan_akademik` (
  `jabatan_akademik_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) NOT NULL,
  `desc` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`jabatan_akademik_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_jabatan_akademik` */

/*Table structure for table `mref_r_jenis_kelamin` */

DROP TABLE IF EXISTS `mref_r_jenis_kelamin`;

CREATE TABLE `mref_r_jenis_kelamin` (
  `jenis_kelamin_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`jenis_kelamin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_jenis_kelamin` */

/*Table structure for table `mref_r_jenjang` */

DROP TABLE IF EXISTS `mref_r_jenjang`;

CREATE TABLE `mref_r_jenjang` (
  `jenjang_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`jenjang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_jenjang` */

/*Table structure for table `mref_r_kabupaten` */

DROP TABLE IF EXISTS `mref_r_kabupaten`;

CREATE TABLE `mref_r_kabupaten` (
  `kabupaten_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`kabupaten_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_kabupaten` */

/*Table structure for table `mref_r_lokasi` */

DROP TABLE IF EXISTS `mref_r_lokasi`;

CREATE TABLE `mref_r_lokasi` (
  `lokasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`lokasi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_lokasi` */

insert  into `mref_r_lokasi`(`lokasi_id`,`name`,`desc`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,'GD 711',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(2,'GD 712',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(3,'GD 713',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(4,'GD 714',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(5,'GD 715',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(6,'GD 716',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(7,'GD 721',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(8,'GD 722',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(9,'GD 723',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(10,'GD 724',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(11,'GD 725',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `mref_r_pekerjaan` */

DROP TABLE IF EXISTS `mref_r_pekerjaan`;

CREATE TABLE `mref_r_pekerjaan` (
  `pekerjaan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`pekerjaan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_pekerjaan` */

/*Table structure for table `mref_r_pendidikan` */

DROP TABLE IF EXISTS `mref_r_pendidikan`;

CREATE TABLE `mref_r_pendidikan` (
  `pendidikan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`pendidikan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_pendidikan` */

/*Table structure for table `mref_r_pengali_nilai` */

DROP TABLE IF EXISTS `mref_r_pengali_nilai`;

CREATE TABLE `mref_r_pengali_nilai` (
  `pengali_nilai_id` int(11) NOT NULL AUTO_INCREMENT,
  `nilai` varchar(3) DEFAULT NULL,
  `pengali` float DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`pengali_nilai_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_pengali_nilai` */

/*Table structure for table `mref_r_penghasilan` */

DROP TABLE IF EXISTS `mref_r_penghasilan`;

CREATE TABLE `mref_r_penghasilan` (
  `penghasilan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`penghasilan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_penghasilan` */

/*Table structure for table `mref_r_predikat_lulus` */

DROP TABLE IF EXISTS `mref_r_predikat_lulus`;

CREATE TABLE `mref_r_predikat_lulus` (
  `predikat_lulus_id` int(11) NOT NULL AUTO_INCREMENT,
  `ipk_minimum` double DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`predikat_lulus_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_predikat_lulus` */

/*Table structure for table `mref_r_provinsi` */

DROP TABLE IF EXISTS `mref_r_provinsi`;

CREATE TABLE `mref_r_provinsi` (
  `provinsi_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(128) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `cerated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`provinsi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_provinsi` */

/*Table structure for table `mref_r_role_pengajar` */

DROP TABLE IF EXISTS `mref_r_role_pengajar`;

CREATE TABLE `mref_r_role_pengajar` (
  `role_pengajar_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) NOT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`role_pengajar_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_role_pengajar` */

/*Table structure for table `mref_r_sem_ta` */

DROP TABLE IF EXISTS `mref_r_sem_ta`;

CREATE TABLE `mref_r_sem_ta` (
  `sem_ta_id` int(11) NOT NULL AUTO_INCREMENT,
  `sem_ta` int(11) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`sem_ta_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_sem_ta` */

insert  into `mref_r_sem_ta`(`sem_ta_id`,`sem_ta`,`desc`,`deleted`,`deleted_by`,`deleted_at`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,1,'Gasal',0,NULL,NULL,NULL,NULL,NULL,NULL),(2,2,'Genap',0,NULL,NULL,NULL,NULL,NULL,NULL),(3,3,'Pendek',0,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `mref_r_sifat_kurikulum` */

DROP TABLE IF EXISTS `mref_r_sifat_kurikulum`;

CREATE TABLE `mref_r_sifat_kurikulum` (
  `sifat_kurikulum_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`sifat_kurikulum_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_sifat_kurikulum` */

/*Table structure for table `mref_r_status_aktif_mahasiswa` */

DROP TABLE IF EXISTS `mref_r_status_aktif_mahasiswa`;

CREATE TABLE `mref_r_status_aktif_mahasiswa` (
  `status_aktif_mahasiswa_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) NOT NULL,
  `desc` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`status_aktif_mahasiswa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_status_aktif_mahasiswa` */

/*Table structure for table `mref_r_status_aktif_pegawai` */

DROP TABLE IF EXISTS `mref_r_status_aktif_pegawai`;

CREATE TABLE `mref_r_status_aktif_pegawai` (
  `status_aktif_pegawai_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) NOT NULL,
  `desc` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`status_aktif_pegawai_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_status_aktif_pegawai` */

insert  into `mref_r_status_aktif_pegawai`(`status_aktif_pegawai_id`,`nama`,`desc`,`created_at`,`updated_at`,`created_by`,`updated_by`,`deleted`,`deleted_at`,`deleted_by`) values (1,'Aktif',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL),(2,'Honorer',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL),(3,'Nonaktif',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL);

/*Table structure for table `mref_r_status_ikatan_kerja_dosen` */

DROP TABLE IF EXISTS `mref_r_status_ikatan_kerja_dosen`;

CREATE TABLE `mref_r_status_ikatan_kerja_dosen` (
  `status_ikatan_kerja_dosen_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) NOT NULL,
  `desc` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`status_ikatan_kerja_dosen_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_status_ikatan_kerja_dosen` */

/*Table structure for table `mref_r_status_ikatan_kerja_pegawai` */

DROP TABLE IF EXISTS `mref_r_status_ikatan_kerja_pegawai`;

CREATE TABLE `mref_r_status_ikatan_kerja_pegawai` (
  `status_ikatan_kerja_pegawai_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) NOT NULL,
  `desc` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`status_ikatan_kerja_pegawai_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_status_ikatan_kerja_pegawai` */

insert  into `mref_r_status_ikatan_kerja_pegawai`(`status_ikatan_kerja_pegawai_id`,`nama`,`desc`,`created_at`,`updated_at`,`created_by`,`updated_by`,`deleted`,`deleted_at`,`deleted_by`) values (1,'Kontrak','','2015-03-27 05:58:38','2015-04-30 10:22:41','1','1',0,NULL,NULL),(2,'Harian/Honor','','2015-04-30 10:22:51','2015-04-30 10:22:51','1','1',0,NULL,NULL),(3,'Staf Ahli','','2015-08-24 16:43:57','2015-08-24 16:43:57','1','1',0,NULL,NULL),(4,'Tetap','','2015-08-24 16:44:19','2015-08-24 16:44:19','1','1',0,NULL,NULL);

/*Table structure for table `mref_r_status_marital` */

DROP TABLE IF EXISTS `mref_r_status_marital`;

CREATE TABLE `mref_r_status_marital` (
  `status_marital_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`status_marital_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_status_marital` */

/*Table structure for table `mref_r_ta` */

DROP TABLE IF EXISTS `mref_r_ta`;

CREATE TABLE `mref_r_ta` (
  `ta_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` int(5) NOT NULL,
  `desc` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ta_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `mref_r_ta` */

insert  into `mref_r_ta`(`ta_id`,`nama`,`desc`,`deleted`,`deleted_at`,`deleted_by`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,2016,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(2,2017,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(3,2018,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `nlai_ext_mhs` */

DROP TABLE IF EXISTS `nlai_ext_mhs`;

CREATE TABLE `nlai_ext_mhs` (
  `ext_mhs_id` int(11) NOT NULL AUTO_INCREMENT,
  `dim_id` int(11) DEFAULT NULL,
  `tgl_test` date DEFAULT NULL,
  `ta` varchar(5) DEFAULT NULL,
  `sem_ta` int(11) DEFAULT '1',
  `ext_id` int(11) DEFAULT NULL,
  `score` varchar(32) DEFAULT NULL,
  `keterangan` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ext_mhs_id`),
  KEY `FK_nlai_ext_mhs` (`ext_id`),
  KEY `FK_nlai_ext_mhs_dim` (`dim_id`),
  CONSTRAINT `FK_nlai_ext_mhs` FOREIGN KEY (`ext_id`) REFERENCES `nlai_r_ext` (`ext_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_nlai_ext_mhs_dim` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nlai_ext_mhs` */

/*Table structure for table `nlai_file_nilai` */

DROP TABLE IF EXISTS `nlai_file_nilai`;

CREATE TABLE `nlai_file_nilai` (
  `file_nilai_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_file` text,
  `kode_file` varchar(100) DEFAULT NULL,
  `ket` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`file_nilai_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nlai_file_nilai` */

/*Table structure for table `nlai_komponen_tambahan` */

DROP TABLE IF EXISTS `nlai_komponen_tambahan`;

CREATE TABLE `nlai_komponen_tambahan` (
  `komponen_tambahan_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kur` varchar(4) DEFAULT NULL,
  `kode_mk` varchar(11) DEFAULT NULL,
  `ta` varchar(4) DEFAULT NULL,
  `sem_ta` int(11) DEFAULT NULL,
  `nilai_tambahan_1` float DEFAULT NULL,
  `nilai_tambahan_2` float DEFAULT NULL,
  `nilai_tambahan_3` float DEFAULT NULL,
  `nilai_tambahan_4` float DEFAULT NULL,
  `nilai_tambahan_5` float DEFAULT NULL,
  `nm_tambahan_1` varchar(45) DEFAULT 'Nilai Tambahan 1',
  `nm_tambahan_2` varchar(45) DEFAULT 'Nilai Tambahan 2',
  `nm_tambahan_3` varchar(45) DEFAULT 'Nilai Tambahan 3',
  `nm_tambahan_4` varchar(45) DEFAULT 'Nilai Tambahan 4',
  `nm_tambahan_5` varchar(45) DEFAULT 'Nilai Tambahan 5',
  `kurikulum_syllabus_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`komponen_tambahan_id`),
  KEY `FK_nlai_komponen_tambahan_syllabus` (`kurikulum_syllabus_id`),
  CONSTRAINT `FK_nlai_komponen_tambahan_syllabus` FOREIGN KEY (`kurikulum_syllabus_id`) REFERENCES `prkl_kurikulum_syllabus` (`kurikulum_syllabus_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nlai_komponen_tambahan` */

/*Table structure for table `nlai_komposisi_nilai` */

DROP TABLE IF EXISTS `nlai_komposisi_nilai`;

CREATE TABLE `nlai_komposisi_nilai` (
  `komposisi_nilai_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kur` int(4) DEFAULT '0',
  `kode_mk` varchar(11) DEFAULT NULL,
  `ta` varchar(30) DEFAULT '0',
  `sem_ta` int(11) DEFAULT NULL,
  `nilai_praktikum` float DEFAULT NULL,
  `nilai_quis` float DEFAULT NULL,
  `nilai_uts` float DEFAULT NULL,
  `nilai_uas` float DEFAULT NULL,
  `nilai_tugas` float DEFAULT NULL,
  `nm_praktikum` varchar(20) NOT NULL DEFAULT 'Nilai Praktikum',
  `nm_quis` varchar(20) NOT NULL DEFAULT 'Nilai Quis',
  `nm_uts` varchar(20) NOT NULL DEFAULT 'Nilai UTS',
  `nm_uas` varchar(20) NOT NULL DEFAULT 'Nilai UAS',
  `nm_tugas` varchar(20) NOT NULL DEFAULT 'Nilai Tugas',
  `kurikulum_syllabus_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`komposisi_nilai_id`),
  KEY `fk_t_komposisi_nilai_t_kurikulum1_idx` (`kurikulum_syllabus_id`),
  CONSTRAINT `FK_nlai_komposisi_nilai_syllabus` FOREIGN KEY (`kurikulum_syllabus_id`) REFERENCES `prkl_kurikulum_syllabus` (`kurikulum_syllabus_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nlai_komposisi_nilai` */

/*Table structure for table `nlai_komposisi_nilai_uts_uas` */

DROP TABLE IF EXISTS `nlai_komposisi_nilai_uts_uas`;

CREATE TABLE `nlai_komposisi_nilai_uts_uas` (
  `komposisi_nilai_uts_uas_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kur` int(4) DEFAULT NULL,
  `kode_mk` varchar(11) DEFAULT NULL,
  `ta` varchar(4) DEFAULT NULL,
  `sem_ta` int(11) DEFAULT NULL,
  `kurikulum_syllabus_id` int(11) DEFAULT NULL,
  `nilai_uts_teori` float DEFAULT NULL,
  `nilai_uts_praktikum` float DEFAULT NULL,
  `nilai_uas_teori` float DEFAULT NULL,
  `nilai_uas_praktikum` float DEFAULT NULL,
  `nm_uts_teori` varchar(50) DEFAULT 'UTS_TEORI',
  `nm_uts_praktikum` varchar(50) DEFAULT 'UTS_PRAKTIKUM',
  `nm_uas_teori` varchar(50) DEFAULT 'UAS_TEORI',
  `nm_uas_praktikum` varchar(50) DEFAULT 'UAS_PRAKTIKUM',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`komposisi_nilai_uts_uas_id`),
  KEY `FK_nlai_komposisi_nilai_uts_uas` (`kurikulum_syllabus_id`),
  CONSTRAINT `FK_nlai_komposisi_nilai_uts_uas` FOREIGN KEY (`kurikulum_syllabus_id`) REFERENCES `prkl_kurikulum_syllabus` (`kurikulum_syllabus_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nlai_komposisi_nilai_uts_uas` */

/*Table structure for table `nlai_nilai` */

DROP TABLE IF EXISTS `nlai_nilai`;

CREATE TABLE `nlai_nilai` (
  `nilai_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kur` int(4) NOT NULL DEFAULT '0',
  `kode_mk` varchar(11) NOT NULL,
  `ta` varchar(30) NOT NULL DEFAULT '0',
  `sem_ta` int(11) DEFAULT NULL,
  `nim` varchar(8) NOT NULL DEFAULT '',
  `komponen_ke` int(1) DEFAULT '0',
  `na` float DEFAULT '0',
  `nilai` char(3) DEFAULT NULL,
  `na_remedial` float DEFAULT NULL,
  `nilai_remedial` char(3) DEFAULT NULL,
  `kelas` varchar(5) DEFAULT NULL,
  `sks` int(11) NOT NULL COMMENT 'Jumlah SKS',
  `sem` int(11) DEFAULT NULL COMMENT 'Semseter Kurikulum',
  `wali_approval` varchar(100) DEFAULT NULL,
  `dir_approval` varchar(100) DEFAULT NULL,
  `dosen_approval` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `dim_id` int(11) DEFAULT NULL,
  `kurikulum_syllabus_id` int(11) DEFAULT NULL,
  `ispublish` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`nilai_id`),
  KEY `NIM` (`nim`),
  KEY `fk_t_nilai_t_dim1_idx` (`dim_id`),
  KEY `fk_t_nilai_t_kurikulum1_idx` (`kurikulum_syllabus_id`),
  CONSTRAINT `FK_nlai_nilai` FOREIGN KEY (`kurikulum_syllabus_id`) REFERENCES `prkl_kurikulum_syllabus` (`kurikulum_syllabus_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_t_nilai_t_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nlai_nilai` */

/*Table structure for table `nlai_nilai_komponen_tambahan` */

DROP TABLE IF EXISTS `nlai_nilai_komponen_tambahan`;

CREATE TABLE `nlai_nilai_komponen_tambahan` (
  `nilai_komponen_tambahan_id` int(11) NOT NULL AUTO_INCREMENT,
  `kurikulum_syllabus_id` int(11) DEFAULT NULL,
  `id_kur` varchar(4) DEFAULT NULL,
  `kode_mk` varchar(50) DEFAULT NULL,
  `ta` int(11) DEFAULT NULL,
  `sem_ta` int(1) DEFAULT NULL,
  `komponen_ke` int(11) DEFAULT NULL,
  `komponen` varchar(45) DEFAULT NULL,
  `nim` varchar(10) DEFAULT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `nilai` float DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`nilai_komponen_tambahan_id`),
  KEY `FK_nlai_nilai_komponen_tambahan_syllabus` (`kurikulum_syllabus_id`),
  KEY `FK_nlai_nilai_komponen_tambahan-dim` (`dim_id`),
  CONSTRAINT `FK_nlai_nilai_komponen_tambahan-dim` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_nlai_nilai_komponen_tambahan_syllabus` FOREIGN KEY (`kurikulum_syllabus_id`) REFERENCES `prkl_kurikulum_syllabus` (`kurikulum_syllabus_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nlai_nilai_komponen_tambahan` */

/*Table structure for table `nlai_nilai_praktikum` */

DROP TABLE IF EXISTS `nlai_nilai_praktikum`;

CREATE TABLE `nlai_nilai_praktikum` (
  `nilai_praktikum_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kur` int(4) NOT NULL DEFAULT '0',
  `kode_mk` varchar(11) NOT NULL,
  `ta` varchar(4) NOT NULL DEFAULT '0',
  `sem_ta` int(11) DEFAULT NULL,
  `nim` varchar(8) NOT NULL DEFAULT '',
  `komponen` varchar(30) DEFAULT NULL,
  `dosen_approval` varchar(100) DEFAULT NULL,
  `komponen_ke` smallint(6) DEFAULT NULL,
  `nilai` float NOT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `kurikulum_syllabus_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`nilai_praktikum_id`),
  KEY `fk_t_nilai_praktikum_t_dim1_idx` (`dim_id`),
  KEY `fk_t_nilai_praktikum_t_kurikulum1_idx` (`kurikulum_syllabus_id`),
  CONSTRAINT `FK_nlai_nilai_praktikum_syllabus` FOREIGN KEY (`kurikulum_syllabus_id`) REFERENCES `prkl_kurikulum_syllabus` (`kurikulum_syllabus_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_t_nilai_praktikum_t_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nlai_nilai_praktikum` */

/*Table structure for table `nlai_nilai_quis` */

DROP TABLE IF EXISTS `nlai_nilai_quis`;

CREATE TABLE `nlai_nilai_quis` (
  `nilai_quis_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kur` int(4) NOT NULL DEFAULT '0',
  `kode_mk` varchar(11) NOT NULL,
  `ta` varchar(30) NOT NULL DEFAULT '0',
  `sem_ta` int(11) DEFAULT NULL,
  `nim` varchar(8) NOT NULL DEFAULT '',
  `komponen` varchar(30) DEFAULT NULL,
  `dosen_approval` varchar(100) DEFAULT NULL,
  `komponen_ke` smallint(6) DEFAULT NULL,
  `nilai` float DEFAULT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `kurikulum_syllabus_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`nilai_quis_id`),
  KEY `fk_t_nilai_quis_t_dim1_idx` (`dim_id`),
  KEY `fk_t_nilai_quis_t_kurikulum1_idx` (`kurikulum_syllabus_id`),
  CONSTRAINT `FK_nlai_nilai_quis_syllabus` FOREIGN KEY (`kurikulum_syllabus_id`) REFERENCES `prkl_kurikulum_syllabus` (`kurikulum_syllabus_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_t_nilai_quis_t_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nlai_nilai_quis` */

/*Table structure for table `nlai_nilai_tugas` */

DROP TABLE IF EXISTS `nlai_nilai_tugas`;

CREATE TABLE `nlai_nilai_tugas` (
  `nilai_tugas_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kur` int(4) NOT NULL DEFAULT '0',
  `kode_mk` varchar(11) NOT NULL,
  `ta` varchar(30) NOT NULL DEFAULT '0',
  `sem_ta` int(11) DEFAULT NULL,
  `nim` varchar(8) NOT NULL DEFAULT '',
  `komponen` varchar(30) DEFAULT NULL,
  `dosen_approval` varchar(100) DEFAULT NULL,
  `komponen_ke` smallint(6) DEFAULT NULL,
  `nilai` float DEFAULT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `kurikulum_syllabus_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`nilai_tugas_id`),
  KEY `fk_t_nilai_tugas_t_dim1_idx` (`dim_id`),
  KEY `fk_t_nilai_tugas_t_kurikulum1_idx` (`kurikulum_syllabus_id`),
  CONSTRAINT `FK_nlai_nilai_tugas_syllabus` FOREIGN KEY (`kurikulum_syllabus_id`) REFERENCES `prkl_kurikulum_syllabus` (`kurikulum_syllabus_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_t_nilai_tugas_t_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nlai_nilai_tugas` */

/*Table structure for table `nlai_nilai_uas` */

DROP TABLE IF EXISTS `nlai_nilai_uas`;

CREATE TABLE `nlai_nilai_uas` (
  `nilai_uas_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kur` int(4) NOT NULL DEFAULT '0',
  `kode_mk` varchar(11) NOT NULL,
  `ta` varchar(30) NOT NULL DEFAULT '0',
  `sem_ta` int(11) DEFAULT NULL,
  `nim` varchar(8) NOT NULL DEFAULT '',
  `komponen` varchar(30) DEFAULT NULL,
  `dosen_approval` varchar(100) DEFAULT NULL,
  `komponen_ke` smallint(6) DEFAULT NULL,
  `nilai` float DEFAULT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `kurikulum_syllabus_id` int(11) DEFAULT NULL,
  `ispublish` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`nilai_uas_id`),
  KEY `fk_t_nilai_uas_t_dim1_idx` (`dim_id`),
  KEY `fk_t_nilai_uas_t_kurikulum1_idx` (`kurikulum_syllabus_id`),
  CONSTRAINT `FK_nlai_nilai_uas_syllabus` FOREIGN KEY (`kurikulum_syllabus_id`) REFERENCES `prkl_kurikulum_syllabus` (`kurikulum_syllabus_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_t_nilai_uas_t_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nlai_nilai_uas` */

/*Table structure for table `nlai_nilai_uts` */

DROP TABLE IF EXISTS `nlai_nilai_uts`;

CREATE TABLE `nlai_nilai_uts` (
  `nilai_uts_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kur` int(4) NOT NULL DEFAULT '0',
  `kode_mk` varchar(11) NOT NULL,
  `ta` varchar(30) NOT NULL DEFAULT '0',
  `sem_ta` int(11) DEFAULT NULL,
  `nim` varchar(8) NOT NULL DEFAULT '',
  `komponen` varchar(30) DEFAULT NULL,
  `dosen_approval` varchar(100) DEFAULT NULL,
  `komponen_ke` smallint(6) DEFAULT NULL,
  `nilai` float DEFAULT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `kurikulum_syllabus_id` int(11) DEFAULT NULL,
  `ispublish` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`nilai_uts_id`),
  KEY `fk_t_nilai_uts_t_dim1_idx` (`dim_id`),
  KEY `fk_t_nilai_uts_t_kurikulum1_idx` (`kurikulum_syllabus_id`),
  CONSTRAINT `FK_nlai_nilai_uts_syllabus` FOREIGN KEY (`kurikulum_syllabus_id`) REFERENCES `prkl_kurikulum_syllabus` (`kurikulum_syllabus_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_t_nilai_uts_t_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nlai_nilai_uts` */

/*Table structure for table `nlai_r_ext` */

DROP TABLE IF EXISTS `nlai_r_ext`;

CREATE TABLE `nlai_r_ext` (
  `ext_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) NOT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ext_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nlai_r_ext` */

/*Table structure for table `nlai_rentang_nilai` */

DROP TABLE IF EXISTS `nlai_rentang_nilai`;

CREATE TABLE `nlai_rentang_nilai` (
  `rentang_nilai_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kur` int(4) NOT NULL DEFAULT '0',
  `kode_mk` varchar(11) NOT NULL,
  `ta` varchar(30) NOT NULL DEFAULT '0',
  `sem_ta` int(11) DEFAULT NULL,
  `a` float DEFAULT NULL,
  `ab` varchar(8) DEFAULT NULL,
  `b` float DEFAULT NULL,
  `bc` varchar(8) DEFAULT NULL,
  `c` float DEFAULT NULL,
  `d` float DEFAULT NULL,
  `e` float DEFAULT NULL,
  `kurikulum_syllabus_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`rentang_nilai_id`),
  KEY `fk_t_rentang_nilai_t_kurikulum1_idx` (`kurikulum_syllabus_id`),
  CONSTRAINT `FK_nlai_rentang_nilai_syllabus` FOREIGN KEY (`kurikulum_syllabus_id`) REFERENCES `prkl_kurikulum_syllabus` (`kurikulum_syllabus_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nlai_rentang_nilai` */

/*Table structure for table `nlai_uas_detail` */

DROP TABLE IF EXISTS `nlai_uas_detail`;

CREATE TABLE `nlai_uas_detail` (
  `uas_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kur` int(4) DEFAULT NULL,
  `kode_mk` varchar(11) DEFAULT NULL,
  `ta` varchar(30) DEFAULT NULL,
  `sem_ta` int(11) DEFAULT NULL,
  `nim` varchar(8) DEFAULT NULL,
  `komponen` varchar(30) DEFAULT NULL,
  `komponen_ke` int(11) DEFAULT NULL,
  `nilai` float DEFAULT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `kurikulum_syllabus_id` int(11) DEFAULT NULL,
  `komposisi_nilai_uts_uas_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`uas_detail_id`),
  KEY `FK_nlai_uas_detail` (`kurikulum_syllabus_id`),
  KEY `FK_nlai_uas_detail_komposisi` (`komposisi_nilai_uts_uas_id`),
  KEY `FK_nlai_uas_detail_dim` (`dim_id`),
  CONSTRAINT `FK_nlai_uas_detail` FOREIGN KEY (`kurikulum_syllabus_id`) REFERENCES `prkl_kurikulum_syllabus` (`kurikulum_syllabus_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_nlai_uas_detail_dim` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_nlai_uas_detail_komposisi` FOREIGN KEY (`komposisi_nilai_uts_uas_id`) REFERENCES `nlai_komposisi_nilai_uts_uas` (`komposisi_nilai_uts_uas_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nlai_uas_detail` */

/*Table structure for table `nlai_uts_detail` */

DROP TABLE IF EXISTS `nlai_uts_detail`;

CREATE TABLE `nlai_uts_detail` (
  `uts_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kur` int(4) DEFAULT NULL,
  `kode_mk` varchar(11) DEFAULT NULL,
  `ta` varchar(30) DEFAULT NULL,
  `sem_ta` int(11) DEFAULT NULL,
  `nim` varchar(8) DEFAULT NULL,
  `komponen` varchar(30) DEFAULT NULL,
  `komponen_ke` int(11) DEFAULT NULL,
  `nilai` float DEFAULT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `kurikulum_syllabus_id` int(11) DEFAULT NULL,
  `komposisi_nilai_uts_uas_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`uts_detail_id`),
  KEY `FK_nlai_uts_detail` (`kurikulum_syllabus_id`),
  KEY `FK_nlai_uts_detail_komposisi` (`komposisi_nilai_uts_uas_id`),
  KEY `FK_nlai_uts_detail_dim` (`dim_id`),
  CONSTRAINT `FK_nlai_uts_detail` FOREIGN KEY (`kurikulum_syllabus_id`) REFERENCES `prkl_kurikulum_syllabus` (`kurikulum_syllabus_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_nlai_uts_detail_dim` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_nlai_uts_detail_komposisi` FOREIGN KEY (`komposisi_nilai_uts_uas_id`) REFERENCES `nlai_komposisi_nilai_uts_uas` (`komposisi_nilai_uts_uas_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nlai_uts_detail` */

/*Table structure for table `prkl_berita_acara_daftar_hadir` */

DROP TABLE IF EXISTS `prkl_berita_acara_daftar_hadir`;

CREATE TABLE `prkl_berita_acara_daftar_hadir` (
  `berita_acara_daftar_hadir_id` int(11) NOT NULL AUTO_INCREMENT,
  `week` smallint(6) NOT NULL DEFAULT '0',
  `session` smallint(6) NOT NULL DEFAULT '0',
  `ta` int(4) NOT NULL DEFAULT '2002',
  `id_kur` int(4) NOT NULL DEFAULT '2002',
  `kurikulum_id` int(11) DEFAULT NULL,
  `kode_mk` varchar(11) NOT NULL,
  `nim` varchar(8) NOT NULL DEFAULT '',
  `dim_id` int(11) DEFAULT NULL,
  `status` varchar(7) DEFAULT 'H',
  `keterangan` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`berita_acara_daftar_hadir_id`),
  KEY `fk_t_berita_acara_daftar_hadir_t_kurikulum1_idx` (`kurikulum_id`),
  KEY `fk_t_berita_acara_daftar_hadir_t_dim1_idx` (`dim_id`),
  CONSTRAINT `fk_t_berita_acara_daftar_hadir_t_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_t_berita_acara_daftar_hadir_t_kurikulum1` FOREIGN KEY (`kurikulum_id`) REFERENCES `krkm_kuliah` (`kuliah_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_berita_acara_daftar_hadir` */

/*Table structure for table `prkl_berita_acara_kuliah` */

DROP TABLE IF EXISTS `prkl_berita_acara_kuliah`;

CREATE TABLE `prkl_berita_acara_kuliah` (
  `berita_acara_kuliah_id` int(11) NOT NULL AUTO_INCREMENT,
  `week` smallint(6) NOT NULL DEFAULT '0',
  `session` smallint(6) NOT NULL DEFAULT '0',
  `ta` int(4) NOT NULL DEFAULT '2002',
  `id_kur` int(4) NOT NULL DEFAULT '2002',
  `kode_mk` varchar(11) NOT NULL,
  `kurikulum_id` int(11) DEFAULT NULL,
  `kelas` varchar(100) NOT NULL,
  `kelas_id` int(11) DEFAULT NULL,
  `tanggal` date NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `topik` text,
  `ruangan` varchar(100) DEFAULT NULL,
  `aktifitas` varchar(15) DEFAULT NULL,
  `pic` varchar(20) DEFAULT NULL,
  `metode` text,
  `alat_bantu` text,
  `catatan` text,
  `deleted` tinyint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`berita_acara_kuliah_id`),
  KEY `fk_t_berita_acara_kuliah_t_kurikulum1_idx` (`kurikulum_id`),
  KEY `fk_t_berita_acara_kuliah_t_kelas1_idx` (`kelas_id`),
  CONSTRAINT `FK_t_berita_acara_kuliah` FOREIGN KEY (`kelas_id`) REFERENCES `adak_kelas` (`kelas_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_t_berita_acara_kuliah_t_kurikulum1` FOREIGN KEY (`kurikulum_id`) REFERENCES `krkm_kuliah` (`kuliah_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_berita_acara_kuliah` */

/*Table structure for table `prkl_course_unit` */

DROP TABLE IF EXISTS `prkl_course_unit`;

CREATE TABLE `prkl_course_unit` (
  `course_unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `week` smallint(6) NOT NULL DEFAULT '0',
  `session` smallint(6) NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT '',
  `ta` int(4) DEFAULT '2002',
  `id_kur` int(4) DEFAULT '2002',
  `kode_mk` varchar(11) DEFAULT NULL,
  `topik` varchar(255) DEFAULT NULL,
  `sub_topik` text,
  `objektif` text,
  `aktifitas` varchar(15) DEFAULT NULL,
  `pic` varchar(20) DEFAULT NULL,
  `pegawai_id` int(11) DEFAULT NULL,
  `metode` text,
  `alat_bantu` text,
  `ket` varchar(255) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `day` varchar(10) DEFAULT NULL,
  `kurikulum_syllabus_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`course_unit_id`),
  KEY `WEEK` (`week`),
  KEY `SESSION` (`session`),
  KEY `TOPIK` (`topik`),
  KEY `fk_t_course_unit_t_kurikulum1_idx` (`kurikulum_syllabus_id`),
  KEY `FK_prkl_course_unit_pic` (`pegawai_id`),
  CONSTRAINT `FK_prkl_course_unit_kurikulum_syllabus` FOREIGN KEY (`kurikulum_syllabus_id`) REFERENCES `prkl_kurikulum_syllabus` (`kurikulum_syllabus_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_prkl_course_unit_pic` FOREIGN KEY (`pegawai_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_course_unit` */

/*Table structure for table `prkl_course_unit_material` */

DROP TABLE IF EXISTS `prkl_course_unit_material`;

CREATE TABLE `prkl_course_unit_material` (
  `course_unit_material_id` int(11) NOT NULL AUTO_INCREMENT,
  `week` smallint(6) NOT NULL DEFAULT '0',
  `session` smallint(6) NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT '',
  `ta` int(4) NOT NULL DEFAULT '2002',
  `id_kur` int(4) NOT NULL DEFAULT '2002',
  `kode_mk` varchar(11) NOT NULL,
  `id_material` varchar(255) NOT NULL DEFAULT '',
  `kurikulum_id` int(11) DEFAULT NULL,
  `material_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`course_unit_material_id`),
  KEY `fk_t_course_unit_material_t_kurikulum1_idx` (`kurikulum_id`),
  KEY `fk_t_course_unit_material_t_material1_idx` (`material_id`),
  CONSTRAINT `fk_t_course_unit_material_t_kurikulum1` FOREIGN KEY (`kurikulum_id`) REFERENCES `krkm_kuliah` (`kuliah_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_t_course_unit_material_t_material1` FOREIGN KEY (`material_id`) REFERENCES `prkl_material` (`material_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_course_unit_material` */

/*Table structure for table `prkl_file` */

DROP TABLE IF EXISTS `prkl_file`;

CREATE TABLE `prkl_file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `no` varchar(10) NOT NULL DEFAULT '0',
  `ta` int(4) NOT NULL DEFAULT '2002',
  `id_kur` int(4) NOT NULL DEFAULT '2002',
  `kode_mk` varchar(11) NOT NULL,
  `nama_file` varchar(255) NOT NULL DEFAULT '',
  `owner` varchar(30) NOT NULL DEFAULT '',
  `ket` varchar(255) DEFAULT NULL,
  `kurikulum_syllabus_id` int(11) DEFAULT NULL,
  `no_group_file` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `fk_t_file_t_kurikulum1_idx` (`kurikulum_syllabus_id`),
  CONSTRAINT `FK_prkl_file_kuri_syllabus` FOREIGN KEY (`kurikulum_syllabus_id`) REFERENCES `prkl_kurikulum_syllabus` (`kurikulum_syllabus_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_file` */

/*Table structure for table `prkl_file_materi` */

DROP TABLE IF EXISTS `prkl_file_materi`;

CREATE TABLE `prkl_file_materi` (
  `file_materi_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_file` text NOT NULL,
  `kode_file` varchar(50) DEFAULT NULL,
  `ket` text NOT NULL,
  `materi_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`file_materi_id`),
  KEY `FK_prkl_file_materi` (`materi_id`),
  CONSTRAINT `FK_prkl_file_materi` FOREIGN KEY (`materi_id`) REFERENCES `prkl_materi` (`materi_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_file_materi` */

/*Table structure for table `prkl_file_praktikum` */

DROP TABLE IF EXISTS `prkl_file_praktikum`;

CREATE TABLE `prkl_file_praktikum` (
  `file_praktikum_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_file` text NOT NULL,
  `kode_file` varchar(50) DEFAULT NULL,
  `ket` text NOT NULL,
  `praktikum_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`file_praktikum_id`),
  KEY `FK_prkl_file_praktikum` (`praktikum_id`),
  CONSTRAINT `FK_prkl_file_praktikum` FOREIGN KEY (`praktikum_id`) REFERENCES `prkl_praktikum` (`praktikum_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_file_praktikum` */

/*Table structure for table `prkl_file_syllabus` */

DROP TABLE IF EXISTS `prkl_file_syllabus`;

CREATE TABLE `prkl_file_syllabus` (
  `file_syllabus_id` int(11) NOT NULL AUTO_INCREMENT,
  `kurikulum_syllabus_id` int(11) DEFAULT NULL,
  `nama_file` varchar(200) DEFAULT NULL,
  `ket` text,
  `kode_file` varchar(200) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`file_syllabus_id`),
  KEY `FK_prkl_file_syllabus` (`kurikulum_syllabus_id`),
  CONSTRAINT `FK_prkl_file_syllabus` FOREIGN KEY (`kurikulum_syllabus_id`) REFERENCES `prkl_kurikulum_syllabus` (`kurikulum_syllabus_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_file_syllabus` */

/*Table structure for table `prkl_group_kuliah` */

DROP TABLE IF EXISTS `prkl_group_kuliah`;

CREATE TABLE `prkl_group_kuliah` (
  `group_kuliah_id` int(11) NOT NULL AUTO_INCREMENT,
  `kurikulum_syllabus_id` int(11) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`group_kuliah_id`),
  KEY `FK_prkl_group_kuliah_syllabus` (`kurikulum_syllabus_id`),
  CONSTRAINT `FK_prkl_group_kuliah_syllabus` FOREIGN KEY (`kurikulum_syllabus_id`) REFERENCES `prkl_kurikulum_syllabus` (`kurikulum_syllabus_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_group_kuliah` */

/*Table structure for table `prkl_info_ta` */

DROP TABLE IF EXISTS `prkl_info_ta`;

CREATE TABLE `prkl_info_ta` (
  `info_ta_id` int(11) NOT NULL AUTO_INCREMENT,
  `dim_id` int(11) NOT NULL,
  `judul_ta` text NOT NULL,
  `pembimbing_1` int(11) NOT NULL,
  `pembimbing_2` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`info_ta_id`),
  KEY `FK_prkl_info_ta_dim` (`dim_id`),
  KEY `FK_prkl_info_ta_pembimbing1` (`pembimbing_1`),
  KEY `FK_prkl_info_ta_pembimbing2` (`pembimbing_2`),
  CONSTRAINT `FK_prkl_info_ta_dim` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_prkl_info_ta_pembimbing1` FOREIGN KEY (`pembimbing_1`) REFERENCES `hrdx_dosen` (`dosen_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_prkl_info_ta_pembimbing2` FOREIGN KEY (`pembimbing_2`) REFERENCES `hrdx_dosen` (`dosen_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_info_ta` */

/*Table structure for table `prkl_jadwal` */

DROP TABLE IF EXISTS `prkl_jadwal`;

CREATE TABLE `prkl_jadwal` (
  `jadwal_id` int(11) NOT NULL AUTO_INCREMENT,
  `week` smallint(6) NOT NULL DEFAULT '0',
  `tanggal` date NOT NULL,
  `session` smallint(6) NOT NULL DEFAULT '0',
  `ta` int(4) NOT NULL DEFAULT '2002',
  `id_kur` int(4) NOT NULL DEFAULT '2002',
  `kode_mk` varchar(11) NOT NULL,
  `kelas` varchar(20) NOT NULL DEFAULT '',
  `ruangan` varchar(20) DEFAULT NULL,
  `topik` varchar(255) DEFAULT NULL,
  `sub_topik` text,
  `objektif` text,
  `aktifitas` varchar(15) DEFAULT NULL,
  `pic` varchar(20) NOT NULL DEFAULT '',
  `metode` text,
  `alat_bantu` text,
  `ket` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `createad_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`jadwal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_jadwal` */

/*Table structure for table `prkl_krs_detail` */

DROP TABLE IF EXISTS `prkl_krs_detail`;

CREATE TABLE `prkl_krs_detail` (
  `krs_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `krs_mhs_id` int(11) DEFAULT NULL,
  `kuliah_id` int(11) DEFAULT NULL,
  `pengajaran_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`krs_detail_id`),
  KEY `FK_prkl_krs_detail` (`krs_mhs_id`),
  KEY `FK_prkl_krs_detail_kuliah` (`kuliah_id`),
  KEY `FK_prkl_krs_detail_pengajaran` (`pengajaran_id`),
  CONSTRAINT `FK_prkl_krs_detail` FOREIGN KEY (`krs_mhs_id`) REFERENCES `prkl_krs_mhs` (`krs_mhs_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_prkl_krs_detail_kuliah` FOREIGN KEY (`kuliah_id`) REFERENCES `krkm_kuliah` (`kuliah_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_prkl_krs_detail_pengajaran` FOREIGN KEY (`pengajaran_id`) REFERENCES `adak_pengajaran` (`pengajaran_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_krs_detail` */

/*Table structure for table `prkl_krs_mhs` */

DROP TABLE IF EXISTS `prkl_krs_mhs`;

CREATE TABLE `prkl_krs_mhs` (
  `krs_mhs_id` int(11) NOT NULL AUTO_INCREMENT,
  `dim_id` int(11) NOT NULL,
  `nim` varchar(8) NOT NULL,
  `sem_ta` varchar(2) NOT NULL,
  `ta` varchar(5) NOT NULL,
  `tahun_kurikulum_id` int(11) NOT NULL,
  `status_approval` tinyint(1) DEFAULT '0',
  `status_periode` varchar(4) DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL COMMENT 'dosen_id',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`krs_mhs_id`),
  KEY `FK_prkl_krs_mhs_ta_kur` (`tahun_kurikulum_id`),
  KEY `FK_prkl_krs_mhs_ta` (`ta`),
  KEY `FK_prkl_krs_mhs-dim` (`dim_id`),
  KEY `FK_prkl_krs_mhs_wali` (`approved_by`),
  CONSTRAINT `FK_prkl_krs_mhs-dim` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_prkl_krs_mhs_pegawai_id` FOREIGN KEY (`approved_by`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_prkl_krs_mhs_ta_kur` FOREIGN KEY (`tahun_kurikulum_id`) REFERENCES `krkm_r_tahun_kurikulum` (`tahun_kurikulum_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_krs_mhs` */

/*Table structure for table `prkl_krs_review` */

DROP TABLE IF EXISTS `prkl_krs_review`;

CREATE TABLE `prkl_krs_review` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_by` int(11) DEFAULT NULL,
  `krs_mhs_id` int(11) DEFAULT NULL,
  `comment` text,
  `tgl_comment` date DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`review_id`),
  KEY `FK_prkl_krs_review_dosen` (`comment_by`),
  KEY `FK_prkl_krs_review` (`krs_mhs_id`),
  CONSTRAINT `FK_prkl_krs_review` FOREIGN KEY (`krs_mhs_id`) REFERENCES `prkl_krs_mhs` (`krs_mhs_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_prkl_krs_review_dosen` FOREIGN KEY (`comment_by`) REFERENCES `sysx_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_krs_review` */

/*Table structure for table `prkl_kuesioner_materi` */

DROP TABLE IF EXISTS `prkl_kuesioner_materi`;

CREATE TABLE `prkl_kuesioner_materi` (
  `kuesioner_materi_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kuesioner` int(11) NOT NULL,
  `id_kur` int(11) DEFAULT NULL,
  `ta` int(11) DEFAULT NULL,
  `sem` int(11) DEFAULT NULL,
  `kode_mk` varchar(11) DEFAULT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `status` varchar(50) DEFAULT 'NOT ACTIVE',
  `pengajar` varchar(20) DEFAULT NULL,
  `time_activated` datetime DEFAULT NULL,
  `kurikulum_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_id` datetime DEFAULT NULL,
  `updated_id` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`kuesioner_materi_id`),
  KEY `FK_t_kuesioner_materi` (`kurikulum_id`),
  CONSTRAINT `FK_t_kuesioner_materi` FOREIGN KEY (`kurikulum_id`) REFERENCES `krkm_kuliah` (`kuliah_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_kuesioner_materi` */

/*Table structure for table `prkl_kuesioner_praktikum` */

DROP TABLE IF EXISTS `prkl_kuesioner_praktikum`;

CREATE TABLE `prkl_kuesioner_praktikum` (
  `kuesioner_praktikum_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kuesioner` int(11) NOT NULL,
  `id_kur` int(11) DEFAULT NULL,
  `ta` int(11) DEFAULT NULL,
  `sem` int(11) DEFAULT NULL,
  `kode_mk` varchar(11) DEFAULT NULL,
  `kuliah_id` int(11) DEFAULT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `status` varchar(20) DEFAULT 'NOT ACTIVE',
  `pengajar` varchar(50) DEFAULT NULL,
  `time_activated` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`kuesioner_praktikum_id`),
  KEY `FK_t_kuesioner_praktikum` (`kuliah_id`),
  CONSTRAINT `FK_t_kuesioner_praktikum` FOREIGN KEY (`kuliah_id`) REFERENCES `krkm_kuliah` (`kuliah_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_kuesioner_praktikum` */

/*Table structure for table `prkl_kurikulum_syllabus` */

DROP TABLE IF EXISTS `prkl_kurikulum_syllabus`;

CREATE TABLE `prkl_kurikulum_syllabus` (
  `kurikulum_syllabus_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kur` int(11) DEFAULT '2001',
  `kode_mk` varchar(11) DEFAULT NULL,
  `ta` int(4) DEFAULT '2005',
  `sem_ta` int(11) NOT NULL,
  `map_to_kurikulum_syllabus_id` int(11) DEFAULT NULL,
  `prerequisites` text,
  `corerequisites` text,
  `course_description` text,
  `course_objectives` text,
  `learning_outcomes` text,
  `course_format` text,
  `grading_procedure` text,
  `course_content` text,
  `reference` text,
  `tools` text,
  `kuliah_id` int(11) DEFAULT NULL,
  `ta_id` int(11) NOT NULL,
  `meetings` varchar(100) DEFAULT NULL,
  `tipe` varchar(25) DEFAULT NULL,
  `level` varchar(15) DEFAULT NULL,
  `web_page` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`kurikulum_syllabus_id`),
  KEY `KODE_MK_2` (`kode_mk`),
  KEY `ID_KUR` (`id_kur`),
  KEY `KODE_MK_3` (`kode_mk`),
  KEY `TA` (`ta`),
  KEY `fk_t_kurikulum_syllabus_t_kurikulum1_idx` (`kuliah_id`),
  KEY `FK_prkl_kurikulum_syllabus_ta` (`ta_id`),
  CONSTRAINT `FK_prkl_kurikulum_syllabus` FOREIGN KEY (`kuliah_id`) REFERENCES `krkm_kuliah` (`kuliah_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_prkl_kurikulum_syllabus_ta` FOREIGN KEY (`ta_id`) REFERENCES `mref_r_ta` (`ta_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_kurikulum_syllabus` */

/*Table structure for table `prkl_kurikulum_syllabus_file` */

DROP TABLE IF EXISTS `prkl_kurikulum_syllabus_file`;

CREATE TABLE `prkl_kurikulum_syllabus_file` (
  `kurikulum_syllabus_file_id` int(11) NOT NULL AUTO_INCREMENT,
  `kurikulum_syllabus_id` int(11) DEFAULT NULL,
  `nama_file` varchar(255) DEFAULT NULL,
  `kode_file` varchar(255) DEFAULT NULL,
  `keterangan` text,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`kurikulum_syllabus_file_id`),
  KEY `FK_prkl_kurikulum_syllabus_file` (`kurikulum_syllabus_id`),
  CONSTRAINT `FK_prkl_kurikulum_syllabus_file` FOREIGN KEY (`kurikulum_syllabus_id`) REFERENCES `prkl_kurikulum_syllabus` (`kurikulum_syllabus_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_kurikulum_syllabus_file` */

/*Table structure for table `prkl_materi` */

DROP TABLE IF EXISTS `prkl_materi`;

CREATE TABLE `prkl_materi` (
  `materi_id` int(11) NOT NULL AUTO_INCREMENT,
  `no` smallint(6) NOT NULL DEFAULT '0',
  `ta` int(4) NOT NULL DEFAULT '2002',
  `id_kur` int(4) NOT NULL DEFAULT '2002',
  `kode_mk` varchar(11) NOT NULL,
  `minggu_ke` char(2) DEFAULT NULL,
  `sesi` smallint(6) DEFAULT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `inisial` varchar(15) DEFAULT NULL,
  `isi` longtext,
  `tgl_sesi` datetime DEFAULT NULL,
  `tgl_view` datetime DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `counter` int(11) DEFAULT '0',
  `kurikulum_syllabus_id` int(11) DEFAULT NULL,
  `group_kuliah_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`materi_id`),
  KEY `FK_prkl_materi_syllabus` (`kurikulum_syllabus_id`),
  KEY `FK_prkl_materi_group_kuliah` (`group_kuliah_id`),
  CONSTRAINT `FK_prkl_materi_group_kuliah` FOREIGN KEY (`group_kuliah_id`) REFERENCES `prkl_group_kuliah` (`group_kuliah_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_prkl_materi_syllabus` FOREIGN KEY (`kurikulum_syllabus_id`) REFERENCES `prkl_kurikulum_syllabus` (`kurikulum_syllabus_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_materi` */

/*Table structure for table `prkl_material` */

DROP TABLE IF EXISTS `prkl_material`;

CREATE TABLE `prkl_material` (
  `material_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_material` varchar(30) NOT NULL DEFAULT '',
  `kategori` varchar(255) NOT NULL DEFAULT '',
  `ta` int(4) NOT NULL DEFAULT '0',
  `id_kur` int(4) NOT NULL DEFAULT '0',
  `kode_mk` varchar(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `aktifitas` varchar(255) DEFAULT NULL,
  `ket_setoran` varchar(255) DEFAULT NULL,
  `batas_akhir` datetime DEFAULT NULL,
  `metoda_penyerahan` enum('Hardcopy','Email','Web') DEFAULT 'Hardcopy',
  `tujuan` varchar(255) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `tgl_view` bigint(20) DEFAULT NULL,
  `status` char(1) NOT NULL DEFAULT '1',
  `komentar` char(1) DEFAULT '0',
  `isi` longtext,
  `material_kategori_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`material_id`),
  KEY `ID_MATERIAL` (`material_id`),
  KEY `fk_t_material_t_material_kategori1_idx` (`material_kategori_id`),
  CONSTRAINT `fk_t_material_t_material_kategori1` FOREIGN KEY (`material_kategori_id`) REFERENCES `prkl_material_kategori` (`material_kategori_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_material` */

/*Table structure for table `prkl_material_files` */

DROP TABLE IF EXISTS `prkl_material_files`;

CREATE TABLE `prkl_material_files` (
  `material_files_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_material` varchar(30) NOT NULL DEFAULT '0',
  `nama_file` varchar(255) DEFAULT NULL,
  `kode_file` varchar(100) DEFAULT NULL COMMENT 'id file dari puro',
  `data` longblob,
  `ket` varchar(255) DEFAULT NULL,
  `tipe` varchar(50) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `lokasi` enum('DB','FILE') NOT NULL DEFAULT 'DB',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`material_files_id`),
  KEY `ID_MATERIAL` (`id_material`),
  KEY `NAMA_FILE` (`nama_file`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_material_files` */

/*Table structure for table `prkl_material_kategori` */

DROP TABLE IF EXISTS `prkl_material_kategori`;

CREATE TABLE `prkl_material_kategori` (
  `material_kategori_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(255) NOT NULL DEFAULT '',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`material_kategori_id`),
  UNIQUE KEY `KATEGORI_UNIQUE` (`kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_material_kategori` */

/*Table structure for table `prkl_penilaian_materi` */

DROP TABLE IF EXISTS `prkl_penilaian_materi`;

CREATE TABLE `prkl_penilaian_materi` (
  `penilaian_materi_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kuesioner` int(11) DEFAULT NULL,
  `id_kur` int(11) DEFAULT NULL,
  `ta` int(11) DEFAULT NULL,
  `kode_mk` varchar(20) DEFAULT NULL,
  `peserta` varchar(20) DEFAULT NULL,
  `s1` int(11) DEFAULT NULL,
  `s2` int(11) DEFAULT NULL,
  `s3` int(11) DEFAULT NULL,
  `s4` int(11) DEFAULT NULL,
  `s5` int(11) DEFAULT NULL,
  `s6` int(11) DEFAULT NULL,
  `skor_total` int(11) DEFAULT NULL,
  `kuesioner_materi_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`penilaian_materi_id`),
  KEY `fk_t_penilaian_materi_t_kuesioner_materi1_idx` (`kuesioner_materi_id`),
  CONSTRAINT `fk_t_penilaian_materi_t_kuesioner_materi1` FOREIGN KEY (`kuesioner_materi_id`) REFERENCES `prkl_kuesioner_materi` (`kuesioner_materi_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_penilaian_materi` */

/*Table structure for table `prkl_penilaian_materi_nilai` */

DROP TABLE IF EXISTS `prkl_penilaian_materi_nilai`;

CREATE TABLE `prkl_penilaian_materi_nilai` (
  `penilaian_materi_nilai_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kuesioner` int(11) DEFAULT NULL,
  `peserta` varchar(20) DEFAULT NULL,
  `kesulitan_materi` varchar(10) DEFAULT NULL,
  `pemahaman_mahasiswa` varchar(10) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`penilaian_materi_nilai_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_penilaian_materi_nilai` */

/*Table structure for table `prkl_penilaian_praktikum` */

DROP TABLE IF EXISTS `prkl_penilaian_praktikum`;

CREATE TABLE `prkl_penilaian_praktikum` (
  `penilaian_praktikum_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kuesioner` int(11) DEFAULT NULL,
  `id_kur` int(11) DEFAULT NULL,
  `ta` int(11) DEFAULT NULL,
  `kode_mk` varchar(20) DEFAULT NULL,
  `peserta` varchar(20) DEFAULT NULL,
  `s1` int(11) DEFAULT NULL,
  `s2` int(11) DEFAULT NULL,
  `s3` int(11) DEFAULT NULL,
  `s4` int(11) DEFAULT NULL,
  `s5` int(11) DEFAULT NULL,
  `s6` int(11) DEFAULT NULL,
  `skor_total` int(11) DEFAULT NULL,
  `kuesioner_praktikum_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`penilaian_praktikum_id`),
  KEY `fk_t_penilaian_praktikum_t_kuesioner_praktikum1_idx` (`kuesioner_praktikum_id`),
  CONSTRAINT `fk_t_penilaian_praktikum_t_kuesioner_praktikum1` FOREIGN KEY (`kuesioner_praktikum_id`) REFERENCES `prkl_kuesioner_praktikum` (`kuesioner_praktikum_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_penilaian_praktikum` */

/*Table structure for table `prkl_penilaian_praktikum_nilai` */

DROP TABLE IF EXISTS `prkl_penilaian_praktikum_nilai`;

CREATE TABLE `prkl_penilaian_praktikum_nilai` (
  `penilaian_praktikum_nilai_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kuesioner` int(11) DEFAULT NULL,
  `peserta` varchar(20) DEFAULT NULL,
  `penyelesaian` varchar(20) DEFAULT NULL,
  `kesulitan_praktikum` varchar(20) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`penilaian_praktikum_nilai_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_penilaian_praktikum_nilai` */

/*Table structure for table `prkl_penilaian_tim_pengajar` */

DROP TABLE IF EXISTS `prkl_penilaian_tim_pengajar`;

CREATE TABLE `prkl_penilaian_tim_pengajar` (
  `penilaian_tim_pengajar_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kuesioner` int(11) DEFAULT NULL,
  `user_id` varchar(20) DEFAULT NULL,
  `user_name` varchar(20) DEFAULT NULL,
  `ta` int(11) DEFAULT NULL,
  `kode_mk` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `k1` int(11) DEFAULT NULL,
  `k2` int(11) DEFAULT NULL,
  `k3` int(11) DEFAULT NULL,
  `k4` int(11) DEFAULT NULL,
  `k5` int(11) DEFAULT NULL,
  `k6` int(11) DEFAULT NULL,
  `skor_total` int(11) DEFAULT '0',
  `profile_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`penilaian_tim_pengajar_id`),
  KEY `FK_prkl_penilaian_tim_pengajar_profile` (`profile_id`),
  CONSTRAINT `FK_prkl_penilaian_tim_pengajar_profile` FOREIGN KEY (`profile_id`) REFERENCES `hrdx_profile` (`profile_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_penilaian_tim_pengajar` */

/*Table structure for table `prkl_penilaian_tim_pengajar_nilai` */

DROP TABLE IF EXISTS `prkl_penilaian_tim_pengajar_nilai`;

CREATE TABLE `prkl_penilaian_tim_pengajar_nilai` (
  `penilaian_tim_pengajar_nilai_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kuesioner` int(11) DEFAULT NULL,
  `user` varchar(20) DEFAULT NULL,
  `dosen_id` varchar(20) DEFAULT NULL,
  `ta` int(11) DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL,
  `nilai` varchar(5) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`penilaian_tim_pengajar_nilai_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_penilaian_tim_pengajar_nilai` */

/*Table structure for table `prkl_praktikum` */

DROP TABLE IF EXISTS `prkl_praktikum`;

CREATE TABLE `prkl_praktikum` (
  `praktikum_id` int(11) NOT NULL AUTO_INCREMENT,
  `no` smallint(6) NOT NULL DEFAULT '0',
  `ta` int(4) NOT NULL DEFAULT '0',
  `id_kur` int(4) NOT NULL DEFAULT '0',
  `kode_mk` varchar(11) NOT NULL,
  `minggu_ke` smallint(6) DEFAULT NULL,
  `sesi` smallint(6) DEFAULT NULL,
  `topik` varchar(255) DEFAULT NULL,
  `aktifitas` varchar(255) DEFAULT NULL,
  `waktu_pengerjaan` varchar(50) DEFAULT NULL,
  `setoran` varchar(255) DEFAULT NULL,
  `batas_akhir` varchar(50) DEFAULT NULL,
  `tempat_penyerahan` varchar(50) DEFAULT NULL,
  `tujuan` varchar(255) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `tgl_view` datetime DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `counter` int(11) DEFAULT '1',
  `isi` longtext,
  `kurikulum_syllabus_id` int(11) DEFAULT NULL,
  `group_kuliah_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`praktikum_id`),
  KEY `NO` (`no`),
  KEY `TOPIK` (`topik`),
  KEY `fk_t_praktikum_t_kurikulum1_idx` (`kurikulum_syllabus_id`),
  KEY `FK_prkl_praktikum_group_kuliah` (`group_kuliah_id`),
  CONSTRAINT `FK_prkl_praktikum_group_kuliah` FOREIGN KEY (`group_kuliah_id`) REFERENCES `prkl_group_kuliah` (`group_kuliah_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_prkl_praktikum_syllabus` FOREIGN KEY (`kurikulum_syllabus_id`) REFERENCES `prkl_kurikulum_syllabus` (`kurikulum_syllabus_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_praktikum` */

/*Table structure for table `prkl_rpp` */

DROP TABLE IF EXISTS `prkl_rpp`;

CREATE TABLE `prkl_rpp` (
  `rpp_id` int(11) NOT NULL AUTO_INCREMENT,
  `minggu` int(2) DEFAULT NULL,
  `sesi` int(2) DEFAULT NULL,
  `topik` text,
  `kurikulum_syllabus_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`rpp_id`),
  KEY `FK_prkl_rpp_kurikulum_syllabus` (`kurikulum_syllabus_id`),
  CONSTRAINT `FK_prkl_rpp_kurikulum_syllabus` FOREIGN KEY (`kurikulum_syllabus_id`) REFERENCES `prkl_kurikulum_syllabus` (`kurikulum_syllabus_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_rpp` */

/*Table structure for table `prkl_ruangan` */

DROP TABLE IF EXISTS `prkl_ruangan`;

CREATE TABLE `prkl_ruangan` (
  `ruangan_id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_ruangan` varchar(20) NOT NULL DEFAULT '',
  `short_name` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(200) DEFAULT NULL,
  `kapasitas` int(11) DEFAULT NULL,
  `ket` text,
  `status` char(1) NOT NULL DEFAULT '1',
  `update_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ruangan_id`),
  UNIQUE KEY `KODE_RUANGAN_UNIQUE` (`kode_ruangan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `prkl_ruangan` */

/*Table structure for table `produk` */

DROP TABLE IF EXISTS `produk`;

CREATE TABLE `produk` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `harga` float NOT NULL,
  `jumlah` int(20) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `id_kategori` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `produk` */

/*Table structure for table `rakx_detil_program` */

DROP TABLE IF EXISTS `rakx_detil_program`;

CREATE TABLE `rakx_detil_program` (
  `detil_program_id` int(11) NOT NULL AUTO_INCREMENT,
  `program_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `desc` text,
  `jumlah` decimal(19,4) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`detil_program_id`),
  KEY `fk_detil_program_program_idx` (`program_id`),
  CONSTRAINT `fk_detil_program_program` FOREIGN KEY (`program_id`) REFERENCES `rakx_program` (`program_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rakx_detil_program` */

/*Table structure for table `rakx_mata_anggaran` */

DROP TABLE IF EXISTS `rakx_mata_anggaran`;

CREATE TABLE `rakx_mata_anggaran` (
  `mata_anggaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `standar_id` int(11) DEFAULT NULL,
  `kode_anggaran` varchar(45) NOT NULL,
  `name` varchar(100) NOT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`mata_anggaran_id`),
  KEY `fk_mata_anggaran_standar_idx` (`standar_id`),
  KEY `KODE_ANGGARAN` (`kode_anggaran`),
  KEY `NAME` (`name`),
  CONSTRAINT `fk_mata_anggaran_standar` FOREIGN KEY (`standar_id`) REFERENCES `rakx_r_standar` (`standar_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rakx_mata_anggaran` */

/*Table structure for table `rakx_program` */

DROP TABLE IF EXISTS `rakx_program`;

CREATE TABLE `rakx_program` (
  `program_id` int(11) NOT NULL AUTO_INCREMENT,
  `struktur_jabatan_has_mata_anggaran_id` int(11) NOT NULL,
  `kode_program` varchar(45) NOT NULL,
  `name` text NOT NULL,
  `tujuan` text,
  `sasaran` text,
  `target` text,
  `desc` text,
  `rencana_strategis_id` int(11) DEFAULT NULL,
  `volume` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `harga_satuan` decimal(19,4) NOT NULL,
  `jumlah_sebelum_revisi` decimal(19,4) NOT NULL,
  `jumlah` decimal(19,4) DEFAULT NULL,
  `status_program_id` int(11) DEFAULT NULL,
  `diusulkan_oleh` int(11) NOT NULL,
  `tanggal_diusulkan` datetime DEFAULT NULL,
  `dilaksanakan_oleh` int(11) DEFAULT NULL,
  `disetujui_oleh` int(11) DEFAULT NULL,
  `tanggal_disetujui` datetime DEFAULT NULL,
  `ditolak_oleh` int(11) DEFAULT NULL,
  `tanggal_ditolak` datetime DEFAULT NULL,
  `is_revisi` tinyint(1) DEFAULT '0',
  `direvisi_oleh` int(11) DEFAULT NULL,
  `tanggal_direvisi` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`program_id`),
  KEY `fk_program_satuan_idx` (`satuan_id`),
  KEY `fk_program_rencana_strategis_idx` (`rencana_strategis_id`),
  KEY `fk_program_struktur_jabatan_has_mata_anggaran_idx` (`struktur_jabatan_has_mata_anggaran_id`),
  KEY `fk_program_status_program_idx` (`status_program_id`),
  KEY `fk_program_pengusul_idx` (`diusulkan_oleh`),
  KEY `fk_program_pelaksana_idx` (`dilaksanakan_oleh`),
  KEY `fk_program_perevisi_idx` (`direvisi_oleh`),
  KEY `fk_program_disetujui_idx` (`disetujui_oleh`),
  KEY `fk_program_ditolak_idx` (`ditolak_oleh`),
  KEY `KODE_PROGRAM` (`kode_program`),
  CONSTRAINT `fk_program_dilaksanakan` FOREIGN KEY (`dilaksanakan_oleh`) REFERENCES `inst_struktur_jabatan` (`struktur_jabatan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_program_direvisi` FOREIGN KEY (`direvisi_oleh`) REFERENCES `inst_pejabat` (`pejabat_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_program_disetujui` FOREIGN KEY (`disetujui_oleh`) REFERENCES `inst_pejabat` (`pejabat_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_program_ditolak` FOREIGN KEY (`ditolak_oleh`) REFERENCES `inst_pejabat` (`pejabat_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_program_diusulkan` FOREIGN KEY (`diusulkan_oleh`) REFERENCES `inst_pejabat` (`pejabat_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_program_rencana_strategis` FOREIGN KEY (`rencana_strategis_id`) REFERENCES `rakx_r_rencana_strategis` (`rencana_strategis_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_program_satuan` FOREIGN KEY (`satuan_id`) REFERENCES `rakx_r_satuan` (`satuan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_program_status_program` FOREIGN KEY (`status_program_id`) REFERENCES `rakx_r_status_program` (`status_program_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_program_struktur_jabatan_has_mata_anggaran` FOREIGN KEY (`struktur_jabatan_has_mata_anggaran_id`) REFERENCES `rakx_struktur_jabatan_has_mata_anggaran` (`struktur_jabatan_has_mata_anggaran_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rakx_program` */

/*Table structure for table `rakx_program_has_sumber_dana` */

DROP TABLE IF EXISTS `rakx_program_has_sumber_dana`;

CREATE TABLE `rakx_program_has_sumber_dana` (
  `program_has_sumber_dana_id` int(11) NOT NULL AUTO_INCREMENT,
  `program_id` int(11) NOT NULL,
  `sumber_dana_id` int(11) NOT NULL,
  `jumlah` decimal(19,4) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`program_has_sumber_dana_id`),
  KEY `fk_program_has_sumber_dana_program_idx` (`program_id`),
  KEY `fk_program_has_sumber_dana_sumber_dana_idx` (`sumber_dana_id`),
  CONSTRAINT `fk_program_has_sumber_dana_program` FOREIGN KEY (`program_id`) REFERENCES `rakx_program` (`program_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_program_has_sumber_dana_sumber_dana` FOREIGN KEY (`sumber_dana_id`) REFERENCES `rakx_r_sumber_dana` (`sumber_dana_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rakx_program_has_sumber_dana` */

/*Table structure for table `rakx_program_has_waktu` */

DROP TABLE IF EXISTS `rakx_program_has_waktu`;

CREATE TABLE `rakx_program_has_waktu` (
  `program_has_waktu_id` int(11) NOT NULL AUTO_INCREMENT,
  `program_id` int(11) NOT NULL,
  `bulan_id` int(11) NOT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`program_has_waktu_id`),
  KEY `fk_program_has_waktu_program_idx` (`program_id`),
  KEY `fk_program_has_waktu_bulan_idx` (`bulan_id`),
  CONSTRAINT `fk_program_has_waktu_bulan` FOREIGN KEY (`bulan_id`) REFERENCES `rakx_r_bulan` (`bulan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_program_has_waktu_program` FOREIGN KEY (`program_id`) REFERENCES `rakx_program` (`program_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rakx_program_has_waktu` */

/*Table structure for table `rakx_r_bulan` */

DROP TABLE IF EXISTS `rakx_r_bulan`;

CREATE TABLE `rakx_r_bulan` (
  `bulan_id` int(11) NOT NULL AUTO_INCREMENT,
  `bulan` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`bulan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rakx_r_bulan` */

/*Table structure for table `rakx_r_rencana_strategis` */

DROP TABLE IF EXISTS `rakx_r_rencana_strategis`;

CREATE TABLE `rakx_r_rencana_strategis` (
  `rencana_strategis_id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor` varchar(11) NOT NULL,
  `name` text NOT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`rencana_strategis_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rakx_r_rencana_strategis` */

/*Table structure for table `rakx_r_satuan` */

DROP TABLE IF EXISTS `rakx_r_satuan`;

CREATE TABLE `rakx_r_satuan` (
  `satuan_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`satuan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rakx_r_satuan` */

/*Table structure for table `rakx_r_standar` */

DROP TABLE IF EXISTS `rakx_r_standar`;

CREATE TABLE `rakx_r_standar` (
  `standar_id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor` int(11) NOT NULL,
  `name` text NOT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`standar_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rakx_r_standar` */

/*Table structure for table `rakx_r_status_program` */

DROP TABLE IF EXISTS `rakx_r_status_program`;

CREATE TABLE `rakx_r_status_program` (
  `status_program_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`status_program_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rakx_r_status_program` */

/*Table structure for table `rakx_r_sumber_dana` */

DROP TABLE IF EXISTS `rakx_r_sumber_dana`;

CREATE TABLE `rakx_r_sumber_dana` (
  `sumber_dana_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`sumber_dana_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rakx_r_sumber_dana` */

/*Table structure for table `rakx_r_tahun_anggaran` */

DROP TABLE IF EXISTS `rakx_r_tahun_anggaran`;

CREATE TABLE `rakx_r_tahun_anggaran` (
  `tahun_anggaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `tahun` year(4) NOT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`tahun_anggaran_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rakx_r_tahun_anggaran` */

/*Table structure for table `rakx_review_program` */

DROP TABLE IF EXISTS `rakx_review_program`;

CREATE TABLE `rakx_review_program` (
  `review_program_id` int(11) NOT NULL AUTO_INCREMENT,
  `program_id` int(11) NOT NULL,
  `pejabat_id` int(11) NOT NULL,
  `review` text NOT NULL,
  `tanggal_review` datetime NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`review_program_id`),
  KEY `fk_review_anggaran_program_idx` (`program_id`),
  KEY `TANGGAL_REVIEW` (`tanggal_review`),
  KEY `fk_review_anggaran_pejabat_idx` (`pejabat_id`),
  CONSTRAINT `fk_review_anggaran_pejabat` FOREIGN KEY (`pejabat_id`) REFERENCES `inst_pejabat` (`pejabat_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_review_anggaran_program` FOREIGN KEY (`program_id`) REFERENCES `rakx_program` (`program_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rakx_review_program` */

/*Table structure for table `rakx_struktur_jabatan_has_mata_anggaran` */

DROP TABLE IF EXISTS `rakx_struktur_jabatan_has_mata_anggaran`;

CREATE TABLE `rakx_struktur_jabatan_has_mata_anggaran` (
  `struktur_jabatan_has_mata_anggaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `struktur_jabatan_id` int(11) NOT NULL,
  `mata_anggaran_id` int(11) NOT NULL,
  `tahun_anggaran_id` int(11) NOT NULL,
  `subtotal` decimal(19,4) DEFAULT '0.0000',
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`struktur_jabatan_has_mata_anggaran_id`),
  KEY `fk_struktur_jabatan_has_mata_anggaran_struktur_jabatan_idx` (`struktur_jabatan_id`),
  KEY `fk_struktur_jabatan_has_mata_anggaran_mata_anggaran_idx` (`mata_anggaran_id`),
  KEY `fk_struktur_jabatan_has_mata_anggaran_tahun_anggaran_idx` (`tahun_anggaran_id`),
  CONSTRAINT `fk_struktur_jabatan_has_mata_anggaran_mata_anggaran` FOREIGN KEY (`mata_anggaran_id`) REFERENCES `rakx_mata_anggaran` (`mata_anggaran_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_struktur_jabatan_has_mata_anggaran_struktur_jabatan` FOREIGN KEY (`struktur_jabatan_id`) REFERENCES `inst_struktur_jabatan` (`struktur_jabatan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_struktur_jabatan_has_mata_anggaran_tahun_anggaran` FOREIGN KEY (`tahun_anggaran_id`) REFERENCES `rakx_r_tahun_anggaran` (`tahun_anggaran_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rakx_struktur_jabatan_has_mata_anggaran` */

/*Table structure for table `rist_beban_kerja` */

DROP TABLE IF EXISTS `rist_beban_kerja`;

CREATE TABLE `rist_beban_kerja` (
  `beban_kerja_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_kegiatan` text,
  `bukti_penugasan` text,
  `beban_sks` tinyint(2) DEFAULT NULL,
  `masa_pelaksanaan` varchar(64) DEFAULT NULL,
  `bukti_dokumen` text,
  `capaian_persen` double DEFAULT NULL,
  `capaian_sks` tinyint(2) DEFAULT NULL,
  `penilaian_asesor` varchar(64) DEFAULT NULL,
  `bidang_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`beban_kerja_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rist_beban_kerja` */

/*Table structure for table `rist_bidang` */

DROP TABLE IF EXISTS `rist_bidang`;

CREATE TABLE `rist_bidang` (
  `bidang_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `desc` text,
  `rist_penelitian_id` int(11) DEFAULT NULL,
  `rist_pendidikan_id` int(11) DEFAULT NULL,
  `rist_pengabdian_id` int(11) DEFAULT NULL,
  `rist_penunjang_id` int(11) DEFAULT NULL,
  `kategori_profesor` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`bidang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rist_bidang` */

/*Table structure for table `rprt_complaint` */

DROP TABLE IF EXISTS `rprt_complaint`;

CREATE TABLE `rprt_complaint` (
  `complaint_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `bagian_id` int(11) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `desc` text,
  `status_id` int(11) DEFAULT NULL,
  `estimated_date` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`complaint_id`),
  KEY `FK_rprt_complaint_status` (`status_id`),
  KEY `FK_rprt_complaint_user` (`user_id`),
  KEY `FK_rprt_complaint_bagian` (`bagian_id`),
  CONSTRAINT `FK_rprt_complaint_bagian` FOREIGN KEY (`bagian_id`) REFERENCES `rprt_r_bagian` (`bagian_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_rprt_complaint_status` FOREIGN KEY (`status_id`) REFERENCES `rprt_r_status` (`status_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_rprt_complaint_user` FOREIGN KEY (`user_id`) REFERENCES `sysx_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rprt_complaint` */

/*Table structure for table `rprt_r_bagian` */

DROP TABLE IF EXISTS `rprt_r_bagian`;

CREATE TABLE `rprt_r_bagian` (
  `bagian_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`bagian_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rprt_r_bagian` */

/*Table structure for table `rprt_r_status` */

DROP TABLE IF EXISTS `rprt_r_status`;

CREATE TABLE `rprt_r_status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `desc` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rprt_r_status` */

/*Table structure for table `rprt_response` */

DROP TABLE IF EXISTS `rprt_response`;

CREATE TABLE `rprt_response` (
  `response_id` int(11) NOT NULL AUTO_INCREMENT,
  `complaint_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comment` varchar(200) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`response_id`),
  KEY `FK_rprt_response_complaint` (`complaint_id`),
  KEY `FK_rprt_response_user` (`user_id`),
  CONSTRAINT `FK_rprt_response_complaint` FOREIGN KEY (`complaint_id`) REFERENCES `rprt_complaint` (`complaint_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_rprt_response_user` FOREIGN KEY (`user_id`) REFERENCES `sysx_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rprt_response` */

/*Table structure for table `rprt_user_has_bagian` */

DROP TABLE IF EXISTS `rprt_user_has_bagian`;

CREATE TABLE `rprt_user_has_bagian` (
  `user_has_bagian_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `bagian_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`user_has_bagian_id`),
  KEY `FK_rprt_user_has_bagian_user` (`user_id`),
  KEY `FK_rprt_user_has_bagian_bagian` (`bagian_id`),
  CONSTRAINT `FK_rprt_user_has_bagian_bagian` FOREIGN KEY (`bagian_id`) REFERENCES `rprt_r_bagian` (`bagian_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_rprt_user_has_bagian_user` FOREIGN KEY (`user_id`) REFERENCES `sysx_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rprt_user_has_bagian` */

/*Table structure for table `schd_event` */

DROP TABLE IF EXISTS `schd_event`;

CREATE TABLE `schd_event` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `calender_id` int(11) NOT NULL,
  `event` varchar(150) NOT NULL,
  `desc` text,
  `start` datetime NOT NULL,
  `end` datetime DEFAULT NULL,
  `all_day` tinyint(1) DEFAULT '0',
  `lokasi_id` int(11) DEFAULT NULL,
  `lokasi_text` varchar(250) DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`event_id`),
  KEY `FK_schd_event` (`lokasi_id`),
  KEY `FK_schd_event_calender` (`calender_id`),
  CONSTRAINT `FK_schd_event` FOREIGN KEY (`lokasi_id`) REFERENCES `invt_r_lokasi` (`lokasi_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_schd_event_calender` FOREIGN KEY (`calender_id`) REFERENCES `schd_r_calender` (`calender_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `schd_event` */

/*Table structure for table `schd_event_invitee` */

DROP TABLE IF EXISTS `schd_event_invitee`;

CREATE TABLE `schd_event_invitee` (
  `event_invitee_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`event_invitee_id`),
  KEY `FK_schd_event_invitee` (`event_id`),
  KEY `FK_schd_event_invitee-user` (`user_id`),
  CONSTRAINT `FK_schd_event_invitee` FOREIGN KEY (`event_id`) REFERENCES `schd_event` (`event_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_schd_event_invitee-user` FOREIGN KEY (`user_id`) REFERENCES `sysx_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `schd_event_invitee` */

/*Table structure for table `schd_file_event` */

DROP TABLE IF EXISTS `schd_file_event`;

CREATE TABLE `schd_file_event` (
  `file_event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `nama_file` varchar(200) DEFAULT NULL,
  `kode_file` varchar(200) DEFAULT NULL,
  `ket` text,
  `ta` int(11) DEFAULT NULL,
  `sem_ta` int(11) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`file_event_id`),
  KEY `FK_schd_file_event_file` (`event_id`),
  CONSTRAINT `FK_schd_file_event_file` FOREIGN KEY (`event_id`) REFERENCES `schd_event` (`event_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `schd_file_event` */

/*Table structure for table `schd_jadwal_kuliah` */

DROP TABLE IF EXISTS `schd_jadwal_kuliah`;

CREATE TABLE `schd_jadwal_kuliah` (
  `jadwal_kuliah_id` int(11) NOT NULL AUTO_INCREMENT,
  `kuliah_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `kelas_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`jadwal_kuliah_id`),
  KEY `FK_schd_jadwal_kuliah` (`event_id`),
  KEY `FK_schd_jadwal_kuliah_kuliah` (`kuliah_id`),
  KEY `FK_schd_jadwal_kuliah_kelas` (`kelas_id`),
  CONSTRAINT `FK_schd_jadwal_kuliah` FOREIGN KEY (`event_id`) REFERENCES `schd_event` (`event_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_schd_jadwal_kuliah_kelas` FOREIGN KEY (`kelas_id`) REFERENCES `adak_kelas` (`kelas_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_schd_jadwal_kuliah_kuliah` FOREIGN KEY (`kuliah_id`) REFERENCES `krkm_kuliah` (`kuliah_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `schd_jadwal_kuliah` */

/*Table structure for table `schd_r_calender` */

DROP TABLE IF EXISTS `schd_r_calender`;

CREATE TABLE `schd_r_calender` (
  `calender_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `desc` text,
  `is_public` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`calender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `schd_r_calender` */

/*Table structure for table `schd_subscriber` */

DROP TABLE IF EXISTS `schd_subscriber`;

CREATE TABLE `schd_subscriber` (
  `subscriber_id` int(11) NOT NULL AUTO_INCREMENT,
  `calender_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`subscriber_id`),
  KEY `FK_schd_subscriber` (`calender_id`),
  CONSTRAINT `FK_schd_subscriber` FOREIGN KEY (`calender_id`) REFERENCES `schd_r_calender` (`calender_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `schd_subscriber` */

/*Table structure for table `srvy_kuesioner` */

DROP TABLE IF EXISTS `srvy_kuesioner`;

CREATE TABLE `srvy_kuesioner` (
  `kuesioner_id` int(11) NOT NULL AUTO_INCREMENT,
  `sem` int(11) NOT NULL DEFAULT '0',
  `ta` int(11) NOT NULL DEFAULT '0',
  `kode_mk` varchar(11) DEFAULT NULL,
  `nama` varchar(255) NOT NULL DEFAULT '',
  `keterangan` text,
  `instruksi_pengisian` text,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `peserta_kuis` text,
  `is_login` char(1) NOT NULL DEFAULT '1',
  `nilai` float DEFAULT NULL,
  `wajib` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `id_kuesioner` int(11) DEFAULT NULL,
  PRIMARY KEY (`kuesioner_id`),
  KEY `ID_KUESIONER` (`kuesioner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `srvy_kuesioner` */

/*Table structure for table `srvy_kuesioner_jawaban_peserta` */

DROP TABLE IF EXISTS `srvy_kuesioner_jawaban_peserta`;

CREATE TABLE `srvy_kuesioner_jawaban_peserta` (
  `kuesioner_jawaban_peserta_id` int(11) NOT NULL AUTO_INCREMENT,
  `kuesioner_id` int(11) DEFAULT NULL,
  `kuesioner_pertanyaan_id` int(11) DEFAULT NULL,
  `jawaban` text,
  `peserta` varchar(30) DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `id_kuesioner` int(11) DEFAULT '0',
  `id_pertanyaan` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`kuesioner_jawaban_peserta_id`),
  KEY `FK_srvy_kuesioner_jawaban_peserta` (`kuesioner_pertanyaan_id`),
  KEY `FK_srvy_kuesioner_jawaban_peserta_kuesioner` (`kuesioner_id`),
  KEY `FK_srvy_kuesioner_jawaban_peserta_user` (`user_id`),
  CONSTRAINT `FK_srvy_kuesioner_jawaban_peserta` FOREIGN KEY (`kuesioner_pertanyaan_id`) REFERENCES `srvy_kuesioner_pertanyaan` (`kuesioner_pertanyaan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_srvy_kuesioner_jawaban_peserta_kuesioner` FOREIGN KEY (`kuesioner_id`) REFERENCES `srvy_kuesioner` (`kuesioner_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_srvy_kuesioner_jawaban_peserta_user` FOREIGN KEY (`user_id`) REFERENCES `sysx_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `srvy_kuesioner_jawaban_peserta` */

/*Table structure for table `srvy_kuesioner_opsi` */

DROP TABLE IF EXISTS `srvy_kuesioner_opsi`;

CREATE TABLE `srvy_kuesioner_opsi` (
  `kuesioner_opsi_id` int(11) NOT NULL AUTO_INCREMENT,
  `no_opsi` int(11) NOT NULL,
  `kuesioner_pertanyaan_id` int(11) DEFAULT NULL,
  `ket_opsi` text NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `id_pertanyaan` varchar(20) DEFAULT '0',
  PRIMARY KEY (`kuesioner_opsi_id`),
  KEY `NO_OPSI` (`kuesioner_opsi_id`),
  KEY `FK_srvy_kuesioner_opsi_pertanyaan` (`kuesioner_pertanyaan_id`),
  CONSTRAINT `FK_srvy_kuesioner_opsi_pertanyaan` FOREIGN KEY (`kuesioner_pertanyaan_id`) REFERENCES `srvy_kuesioner_pertanyaan` (`kuesioner_pertanyaan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `srvy_kuesioner_opsi` */

/*Table structure for table `srvy_kuesioner_pertanyaan` */

DROP TABLE IF EXISTS `srvy_kuesioner_pertanyaan`;

CREATE TABLE `srvy_kuesioner_pertanyaan` (
  `kuesioner_pertanyaan_id` int(11) NOT NULL AUTO_INCREMENT,
  `kuesioner_id` int(11) DEFAULT NULL,
  `nomor` int(11) DEFAULT NULL,
  `pertanyaan` text,
  `tipe_opsi` enum('C','R','T') NOT NULL DEFAULT 'R',
  `kategori` varchar(200) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `id_kuesioner` int(11) DEFAULT '0',
  `id_pertanyaan` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`kuesioner_pertanyaan_id`),
  KEY `ID_KUESIONER` (`id_kuesioner`),
  KEY `ID_PERTANYAAN` (`id_pertanyaan`),
  KEY `fk_t_kuesioner_pertanyaan_t_kuesioner1_idx` (`kuesioner_id`),
  CONSTRAINT `fk_t_kuesioner_pertanyaan_t_kuesioner1` FOREIGN KEY (`kuesioner_id`) REFERENCES `srvy_kuesioner` (`kuesioner_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `srvy_kuesioner_pertanyaan` */

/*Table structure for table `srvy_polling` */

DROP TABLE IF EXISTS `srvy_polling`;

CREATE TABLE `srvy_polling` (
  `polling_id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` varchar(20) DEFAULT NULL,
  `kategori` varchar(50) DEFAULT 'All',
  `judul` varchar(255) DEFAULT NULL,
  `pertanyaan` text,
  `ket` text,
  `tgl_exp` datetime DEFAULT NULL,
  `tgl_view` datetime DEFAULT NULL,
  `wajib` tinyint(1) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`polling_id`),
  UNIQUE KEY `POLL_ID_UNIQUE` (`poll_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `srvy_polling` */

/*Table structure for table `srvy_pollopsi` */

DROP TABLE IF EXISTS `srvy_pollopsi`;

CREATE TABLE `srvy_pollopsi` (
  `pollopsi_id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` varchar(20) DEFAULT NULL,
  `no_opsi` smallint(6) DEFAULT '0',
  `polling_id` int(11) DEFAULT NULL,
  `opsi` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`pollopsi_id`),
  KEY `fk_t_POLLOPSI_t_POLLING1_idx` (`polling_id`),
  CONSTRAINT `fk_t_POLLOPSI_t_POLLING1` FOREIGN KEY (`polling_id`) REFERENCES `srvy_polling` (`polling_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `srvy_pollopsi` */

/*Table structure for table `srvy_pollvote` */

DROP TABLE IF EXISTS `srvy_pollvote`;

CREATE TABLE `srvy_pollvote` (
  `pollvote_id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` varchar(20) DEFAULT NULL,
  `polling_id` int(11) DEFAULT NULL,
  `no_opsi` smallint(6) DEFAULT '0',
  `pollopsi_id` int(11) DEFAULT NULL,
  `vote_by` int(11) DEFAULT NULL,
  `vote_by_old` varchar(50) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`pollvote_id`),
  KEY `FK_t_pollvote` (`pollopsi_id`),
  KEY `FK_srvy_pollvote_polling_id` (`polling_id`),
  CONSTRAINT `FK_srvy_pollvote_polling_id` FOREIGN KEY (`polling_id`) REFERENCES `srvy_polling` (`polling_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_t_pollvote` FOREIGN KEY (`pollopsi_id`) REFERENCES `srvy_pollopsi` (`pollopsi_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `srvy_pollvote` */

/*Table structure for table `srvy_workgroup_kuesioner` */

DROP TABLE IF EXISTS `srvy_workgroup_kuesioner`;

CREATE TABLE `srvy_workgroup_kuesioner` (
  `workgroup_id` int(11) DEFAULT NULL,
  `kuesioner_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  KEY `FK_srvy_workgroup_kuesioner_wg` (`workgroup_id`),
  KEY `FK_srvy_workgroup_kuesioner_kuesioner` (`kuesioner_id`),
  CONSTRAINT `FK_srvy_workgroup_kuesioner_kuesioner` FOREIGN KEY (`kuesioner_id`) REFERENCES `srvy_kuesioner` (`kuesioner_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_srvy_workgroup_kuesioner_wg` FOREIGN KEY (`workgroup_id`) REFERENCES `sysx_workgroup` (`workgroup_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `srvy_workgroup_kuesioner` */

/*Table structure for table `srvy_workgroup_polling` */

DROP TABLE IF EXISTS `srvy_workgroup_polling`;

CREATE TABLE `srvy_workgroup_polling` (
  `workgroup_id` int(11) DEFAULT NULL,
  `polling_id` int(11) DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  KEY `FK_srvy_workgroup_polling_wk` (`workgroup_id`),
  KEY `FK_srvy_workgroup_polling_poll` (`polling_id`),
  CONSTRAINT `FK_srvy_workgroup_polling_poll` FOREIGN KEY (`polling_id`) REFERENCES `srvy_polling` (`polling_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_srvy_workgroup_polling_wk` FOREIGN KEY (`workgroup_id`) REFERENCES `sysx_workgroup` (`workgroup_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `srvy_workgroup_polling` */

/*Table structure for table `sysx_action` */

DROP TABLE IF EXISTS `sysx_action`;

CREATE TABLE `sysx_action` (
  `action_id` int(11) NOT NULL AUTO_INCREMENT,
  `controller_id` int(11) NOT NULL,
  `identifier` varchar(32) NOT NULL COMMENT 'Action Unique ID',
  `desc` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL COMMENT '	',
  `updated_by` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`action_id`),
  KEY `fk_action_controller1_idx` (`controller_id`),
  CONSTRAINT `fk_action_controller1` FOREIGN KEY (`controller_id`) REFERENCES `sysx_controller` (`controller_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sysx_action` */

/*Table structure for table `sysx_application` */

DROP TABLE IF EXISTS `sysx_application`;

CREATE TABLE `sysx_application` (
  `application_id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(32) NOT NULL COMMENT 'Application Unique ID',
  `desc` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL COMMENT '	',
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL COMMENT '	',
  `updated_by` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`application_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `sysx_application` */

insert  into `sysx_application`(`application_id`,`identifier`,`desc`,`created_at`,`updated_at`,`created_by`,`updated_by`,`deleted`,`deleted_at`,`deleted_by`) values (1,'cis_lite',NULL,NULL,NULL,NULL,NULL,0,NULL,NULL);

/*Table structure for table `sysx_authentication_method` */

DROP TABLE IF EXISTS `sysx_authentication_method`;

CREATE TABLE `sysx_authentication_method` (
  `authentication_method_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `server_address` varchar(45) DEFAULT NULL,
  `authentication_string` varchar(255) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `redirected` tinyint(1) DEFAULT '0',
  `redirect_to` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`authentication_method_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `sysx_authentication_method` */

insert  into `sysx_authentication_method`(`authentication_method_id`,`name`,`server_address`,`authentication_string`,`desc`,`redirected`,`redirect_to`,`created_at`,`updated_at`,`created_by`,`updated_by`,`deleted`,`deleted_at`,`deleted_by`) values (1,'Local Database',NULL,'DATABASE',NULL,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL);

/*Table structure for table `sysx_config` */

DROP TABLE IF EXISTS `sysx_config`;

CREATE TABLE `sysx_config` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `key` varchar(100) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL COMMENT '	',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`config_id`),
  KEY `fk_config_application1_idx` (`application_id`),
  CONSTRAINT `fk_config_application1` FOREIGN KEY (`application_id`) REFERENCES `sysx_application` (`application_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `sysx_config` */

insert  into `sysx_config`(`config_id`,`application_id`,`key`,`value`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (2,1,'tahun_ajaran','2017',NULL,NULL,NULL,NULL);

/*Table structure for table `sysx_controller` */

DROP TABLE IF EXISTS `sysx_controller`;

CREATE TABLE `sysx_controller` (
  `controller_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `identifier` varchar(32) NOT NULL COMMENT 'Controller Unique ID',
  `desc` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL COMMENT '	',
  `updated_by` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`controller_id`),
  KEY `fk_controller_module1_idx` (`module_id`),
  CONSTRAINT `fk_controller_module1` FOREIGN KEY (`module_id`) REFERENCES `sysx_module` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sysx_controller` */

/*Table structure for table `sysx_job_allocation` */

DROP TABLE IF EXISTS `sysx_job_allocation`;

CREATE TABLE `sysx_job_allocation` (
  `job_allocation_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_definition_id` int(11) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`job_allocation_id`),
  KEY `fk_job_allocation_job_definition1_idx` (`job_definition_id`),
  CONSTRAINT `fk_job_allocation_job_definition1` FOREIGN KEY (`job_definition_id`) REFERENCES `sysx_job_definition` (`job_definition_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sysx_job_allocation` */

/*Table structure for table `sysx_job_definition` */

DROP TABLE IF EXISTS `sysx_job_definition`;

CREATE TABLE `sysx_job_definition` (
  `job_definition_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`job_definition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sysx_job_definition` */

/*Table structure for table `sysx_log` */

DROP TABLE IF EXISTS `sysx_log`;

CREATE TABLE `sysx_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `message` varchar(500) DEFAULT NULL,
  `host` varchar(45) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  KEY `fk_Log_user1_idx` (`user_id`),
  CONSTRAINT `fk_Log_user1` FOREIGN KEY (`user_id`) REFERENCES `sysx_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sysx_log` */

/*Table structure for table `sysx_menu_group` */

DROP TABLE IF EXISTS `sysx_menu_group`;

CREATE TABLE `sysx_menu_group` (
  `menu_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`menu_group_id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sysx_menu_group` */

/*Table structure for table `sysx_menu_item` */

DROP TABLE IF EXISTS `sysx_menu_item`;

CREATE TABLE `sysx_menu_item` (
  `menu_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_group_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `alt` varchar(100) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `is_ajax` tinyint(1) NOT NULL DEFAULT '0',
  `container_id` varchar(45) DEFAULT NULL,
  `disabled` tinyint(1) DEFAULT '0',
  `order_number` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`menu_item_id`),
  KEY `fk_menu_item_menu_group1_idx` (`menu_group_id`),
  CONSTRAINT `fk_menu_item_menu_group1` FOREIGN KEY (`menu_group_id`) REFERENCES `sysx_menu_group` (`menu_group_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sysx_menu_item` */

/*Table structure for table `sysx_module` */

DROP TABLE IF EXISTS `sysx_module`;

CREATE TABLE `sysx_module` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `identifier` varchar(32) NOT NULL COMMENT 'Module Unique ID',
  `desc` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL COMMENT '	',
  `updated_by` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`module_id`),
  KEY `fk_module_application1_idx` (`application_id`),
  CONSTRAINT `fk_module_application1` FOREIGN KEY (`application_id`) REFERENCES `sysx_application` (`application_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sysx_module` */

/*Table structure for table `sysx_permission` */

DROP TABLE IF EXISTS `sysx_permission`;

CREATE TABLE `sysx_permission` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `action_id` int(11) NOT NULL,
  `identifier` varchar(32) NOT NULL COMMENT 'Permission Unique ID',
  `desc` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL COMMENT '	',
  `updated_by` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`permission_id`),
  KEY `fk_permission_action1_idx` (`action_id`),
  CONSTRAINT `fk_permission_action1` FOREIGN KEY (`action_id`) REFERENCES `sysx_action` (`action_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sysx_permission` */

/*Table structure for table `sysx_profile` */

DROP TABLE IF EXISTS `sysx_profile`;

CREATE TABLE `sysx_profile` (
  `profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `avatar_url` varchar(255) DEFAULT NULL,
  `email_2` varchar(100) DEFAULT NULL,
  `mobile_phone_1` varchar(45) DEFAULT NULL,
  `mobile_phone_2` varchar(45) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(100) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL COMMENT '	',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sysx_profile` */

/*Table structure for table `sysx_role` */

DROP TABLE IF EXISTS `sysx_role`;

CREATE TABLE `sysx_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL COMMENT '	',
  `updated_by` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sysx_role` */

/*Table structure for table `sysx_role_has_action` */

DROP TABLE IF EXISTS `sysx_role_has_action`;

CREATE TABLE `sysx_role_has_action` (
  `role_id` int(11) NOT NULL,
  `action_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL COMMENT '	',
  `updated_by` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`role_id`,`action_id`),
  KEY `fk_role_has_action_action1_idx` (`action_id`),
  KEY `fk_role_has_action_role1_idx` (`role_id`),
  CONSTRAINT `fk_role_has_action_action1` FOREIGN KEY (`action_id`) REFERENCES `sysx_action` (`action_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_role_has_action_role1` FOREIGN KEY (`role_id`) REFERENCES `sysx_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sysx_role_has_action` */

/*Table structure for table `sysx_role_has_application` */

DROP TABLE IF EXISTS `sysx_role_has_application`;

CREATE TABLE `sysx_role_has_application` (
  `role_id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL COMMENT '	',
  `updated_by` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`role_id`,`application_id`),
  KEY `fk_role_has_application_application1_idx` (`application_id`),
  KEY `fk_role_has_application_role1_idx` (`role_id`),
  CONSTRAINT `fk_role_has_application_application1` FOREIGN KEY (`application_id`) REFERENCES `sysx_application` (`application_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_role_has_application_role1` FOREIGN KEY (`role_id`) REFERENCES `sysx_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sysx_role_has_application` */

/*Table structure for table `sysx_role_has_controller` */

DROP TABLE IF EXISTS `sysx_role_has_controller`;

CREATE TABLE `sysx_role_has_controller` (
  `role_id` int(11) NOT NULL,
  `controller_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL COMMENT '	',
  `updated_by` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`role_id`,`controller_id`),
  KEY `fk_role_has_controller_controller1_idx` (`controller_id`),
  KEY `fk_role_has_controller_role1_idx` (`role_id`),
  CONSTRAINT `fk_role_has_controller_controller1` FOREIGN KEY (`controller_id`) REFERENCES `sysx_controller` (`controller_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_role_has_controller_role1` FOREIGN KEY (`role_id`) REFERENCES `sysx_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sysx_role_has_controller` */

/*Table structure for table `sysx_role_has_menu_item` */

DROP TABLE IF EXISTS `sysx_role_has_menu_item`;

CREATE TABLE `sysx_role_has_menu_item` (
  `menu_item_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL COMMENT '	',
  `updated_by` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`menu_item_id`,`role_id`),
  KEY `fk_menu_item_has_role_role1_idx` (`role_id`),
  KEY `fk_menu_item_has_role_menu_item1_idx` (`menu_item_id`),
  CONSTRAINT `fk_menu_item_has_role_menu_item1` FOREIGN KEY (`menu_item_id`) REFERENCES `sysx_menu_item` (`menu_item_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_menu_item_has_role_role1` FOREIGN KEY (`role_id`) REFERENCES `sysx_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sysx_role_has_menu_item` */

/*Table structure for table `sysx_role_has_module` */

DROP TABLE IF EXISTS `sysx_role_has_module`;

CREATE TABLE `sysx_role_has_module` (
  `role_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL COMMENT '	',
  `updated_by` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`role_id`,`module_id`),
  KEY `fk_role_has_module_module1_idx` (`module_id`),
  KEY `fk_role_has_module_role1_idx` (`role_id`),
  CONSTRAINT `fk_role_has_module_module1` FOREIGN KEY (`module_id`) REFERENCES `sysx_module` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_role_has_module_role1` FOREIGN KEY (`role_id`) REFERENCES `sysx_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sysx_role_has_module` */

/*Table structure for table `sysx_role_has_permission` */

DROP TABLE IF EXISTS `sysx_role_has_permission`;

CREATE TABLE `sysx_role_has_permission` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL COMMENT '	',
  `updated_by` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`role_id`,`permission_id`),
  KEY `fk_role_has_permission_permission2_idx` (`permission_id`),
  KEY `fk_role_has_permission_role2_idx` (`role_id`),
  CONSTRAINT `fk_role_has_permission_permission2` FOREIGN KEY (`permission_id`) REFERENCES `sysx_permission` (`permission_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_role_has_permission_role2` FOREIGN KEY (`role_id`) REFERENCES `sysx_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sysx_role_has_permission` */

/*Table structure for table `sysx_role_has_task` */

DROP TABLE IF EXISTS `sysx_role_has_task`;

CREATE TABLE `sysx_role_has_task` (
  `role_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL COMMENT '	',
  `updated_by` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`role_id`,`task_id`),
  KEY `fk_role_has_permission_permission1_idx` (`task_id`),
  KEY `fk_role_has_permission_role1_idx` (`role_id`),
  CONSTRAINT `fk_role_has_permission_permission1` FOREIGN KEY (`task_id`) REFERENCES `sysx_task` (`task_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_role_has_permission_role1` FOREIGN KEY (`role_id`) REFERENCES `sysx_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sysx_role_has_task` */

/*Table structure for table `sysx_task` */

DROP TABLE IF EXISTS `sysx_task`;

CREATE TABLE `sysx_task` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `desc` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL COMMENT '	',
  `updated_by` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`task_id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sysx_task` */

/*Table structure for table `sysx_telkom_sso_user` */

DROP TABLE IF EXISTS `sysx_telkom_sso_user`;

CREATE TABLE `sysx_telkom_sso_user` (
  `telkom_sso_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `forward_auth` tinyint(1) DEFAULT '0' COMMENT 'Forward authentication to local authentication system, or terminate here with provided password',
  `active` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`telkom_sso_user_id`),
  KEY `FK_sysx_telkom_sso_user` (`user_id`),
  CONSTRAINT `FK_sysx_telkom_sso_user` FOREIGN KEY (`user_id`) REFERENCES `sysx_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sysx_telkom_sso_user` */

/*Table structure for table `sysx_telkom_sso_user_log` */

DROP TABLE IF EXISTS `sysx_telkom_sso_user_log`;

CREATE TABLE `sysx_telkom_sso_user_log` (
  `telkom_sso_user_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `telkom_sso_user_id` int(11) NOT NULL,
  `action` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`telkom_sso_user_log_id`),
  KEY `FK_sysx_telkom_sso_user_log` (`telkom_sso_user_id`),
  CONSTRAINT `FK_sysx_telkom_sso_user_log` FOREIGN KEY (`telkom_sso_user_id`) REFERENCES `sysx_telkom_sso_user` (`telkom_sso_user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sysx_telkom_sso_user_log` */

/*Table structure for table `sysx_user` */

DROP TABLE IF EXISTS `sysx_user`;

CREATE TABLE `sysx_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) DEFAULT NULL,
  `sysx_key` varchar(32) DEFAULT NULL,
  `authentication_method_id` int(11) NOT NULL DEFAULT '1',
  `username` varchar(255) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL COMMENT '	',
  `updated_by` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `fk_user_profile1_idx` (`profile_id`),
  KEY `fk_user_authentication_method1_idx` (`authentication_method_id`),
  CONSTRAINT `fk_user_authentication_method1` FOREIGN KEY (`authentication_method_id`) REFERENCES `sysx_authentication_method` (`authentication_method_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_profile1` FOREIGN KEY (`profile_id`) REFERENCES `sysx_profile` (`profile_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3109 DEFAULT CHARSET=utf8;

/*Data for the table `sysx_user` */

insert  into `sysx_user`(`user_id`,`profile_id`,`sysx_key`,`authentication_method_id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`created_at`,`updated_at`,`created_by`,`updated_by`,`deleted`,`deleted_at`,`deleted_by`) values (3097,NULL,'EWpd9wHy1YZpmGTSsP3_QFW5loktTIPU',1,'root','fxiViURunAzSxsCjfgbDseq4mFzcHX-L','$2y$13$HW588THKyGY4JHlNIPbjsuLUQjrLfKB0uWuDA7/eOAdgef/NUeYHa',NULL,'root@local.host',1,NULL,NULL,NULL,NULL,0,NULL,NULL),(3098,NULL,'GHFBAe_kjL5dsfxSg5OCKf9R1AOd6HAB',1,'iss16022','GgRfrnyQvYCZce_nPy4zMIQMrOBzRY8C','$2y$13$XKrnhn4MLQqjd4JaZJYYheJT/9mzvbzDddGmW8S/ccmG4hD1z7/U.',NULL,'if316021@students.del.ac.id',1,'2018-04-25 08:58:54','2018-04-25 08:58:54','root','root',0,NULL,NULL),(3099,NULL,'4f9x3vcxdF1DA4MOnkFyYqnD7Qrr9q4j',1,'baak','-JUGFZHbIeTzcqtDyBQvyR6EHDCS2Ubs','$2y$13$r/MF19nYj6AZvUoTIVTaQ.j1tRWoU3uycAup9idYwZvsEjn3Ogl.q',NULL,'baak@del.ac.id',1,'2018-05-22 14:46:56','2018-05-22 14:46:56','if316021','if316021',0,NULL,NULL),(3100,NULL,'qXQEYXpL5_4RPNQtoDH5NTrcr0EloPLx',1,'keasramaan','soWpaW0NHeKz5His4frvMwZEH85qp6gS','$2y$13$jnZ7f5MXJSwqagWfZ1N/bOeo7kW3qJxNX/MYjmQQZ5KKQ43hOH2gS',NULL,'keasramaan@del.ac.id',1,'2018-05-22 14:47:21','2018-05-22 14:47:21','if316021','if316021',0,NULL,NULL),(3101,NULL,'qv2Ui_y2av6wPumMneXlSGCk5ljhRUIr',1,'dosen','pi9ckMvRdohWE3HVvRgVYxs1Y7Sr5DDF','$2y$13$DMKK4FKNUKgy8Pr4jYZHVeQsSegnT82imhfOW0K9lbZbiBN8F7IH2',NULL,'dosen@del.ac.id',1,'2018-05-22 14:48:28','2018-05-22 14:48:28','if316021','if316021',0,NULL,NULL),(3102,NULL,'B5rCRsITfsTgRoIeAxKbvl9HqrptUCwP',1,'kemahasiswaan','vo3YvrKEi62SZTI9DzYbRFR7jHRSw2bk','$2y$13$Lu64C7N2EB6LVQj3X/ayh.M9N8/IUfRw/Ph7Gxm4bBy5HVCIawr8y',NULL,'kemahasiswaan@del.ac.id',1,'2018-05-22 14:48:54','2018-05-22 14:48:54','if316021','if316021',0,NULL,NULL),(3103,NULL,'WaU6VYkkI-iduH43vfekF0ugfnCX3vNS',1,'11S15027','gM_fhTDB4tq4SY9BKl9xz8oZ3CRtQ7Av','$2y$13$zB6a7VZIZAC/FAFv4sN1TunFxSoIQhRDVrwhF8i9oKWDEw6M3Nv.2',NULL,'11S15027@students.del.ac.id',1,'2018-07-23 08:43:40','2018-07-23 08:43:40','keasramaan','keasramaan',0,NULL,NULL),(3104,NULL,'hUSRoooWwJYHtU3RKiY39sFJhuf-I19s',1,'rektor','L1t6ELP-V00Tx-aIrYqf-ElEc5KJW0-L','$2y$13$ylJxfoqAErX2wBe0fYnuL.nGyaO0TnHR1qK6gNGm6SWEhewgk21B6',NULL,'rektor@del.ac.id',1,'2018-08-24 14:44:50','2018-08-24 14:44:50','dosen','dosen',0,NULL,NULL),(3105,NULL,'03EncoYOW7tVtkasSgd8hgJ0QrxkCYGI',1,'wakilrektor2','rJxk7NxtuIhPhMUAw_xX_lRx8VHOhtEn','$2y$13$y6mTzXfJvMYYchQ53mdp5eEQKp1WI.Ib7ZdO4ZV8s9CEaxNipaX7W',NULL,'wakilrektor2@del.ac.id',1,'2018-08-25 23:50:32','2018-08-25 23:50:32','baak','baak',0,NULL,NULL),(3106,NULL,NULL,1,'iss16022','','iss16022','iss16022','iss16022@students.del.ac.id',10,NULL,NULL,NULL,NULL,0,NULL,NULL),(3107,NULL,'1CK-Jp1aat3oJNnvtxiLc_zZJNnO0fxM',1,'andre','cnUh1o6exJfi-e8E2LjBKFQqFJHHtIsC','$2y$13$ShbPLNGEP6ou6fKuAWsJCObe2gYDn3vQsEsZYTxjWfMEVwJYl7NNK',NULL,'andre@gmail.com',1,'2019-05-27 17:50:11','2019-05-27 17:50:11','iss16022','iss16022',0,NULL,NULL),(3108,NULL,'1sfD6Lu5W8vO80piwlqaPiJ4eAJG6JCI',1,'itok','Hv3kKrKJSSwB_RFyOM1BqSPptGCwCDC5','$2y$13$6NKBE9NWyMDPyuSHQdfcz.pQuiOXIOQWr2OwrApEbo5iv7B.QPG8y',NULL,'itok@gmail.com',1,'2019-05-27 18:38:37','2019-05-27 18:38:37','andre','andre',0,NULL,NULL);

/*Table structure for table `sysx_user_config` */

DROP TABLE IF EXISTS `sysx_user_config`;

CREATE TABLE `sysx_user_config` (
  `user_config_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `key` varchar(100) NOT NULL,
  `value` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL COMMENT '    ',
  `updated_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_config_id`),
  KEY `fk_sysx_user_config_sysx_application1_idx` (`application_id`),
  KEY `fk_sysx_user_config_sysx_user1_idx` (`user_id`),
  CONSTRAINT `fk_sysx_user_config_sysx_application1` FOREIGN KEY (`application_id`) REFERENCES `sysx_application` (`application_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sysx_user_config_sysx_user1` FOREIGN KEY (`user_id`) REFERENCES `sysx_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sysx_user_config` */

/*Table structure for table `sysx_user_has_role` */

DROP TABLE IF EXISTS `sysx_user_has_role`;

CREATE TABLE `sysx_user_has_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL COMMENT '	',
  `updated_by` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `fk_user_has_role_role1_idx` (`role_id`),
  KEY `fk_user_has_role_user_idx` (`user_id`),
  CONSTRAINT `fk_user_has_role_role1` FOREIGN KEY (`role_id`) REFERENCES `sysx_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_has_role_user` FOREIGN KEY (`user_id`) REFERENCES `sysx_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sysx_user_has_role` */

/*Table structure for table `sysx_user_has_workgroup` */

DROP TABLE IF EXISTS `sysx_user_has_workgroup`;

CREATE TABLE `sysx_user_has_workgroup` (
  `user_id` int(11) NOT NULL,
  `workgroup_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`user_id`,`workgroup_id`),
  KEY `fk_user_has_workgroup_workgroup1_idx` (`workgroup_id`),
  CONSTRAINT `fk_user_has_workgroup_user1` FOREIGN KEY (`user_id`) REFERENCES `sysx_user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_workgroup_workgroup1` FOREIGN KEY (`workgroup_id`) REFERENCES `sysx_workgroup` (`workgroup_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sysx_user_has_workgroup` */

/*Table structure for table `sysx_workgroup` */

DROP TABLE IF EXISTS `sysx_workgroup`;

CREATE TABLE `sysx_workgroup` (
  `workgroup_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `protected` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`workgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sysx_workgroup` */

/*Table structure for table `tmbh_agenda` */

DROP TABLE IF EXISTS `tmbh_agenda`;

CREATE TABLE `tmbh_agenda` (
  `agenda_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_agenda` varchar(20) NOT NULL DEFAULT '',
  `judul` varchar(255) DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  `isi` longtext,
  `status` char(1) DEFAULT '1',
  `tgl_start` datetime NOT NULL,
  `tgl_end` datetime NOT NULL,
  `waktu_notifikasi` int(11) DEFAULT NULL,
  `notifikasi_by_email` char(1) NOT NULL DEFAULT '0',
  `notifikasi_by_popup` char(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`agenda_id`),
  UNIQUE KEY `AGENDA_ID_UNIQUE` (`id_agenda`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tmbh_agenda` */

/*Table structure for table `tmbh_file_pengumuman` */

DROP TABLE IF EXISTS `tmbh_file_pengumuman`;

CREATE TABLE `tmbh_file_pengumuman` (
  `file_pengumuman_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_file` text NOT NULL,
  `kode_file` varchar(50) DEFAULT NULL,
  `ket` text NOT NULL,
  `pengumuman_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` varchar(32) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`file_pengumuman_id`),
  KEY `FK_tmbh_file_pengumuman` (`pengumuman_id`),
  CONSTRAINT `FK_tmbh_file_pengumuman` FOREIGN KEY (`pengumuman_id`) REFERENCES `tmbh_pengumuman` (`pengumuman_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tmbh_file_pengumuman` */

/*Table structure for table `tmbh_kamus_it` */

DROP TABLE IF EXISTS `tmbh_kamus_it`;

CREATE TABLE `tmbh_kamus_it` (
  `kamus_it_id` int(11) NOT NULL AUTO_INCREMENT,
  `word` varchar(255) NOT NULL DEFAULT '',
  `keterangan` text,
  `kategori` varchar(30) DEFAULT NULL,
  `status` char(1) DEFAULT '1',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`kamus_it_id`),
  UNIQUE KEY `WORD_UNIQUE` (`word`),
  KEY `WORD` (`word`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tmbh_kamus_it` */

/*Table structure for table `tmbh_kegiatan` */

DROP TABLE IF EXISTS `tmbh_kegiatan`;

CREATE TABLE `tmbh_kegiatan` (
  `kegiatan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kegiatan` varchar(255) NOT NULL DEFAULT '',
  `penyelengara` varchar(100) DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_akhir` date DEFAULT NULL,
  `keterangan` text,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`kegiatan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tmbh_kegiatan` */

/*Table structure for table `tmbh_news` */

DROP TABLE IF EXISTS `tmbh_news`;

CREATE TABLE `tmbh_news` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_kategori_id` int(11) DEFAULT NULL,
  `id_news` varchar(20) NOT NULL DEFAULT '',
  `judul` varchar(255) DEFAULT NULL,
  `kat_id` varchar(30) DEFAULT NULL,
  `ket_gambar` varchar(255) DEFAULT NULL,
  `pre` text,
  `isi` longtext,
  `sumber` varchar(50) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `status` char(1) DEFAULT '1',
  `listing` char(1) DEFAULT '1',
  `komentar` char(1) DEFAULT '0',
  `language` char(3) NOT NULL DEFAULT 'INA',
  `tgl_start` datetime NOT NULL,
  `tgl_end` datetime NOT NULL,
  `last_post` bigint(20) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`news_id`),
  KEY `JUDUL` (`judul`),
  KEY `fk_t_NEWS_t_NEWS_KATEGORI1_idx` (`news_kategori_id`),
  CONSTRAINT `fk_t_NEWS_t_NEWS_KATEGORI1` FOREIGN KEY (`news_kategori_id`) REFERENCES `tmbh_news_kategori` (`news_kategori_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tmbh_news` */

/*Table structure for table `tmbh_news_files` */

DROP TABLE IF EXISTS `tmbh_news_files`;

CREATE TABLE `tmbh_news_files` (
  `news_files_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_news` varchar(20) NOT NULL DEFAULT '0',
  `nama_file` varchar(255) NOT NULL DEFAULT '',
  `ket` varchar(255) DEFAULT NULL,
  `tipe` varchar(50) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`news_files_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tmbh_news_files` */

/*Table structure for table `tmbh_news_kategori` */

DROP TABLE IF EXISTS `tmbh_news_kategori`;

CREATE TABLE `tmbh_news_kategori` (
  `news_kategori_id` int(11) NOT NULL AUTO_INCREMENT,
  `kat_id` varchar(30) NOT NULL,
  `pkat_id` varchar(30) DEFAULT NULL,
  `kat_nama_ina` varchar(255) NOT NULL DEFAULT '',
  `kat_nama_eng` varchar(255) DEFAULT NULL,
  `kat_ket` varchar(255) DEFAULT NULL,
  `kat_icon` blob,
  `no_urut` smallint(6) NOT NULL DEFAULT '0',
  `kat_list` char(1) NOT NULL DEFAULT '1',
  `tipe` varchar(30) NOT NULL DEFAULT 'konten',
  `menu` varchar(255) DEFAULT NULL,
  `params` varchar(255) DEFAULT NULL,
  `put_as_menu` varchar(30) NOT NULL DEFAULT '',
  `views` char(1) NOT NULL DEFAULT '1',
  `r_akses` varchar(255) DEFAULT NULL,
  `w_akses` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`news_kategori_id`),
  UNIQUE KEY `KAT_ID_UNIQUE` (`kat_id`),
  KEY `KAT_ID` (`kat_id`),
  KEY `PKAT_ID` (`pkat_id`),
  KEY `KAT_NAMA_INA` (`kat_nama_ina`),
  KEY `KAT_NAMA_ENG` (`kat_nama_eng`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tmbh_news_kategori` */

/*Table structure for table `tmbh_news_komentar` */

DROP TABLE IF EXISTS `tmbh_news_komentar`;

CREATE TABLE `tmbh_news_komentar` (
  `news_komentar_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_komentar` varchar(10) NOT NULL DEFAULT '',
  `id_news` varchar(20) NOT NULL DEFAULT '',
  `id_parent` varchar(10) DEFAULT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `isi` longtext,
  `pengirim` varchar(50) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`news_komentar_id`),
  KEY `ID_KOMENTAR` (`id_komentar`),
  KEY `JUDUL` (`judul`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tmbh_news_komentar` */

/*Table structure for table `tmbh_pengumuman` */

DROP TABLE IF EXISTS `tmbh_pengumuman`;

CREATE TABLE `tmbh_pengumuman` (
  `pengumuman_id` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(10) NOT NULL DEFAULT '',
  `kategori` varchar(50) NOT NULL DEFAULT '',
  `judul` varchar(255) DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `isi` longtext,
  `tgl_exp` date DEFAULT NULL,
  `post_web` char(1) NOT NULL DEFAULT '1',
  `post_dinding` char(1) NOT NULL DEFAULT '0',
  `post_mail` char(1) NOT NULL DEFAULT '0',
  `done_tempel` char(1) NOT NULL DEFAULT '0',
  `done_cabut` char(1) NOT NULL DEFAULT '0',
  `isSticky` char(1) DEFAULT '0',
  `owner` int(11) DEFAULT NULL,
  `user_old` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`pengumuman_id`),
  KEY `FK_tmbh_pengumuman` (`owner`),
  CONSTRAINT `FK_tmbh_pengumuman` FOREIGN KEY (`owner`) REFERENCES `sysx_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tmbh_pengumuman` */

/*Table structure for table `tmbh_software_tools` */

DROP TABLE IF EXISTS `tmbh_software_tools`;

CREATE TABLE `tmbh_software_tools` (
  `software_tools_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kur` int(4) NOT NULL DEFAULT '0',
  `kode_mk` varchar(8) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `kurikulum_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`software_tools_id`),
  KEY `fk_t_software_tools_t_kurikulum1_idx` (`kurikulum_id`),
  CONSTRAINT `fk_t_software_tools_t_kurikulum1` FOREIGN KEY (`kurikulum_id`) REFERENCES `krkm_kuliah` (`kuliah_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tmbh_software_tools` */

/*Table structure for table `transaksi` */

DROP TABLE IF EXISTS `transaksi`;

CREATE TABLE `transaksi` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `userid` int(20) NOT NULL,
  `tanggaltrans` date NOT NULL,
  `id_produk` int(20) NOT NULL,
  `Nama Produk` varchar(255) NOT NULL,
  `Harga` float NOT NULL,
  `Jumlah` int(10) NOT NULL,
  `Total` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `transaksi` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `userid` int(20) NOT NULL AUTO_INCREMENT,
  `password` int(20) NOT NULL,
  `hakAkses` varchar(20) NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=11415032 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`userid`,`password`,`hakAkses`) values (11415031,11415031,'Kasir');

/* Procedure structure for procedure `create_syllabus_by_komposisi_nilai` */

/*!50003 DROP PROCEDURE IF EXISTS  `create_syllabus_by_komposisi_nilai` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`cis_db_admin`@`%` PROCEDURE `create_syllabus_by_komposisi_nilai`()
BEGIN
DECLARE ta_syllabus VARCHAR(10);
declare _id_kur,_kode_mk,_ta,_sem_ta,_kuliah_id,_ta_id varchar(10);
declare rows_count,i, syllabus_count int;
DECLARE curs1 CURSOR FOR SELECT count(a.ta)
FROM `nlai_komposisi_nilai` a
LEFT OUTER JOIN `prkl_kurikulum_syllabus` b
ON a.`kurikulum_syllabus_id` = b.`kurikulum_syllabus_id`
INNER JOIN `krkm_kuliah` c
ON a.`id_kur`=c.`id_kur` AND a.`kode_mk`=c.`kode_mk`
INNER JOIN `mref_r_ta` d
ON a.`ta`=d.`nama`;
DECLARE curs2 cursor for SELECT b.ta,a.id_kur,a.kode_mk,a.ta,a.sem_ta,c.kuliah_id,d.ta_id
FROM `nlai_komposisi_nilai` a
LEFT OUTER JOIN `prkl_kurikulum_syllabus` b
ON a.`kurikulum_syllabus_id` = b.`kurikulum_syllabus_id`
INNER JOIN `krkm_kuliah` c
ON a.`id_kur`=c.`id_kur` AND a.`kode_mk`=c.`kode_mk`
INNER JOIN `mref_r_ta` d
ON a.`ta`=d.`nama`;
open curs1;
	fetch curs1 into rows_count;
close curs1;
open curs2;
Set i=1;
REPEAT
    FETCH curs2 INTO ta_syllabus,_id_kur,_kode_mk,_ta,_sem_ta,_kuliah_id,_ta_id;
    if ta_syllabus is null then
    
	Select count(*) into syllabus_count FROM `prkl_kurikulum_syllabus` WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta;
	
	if syllabus_count=0 then
		insert into `prkl_kurikulum_syllabus` (`id_kur`,`kode_mk`,`ta`,`sem_ta`,`kuliah_id`,`ta_id`) values(_id_kur,_kode_mk,_ta,_sem_ta,_kuliah_id,_ta_id);
	end if;
		
	update `nlai_komposisi_nilai`
	set `kurikulum_syllabus_id`=(select `kurikulum_syllabus_id` from `prkl_kurikulum_syllabus` where `id_kur`=_id_kur and `kode_mk`=_kode_mk and `ta`=_ta)
	WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta;
	
	UPDATE `prkl_materi`
	SET `kurikulum_syllabus_id`=(SELECT `kurikulum_syllabus_id` FROM `prkl_kurikulum_syllabus` WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta)
	WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta;
	
	UPDATE `prkl_praktikum`
	SET `kurikulum_syllabus_id`=(SELECT `kurikulum_syllabus_id` FROM `prkl_kurikulum_syllabus` WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta)
	WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta;
	
	UPDATE `nlai_nilai`
	SET `kurikulum_syllabus_id`=(SELECT `kurikulum_syllabus_id` FROM `prkl_kurikulum_syllabus` WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta)
	WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta;
	
	UPDATE `nlai_nilai_praktikum`
	SET `kurikulum_syllabus_id`=(SELECT `kurikulum_syllabus_id` FROM `prkl_kurikulum_syllabus` WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta)
	WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta;
	
	UPDATE `nlai_nilai_quis`
	SET `kurikulum_syllabus_id`=(SELECT `kurikulum_syllabus_id` FROM `prkl_kurikulum_syllabus` WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta)
	WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta;
	
	UPDATE `nlai_nilai_tugas`
	SET `kurikulum_syllabus_id`=(SELECT `kurikulum_syllabus_id` FROM `prkl_kurikulum_syllabus` WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta)
	WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta;
	
	UPDATE `nlai_nilai_uas`
	SET `kurikulum_syllabus_id`=(SELECT `kurikulum_syllabus_id` FROM `prkl_kurikulum_syllabus` WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta)
	WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta;
	
	UPDATE `nlai_nilai_uts`
	SET `kurikulum_syllabus_id`=(SELECT `kurikulum_syllabus_id` FROM `prkl_kurikulum_syllabus` WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta)
	WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta;
	
	UPDATE `nlai_rentang_nilai`
	SET `kurikulum_syllabus_id`=(SELECT `kurikulum_syllabus_id` FROM `prkl_kurikulum_syllabus` WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta)
	WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta;
    end if;
    set i=i+1;
UNTIL i>rows_count END REPEAT;
close curs2;
END */$$
DELIMITER ;

/* Procedure structure for procedure `create_syllabus_by_nilai` */

/*!50003 DROP PROCEDURE IF EXISTS  `create_syllabus_by_nilai` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`cis_db_admin`@`%` PROCEDURE `create_syllabus_by_nilai`()
BEGIN
DECLARE syllabus_id VARCHAR(10);
DECLARE _id_kur,_kode_mk,_ta,_sem_ta,_kuliah_id,_ta_id VARCHAR(10);
DECLARE rows_count,i, syllabus_count INT;
DECLARE curs1 CURSOR FOR SELECT COUNT(a.ta)
FROM `nlai_nilai` a
LEFT OUTER JOIN `krkm_kuliah` c
ON a.`id_kur`=c.`id_kur` AND a.`kode_mk`=c.`kode_mk`
INNER JOIN `mref_r_ta` d
ON a.`ta`=d.`nama`;
DECLARE curs2 CURSOR FOR SELECT a.kurikulum_syllabus_id,a.id_kur,a.kode_mk,a.ta,a.sem_ta,c.kuliah_id,d.ta_id
FROM `nlai_nilai` a
LEFT OUTER JOIN `krkm_kuliah` c
ON a.`id_kur`=c.`id_kur` AND a.`kode_mk`=c.`kode_mk`
INNER JOIN `mref_r_ta` d
ON a.`ta`=d.`nama`;
OPEN curs1;
	FETCH curs1 INTO rows_count;
CLOSE curs1;
OPEN curs2;
SET i=1;
REPEAT
    FETCH curs2 INTO syllabus_id,_id_kur,_kode_mk,_ta,_sem_ta,_kuliah_id,_ta_id;
    IF syllabus_id IS NULL THEN
    
	SELECT COUNT(*) INTO syllabus_count FROM `prkl_kurikulum_syllabus` WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta;
	
	IF syllabus_count=0 THEN
		INSERT INTO `prkl_kurikulum_syllabus` (`id_kur`,`kode_mk`,`ta`,`sem_ta`,`kuliah_id`,`ta_id`) VALUES(_id_kur,_kode_mk,_ta,_sem_ta,_kuliah_id,_ta_id);
	END IF;
			
	UPDATE `nlai_nilai`
	SET `kurikulum_syllabus_id`=(SELECT `kurikulum_syllabus_id` FROM `prkl_kurikulum_syllabus` WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta)
	WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta;
	
	UPDATE `nlai_rentang_nilai`
	SET `kurikulum_syllabus_id`=(SELECT `kurikulum_syllabus_id` FROM `prkl_kurikulum_syllabus` WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta)
	WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta;
    END IF;
    SET i=i+1;
UNTIL i>rows_count END REPEAT;
CLOSE curs2;
END */$$
DELIMITER ;

/* Procedure structure for procedure `create_syllabus_by_prkl_materi` */

/*!50003 DROP PROCEDURE IF EXISTS  `create_syllabus_by_prkl_materi` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`cis_db_admin`@`%` PROCEDURE `create_syllabus_by_prkl_materi`()
BEGIN
DECLARE syllabus_id VARCHAR(10);
DECLARE _id_kur,_kode_mk,_ta,_sem,_kuliah_id,_ta_id VARCHAR(10);
DECLARE rows_count,i, syllabus_count,_sem_ta INT;
DECLARE curs1 CURSOR FOR SELECT COUNT(a.ta)
FROM `prkl_materi` a
LEFT OUTER JOIN `krkm_kuliah` c
ON a.`id_kur`=c.`id_kur` AND a.`kode_mk`=c.`kode_mk`
INNER JOIN `mref_r_ta` d
ON a.`ta`=d.`nama`;
DECLARE curs2 CURSOR FOR SELECT a.`kurikulum_syllabus_id`,a.id_kur,a.kode_mk,a.ta,c.sem,c.kuliah_id,d.ta_id
FROM `prkl_materi` a
LEFT OUTER JOIN `krkm_kuliah` c
ON a.`id_kur`=c.`id_kur` AND a.`kode_mk`=c.`kode_mk`
INNER JOIN `mref_r_ta` d
ON a.`ta`=d.`nama`;
OPEN curs1;
	FETCH curs1 INTO rows_count;
CLOSE curs1;
OPEN curs2;
SET i=1;
REPEAT
    FETCH curs2 INTO syllabus_id,_id_kur,_kode_mk,_ta,_sem,_kuliah_id,_ta_id;
    IF syllabus_id IS NULL THEN
    
	SELECT COUNT(*) INTO syllabus_count FROM `prkl_kurikulum_syllabus` WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta;
	IF (_sem MOD 2)=0 THEN
		SET _sem_ta=2;
	ELSE
		SET _sem_ta=1;
	END IF;
	
	
	IF syllabus_count=0 THEN
		INSERT INTO `prkl_kurikulum_syllabus` (`id_kur`,`kode_mk`,`ta`,`sem_ta`,`kuliah_id`,`ta_id`) VALUES(_id_kur,_kode_mk,_ta,_sem_ta,_kuliah_id,_ta_id);
	END IF;
		
	UPDATE `prkl_materi`
	SET `kurikulum_syllabus_id`=(SELECT `kurikulum_syllabus_id` FROM `prkl_kurikulum_syllabus` WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta)
	WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta;
    END IF;
    SET i=i+1;
UNTIL i>rows_count END REPEAT;
CLOSE curs2;
END */$$
DELIMITER ;

/* Procedure structure for procedure `create_syllabus_by_prkl_praktikum` */

/*!50003 DROP PROCEDURE IF EXISTS  `create_syllabus_by_prkl_praktikum` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`cis_db_admin`@`%` PROCEDURE `create_syllabus_by_prkl_praktikum`()
BEGIN
DECLARE syllabus_id VARCHAR(10);
DECLARE _id_kur,_kode_mk,_ta,_sem,_kuliah_id,_ta_id VARCHAR(10);
DECLARE rows_count,i, syllabus_count,_sem_ta INT;
DECLARE curs1 CURSOR FOR SELECT COUNT(a.ta)
FROM `prkl_praktikum` a
LEFT OUTER JOIN `krkm_kuliah` c
ON a.`id_kur`=c.`id_kur` AND a.`kode_mk`=c.`kode_mk`
INNER JOIN `mref_r_ta` d
ON a.`ta`=d.`nama`;
DECLARE curs2 CURSOR FOR SELECT a.`kurikulum_syllabus_id`,a.id_kur,a.kode_mk,a.ta,c.sem,c.kuliah_id,d.ta_id
FROM `prkl_praktikum` a
LEFT OUTER JOIN `krkm_kuliah` c
ON a.`id_kur`=c.`id_kur` AND a.`kode_mk`=c.`kode_mk`
INNER JOIN `mref_r_ta` d
ON a.`ta`=d.`nama`;
OPEN curs1;
	FETCH curs1 INTO rows_count;
CLOSE curs1;
OPEN curs2;
SET i=1;
REPEAT
    FETCH curs2 INTO syllabus_id,_id_kur,_kode_mk,_ta,_sem,_kuliah_id,_ta_id;
    IF syllabus_id IS NULL THEN
    
	SELECT COUNT(*) INTO syllabus_count FROM `prkl_kurikulum_syllabus` WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta;
	IF (_sem MOD 2)=0 THEN
		SET _sem_ta=2;
	ELSE
		SET _sem_ta=1;
	END IF;
	
	
	IF syllabus_count=0 THEN
		INSERT INTO `prkl_kurikulum_syllabus` (`id_kur`,`kode_mk`,`ta`,`sem_ta`,`kuliah_id`,`ta_id`) VALUES(_id_kur,_kode_mk,_ta,_sem_ta,_kuliah_id,_ta_id);
	END IF;
		
	UPDATE `prkl_praktikum`
	SET `kurikulum_syllabus_id`=(SELECT `kurikulum_syllabus_id` FROM `prkl_kurikulum_syllabus` WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta)
	WHERE `id_kur`=_id_kur AND `kode_mk`=_kode_mk AND `ta`=_ta;
    END IF;
    SET i=i+1;
UNTIL i>rows_count END REPEAT;
CLOSE curs2;
END */$$
DELIMITER ;

/* Procedure structure for procedure `migrate_data_to_kuliah_prodi` */

/*!50003 DROP PROCEDURE IF EXISTS  `migrate_data_to_kuliah_prodi` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`cis_db_admin`@`%` PROCEDURE `migrate_data_to_kuliah_prodi`()
BEGIN
DECLARE _kuliah_id, _ref_kbk_id, _sem INT;
DECLARE krkm_kuliah_count, i, j, max_prodi INT;
DECLARE curs_krkm_kuliah CURSOR FOR SELECT `kuliah_id`, `ref_kbk_id`, `sem` FROM `krkm_kuliah`;
DECLARE curs_krkm_kuliah_count CURSOR FOR SELECT COUNT(*) FROM `krkm_kuliah`;
OPEN curs_krkm_kuliah_count;
	FETCH curs_krkm_kuliah_count INTO krkm_kuliah_count;
CLOSE curs_krkm_kuliah_count;
OPEN curs_krkm_kuliah;
	SET i=1;
	REPEAT
	FETCH curs_krkm_kuliah INTO _kuliah_id, _ref_kbk_id, _sem;
	IF _ref_kbk_id=11 THEN
		INSERT INTO `krkm_kuliah_prodi` (`kuliah_id`, `ref_kbk_id`, `sem`) VALUES (_kuliah_id, 1, _sem);
		INSERT INTO `krkm_kuliah_prodi` (`kuliah_id`, `ref_kbk_id`, `sem`) VALUES (_kuliah_id, 2, _sem);
		INSERT INTO `krkm_kuliah_prodi` (`kuliah_id`, `ref_kbk_id`, `sem`) VALUES (_kuliah_id, 3, _sem);
		INSERT INTO `krkm_kuliah_prodi` (`kuliah_id`, `ref_kbk_id`, `sem`) VALUES (_kuliah_id, 4, _sem);
		INSERT INTO `krkm_kuliah_prodi` (`kuliah_id`, `ref_kbk_id`, `sem`) VALUES (_kuliah_id, 5, _sem);
		INSERT INTO `krkm_kuliah_prodi` (`kuliah_id`, `ref_kbk_id`, `sem`) VALUES (_kuliah_id, 6, _sem);
		INSERT INTO `krkm_kuliah_prodi` (`kuliah_id`, `ref_kbk_id`, `sem`) VALUES (_kuliah_id, 7, _sem);
		INSERT INTO `krkm_kuliah_prodi` (`kuliah_id`, `ref_kbk_id`, `sem`) VALUES (_kuliah_id, 8, _sem);
		INSERT INTO `krkm_kuliah_prodi` (`kuliah_id`, `ref_kbk_id`, `sem`) VALUES (_kuliah_id, 9, _sem);
		INSERT INTO `krkm_kuliah_prodi` (`kuliah_id`, `ref_kbk_id`, `sem`) VALUES (_kuliah_id, 10, _sem);
	ELSEIF _ref_kbk_id=12 THEN
		INSERT INTO `krkm_kuliah_prodi` (`kuliah_id`, `ref_kbk_id`, `sem`) VALUES (_kuliah_id, 1, _sem);
		INSERT INTO `krkm_kuliah_prodi` (`kuliah_id`, `ref_kbk_id`, `sem`) VALUES (_kuliah_id, 2, _sem);
		INSERT INTO `krkm_kuliah_prodi` (`kuliah_id`, `ref_kbk_id`, `sem`) VALUES (_kuliah_id, 3, _sem);
	ELSEIF _ref_kbk_id=13 THEN
		INSERT INTO `krkm_kuliah_prodi` (`kuliah_id`, `ref_kbk_id`, `sem`) VALUES (_kuliah_id, 4, _sem);
	ELSEIF _ref_kbk_id=14 THEN
		INSERT INTO `krkm_kuliah_prodi` (`kuliah_id`, `ref_kbk_id`, `sem`) VALUES (_kuliah_id, 6, _sem);
		INSERT INTO `krkm_kuliah_prodi` (`kuliah_id`, `ref_kbk_id`, `sem`) VALUES (_kuliah_id, 7, _sem);
		INSERT INTO `krkm_kuliah_prodi` (`kuliah_id`, `ref_kbk_id`, `sem`) VALUES (_kuliah_id, 8, _sem);
		INSERT INTO `krkm_kuliah_prodi` (`kuliah_id`, `ref_kbk_id`, `sem`) VALUES (_kuliah_id, 9, _sem);
		INSERT INTO `krkm_kuliah_prodi` (`kuliah_id`, `ref_kbk_id`, `sem`) VALUES (_kuliah_id, 10, _sem);
	ELSE
		INSERT INTO `krkm_kuliah_prodi` (`kuliah_id`, `ref_kbk_id`, `sem`) VALUES (_kuliah_id, _ref_kbk_id, _sem);
	END IF;
	SET i=i+1;
	UNTIL i>krkm_kuliah_count END REPEAT;
CLOSE curs_krkm_kuliah;
END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
